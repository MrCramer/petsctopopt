
#include <cmath>
//#include <exception>

#include "ElasTensors.h"
#include "FMatrix.h"


// Tensor classes


class SIMPTensor : public TensorBase {
  public:
    SIMPTensor(double lambda, double mu, double start_P, int start_it, double end_P, int end_it)
              : start_P_(start_P), end_P_(end_P), lambda_(lambda), mu_(mu), start_it_(start_it), end_it_(end_it) { }

    SIMPTensor(double P, double lambda, double mu)
              : start_P_(P), end_P_(P), lambda_(lambda), mu_(mu), start_it_(-1), end_it_(-1) { }

    
    PetscErrorCode EvalTensor(const double *design, int it, Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      
      double rho = design[0];
      double coeff = std::pow(rho, GetP(it));
      tensor.Resize(3,3,3,3);
      for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
      for(int k=0; k<3; ++k)
      for(int l=0; l<3; ++l) {
        tensor(i,j,k,l) = coeff *
                          ( (i==j&&k==l)*lambda_
                          + (i==k&&j==l)*mu_
                          + (i==l&&j==k)*mu_    );
      }
      PetscFunctionReturn(0);
    }

    PetscErrorCode EvalDTensor(const double *design,
                               int it,
                               const int design_var,
                               Array4d<double> &dtensor) const {
      PetscFunctionBeginUser;
      if(design_var!=0) {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
                "SIMP dtensor provided design_var != 0, only design variable is 0: rho");
      }
      double rho = design[0];
      double P = GetP(it);
      double coeff = P * std::pow(rho, P-1.);
      dtensor.Resize(3,3,3,3);
      for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
      for(int k=0; k<3; ++k)
      for(int l=0; l<3; ++l) {
        dtensor(i,j,k,l) = coeff *
                           ( (i==j&&k==l)*lambda_
                           + (i==k&&j==l)*mu_
                           + (i==l&&j==k)*mu_    );
      }
      
      PetscFunctionReturn(0);
    }

  private:
    double GetP(int it) const {
      if(it >= end_it_) {
        return end_P_;

      } else if(it <= start_it_) {
        return start_P_;

      } else {
        return start_P_ + (end_P_ - start_P_)/double(end_it_-start_it_)*double(it-start_it_);
      }
    }

  public:
    const double start_P_, end_P_, lambda_, mu_;
    const int start_it_, end_it_;
};


class SIMPTensor2d : public TensorBase {
  public:
    SIMPTensor2d(double P, double E, double nu) : P_(P), E_(E), nu_(nu) { }

    PetscErrorCode EvalTensor(const double *design, int it, Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      
      double rho = design[0];
      double f = std::pow(rho,P_) * E_ / (1.0 - nu_*nu_);
      tensor.Resize(3,3,3,3);
      for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
      for(int k=0; k<3; ++k)
      for(int l=0; l<3; ++l) {
        if(i==j && k==l) {
          if(j==k) {
            tensor(i,j,k,l) = f;
            continue;
          } else {
            tensor(i,j,k,l) = f * nu_;
            continue;
          }
        } else if((i==k && j==l) || (i==l && j==k)) {
          tensor(i,j,k,l) = f * (1.0-nu_)/2.0;
          continue;
        }
        tensor(i,j,k,l) = 0.0;
      }
      PetscFunctionReturn(0);
    }
    
    PetscErrorCode EvalDTensor(const double *design, int it, int d, Array4d<double> &dtensor) const {
      PetscFunctionBeginUser;
      if(d!=0) {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
                "SIMP_2D dtensor provided design_var != 0, only design variable is 0: rho");
      }
      double rho = design[0];
      double f = P_ * std::pow(rho,P_-1) * E_ / (1.0 - nu_*nu_);
      dtensor.Resize(3,3,3,3);
      for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
      for(int k=0; k<3; ++k)
      for(int l=0; l<3; ++l) {
        if(i==j && k==l) {
          if(j==k) {
            dtensor(i,j,k,l) = f;
            continue;
          } else {
            dtensor(i,j,k,l) = f * nu_;
            continue;
          }
        } else if((i==k && j==l) || (i==l && j==k)) {
          dtensor(i,j,k,l) = f * (1.0-nu_)/2.0;
          continue;
        }
        dtensor(i,j,k,l) = 0.0;
      }
      PetscFunctionReturn(0);
    }
  public:
    const double P_, E_, nu_;
};


#define IMS_INTERP(a,r)  ( (1+a)*r/(1+a*r) )
#define IMS_INTERPD(a,r) ( (1+a)/(1+a*r)/(1+a*r) )

class IMSTensor : public TensorBase {
  public:
    IMSTensor(double a_11, double a_12, double a_44, double lambda, double mu)
      : a_11_(a_11), a_12_(a_12), a_44_(a_44), lambda_(lambda), mu_(mu) { }

    PetscErrorCode EvalTensor(const double *design, int it, Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      double rho = design[0];
      tensor.Resize(3,3,3,3);
      for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
      for(int k=0; k<3; ++k)
      for(int l=0; l<3; ++l) {
        if(i==j && k==l) {
          if(i==k) {
            // E_1111 / E_11
            tensor(i,j,k,l) = (lambda_ + 2.0*mu_)*IMS_INTERP(a_11_, rho);
            continue;
          } else {
            // E_1122 / E_12
            tensor(i,j,k,l) = lambda_*IMS_INTERP(a_12_, rho);
            continue;
          }
        } else {
          if((i==k && j==l) || (i==l && j==k)) {
            // E_1212 / E_44
            tensor(i,j,k,l) = mu_*IMS_INTERP(a_44_, rho);
            continue;
          }
        }
        tensor(i,j,k,l) = 0.0;
      }
      PetscFunctionReturn(0);
    }

    PetscErrorCode EvalDTensor(const double *design, int it, int d, Array4d<double> &dtensor) const {
      PetscFunctionBeginUser;
      dtensor.Resize(3,3,3,3);
      double rho = design[0];
      for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
      for(int k=0; k<3; ++k)
      for(int l=0; l<3; ++l) {
        if(i==j && k==l) {
          if(i==k) {
            // E_1111 / E_11
            dtensor(i,j,k,l) = (lambda_ + 2.0*mu_)*IMS_INTERPD(a_11_, rho);
            continue;
          } else {
            // E_1122 / E_12
            dtensor(i,j,k,l) = lambda_*IMS_INTERPD(a_12_, rho);
            continue;
          }
        } else {
          if((i==k && j==l) || (i==l && j==k)) {
            // E_1212 / E_44
            dtensor(i,j,k,l) = mu_*IMS_INTERPD(a_44_, rho);
            continue;
          }
        }
        dtensor(i,j,k,l) = 0.0;
      }
      PetscFunctionReturn(0);
    }

  private:
    double a_11_, a_12_, a_44_, lambda_, mu_;
};

#define IMS_INTERP3(a,r)  ( a[0] - (1-r)/(a[1] - a[2]*r) )
#define IMS_INTERPD3(a,r) ( 1/(a[1]-a[2]*r) - a[2]*(1-r)/((a[1]-a[2]*r)*(a[1]-a[2]*r)) )

class IMS3Tensor : public TensorBase {
  public:
    IMS3Tensor(std::vector<double> a_11, std::vector<double> a_12, std::vector<double> a_44,
              double lambda, double mu)
      : a_11_(a_11), a_12_(a_12), a_44_(a_44), lambda_(lambda), mu_(mu) { }

    PetscErrorCode EvalTensor(const double *design, int it, Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      double rho = design[0];
      tensor.Resize(3,3,3,3);
      for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
      for(int k=0; k<3; ++k)
      for(int l=0; l<3; ++l) {
        if(i==j && k==l) {
          if(i==k) {
            // E_1111 / E_11
            tensor(i,j,k,l) = (lambda_ + 2.0*mu_)*IMS_INTERP3(a_11_, rho);
            continue;
          } else {
            // E_1122 / E_12
            tensor(i,j,k,l) = lambda_*IMS_INTERP3(a_12_, rho);
            continue;
          }
        } else {
          if((i==k && j==l) || (i==l && j==k)) {
            // E_1212 / E_44
            tensor(i,j,k,l) = mu_*IMS_INTERP3(a_44_, rho);
            continue;
          }
        }
        tensor(i,j,k,l) = 0.0;
      }
      PetscFunctionReturn(0);
    }

    PetscErrorCode EvalDTensor(const double *design, int it, int d, Array4d<double> &dtensor) const {
      PetscFunctionBeginUser;
      dtensor.Resize(3,3,3,3);
      double rho = design[0];
      for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
      for(int k=0; k<3; ++k)
      for(int l=0; l<3; ++l) {
        if(i==j && k==l) {
          if(i==k) {
            // E_1111 / E_11
            dtensor(i,j,k,l) = (lambda_ + 2.0*mu_)*IMS_INTERPD3(a_11_, rho);
            continue;
          } else {
            // E_1122 / E_12
            dtensor(i,j,k,l) = lambda_*IMS_INTERPD3(a_12_, rho);
            continue;
          }
        } else {
          if((i==k && j==l) || (i==l && j==k)) {
            // E_1212 / E_44
            dtensor(i,j,k,l) = mu_*IMS_INTERPD3(a_44_, rho);
            continue;
          }
        }
        dtensor(i,j,k,l) = 0.0;
      }
      PetscFunctionReturn(0);
    }

  private:
    std::vector<double> a_11_, a_12_, a_44_;
    double lambda_, mu_;
};


class LinearInterpTensor : public TensorBase {
  public:
    LinearInterpTensor(std::unique_ptr<TensorBase> sub_tensor) : sub_tensor_(std::move(sub_tensor)) { }

    PetscErrorCode EvalTensor(const double *design, int it, Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      
      ierr =  sub_tensor_->EvalTensor(design+1, it, tensor);                           CHKERRQ(ierr);
      for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
      for(int k=0; k<3; ++k)
      for(int l=0; l<3; ++l) {
        tensor(i,j,k,l) *= design[0];
      }
      PetscFunctionReturn(0);
    }
    
    PetscErrorCode EvalDTensor(const double *design, int it, int d, Array4d<double> &dtensor) const {
      PetscFunctionBeginUser;
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE, "linear interpolate tensor cannot be used in optimisation");
      PetscFunctionReturn(0);
    }

  private:
    std::unique_ptr<TensorBase> sub_tensor_;
};


PetscErrorCode Matrix4DTensorApply(const double T[3][3], const Array4d<double> &base, Array4d<double> &result) {
  PetscFunctionBeginUser;
  result.Resize(3,3,3,3);
  // This can be optimised by using the voigt array
  for(int i=0; i<3; ++i)
  for(int j=0; j<3; ++j)
  for(int k=0; k<3; ++k)
  for(int l=0; l<3; ++l) {
    double v = 0.0;
    for(int i2=0; i2<3; ++i2)
    for(int j2=0; j2<3; ++j2)
    for(int k2=0; k2<3; ++k2)
    for(int l2=0; l2<3; ++l2) {
      v += T[i][i2] * T[j][j2] * T[k][k2] * T[l][l2] * base(i2,j2,k2,l2);
    }
    result(i,j,k,l) = v;
  }
  PetscFunctionReturn(0);
}


class SwitchTensor : public TensorBase {
  public:
    struct SwitchSubTensor {
      std::unique_ptr<TensorBase> sub_tensor;
      int design_dof;
      bool reuse_switch_var;
      double range[2];
    };

    SwitchTensor(std::vector<SwitchSubTensor> &&subs) : sub_tensors_(std::move(subs)) { }

    PetscErrorCode EvalTensor(const double *design, int it, Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      for(auto &sub : sub_tensors_) {
        if(sub.range[0] < design[0] && design[0] < sub.range[1]) {
          if(sub.reuse_switch_var) {
            ierr =  sub.sub_tensor->EvalTensor(design + 0, it, tensor);                CHKERRQ(ierr);
          } else {
            ierr =  sub.sub_tensor->EvalTensor(design + 1, it, tensor);                CHKERRQ(ierr);
          }
          PetscFunctionReturn(0);
        }
      }
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
               "Switch tensor could not find a sub tensor for design variable %f", design[0]);
      PetscFunctionReturn(0);
    }

    PetscErrorCode EvalDTensor(const double *design, int it, int d, Array4d<double> &dtensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      for(auto &sub : sub_tensors_) {
        if(sub.range[0] < design[0] && design[0] < sub.range[1]) {
          if(sub.reuse_switch_var) {
            if(d>=sub.design_dof) {
              // If the derivitive is for a design variable higher than the number of
              // that design var.
              dtensor.Resize(3,3,3,3);
              dtensor.Set(0.0);
            } else {
              ierr =  sub.sub_tensor->EvalDTensor(design, it, d, dtensor);             CHKERRQ(ierr);
            }
          } else {
            if(d==0 || d-1>=sub.design_dof) {
              // If the derivitive is for a design variable higher than the number of
              // dof, or for the switch variable then just give back a zero, in those 
              // cases we're not dependent on that design var.
              dtensor.Resize(3,3,3,3);
              dtensor.Set(0.0);
            } else {
              ierr =  sub.sub_tensor->EvalDTensor(design + 1, it, d-1, dtensor);       CHKERRQ(ierr);
            }
          }
          PetscFunctionReturn(0);
        }
      }
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
               "Switch tensor could not find a sub tensor for design variable %f", design[0]);
      PetscFunctionReturn(0);
    }

  private:
    std::vector<SwitchSubTensor> sub_tensors_;
};


class RotationTensorZXZ : public TensorBase {
  public:
    RotationTensorZXZ(std::unique_ptr<TensorBase> sub_tensor) : sub_tensor_(std::move(sub_tensor)) { }

    PetscErrorCode EvalTensor(const double *design, int it, Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      
      double s1 = std::sin(design[0]), c1 = std::cos(design[0]);
      double s2 = std::sin(design[1]), c2 = std::cos(design[1]);
      double s3 = std::sin(design[2]), c3 = std::cos(design[2]);
      // Rotation matrix rt is composed of Rz(th3)*Rx(th2)*Rz(th1) corresponding to an 
      // application of the rotations: th1 about the z-axis, th2 about the x axis, and
      // finally th3 about the z-axis in that order.
      double rt[3][3] = {
        {c1*c3-c2*s1*s3, -c3*s1-c2*c1*s3,  s2*s3},
        {c1*s3+c2*c3*s1,  c1*c2*c3-s1*s3, -c3*s2},
        {s1*s2,           c1*s2,           c2   }};
      
      ierr =  sub_tensor_->EvalTensor(design+3, it, tensor_temp_);                     CHKERRQ(ierr);
      ierr =  Matrix4DTensorApply(rt, tensor_temp_, tensor);                           CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

    PetscErrorCode EvalDTensor(const double *design, int it, int d, Array4d<double> &dtensor) const {
      PetscFunctionBeginUser;
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE, "rotation tensor cannot be used in optimisation");
      PetscFunctionReturn(0);
    }

  private:
    std::unique_ptr<TensorBase> sub_tensor_;
    mutable Array4d<double> tensor_temp_;
};


class StaticMatrixTransformationTensor : public TensorBase {
  public:
    StaticMatrixTransformationTensor(const double T[3][3], std::unique_ptr<TensorBase> sub_tensor)
      : sub_tensor_(std::move(sub_tensor)) {
      for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j) {
        T_[i][j] = T[i][j];
      }
    }

    PetscErrorCode EvalTensor(const double *design, int it, Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      ierr =  sub_tensor_->EvalTensor(design, it, tensor_temp_);                       CHKERRQ(ierr);
      ierr =  Matrix4DTensorApply(T_, tensor_temp_, tensor);                           CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

    PetscErrorCode EvalDTensor(const double *design, int it, int d, Array4d<double> &dtensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      // We have no design variables so just pass it on in
      ierr =  sub_tensor_->EvalDTensor(design, it, d, tensor_temp_);                   CHKERRQ(ierr);
      ierr =  Matrix4DTensorApply(T_, tensor_temp_, dtensor);                          CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

  private:
    std::unique_ptr<TensorBase> sub_tensor_;
    double T_[3][3];
    mutable Array4d<double> tensor_temp_;
};


class ScalarMultiplyTensor : public TensorBase {
  public:
    ScalarMultiplyTensor(double scalar, std::unique_ptr<TensorBase> sub_tensor)
      : scalar_(scalar), sub_tensor_(std::move(sub_tensor)) { }

    PetscErrorCode EvalTensor(const double *design, int it, Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      ierr =  sub_tensor_->EvalTensor(design,it,tensor);                               CHKERRQ(ierr);
      for(auto &el : tensor.GetData()) {
        el *= scalar_;
      }
      PetscFunctionReturn(0);
    }

    PetscErrorCode EvalDTensor(const double *design, int it, int d, Array4d<double> &dtensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      ierr =  sub_tensor_->EvalDTensor(design,it,d,dtensor);                           CHKERRQ(ierr);
      for(auto &el : dtensor.GetData()) {
        el *= scalar_;
      }
      PetscFunctionReturn(0);
    }

  private:
    double scalar_;
    std::unique_ptr<TensorBase> sub_tensor_;
};


PetscErrorCode FillOrthoTensor(double c11, double c12, double c44, Array4d<double> &tensor) {
  PetscFunctionBeginUser;
  tensor.Resize(3,3,3,3);
  for(int i=0; i<3; ++i)
  for(int j=0; j<3; ++j)
  for(int k=0; k<3; ++k)
  for(int l=0; l<3; ++l) {
    if(i==j && k==l) {
      // Top left hand quadrant of 6x6 matrix
      if(i==k) {
        tensor(i,j,k,l) = c11;
      } else {
        tensor(i,j,k,l) = c12;
      }
    } else {
      // The shearing part of 6x6 matrix (bottom right)
      if((i==k && j==l) || (i==l && j==k)) {
        tensor(i,j,k,l) = c44;
      } else {
        tensor(i,j,k,l) = 0.0;
      }
    }
  }
  PetscFunctionReturn(0);
}



class OrthotropicTensor : public TensorBase {
  public:
    OrthotropicTensor(double C11, double C12, double C44) : C11_(C11), C12_(C12), C44_(C44) { }

    PetscErrorCode EvalTensor(const double *design, int it, Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      ierr =  FillOrthoTensor(C11_,C12_,C44_,tensor);                                  CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

    PetscErrorCode EvalDTensor(const double *design, int it, int d, Array4d<double> &dtensor) const {
      PetscFunctionBeginUser;
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE, "orthotropic tensor cannot be used in optimisation");
      PetscFunctionReturn(0);
    }

  private:
   double C11_, C12_, C44_;
};


class PolynomialOrthotropicTensor : public TensorBase {
  /* An orthotropic tensor where each of the 3 degrees of freedom are interpolated with
   * a polynomial.
   *
   * The interpolating polynomials are described with their coefficients in Axx 
   * beginning with x^2. The polynomials p are fixed with p(0)==0 and p(1)==1. In
   * particular, they are:
   *
   *   p(x) = (1-sum(A))*x + A[0]*x^2 + A[1]*x^3 + .. + A[N-1]*x^(N+1)
   *
   */
  public:
    PolynomialOrthotropicTensor(double C11, double C12, double C44,
                                std::vector<double> A11,
                                std::vector<double> A12,
                                std::vector<double> A44) : C11_(C11), C12_(C12), C44_(C44),
                                                           A11_(A11), A12_(A12), A44_(A44) { }

    PetscErrorCode EvalTensor(const double *design, int it, Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      double c11 = GetInterpFactor(*design,A11_) * C11_;
      double c12 = GetInterpFactor(*design,A12_) * C12_;
      double c44 = GetInterpFactor(*design,A44_) * C44_;
      ierr =  FillOrthoTensor(c11,c12,c44,tensor);                                     CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

    PetscErrorCode EvalDTensor(const double *design, int it, int d, Array4d<double> &dtensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      if(d!=0) {
        SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
          "PolynomialOrthotropicTensor asked for derivitive of design var %d, only takes 1 (index 0)",d);
      }
      double c11 = GetDInterpFactor(*design,A11_) * C11_;
      double c12 = GetDInterpFactor(*design,A12_) * C12_;
      double c44 = GetDInterpFactor(*design,A44_) * C44_;
      ierr =  FillOrthoTensor(c11,c12,c44,dtensor);                                    CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

  private:
    static double GetInterpFactor(double x, const std::vector<double> &A) {
      double x_pow   = x; // A[0] is with x^2
      double val     = 0;
      double x1_coef = 1;
      for(size_t i=0; i<A.size(); ++i) {
        x_pow   *= x;
        val     += A[i]*x_pow;
        x1_coef -= A[i];
      }
      val += x1_coef*x;
      return val;
    }

    static double GetDInterpFactor(double x, const std::vector<double> &A) {
      double x_pow   = 1; // A[0] is with x^1 (down from x^2)
      double val     = 0;
      double x1_coef = 1;
      for(size_t i=0; i<A.size(); ++i) {
        x_pow   *= x;
        val     += (i+2)*A[i]*x_pow;
        x1_coef -= A[i];
      }
      val += x1_coef;
      return val;
    }

    double C11_, C12_, C44_;
    std::vector<double> A11_, A12_, A44_;
};


class CubicSpline {
  public:
    CubicSpline(std::vector<double> x, std::vector<double> y)
      : x_(std::move(x)),y_(std::move(y)),k_(GetCubicSplineKsNatural(x_,y_)) { }

    double Eval(double x) const {
      for(unsigned i=0; i<x_.size()-1; ++i) {
        if(x_[i] <= x && x <= x_[i+1]) {
          double dy = y_[i+1]-y_[i];
          double dx = x_[i+1]-x_[i];
          double t = (x - x_[i])/dx;
          double a = +k_[i  ]*dx - dy;
          double b = -k_[i+1]*dx + dy;
          return (1-t)*y_[i] + t*y_[i+1] + t*(1-t)*(a*(1-t)+b*t);
        }
      }
      throw std::domain_error("Cubic spline asked to extrapolate a point on .Eval()");
    }

    double EvalD(double x) const {
      for(unsigned i=0; i<x_.size()-1; ++i) {
        if(x_[i] <= x && x <= x_[i+1]) {
          double dy = y_[i+1]-y_[i];
          double dx = x_[i+1]-x_[i];
          double t = (x - x_[i])/dx;
          double a = +k_[i  ]*dx - dy;
          double b = -k_[i+1]*dx + dy;
          return (-y_[i] + y_[i+1] + (1-t)*(a*(1-t)+b*t) - t*(a*(1-t)+b*t) + t*(1-t)*(b-a))/dx;
        }
      }
      throw std::domain_error("Cubic spline asked to extrapolate a point on .EvalD()");
    }

  private:
    static std::vector<double> GetCubicSplineKsNatural(const std::vector<double> &x,
                                                       const std::vector<double> &y) {
      /* Gets the derivitives (k[i]) at each of the interpolant points such that
       * the resulting function is C^2. Uses natural boundaries: function's second
       * derivitive is set to zero at either end.
       */
      if(x.size()!=y.size()) {
        throw std::runtime_error("Incompatible vector lengths supplied to CubicSpline");
      }
      if(x.size()<2) {
        throw std::runtime_error("Too short (<2) vector supplied to CubicSpline");
      }
      unsigned n = x.size();
      FMatrix K_matrix(n,n);
      std::vector<double> b(n),k(n);
      K_matrix(0,0) = 2/(x[1]-x[0]);
      K_matrix(0,1) = 1/(x[1]-x[0]);
      b[0] = 3*(y[1]-y[0])/((x[1]-x[0])*(x[1]-x[0]));
      for(unsigned i=1; i<n-1; ++i) {
        double dxm1 = x[i]-x[i-1];
        double dxp1 = x[i+1]-x[i];
        K_matrix(i,i-1) = 1/dxm1;
        K_matrix(i,i  ) = 2/dxp1+2/dxm1;
        K_matrix(i,i+1) = 1/dxp1;
        b[i] = 3*(y[i+1]-y[i])/(dxp1*dxp1)
             + 3*(y[i]-y[i-1])/(dxm1*dxm1);
      }
      K_matrix(n-1,n-2) = 1/(x[n-1]-x[n-2]);
      K_matrix(n-1,n-1) = 2/(x[n-1]-x[n-2]);
      b[n-1] = 3*(y[n-1]-y[n-2])/((x[n-1]-x[n-2])*(x[n-1]-x[n-2]));

      K_matrix.Solve(b,k);
      return k;
    }

    std::vector<double> x_,y_,k_;
};


class CubicSplineOrthotropicTensor : public TensorBase {
  /* An othotropic tensor where each of the 3 dof are interpolated with cubic splines.
   * The client should provide a list of points.
   */
  public:
    CubicSplineOrthotropicTensor(double C11, double C12, double C44,
                                 std::vector<double> x11, std::vector<double> y11,
                                 std::vector<double> x12, std::vector<double> y12,
                                 std::vector<double> x44, std::vector<double> y44)
        : C11_(C11), C12_(C12), C44_(C44),
          C11Spline_(x11,y11),C12Spline_(x12,y12),C44Spline_(x44,y44) { }

    PetscErrorCode EvalTensor(const double *design, int it, Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      double c11,c12,c44;
      try {
        c11 = C11_ * C11Spline_.Eval(*design);
        c12 = C12_ * C12Spline_.Eval(*design);
        c44 = C44_ * C44Spline_.Eval(*design);
      } catch(const std::domain_error &e) {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,e.what());
      }
      ierr =  FillOrthoTensor(c11,c12,c44,tensor);                                     CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

    PetscErrorCode EvalDTensor(const double *design, int it, int d, Array4d<double> &dtensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      if(d!=0) {
        SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
          "PolynomialOrthotropicTensor asked for derivitive of design var %d, only takes 1 (index 0)",d);
      }
      double c11,c12,c44;
      try {
        c11 = C11_ * C11Spline_.EvalD(*design);
        c12 = C12_ * C12Spline_.EvalD(*design);
        c44 = C44_ * C44Spline_.EvalD(*design);
      } catch(const std::domain_error &e) {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,e.what());
      }
      ierr =  FillOrthoTensor(c11,c12,c44,dtensor);                                    CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

  private:
    double C11_, C12_, C44_;
    CubicSpline C11Spline_,C12Spline_,C44Spline_;
};


class AxisAlignedEllipsoidal : public TensorBase {
    // Using this ordering we pass around the tensor as a vector of 9
    enum {
      Cxxxx, Cyyyy, Czzzz,
      Cyyzz, Cxxzz, Cxxyy,
      Cyzyz, Cxzxz, Cxyxy
    };

  public:
    PetscErrorCode GetStiffnessTensorValues(const double *design, double tensor_values[9]) const {
      PetscFunctionBeginUser;
      // These values are taken from a mathematica notebook which inverts the tensor
      double    E_x = design[0],    E_y = design[1],    E_z = design[2];
      double Xsi_yz = design[3], Xsi_xz = design[4], Xsi_xy = design[5];
      double denom = 1/(E_x*E_y*E_z)
                   - 4*Xsi_yz*Xsi_yz/((E_y+E_z)*(E_y+E_z)*E_x)
                   - 4*Xsi_xz*Xsi_xz/((E_x+E_z)*(E_x+E_z)*E_y)
                   - 4*Xsi_xy*Xsi_xy/((E_x+E_y)*(E_x+E_y)*E_z)
                   - 16*Xsi_yz*Xsi_xz*Xsi_xy/((E_y+E_z)*(E_x+E_z)*(E_x+E_y));

      denom = 1/denom;
      
      tensor_values[Cxxxx] = denom*(1/(E_y*E_z)-(4*Xsi_yz*Xsi_yz)/((E_y+E_z)*(E_y+E_z)));
      tensor_values[Cyyyy] = denom*(1/(E_x*E_z)-(4*Xsi_xz*Xsi_xz)/((E_x+E_z)*(E_x+E_z)));
      tensor_values[Czzzz] = denom*(1/(E_x*E_y)-(4*Xsi_xy*Xsi_xy)/((E_x+E_y)*(E_x+E_y)));
      tensor_values[Cyyzz] = denom*(2*(Xsi_yz/(E_x*(E_y+E_z))+(2*Xsi_xy*Xsi_xz)/((E_x+E_y)*(E_x+E_z))));
      tensor_values[Cxxzz] = denom*(2*(Xsi_xz/(E_y*(E_x+E_z))+(2*Xsi_xy*Xsi_yz)/((E_x+E_y)*(E_y+E_z))));
      tensor_values[Cxxyy] = denom*(2*(Xsi_xy/(E_z*(E_x+E_y))+(2*Xsi_xz*Xsi_yz)/((E_x+E_z)*(E_y+E_z))));
      tensor_values[Cyzyz] = -(1/(1/E_y-2*std::sqrt(2/(E_y*E_y)+2/(E_z*E_z))+1/E_z-(4*Xsi_yz)/(E_y+E_z)));
      tensor_values[Cxzxz] = -(1/(1/E_x-2*std::sqrt(2/(E_x*E_x)+2/(E_z*E_z))+1/E_z-(4*Xsi_xz)/(E_x+E_z)));
      tensor_values[Cxyxy] = -(1/(1/E_x-2*std::sqrt(2/(E_x*E_x)+2/(E_y*E_y))+1/E_y-(4*Xsi_xy)/(E_x+E_y)));
      PetscFunctionReturn(0);
    }

    PetscErrorCode FillTensorWithValues(const double tensor_values[9], Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      tensor.Resize(3,3,3,3);
      tensor.Set(0.0);
      tensor(0,0,0,0) = tensor_values[Cxxxx];
      tensor(1,1,1,1) = tensor_values[Cyyyy];
      tensor(2,2,2,2) = tensor_values[Czzzz];
      tensor(1,1,2,2) = tensor(2,2,1,1) = tensor_values[Cyyzz];
      tensor(0,0,2,2) = tensor(2,2,0,0) = tensor_values[Cxxzz];
      tensor(0,0,1,1) = tensor(1,1,0,0) = tensor_values[Cxxyy];
      tensor(1,2,1,2) = tensor(1,2,2,1) = tensor(2,1,1,2) = tensor(2,1,2,1) = tensor_values[Cyzyz];
      tensor(0,2,0,2) = tensor(0,2,2,0) = tensor(2,0,0,2) = tensor(2,0,2,0) = tensor_values[Cxzxz];
      tensor(0,1,0,1) = tensor(0,1,1,0) = tensor(1,0,0,1) = tensor(1,0,1,0) = tensor_values[Cxyxy];
      PetscFunctionReturn(0);
    }

    PetscErrorCode EvalTensor(const double *design, int it, Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      double tensor_values[9];
      ierr =  GetStiffnessTensorValues(design,tensor_values);                          CHKERRQ(ierr);
      ierr =  FillTensorWithValues(tensor_values,tensor);                              CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

    void Multiply3x3(const double A[3][3], const double B[3][3], double C[3][3]) const {
      for(int i=0; i<3;++i)
      for(int j=0; j<3;++j) {
        C[i][j] = 0;
        for(int k=0; k<3;++k) {
          C[i][j] += A[i][k]*B[k][j];
        }
      }
    }

    PetscErrorCode CalcDStiffnessFromDCompliance(const double *design,
                                                 const double d_compliance_values[9],
                                                 double d_stiffness_values[9]) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      // This function operates on the identity: d(inv(A)) = -inv(A) * dA * inv(A)
      // and that the stiffness is the inverse of the compliance. The derivatives
      // for compliance are much easier to express and so should be less error 
      // prone.
      double stiffness_values[9];
      ierr =  GetStiffnessTensorValues(design,stiffness_values);                       CHKERRQ(ierr);
      double stiff_block[3][3];
      double d_compl_block[3][3];
      double temp1_block[3][3];
      double temp2_block[3][3];
      // We can treat the top left 3x3 independently as a block
      stiff_block[0][0] = stiffness_values[Cxxxx];
      stiff_block[1][1] = stiffness_values[Cyyyy];
      stiff_block[2][2] = stiffness_values[Czzzz];
      stiff_block[1][2] = stiff_block[2][1] = stiffness_values[Cyyzz];
      stiff_block[0][2] = stiff_block[2][0] = stiffness_values[Cxxzz];
      stiff_block[0][1] = stiff_block[1][0] = stiffness_values[Cxxyy];
      d_compl_block[0][0] = d_compliance_values[Cxxxx];
      d_compl_block[1][1] = d_compliance_values[Cyyyy];
      d_compl_block[2][2] = d_compliance_values[Czzzz];
      d_compl_block[1][2] = d_compl_block[2][1] = d_compliance_values[Cyyzz];
      d_compl_block[0][2] = d_compl_block[2][0] = d_compliance_values[Cxxzz];
      d_compl_block[0][1] = d_compl_block[1][0] = d_compliance_values[Cxxyy];
      Multiply3x3(d_compl_block,stiff_block,temp1_block);
      Multiply3x3(stiff_block,  temp1_block,temp2_block);
      
      d_stiffness_values[Cxxxx] = - temp2_block[0][0];
      d_stiffness_values[Cyyyy] = - temp2_block[1][1];
      d_stiffness_values[Czzzz] = - temp2_block[2][2];
      d_stiffness_values[Cyyzz] = -(temp2_block[1][2]+temp2_block[2][1])/2;
      d_stiffness_values[Cxxzz] = -(temp2_block[0][2]+temp2_block[2][0])/2;
      d_stiffness_values[Cxxyy] = -(temp2_block[0][1]+temp2_block[1][0])/2;
      // As a sanity check, make sure that the antisymmetric part is small
      if(2*std::abs(temp2_block[1][2]-temp2_block[2][1])/(temp2_block[1][2]+temp2_block[2][1])>1e-10 ||
         2*std::abs(temp2_block[0][2]-temp2_block[2][0])/(temp2_block[0][2]+temp2_block[2][0])>1e-10 ||
         2*std::abs(temp2_block[0][1]-temp2_block[1][0])/(temp2_block[0][1]+temp2_block[1][0])>1e-10) {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,
                "Something that was meant to be symmetric wasn't in " __FILE__ " (AxisAlignedEllipsoidal)");
      }
      // As the lower right block is diagonal, we can calc these easily
      d_stiffness_values[Cyzyz] = -stiffness_values[Cyzyz]*d_compliance_values[Cyzyz]*stiffness_values[Cyzyz];
      d_stiffness_values[Cxzxz] = -stiffness_values[Cxzxz]*d_compliance_values[Cxzxz]*stiffness_values[Cxzxz];
      d_stiffness_values[Cxyxy] = -stiffness_values[Cxyxy]*d_compliance_values[Cxyxy]*stiffness_values[Cxyxy];
      PetscFunctionReturn(0);
    }

    PetscErrorCode EvalDTensor(const double *design, int it, int d, Array4d<double> &dtensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      double    E_x = design[0],    E_y = design[1],    E_z = design[2];
      double Xsi_yz = design[3], Xsi_xz = design[4], Xsi_xy = design[5];
      double d_stiffness_values[9];
      switch(d) {
        case 0:
          // E_x
          {
            double d_compliance_values[9] = {
              -1/(E_x*E_x), 0, 0,
              0, 2*Xsi_xz/((E_x+E_z)*(E_x+E_z)), 2*Xsi_xy/((E_x+E_y)*(E_x+E_y)),
              0,
              (E_x*E_x+E_z*E_z-E_x*std::sqrt(8/(E_x*E_x)+8/(E_z*E_z))*E_z*E_z)/(E_x*E_x*(E_x*E_x+E_z*E_z))
                -4*Xsi_xz/((E_x+E_z)*(E_x+E_z)),
              (E_x*E_x+E_y*E_y-E_x*std::sqrt(8/(E_x*E_x)+8/(E_y*E_y))*E_y*E_y)/(E_x*E_x*(E_x*E_x+E_y*E_y))
                -4*Xsi_xy/((E_x+E_y)*(E_x+E_y))
            };
            ierr =  CalcDStiffnessFromDCompliance(design,d_compliance_values,d_stiffness_values);
                                                                                       CHKERRQ(ierr);
          }
          break;
        case 1:
          // E_y
          {
            double d_compliance_values[9] = { 
              0, -1/(E_y*E_y), 0,
              2*Xsi_yz/((E_y+E_z)*(E_y+E_z)), 0, 2*Xsi_xy/((E_x+E_y)*(E_x+E_y)),
              (E_y*E_y+E_z*E_z-E_y*std::sqrt(8/(E_y*E_y)+8/(E_z*E_z))*E_z*E_z)/(E_y*E_y*(E_y*E_y+E_z*E_z))
                -4*Xsi_yz/((E_y+E_z)*(E_y+E_z)),
              0,
              (E_y*E_y+E_x*E_x-E_y*std::sqrt(8/(E_x*E_x)+8/(E_y*E_y))*E_x*E_x)/(E_y*E_y*(E_y*E_y+E_x*E_x))
                -4*Xsi_xy/((E_x+E_y)*(E_x+E_y))
            };
            ierr =  CalcDStiffnessFromDCompliance(design,d_compliance_values,d_stiffness_values);
                                                                                       CHKERRQ(ierr);
          }
          break;
        case 2:
          // E_z
          {
            double d_compliance_values[9] = { 
              0, 0, -1/(E_z*E_z),
              2*Xsi_yz/((E_y+E_z)*(E_y+E_z)), 2*Xsi_xz/((E_x+E_z)*(E_x+E_z)), 0,
              (E_y*E_y+E_z*E_z-E_z*std::sqrt(8/(E_y*E_y)+8/(E_z*E_z))*E_y*E_y)/(E_z*E_z*(E_y*E_y+E_z*E_z))
                -4*Xsi_yz/((E_y+E_z)*(E_y+E_z)),
              (E_x*E_x+E_z*E_z-E_z*std::sqrt(8/(E_x*E_x)+8/(E_z*E_z))*E_x*E_x)/(E_z*E_z*(E_x*E_x+E_z*E_z))
                -4*Xsi_xz/((E_x+E_z)*(E_x+E_z))
            };
            ierr =  CalcDStiffnessFromDCompliance(design,d_compliance_values,d_stiffness_values);
                                                                                       CHKERRQ(ierr);
          }
          break;
        case 3:
          // Xsi_yz
          {
            double d_compliance_values[9] = {
              0,            0, 0,
              -2/(E_y+E_z), 0, 0,
              4/(E_y+E_z),  0, 0
            };
            ierr =  CalcDStiffnessFromDCompliance(design,d_compliance_values,d_stiffness_values);
                                                                                       CHKERRQ(ierr);
          }
          break;
        case 4:
          // Xsi_xz
          {
            double d_compliance_values[9] = { 
              0, 0,            0,
              0, -2/(E_x+E_z), 0,
              0,  4/(E_x+E_z), 0
            };
            ierr =  CalcDStiffnessFromDCompliance(design,d_compliance_values,d_stiffness_values);
                                                                                       CHKERRQ(ierr);
          }
          break;
        case 5:
          // Xsi_xy
          {
            double d_compliance_values[9] = { 
              0, 0, 0,
              0, 0, -2/(E_x+E_y),
              0, 0,  4/(E_x+E_y)
            };
            ierr =  CalcDStiffnessFromDCompliance(design,d_compliance_values,d_stiffness_values);
                                                                                       CHKERRQ(ierr);
          }
          break;
        default:
          SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,
                  "asked for derivative wrt non-existent design variable  " __FILE__ " (AxisAlignedEllipsoidal)");
          break;
          
      }
      ierr =  FillTensorWithValues(d_stiffness_values,dtensor);                        CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }
};


class TrilinearInterpolated : public TensorBase {
  public:
    TrilinearInterpolated(const JSONValue *desc) 
        : tensors_(ReadArray5d(desc->ObjectMember("tensor file")->AsString())),
          interp_pts_{desc->ObjectMember("interpolation points")->ArrayMember(0)->ArrayAsVectorOfDoubles(),
                      desc->ObjectMember("interpolation points")->ArrayMember(1)->ArrayAsVectorOfDoubles(),
                      desc->ObjectMember("interpolation points")->ArrayMember(2)->ArrayAsVectorOfDoubles()} {
      if(tensors_.GetI()!=interp_pts_[0].size() ||
         tensors_.GetJ()!=interp_pts_[1].size() ||
         tensors_.GetK()!=interp_pts_[2].size()) {
        throw std::runtime_error("TriLinearInterpolated tensor: tensor array size does not match interpolation points");
      }
      if(tensors_.GetL()!=6 || tensors_.GetM()!=6) {
        throw std::runtime_error("TriLinearInterpolated tensor: tensor array shape is not (_,_,_,6,6)");
      }
    }

    PetscErrorCode EvalTensor(const double *design, int it, Array4d<double> &tensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;

      tensor.Resize(3,3,3,3);
       
      double alpha[3];
      int index[3];
      double interval_size[3];
      ierr = GetIndicesAndAlphas(design,index,alpha,interval_size);                    CHKERRQ(ierr);

      for(int u=0; u<6; ++u)
      for(int v=0; v<6; ++v) {
        double val = 
          (1-alpha[0])*(1-alpha[1])*(1-alpha[2])*tensors_(index[0],  index[1],  index[2],  u,v) +
          (1-alpha[0])*(1-alpha[1])*   alpha[2] *tensors_(index[0],  index[1],  index[2]+1,u,v) +
          (1-alpha[0])*   alpha[1] *(1-alpha[2])*tensors_(index[0],  index[1]+1,index[2],  u,v) +
          (1-alpha[0])*   alpha[1] *   alpha[2] *tensors_(index[0],  index[1]+1,index[2]+1,u,v) +
             alpha[0] *(1-alpha[1])*(1-alpha[2])*tensors_(index[0]+1,index[1],  index[2],  u,v) +
             alpha[0] *(1-alpha[1])*   alpha[2] *tensors_(index[0]+1,index[1],  index[2]+1,u,v) +
             alpha[0] *   alpha[1] *(1-alpha[2])*tensors_(index[0]+1,index[1]+1,index[2],  u,v) +
             alpha[0] *   alpha[1] *   alpha[2] *tensors_(index[0]+1,index[1]+1,index[2]+1,u,v);

        tensor(voigt[u][0],voigt[u][1],voigt[v][0],voigt[v][1]) = val;
        tensor(voigt[u][1],voigt[u][0],voigt[v][0],voigt[v][1]) = val;
        tensor(voigt[u][0],voigt[u][1],voigt[v][1],voigt[v][0]) = val;
        tensor(voigt[u][1],voigt[u][0],voigt[v][1],voigt[v][0]) = val;
      }
      PetscFunctionReturn(0);
    }

    PetscErrorCode EvalDTensor(const double *design, int it, int d, Array4d<double> &dtensor) const {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;

      dtensor.Resize(3,3,3,3);
       
      double alpha[3];
      int index[3];
      double interval_size[3];
      ierr = GetIndicesAndAlphas(design,index,alpha,interval_size);                    CHKERRQ(ierr);

      double val;
      for(int u=0; u<6; ++u)
      for(int v=0; v<6; ++v) {
        switch(d) {
          case 0:
            val =       -1     *(1-alpha[1])*(1-alpha[2])*tensors_(index[0],  index[1],  index[2],  u,v) +
                        -1     *(1-alpha[1])*   alpha[2] *tensors_(index[0],  index[1],  index[2]+1,u,v) +
                        -1     *   alpha[1] *(1-alpha[2])*tensors_(index[0],  index[1]+1,index[2],  u,v) +
                        -1     *   alpha[1] *   alpha[2] *tensors_(index[0],  index[1]+1,index[2]+1,u,v) +
                         1     *(1-alpha[1])*(1-alpha[2])*tensors_(index[0]+1,index[1],  index[2],  u,v) +
                         1     *(1-alpha[1])*   alpha[2] *tensors_(index[0]+1,index[1],  index[2]+1,u,v) +
                         1     *   alpha[1] *(1-alpha[2])*tensors_(index[0]+1,index[1]+1,index[2],  u,v) +
                         1     *   alpha[1] *   alpha[2] *tensors_(index[0]+1,index[1]+1,index[2]+1,u,v);
            val /= interval_size[0];
            break;
          case 1:
            val =  (1-alpha[0])*     -1     *(1-alpha[2])*tensors_(index[0],  index[1],  index[2],  u,v) +
                   (1-alpha[0])*     -1     *   alpha[2] *tensors_(index[0],  index[1],  index[2]+1,u,v) +
                   (1-alpha[0])*      1     *(1-alpha[2])*tensors_(index[0],  index[1]+1,index[2],  u,v) +
                   (1-alpha[0])*      1     *   alpha[2] *tensors_(index[0],  index[1]+1,index[2]+1,u,v) +
                      alpha[0] *     -1     *(1-alpha[2])*tensors_(index[0]+1,index[1],  index[2],  u,v) +
                      alpha[0] *     -1     *   alpha[2] *tensors_(index[0]+1,index[1],  index[2]+1,u,v) +
                      alpha[0] *      1     *(1-alpha[2])*tensors_(index[0]+1,index[1]+1,index[2],  u,v) +
                      alpha[0] *      1     *   alpha[2] *tensors_(index[0]+1,index[1]+1,index[2]+1,u,v);
            val /= interval_size[1];
            break;
          case 2:
            val =  (1-alpha[0])*(1-alpha[1])*     -1     *tensors_(index[0],  index[1],  index[2],  u,v) +
                   (1-alpha[0])*(1-alpha[1])*      1     *tensors_(index[0],  index[1],  index[2]+1,u,v) +
                   (1-alpha[0])*   alpha[1] *     -1     *tensors_(index[0],  index[1]+1,index[2],  u,v) +
                   (1-alpha[0])*   alpha[1] *      1     *tensors_(index[0],  index[1]+1,index[2]+1,u,v) +
                      alpha[0] *(1-alpha[1])*     -1     *tensors_(index[0]+1,index[1],  index[2],  u,v) +
                      alpha[0] *(1-alpha[1])*      1     *tensors_(index[0]+1,index[1],  index[2]+1,u,v) +
                      alpha[0] *   alpha[1] *     -1     *tensors_(index[0]+1,index[1]+1,index[2],  u,v) +
                      alpha[0] *   alpha[1] *      1     *tensors_(index[0]+1,index[1]+1,index[2]+1,u,v);
            val /= interval_size[2];
            break;
          default:
            throw std::logic_error("TriLinearInterpolated tensor: asked for derivative on bad dimension");
        }
        dtensor(voigt[u][0],voigt[u][1],voigt[v][0],voigt[v][1]) = val;
        dtensor(voigt[u][1],voigt[u][0],voigt[v][0],voigt[v][1]) = val;
        dtensor(voigt[u][0],voigt[u][1],voigt[v][1],voigt[v][0]) = val;
        dtensor(voigt[u][1],voigt[u][0],voigt[v][1],voigt[v][0]) = val;
      }
      PetscFunctionReturn(0);
    }

  private:
    PetscErrorCode GetIndicesAndAlphas(const double *design,
                                       int index[3],
                                       double alpha[3],
                                       double interval_size[3]) const {
      PetscFunctionBeginUser;
      for(int i=0; i<3; ++i) {
        size_t p_i=1;
        while(design[i]>interp_pts_[i][p_i] && p_i<interp_pts_[i].size()-1) p_i++;
        index[i] = p_i-1;
        interval_size[i] = interp_pts_[i][p_i]-interp_pts_[i][p_i-1];
        alpha[i] = (design[i]-interp_pts_[i][p_i-1])/interval_size[i];
      }
      PetscFunctionReturn(0);
    }

    const Array5d<double> tensors_;
    const std::vector<double> interp_pts_[3];
};


PetscErrorCode GetLambdaMu(const JSONValue *desc, double &lambda, double &mu) {
  PetscFunctionBeginUser;
  // Allow them to specify either E/nu or lambda/mu.
  if(desc->ObjectHasMember("E") && desc->ObjectHasMember("nu")) {
    double E = desc->ObjectMember("E" )->AsDouble();
    double n = desc->ObjectMember("nu")->AsDouble();
    lambda = E*n/(1+n)/(1-2*n);
    mu     = E/(2+2*n);
  } else if(desc->ObjectHasMember("lambda") && desc->ObjectHasMember("mu")) {
    lambda = desc->ObjectMember("lambda")->AsDouble();
    mu     = desc->ObjectMember("mu"    )->AsDouble();
  } else {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE, "must provide E/nu or lambda/mu for stiffness tensor");
  }
  PetscFunctionReturn(0);
}


PetscErrorCode ParseTensor(const JSONValue *tensor_desc, int design_vars, std::unique_ptr<TensorBase> &output) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  std::string type_name = tensor_desc->ObjectMember("type")->AsString();
  /***** 2D SIMP *****/
  if(type_name=="SIMP_2D") {
    if(design_vars!=1) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
               "SIMP_2D tensor needs 1 design variable, %d available", design_vars);
    }
    double P  = tensor_desc->ObjectMember("P")->AsDouble();
    double E  = tensor_desc->ObjectMember("E")->AsDouble();
    double nu = tensor_desc->ObjectMember("nu")->AsDouble();
    output.reset(new SIMPTensor2d(P,E,nu));

  /***** 3D SIMP *****/
  } else if(type_name=="SIMP") { 
    if(design_vars!=1) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
               "SIMP tensor needs 1 design variable, %d available", design_vars);
    }
    double lambda, mu;
    ierr =  GetLambdaMu(tensor_desc, lambda, mu);                                      CHKERRQ(ierr);

    if(tensor_desc->ObjectHasMember("start it")) {
      double start_P  = tensor_desc->ObjectMember("start P")->AsDouble();
      double end_P    = tensor_desc->ObjectMember("end P")->AsDouble();
      int    start_it = tensor_desc->ObjectMember("start it")->AsInt();
      int    end_it   = tensor_desc->ObjectMember("end it")->AsInt();
      output.reset(new SIMPTensor(lambda, mu, start_P, start_it, end_P, end_it));

    } else {
      double P = tensor_desc->ObjectMember("P")->AsDouble();
      output.reset(new SIMPTensor(P, lambda, mu));
    }

  /***** 3D IMS  *****/
  } else if (type_name=="IMS") {
    if(design_vars!=1) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
               "IMS tensor needs 1 design variable, %d available", design_vars);
    }
    double a_11   = tensor_desc->ObjectMember("a_11")->AsDouble();
    double a_12   = tensor_desc->ObjectMember("a_12")->AsDouble();
    double a_44   = tensor_desc->ObjectMember("a_44")->AsDouble();
    double lambda, mu;
    ierr =  GetLambdaMu(tensor_desc, lambda, mu);                                      CHKERRQ(ierr);
    output.reset(new IMSTensor(a_11, a_12, a_44, lambda, mu));
 
 } else if (type_name=="IMS3") {
    if(design_vars!=1) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
               "IMS tensor needs 1 design variable, %d available", design_vars);
    }
    std::vector<double> a_11 = tensor_desc->ObjectMember("a_11")->ArrayAsVectorOfDoubles(3);
    std::vector<double> a_12 = tensor_desc->ObjectMember("a_12")->ArrayAsVectorOfDoubles(3);
    std::vector<double> a_44 = tensor_desc->ObjectMember("a_44")->ArrayAsVectorOfDoubles(3);
    
    double lambda, mu;
    ierr =  GetLambdaMu(tensor_desc, lambda, mu);                                      CHKERRQ(ierr);
    output.reset(new IMS3Tensor(a_11, a_12, a_44, lambda, mu));
  
  /***** Rotation *****/
  } else if (type_name=="rotation ZXZ") {
    if(design_vars<3) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
               "tensor rotation (ZXZ) needs at least 3 design variables, %d available", design_vars);
    }
    std::unique_ptr<TensorBase> temp;
    ierr =  ParseTensor(tensor_desc->ObjectMember("sub tensor"), design_vars-3, temp); CHKERRQ(ierr);
    output.reset(new RotationTensorZXZ(std::move(temp)));
  
  } else if (type_name=="static rotation axis-angle") {
    // u is the rotation axis, we normalise it.
    std::vector<double> u = tensor_desc->ObjectMember("axis")->ArrayAsVectorOfDoubles(3);
    double u_mod = std::sqrt(u[0]*u[0] + u[1]*u[1] + u[2]*u[2]);
    u[0] /= u_mod;
    u[1] /= u_mod;
    u[2] /= u_mod;

    double t = tensor_desc->ObjectMember("angle")->AsDouble();
    double ct = std::cos(t), st = std::sin(t);

    double T[3][3] = {{ct+u[0]*u[0]*(1-ct)        ,    u[0]*u[1]*(1-ct)-u[2]*st,    u[0]*u[2]*(1-ct)+u[1]*st},
                      {   u[1]*u[0]*(1-ct)+u[2]*st, ct+u[1]*u[1]*(1-ct)        ,    u[1]*u[2]*(1-ct)-u[0]*st},
                      {   u[2]*u[0]*(1-ct)-u[1]*st,    u[2]*u[1]*(1-ct)+u[0]*st, ct+u[2]*u[2]*(1-ct)        }};
    std::unique_ptr<TensorBase> temp;
    ierr =  ParseTensor(tensor_desc->ObjectMember("sub tensor"), design_vars, temp);   CHKERRQ(ierr);
    output.reset(new StaticMatrixTransformationTensor(T, std::move(temp)));

  /***** Scalar Multiply  *****/
  } else if (type_name=="scalar multiply") {
    std::unique_ptr<TensorBase> temp;
    ierr =  ParseTensor(tensor_desc->ObjectMember("sub tensor"),design_vars,temp);     CHKERRQ(ierr);
    double scalar = tensor_desc->ObjectMember("scalar")->AsDouble();
    output.reset(new ScalarMultiplyTensor(scalar,std::move(temp)));

  /***** Interpolate  *****/
  } else if (type_name=="linearly interpolate") {
    if(design_vars<1) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
               "linear interpolate tensor needs at least 1 design variables, %d available", design_vars);
    }
    std::unique_ptr<TensorBase> temp;
    ierr =  ParseTensor(tensor_desc->ObjectMember("sub tensor"), design_vars-1, temp); CHKERRQ(ierr);
    output.reset(new LinearInterpTensor(std::move(temp)));

  /***** Switch  *****/
  } else if(type_name=="switch") {
    if(design_vars<1) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
               "the switch tensor needs at least 1 design variables, %d available", design_vars);
    }
    std::vector<SwitchTensor::SwitchSubTensor> sub_list;
    int num_subs = tensor_desc->ObjectMember("sub tensor list")->ArraySize();
    for(int i=0; i<num_subs; ++i) {
      const JSONValue *sub_desc = tensor_desc->ObjectMember("sub tensor list")->ArrayMember(i);
      SwitchTensor::SwitchSubTensor sub;
      sub.reuse_switch_var = sub_desc->ObjectMember("reuse switch design variable")->AsBool();
      sub.design_dof       = sub_desc->ObjectMember("design dof")->AsInt();
      sub.range[0]         = sub_desc->ObjectMember("range")->ArrayMember(0)->AsDouble();
      sub.range[1]         = sub_desc->ObjectMember("range")->ArrayMember(1)->AsDouble();
      if(sub.design_dof < 0) {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE, "switch tensor's sub tensor has negative dof");
      }
      if(( sub.reuse_switch_var && sub.design_dof > design_vars) ||
         (!sub.reuse_switch_var && sub.design_dof+1 > design_vars)) {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
                "switch tensor's sub tensor requires more design dof than can be provided");
      }
      ierr =  ParseTensor(sub_desc->ObjectMember("sub tensor"), sub.design_dof, sub.sub_tensor);
                                                                                       CHKERRQ(ierr);
      sub_list.push_back(std::move(sub));
    }
    output.reset(new SwitchTensor(std::move(sub_list)));
 
  /****** Orthotropic *****/
  } else if (type_name=="orthotropic") {
    if(design_vars!=0) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
               "Orthotropic tensor has no design variables, %d available", design_vars);
    }
    double C11 = tensor_desc->ObjectMember("C11")->AsDouble();
    double C12 = tensor_desc->ObjectMember("C12")->AsDouble();
    double C44 = tensor_desc->ObjectMember("C44")->AsDouble();
    output.reset(new OrthotropicTensor(C11,C12,C44));

  } else if (type_name=="polynomially interpolated orthotropic") {
    if(design_vars!=1) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
        "polynomially interpolated orthotropic tensor has 1 design variable, %d available",design_vars);
    }
    double C11 = tensor_desc->ObjectMember("C11")->AsDouble();
    double C12 = tensor_desc->ObjectMember("C12")->AsDouble();
    double C44 = tensor_desc->ObjectMember("C44")->AsDouble();
    std::vector<double> A11 = tensor_desc->ObjectMember("A11")->ArrayAsVectorOfDoubles();
    std::vector<double> A12 = tensor_desc->ObjectMember("A12")->ArrayAsVectorOfDoubles();
    std::vector<double> A44 = tensor_desc->ObjectMember("A44")->ArrayAsVectorOfDoubles();
    output.reset(new PolynomialOrthotropicTensor(C11,C12,C44,A11,A12,A44));

  } else if (type_name=="cubic spline interpolated orthotropic") {
    if(design_vars!=1) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
        "cubic spline interpolated orthotropic tensor has 1 design variable, %d available",design_vars);
    }
    double C11 = tensor_desc->ObjectMember("C11")->AsDouble();
    double C12 = tensor_desc->ObjectMember("C12")->AsDouble();
    double C44 = tensor_desc->ObjectMember("C44")->AsDouble();
    std::vector<double> x11 = tensor_desc->ObjectMember("x11")->ArrayAsVectorOfDoubles();
    std::vector<double> y11 = tensor_desc->ObjectMember("y11")->ArrayAsVectorOfDoubles();
    std::vector<double> x12 = tensor_desc->ObjectMember("x12")->ArrayAsVectorOfDoubles();
    std::vector<double> y12 = tensor_desc->ObjectMember("y12")->ArrayAsVectorOfDoubles();
    std::vector<double> x44 = tensor_desc->ObjectMember("x44")->ArrayAsVectorOfDoubles();
    std::vector<double> y44 = tensor_desc->ObjectMember("y44")->ArrayAsVectorOfDoubles();
    output.reset(new CubicSplineOrthotropicTensor(C11,C12,C44,x11,y11,x12,y12,x44,y44));
  
  /****** Trilinear *****/
  } else if (type_name=="trilinear interpolated") {
    if(design_vars!=3) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
        "trilinear interpolated tensor has 3 design variables, %d available",design_vars);
    }
    output.reset(new TrilinearInterpolated(tensor_desc));
  /****** Isotropic *****/
  } else if (type_name=="isotropic") {
    if(design_vars!=0) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
               "isotropic tensor (3D) has no design variables, %d available", design_vars);
    }
    double lambda, mu;
    ierr =  GetLambdaMu(tensor_desc, lambda, mu);                                      CHKERRQ(ierr);
    output.reset(new OrthotropicTensor(lambda+2*mu, lambda, mu));

  } else if (type_name=="isotropic plane stress") {
    if(design_vars!=0) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
               "isotropic plane stress has no design variables, %d available", design_vars);
    }
    double E  = tensor_desc->ObjectMember("E" )->AsDouble();
    double nu = tensor_desc->ObjectMember("nu")->AsDouble();
    double prefac = E/(1-nu*nu);
    output.reset(new OrthotropicTensor(prefac,prefac*nu,prefac*(1-nu)/2));

  } else if (type_name=="axis aligned ellipsoidal") {
    if(design_vars!=6) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
               "axis aligned ellipsoidal tensor has 6 design variables, %d available", design_vars);
    }
    output.reset(new AxisAlignedEllipsoidal);

  } else {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
      "GetTensor: Unknown tensor type \"%s\"",type_name.c_str());
  }

  PetscFunctionReturn(0);
}


// External Interface


PetscErrorCode GetTensor(const JSONObject *spec, std::unique_ptr<TensorBase> &tensor) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  int design_dof = spec->ObjectMember("design dof")->AsInt();
  const JSONValue *tensor_desc = spec->ObjectMember("stiffness tensor");
  ierr =  ParseTensor(tensor_desc, design_dof, tensor);                                CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

