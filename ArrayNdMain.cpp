
#include "ArrayNd.h"

using namespace std;


/// Fills a array and reads back the values
bool FillAndReadTest2d() {
  bool status = true;

  Array2d<int> F(2, 3);
  if(F.GetI()!=2 || F.GetJ()!=3) {
    std::cout << "* Get size functions test failed Array2d (in fill and read)" << std::endl;
    status = false;
  }

  // Write
  for(int i=0; i<2; i++)
  for(int j=0; j<3; j++)
    F(i,j) = i*0x100 + j*0x010;

  // Read
  for(int i=0; i<2; i++)
  for(int j=0; j<3; j++) {
    if(F(i,j) != i*0x100 + j*0x010) {
      std::cout << "* Fill and read test failed Array2d" << std::endl;
      status = false;
    }
  }
  return status;
}


bool FillAndReadTest3d() {
  bool status = true;

  Array3d<int> F(2, 3, 4);
  if(F.GetI()!=2 || F.GetJ()!=3 || F.GetK()!=4) {
    std::cout << "* Get size functions test failed Array3d (in fill and read)" << std::endl;
    status = false;
  }

  // Write
  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++)
    F(i,j,k) = i*0x100 + j*0x010 + k*0x001;

  // Read
  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++) {
    if(F(i,j,k) != i*0x100 + j*0x010 + k*0x001) {
      std::cout << "* Fill and read test failed Array3d" << std::endl;
      status = false;
    }
  }
  return status;
}

bool FillAndReadTest4d() {
  bool status = true;
  Array4d<int> F(2, 3, 4, 5);

  if(F.GetI()!=2 || F.GetJ()!=3 || F.GetK()!=4 || F.GetL()!=5) {
    std::cout << "* Get size functions test failed Array4d (in fill and read)" << std::endl;
    status = false;
  }

  // Write
  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++) for(int m=0; m<5; m++)
    F(i,j,k,m) = i*0x1000 + j*0x0100 + k*0x0010 + m*0x0001;

  // Read
  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++) for(int m=0; m<5; m++) {
    if(F(i,j,k,m) != i*0x1000 + j*0x0100 + k*0x0010 + m*0x0001) {
      std::cout << "* Fill and read test failed Array4d" << std::endl;
      status = false;
    }
  }
  return status;
}


/// Writes a array and reads back the values
bool FileWriteAndReadTest2d() {
  Array2d<double> F(2, 3);
  // Write
  for(int i=0; i<2; i++)
  for(int j=0; j<3; j++)
    F(i,j) = i*0x100 + j*0x010;

  OutputArray(F, "test.csv");

  // Read
  Array2d<double> F2;
  F2 = ReadArray2d("test.csv");

  if(F.GetI()!=F2.GetI() || F.GetJ()!=F2.GetJ()) {
    std::cout << "* Write and read test failed, read produced incorrect size Array2d" << std::endl;
    return false;
  }

  for(int i=0; i<2; i++)
  for(int j=0; j<3; j++) {
    if(F(i,j) != F2(i,j)) {
      std::cout << "* Write and read test failed, read gave incorrect values Array2d" << std::endl;
      std::cout << F2(i,j) << ' ' << F(i,j) << '\n';
      return false;
    }
  }
  return true;
}


bool FileWriteAndReadTest3d() {
  Array3d<double> F(2, 3, 4);
  // Write
  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++)
    F(i,j,k) = i*0x100 + j*0x010 + k*0x001;

  OutputArray(F, "test.csv");

  // Read
  Array3d<double> F2;
  F2 = ReadArray3d("test.csv");

  if(F.GetI()!=F2.GetI() || F.GetJ()!=F2.GetJ() || F.GetK()!=F2.GetK()) {
    std::cout << "* Write and read test failed, read produced incorrect size Array3d" << std::endl;
    return false;
  }

  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++) {
    if(F(i,j,k) != F2(i,j,k)) {
      std::cout << "* Write and read test failed, read gave incorrect values Array3d" << std::endl;
      std::cout << F2(i,j,k) << ' ' << F(i,j,k) << '\n';
      return false;
    }
  }
  return true;
}


bool FileWriteAndReadTest4d() {
  Array4d<double> F(2, 3, 4, 5);
  // Write
  for(int i=0; i<2; i++)
  for(int j=0; j<3; j++)
  for(int k=0; k<4; k++)
  for(int l=0; l<5; l++)
    F(i,j,k,l) = i*0x1000 + j*0x0100 + k*0x0010 + l*0x0001;

  OutputArray(F, "test.csv");

  // Read
  Array4d<double> F2;
  F2 = ReadArray4d("test.csv");

  if(F.GetI()!=F2.GetI() || F.GetJ()!=F2.GetJ() || F.GetK()!=F2.GetK() || F.GetL()!=F2.GetL()) {
    std::cout << "* Write and read test failed, read produced incorrect size Array4d" << std::endl;
    return false;
  }

  for(int i=0; i<2; i++)
  for(int j=0; j<3; j++)
  for(int k=0; k<4; k++)
  for(int l=0; l<5; l++) {
    if(F(i,j,k,l) != F2(i,j,k,l)) {
      std::cout << "* Write and read test failed, read gave incorrect values Array4d" << std::endl;
      return false;
    }
  }
  return true;
}

/// Sets the Set function by calling it and checking every value
bool SetTest2d() {
  Array2d<int> F(2, 3);
  // Write
  int v = 5218694;
  F.Set(v);

  // Read
  for(int i=0; i<2; i++)
  for(int j=0; j<3; j++) {
    if(F(i,j) != v) {
      std::cout << "* Set test failed Array2d" << std::endl;
      return false;
    }
  }
  return true;
}

bool SetTest3d() {
  Array3d<int> F(2, 3, 4);
  // Write
  int v = 5218694;
  F.Set(v);

  // Read
  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++) {
    if(F(i,j,k) != v) {
      std::cout << "* Set test failed Array3d" << std::endl;
      return false;
    }
  }
  return true;
}

bool SetTest4d() {
  Array4d<int> F(2, 3, 4, 5);
  // Write
  int v = 5218694;
  F.Set(v);

  // Read
  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++) for(int m=0; m<5; m++) {
    if(F(i,j,k,m) != v) {
      std::cout << "* Set test failed Array4d" << std::endl;
      return false;
    }
  }
  return true;
}

/// Purposefully accesses out of range to check the error checking.
// Out of range test is only when in DEBUG
bool OutOfRangeTest2d() {
  #ifndef DEBUG
    std::cout << "- Bounds checking not done as not in DEBUG\n";
    return true;
  #endif

  bool s = true;
  Array2d<int> F(2, 3);
  try {
    F(3,2) = 3;
    s = false;
    std::cout << "* Out of range test failed (i) Array2d\n     (you will have segfaulted)" << std::endl;
  } catch(std::logic_error) {
  }

  try {
    F(1,4) = 3;
    s = false;
    std::cout << "* Out of range test failed (j) Array2d\n    (you will have segfaulted)" << std::endl;
  } catch(std::logic_error) {
  }

  return s;
}

bool OutOfRangeTest3d() {
  #ifndef DEBUG
    std::cout << "- Bounds checking not done as not in DEBUG\n";
    return true;
  #endif

  bool s = true;
  Array3d<int> F(2, 3, 4);
  try {
    F(3,2,2) = 3;
    s = false;
    std::cout << "* Out of range test failed (i) Array3d\n     (you will have segfaulted)" << std::endl;
  } catch(std::logic_error) {
  }

  try {
    F(1,4,2) = 3;
    s = false;
    std::cout << "* Out of range test failed (j) Array3d\n    (you will have segfaulted)" << std::endl;
  } catch(std::logic_error) {
  }

  try {
    F(1,2,5) = 3;
    s = false;
    std::cout << "* Out of range test failed (k) Array3d\n     (you will have segfaulted)" << std::endl;
  } catch(std::logic_error) {
  }
  return s;
}

bool OutOfRangeTest4d() {
  #ifndef DEBUG
    std::cout << "- Bounds checking not done as not in DEBUG\n";
    return true;
  #endif

  bool s = true;
  Array4d<int> F(2, 3, 4, 5);
  try {
    F(3,2,2,2) = 3;
    s = false;
    std::cout << "* Out of range test failed (i) Array4d\n     (you will have segfaulted)" << std::endl;
  } catch(std::logic_error) {
  }

  try {
    F(1,4,2,2) = 3;
    s = false;
    std::cout << "* Out of range test failed (j) Array4d\n    (you will have segfaulted)" << std::endl;
  } catch(std::logic_error) {
  }

  try {
    F(1,2,5,2) = 3;
    s = false;
    std::cout << "* Out of range test failed (k) Array4d\n     (you will have segfaulted)" << std::endl;
  } catch(std::logic_error) {
  }
  try {
    F(1,2,2,6) = 3;
    s = false;
    std::cout << "* Out of range test failed (m) Array4d\n     (you will have segfaulted)" << std::endl;
  } catch(std::logic_error) {
  }
  return s;
}


/// Assigns a array to a blank array (of different size) and then reads back
bool AssignmentTest2d() {
  bool status = true;
  Array2d<int> F(2, 3);

  for(int i=0; i<2; i++)
  for(int j=0; j<3; j++)
    F(i,j) = i*0x100 + j*0x010;

  Array2d<int> G;
  G = F;

  for(int i=0; i<2; i++)
  for(int j=0; j<3; j++) {
    if(G(i,j) != i*0x100 + j*0x010) {
      std::cout << "Assignment Operator test failed (reallocating test) Array2d" << std::endl;
      status = false;
      goto AT2Da;
    }
  }
  AT2Da:

  Array2d<int> H(2, 3);

  H = F;
  for(int i=0; i<2; i++)
  for(int j=0; j<3; j++) {
    if(H(i,j) != i*0x100 + j*0x010) {
      std::cout << "Assignment Operator test failed (same size test) Array2d" << std::endl;
      status = false;
      goto AT2Db;
    }
  }
  AT2Db:

  return status;
}


bool AssignmentTest3d() {
  bool status = true;
  Array3d<int> F(2, 3, 4);

  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++)
    F(i,j,k) = i*0x100 + j*0x010 + k*0x001;

  Array3d<int> G;
  G = F;

  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++) {
    if(G(i,j,k) != i*0x100 + j*0x010 + k*0x001) {
      std::cout << "Assignment Operator test failed (reallocating test) Array3d" << std::endl;
      status = false;
      goto AT3Da;
    }
  }
  AT3Da:

  Array3d<int> H(2, 3, 4);

  H = F;
  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++) {
    if(H(i,j,k) != i*0x100 + j*0x010 + k*0x001) {
      std::cout << "Assignment Operator test failed (same size test) Array3d" << std::endl;
      status = false;
      goto AT3Db;
    }
  }
  AT3Db:

  return status;
}

bool AssignmentTest4d() {
  bool status = true;
  Array4d<int> F(2, 3, 4, 5);

  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++) for(int m=0; m<5; m++)
    F(i,j,k,m) = i*0x1000 + j*0x0100 + k*0x0010 + m*0x0001;

  Array4d<int> G;
  G = F;
  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++) for(int m=0; m<5; m++) {
    if(G(i,j,k,m) != i*0x1000 + j*0x0100 + k*0x0010 + m*0x0001) {
      std::cout << "Assignment Operator test failed (reallocating from zero test) Array4d" << std::endl;
      status = false;
      goto AT4Da;
    }
  }
  AT4Da:

  Array4d<int> H(1, 6, 2, 5);
  H = F;
  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++) for(int m=0; m<5; m++) {
    if(H(i,j,k,m) != i*0x1000 + j*0x0100 + k*0x0010 + m*0x0001) {
      std::cout << "Assignment Operator test failed (reallocating from non-zero test) Array4d" << std::endl;
      status = false;
      goto AT4Db;
    }
  }
  AT4Db:

  Array4d<int> I(2, 3, 4, 5);
  I = F;
  for(int i=0; i<2; i++) for(int j=0; j<3; j++) for(int k=0; k<4; k++) for(int m=0; m<5; m++) {
    if(I(i,j,k,m) != i*0x1000 + j*0x0100 + k*0x0010 + m*0x0001) {
      std::cout << "Assignment Operator test failed (same size test) Array4d" << std::endl;
      status = false;
      goto AT4Dc;
    }
  }
  AT4Dc:

  return status;
}


int main()  {
  std::cout << "ArrayNd Tests, Compiled: " << __TIMESTAMP__ << "\n\n";

  // Runs through a few tests
  bool success = true;

  std::cout << "Runs tests on Array2d" << std::endl;

  success &= FillAndReadTest2d();
  success &= FileWriteAndReadTest2d();
  success &= SetTest2d();
  success &= OutOfRangeTest2d();
  success &= AssignmentTest2d();

  std::cout << "Runs tests on Array3d" << std::endl;

  success &= FillAndReadTest3d();
  success &= FileWriteAndReadTest3d();
  success &= SetTest3d();
  success &= OutOfRangeTest3d();
  success &= AssignmentTest3d();

  std::cout << "Runs tests on Array4d\n" << std::endl;

  success &= FillAndReadTest4d();
  success &= FileWriteAndReadTest4d();
  success &= SetTest4d();
  success &= OutOfRangeTest4d();
  success &= AssignmentTest4d();

  /// Finally:
  if(success) {
    std::cout << "\nTests finished with all tests successful" << std::endl;
  } else {
    std::cout << "\nTests finished with failures." << std::endl;
  }
  return 0;
}
