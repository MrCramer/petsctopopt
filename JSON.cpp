
#include "JSON.h"

#include <sstream>

#include <cmath>


const char* const ONE_INDENTATION = "    ";



/// CLASSES ///


JSONValue* JSONArray::ArrayMember(size_t i) {
  if(i>=values_.size()) {
    std::cerr << "JSONArray " << path_
              << ": asked for element " << i
              << ", have " << values_.size()
              << std::endl;
    throw(std::out_of_range("JSONArray"));
  }
  return values_[i].get();
}


const JSONValue* JSONArray::ArrayMember(size_t i) const {
  if(i>=values_.size()) {
    std::cerr << "JSONArray " << path_
              << ": asked for element " << i
              << ", have " << values_.size()
              << std::endl;
    throw(std::out_of_range("JSONArray"));
  }
  return values_[i].get();
}


std::unique_ptr<JSONValue> JSONArray::Clone() const {
  std::unique_ptr<JSONArray> new_arr(new JSONArray(path_));
  new_arr->values_.reserve(values_.size());
  for(const auto &val : values_) {
    new_arr->values_.push_back(val->Clone());
  }
  return std::unique_ptr<JSONValue>(new_arr.release());
}


std::vector<double> JSONArray::ArrayAsVectorOfDoubles(size_t expected_length) const {
  if(expected_length!=0 && values_.size()!=expected_length) {
    std::cerr << "JSONArray " << path_ << ": unexpected length, wanted " << expected_length
                              << " am " << values_.size() << std::endl;
    throw(std::out_of_range("JSONArray unexpected length"));
  }
  
  std::vector<double> out;
  out.reserve(values_.size());
  for(const auto &val : values_) out.push_back(val->AsDouble());
  return out;
}


std::vector<int> JSONArray::ArrayAsVectorOfInts(size_t expected_length) const {
  std::vector<int> out(values_.size());
  if(expected_length!=0 && out.size()!=expected_length)
    throw(std::out_of_range("JSONArray " + path_ + " unexpected length"));

  for(size_t i=0; i<values_.size(); ++i) {
    out[i] = values_[i]->AsInt();
  }
  return out;
}


void JSONArray::Streamify(std::ostream& stream, std::string indentation) const {
  if(values_.size()!=0) {
    stream << "[\n";
    size_t i;
    for(i=0;i<values_.size()-1; ++i) {
      stream << indentation << ONE_INDENTATION;
      values_[i]->Streamify(stream, indentation + ONE_INDENTATION);
      stream << ",\n";
    }
    stream << indentation << ONE_INDENTATION;
    values_[i]->Streamify(stream, indentation + ONE_INDENTATION);
    stream << "\n" << indentation << "]";
  } else {
    stream << "[]";
  }
  return;
}


JSONValue* JSONObject::ObjectMember(const std::string &key) {
  auto cit = values_.find(key);
  if(cit==values_.end()) {
    std::cerr << "\nKey doesn't exist in JSONObject, looking for:\n  "
              << path_ << "->\"" << key << "\"\n" << std::endl;
    throw(std::out_of_range("JSONObject"));
  }
  return cit->second.get();
}


const JSONValue* JSONObject::ObjectMember(const std::string &key) const {
  auto cit = values_.find(key);
  if(cit==values_.end()) {
    std::cerr << "\nKey doesn't exist in JSONObject, looking for:\n  "
              << path_ << "->\"" << key << "\"\n" << std::endl;
    throw(std::out_of_range("JSONObject"));
  }
  return cit->second.get();
}


void JSONObject::CloneMembersFrom(const JSONObject &rhs) {
  for(auto it=rhs.values_.begin(); it!=rhs.values_.end(); ++it) {
    Set(it->first, it->second->Clone());
  }
}


std::unique_ptr<JSONValue> JSONObject::Clone() const {
  std::unique_ptr<JSONObject> new_obj(new JSONObject(path_));
  new_obj->CloneMembersFrom(*this);
  return std::unique_ptr<JSONValue>(new_obj.release());
}


void JSONObject::Streamify(std::ostream& stream, std::string indentation) const {
  if(!values_.empty()) {
    stream << "{\n";
    auto cit=values_.begin();
    for(unsigned i=0; i<values_.size()-1; ++i) {
      stream << indentation << ONE_INDENTATION << '"' << cit->first << '"' << ": ";
      cit->second->Streamify(stream, indentation + ONE_INDENTATION);
      stream << ",\n";
      ++cit;
    }
    stream << indentation << ONE_INDENTATION << '"' << cit->first << '"' << ": ";
    cit->second->Streamify(stream, indentation + ONE_INDENTATION);
    stream << "\n" << indentation << "}";
  } else {
    stream << "{}";
  }
}


/// FILE PARSER ///

std::string WHITE_SPACE = " \t\n\r";

void JSONAssert(bool test, const char* msg, int line_num, const std::string &path) {
  if(!test) {
    std::cerr << msg << " near line " << line_num << ", in " << path << std::endl;
    throw(parse_error("JSONAssert"));
  }
}


void ClearWhiteSpace(std::istream &stream, int &line_num) {
  char c = stream.peek();
  while(WHITE_SPACE.find_first_of(c)!=std::string::npos) {
    stream.get();
    if(c=='\n') {
      line_num++;
    }
    c = stream.peek();
  }
}


// This function throws if it can't get a character from the stream
char DefGet(std::istream &stream) {
  char c = stream.get();
  if(!stream) {
    std::cerr << "JSON: unexpected end of input stream" << std::endl;
    throw(parse_error("DefGet"));
  }
  return c;
}


// This function throws if it can't peek a character from the stream
char DefPeek(std::istream &stream) {
  char c = stream.peek();
  if(!stream) {
    std::cerr << "JSON: unexpected end of input stream" << std::endl;
    throw(parse_error("DefPeek"));
  }
  return c;
}


// Extracts a string, assumes leading " and consumes the trailing "
std::string ExtractString(std::istream &stream, int &line_num, const std::string &path) {
  JSONAssert(DefGet(stream)=='"', "JSON ExtractString, missing \"", line_num, path);
  char c;
  std::string out = "";
  // Put everything between the " in the string
  while((c=DefGet(stream)) != '"') {
    if(c=='\n') {
      ++line_num;
    }
    if(c =='\\') {
      c = DefGet(stream);
      switch(c) {
        case '\'': out.push_back('\''); break;
        case '"':  out.push_back('"');  break;
        case 'r':  out.push_back('\r'); break;
        case 'n':  out.push_back('\n'); break;
        case 't':  out.push_back('\t'); break;
        case '\\': out.push_back('\\'); break;
        default:
          std::cerr << "JSON: unknown escape character \"" << c << "\" around line " << line_num << std::endl;
          throw(parse_error("unknown escape char parsing JSON string"));
      }
    } else {
      out.push_back(c);
    }
  }
  return out;
}


std::unique_ptr<JSONValue> JSONParseValue(std::istream &stream,
                                          int &line_num,
                                          bool track_path,
                                          const std::string &path);


// Assume leading {, consumes trailing }
void ParseJSONObject(std::istream &stream, JSONObject &obj, int &line_num, bool track_path, const std::string &path) {
  ClearWhiteSpace(stream, line_num);
  JSONAssert(DefGet(stream)=='{', "ParseJSONObject, missing {", line_num, path);

  // Look for a " or a }
  while(true) {
    ClearWhiteSpace(stream, line_num);
    char c = DefPeek(stream);
    if(c=='"') {
      // Found a key
      std::string name = ExtractString(stream, line_num, path);
      ClearWhiteSpace(stream, line_num);
      JSONAssert(DefGet(stream)==':', "JSONParseObject, missing a :", line_num, path);
      ClearWhiteSpace(stream, line_num);
      obj.Set(name, JSONParseValue(stream, line_num, track_path, track_path?path+"->\""+name+"\"":path));
      ClearWhiteSpace(stream, line_num);
      c = DefGet(stream);
      if(c=='}') {
        return;
      }
      JSONAssert(c==',', "JSON: missing a , while parsing an object", line_num, path);


    } else if(c=='}') {
      // Found the end of the object
      DefGet(stream);
      return;

    } else {
      std::cerr << "JSON: can't find name or } reading object around line " << line_num << std::endl;
      throw(parse_error("ParseJSONObject"));
    }
  }
}


// Assumes leading [, consumes the trailing ]
std::unique_ptr<JSONArray> ParseJSONArray(std::istream &stream,
                                          int &line_num,
                                          bool track_path,
                                          const std::string &path) {
  JSONAssert(DefGet(stream)=='[', "ParseJSONArray, missing [", line_num, path);
  std::unique_ptr<JSONArray> temp_owner(new JSONArray(path));
  ClearWhiteSpace(stream, line_num);
  char c = DefPeek(stream);
  if(c==']') {
    DefGet(stream);
    return temp_owner;
  }

  int i = 0;
  while(true) {
    std::stringstream new_path;
    if(track_path) {
      new_path << path << "->[" << (i++) << "]";
    } else {
      new_path << path;
    }
    temp_owner->Push(JSONParseValue(stream, line_num, track_path, new_path.str()));
    ClearWhiteSpace(stream, line_num);
    c = DefGet(stream);
    if(c==']') {
      return temp_owner;
    } else if(c==',') {
      ClearWhiteSpace(stream, line_num);
    } else {
      std::cerr << "JSON: missing , or ] reading array around line " << line_num << std::endl;
      throw(parse_error("ParseJSONArray"));
    }
  }
}


// How many decimal digits can a signed integral type reliably hold? Log10(2^num_binary_digits)
#define NUM_DIGITS(type) ( int(std::log10(2.0)*sizeof(type)*8.0) )

int ExtractInt(std::string str, int &line_num, const std::string &path) {
  JSONAssert(str.find_first_not_of("+-0123456789")==std::string::npos,
             "JSONExtractInt, bad character", line_num, path);
  return std::strtol(str.c_str(), nullptr, 0); // base of zero is auto-detect
}


double ExtractDbl(std::string str, int &line_num, const std::string &path) {
  JSONAssert(str.find_first_not_of("+-.eE0123456789")==std::string::npos,
             "JSONExtractInt, bad character", line_num, path);
  return std::strtod(str.c_str(), nullptr);
}


JSONValue* ExtractNumber(std::istream &stream, int &line_num, const std::string &path) {
  std::string str = "";
  char c;
  c = DefPeek(stream);

  while((WHITE_SPACE+",]}").find_first_of(c)==std::string::npos) {
    str.push_back(DefGet(stream));
    c = DefPeek(stream);
  }

  if(str.find_first_not_of("+-0123456789")==std::string::npos) {
    return new JSONValueInt(ExtractInt(str, line_num, path), path);

  } else if(str.find_first_not_of("+-.eE0123456789")==std::string::npos) {
    return new JSONValueDouble(ExtractDbl(str, line_num, path), path);
  }

  std::cerr << "JSON: bad number character around line " << line_num << std::endl;
  throw(parse_error("ExtractNumber"));
}


// Parse Value parses a value. It does not consume the trailing , ] or }
std::unique_ptr<JSONValue> JSONParseValue(std::istream &stream,
                                          int &line_num,
                                          bool track_path,
                                          const std::string &path) {
  ClearWhiteSpace(stream, line_num);
  char c = DefPeek(stream);
  if(c=='"') {
    return std::unique_ptr<JSONValue>(new JSONValueString(ExtractString(stream,line_num,path), path));

  } else if((c>='0' && c<='9') || c=='+' || c=='-' || c=='.') {
    return std::unique_ptr<JSONValue>(ExtractNumber(stream, line_num, path));

  } else if(c=='t') {
    DefGet(stream);
    bool b = DefGet(stream)=='r' && DefGet(stream)=='u' && DefGet(stream)=='e';
    if(!b) {
      std::cerr << "JSON: bad read of true around line " << line_num << ", in " << path  << std::endl;
      throw(parse_error("JSONParseValue"));
    }
    return std::unique_ptr<JSONValue>(new JSONValueBool(true, path));

  } else if(c=='f') {
    DefGet(stream);
    bool b = DefGet(stream)=='a' && DefGet(stream)=='l' && DefGet(stream)=='s' && DefGet(stream)=='e';
    if(!b) {
      std::cerr << "JSON: bad read of false around line " << line_num << ", in " << path << std::endl;
      throw(parse_error("JSONParseValue"));
    }
    return std::unique_ptr<JSONValue>(new JSONValueBool(false, path));

  } else if(c=='n') {
    DefGet(stream);
    bool b = DefGet(stream)=='u' && DefGet(stream)=='l' && DefGet(stream)=='l';
    if(!b) {
      std::cerr << "JSON: bad read of null around line " << line_num << ", in " << path  << std::endl;
      throw(parse_error("JSONParseValue"));
    }
    return std::unique_ptr<JSONValue>(new JSONValueNull(path));

  } else if(c=='[') {
    return std::unique_ptr<JSONValue>(ParseJSONArray(stream, line_num, track_path, path));

  } else if(c=='{') {
    std::unique_ptr<JSONObject> temp(new JSONObject(path));
    ParseJSONObject(stream, *temp, line_num, track_path, path);
    return std::unique_ptr<JSONValue>(temp.release());

  }
  std::cerr << "JSON: couldn't determine value type around line " << line_num << ", in " << path  << std::endl;
  throw(parse_error("JSONParseValue"));
}


JSONObject ParseJSON(std::istream &stream, bool track_path, const std::string &root_path_name) {
  const std::string path_name = track_path?root_path_name:"";
  JSONObject out(root_path_name);
  int line_num = 1;
  ParseJSONObject(stream, out, line_num, track_path, path_name);
  return out;
}
