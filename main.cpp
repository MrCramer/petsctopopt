
#include <cfenv>
#include <cmath>
#include <fstream>
#include <sstream>

#include <petscksp.h>

#include "Elastic.h"
#include "Optimisers.h"


#define STR_BUFF_SIZE 256

char help_line[] = 
  "Optimising Linear Solver.\n"
  "For solving simple linear proglems and homogenisation problems, and optimising using\n"
  "the material distribution method.\n"
  "\n"
  "GIT Rev: " GIT_REV "\n"
  "\n";


// *** Optimisation Routine *** ///

PetscErrorCode EnforceDesignConstraints(const JSONObject &spec,
                                        Array4d<double>               *design,
                                        Array4d<char>                 *fixed,
                                        Array4d<double>               *d_design,
                                        std::vector<Array4d<double> > *d_constraints) {
  /* Enforces the design constraints on a given design, sets fixed flags appropriately,
   * and corrects the derivitive arrays (for symmetry constraints).
   *
   * If either any of the output arrays are nullptr they will be ignored.
   */
  PetscFunctionBeginUser;
  const int design_dof = spec.ObjectMember("design dof")->AsInt();

  if(fixed) {
    fixed->Resize(design_dof, DOM.I_el, DOM.J_el, DOM.K_el);
    fixed->Set(false);
  }

  if(!spec.ObjectHasMember("domain constraints")) PetscFunctionReturn(0);

  int N = spec.ObjectMember("domain constraints")->ArraySize();
  static std::vector<Array4d<double>> masks(N,Array4d<double>(0,0,0,0));
  
  // Keep track of which symmetries have been applied
  bool mirrored[3] = {false,false,false};
  for(int c=0; c<N; ++c) {
    const JSONValue *constraint = spec.ObjectMember("domain constraints")->ArrayMember(c);
    std::string type = constraint->ObjectMember("type")->AsString();
    if(type == "brick") {
      if(!design && !fixed) continue;
      double val = constraint->ObjectMember("value")->AsDouble();
      int i_s = constraint->ObjectMember("i indices")->ArrayMember(0)->AsInt();
      int i_e = constraint->ObjectMember("i indices")->ArrayMember(1)->AsInt();
      int j_s = constraint->ObjectMember("j indices")->ArrayMember(0)->AsInt();
      int j_e = constraint->ObjectMember("j indices")->ArrayMember(1)->AsInt();
      int k_s = constraint->ObjectMember("k indices")->ArrayMember(0)->AsInt();
      int k_e = constraint->ObjectMember("k indices")->ArrayMember(1)->AsInt();
      int d_s = constraint->ObjectMember("d indices")->ArrayMember(0)->AsInt();
      int d_e = constraint->ObjectMember("d indices")->ArrayMember(1)->AsInt();
      
      if(i_s<0 || i_e>=DOM.I_el ||
         j_s<0 || j_e>=DOM.J_el ||
         k_s<0 || k_e>=DOM.K_el ||
         d_s<0 || d_e>=design_dof) {
        SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
                 "'brick' constraint has invalid bounds (constraint %d)",c);
      }
      for(int i=i_s; i<=i_e; i++)
      for(int j=j_s; j<=j_e; j++)
      for(int k=k_s; k<=k_e; k++)
      for(int d=d_s; d<=d_e; d++) {
        if( fixed)   ( *fixed)(d,i,j,k) = true;
        if(design)   (*design)(d,i,j,k) = val;
      }

    } else if(type == "mask") {
      if(!design && !fixed) continue;
      double val = constraint->ObjectMember("value")->AsDouble();
      if(masks[c].GetI()==0) {
        std::string filename = constraint->ObjectMember("mask file")->AsString();
        masks[c] = ReadElementSizeArray(filename);
      }

      for(int k=0; k<DOM.K_el;   ++k)
      for(int j=0; j<DOM.J_el;   ++j)
      for(int i=0; i<DOM.I_el;   ++i)
      for(int d=0; d<design_dof; ++d) {
        // We use 1.0 here as it guards against accidental use of a different array
        // (say a design array) and can be exactly represented as a floating point
        // number. We use doubles in the first place because I couldn't be bothered
        // reimplementing the saving and loading functions for something different.
        double mask_val = masks[c](d,i,j,k);
        if(std::abs(mask_val-1.0) < 1e-10) {
          if( fixed) ( *fixed)(d,i,j,k) = true;
          if(design) (*design)(d,i,j,k) = val;
        } else if(std::abs(mask_val) > 1e-10) {
          std::cout << "mask value " << mask_val << " found in a mask for a domain constraint" << std::endl;
          SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Mask constraint is not just zeros and ones");
        }
      }

    } else if(type == "symmetry") {
      std::string dir = constraint->ObjectMember("direction")->AsString();
      if(dir=="x" || dir=="X") {
        if(mirrored[0]) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"mirrored twice in X");
        mirrored[0] = true;
        for(int i=(DOM.I_el+1)/2; i<DOM.I_el; ++i)
        for(int j=0;              j<DOM.J_el; ++j)
        for(int k=0;              k<DOM.K_el; ++k)
        for(int d=0;            d<design_dof; ++d) {
          int i_r = DOM.I_el-1-i;
          if(design)   (*design)(d,i,j,k) = (*design)(d,i_r,j,k);
          if( fixed)   ( *fixed)(d,i,j,k) = ( *fixed)(d,i_r,j,k);
          if(d_design) (*d_design)(d,i_r,j,k) += (*d_design)(d,i,j,k);
          if(d_constraints) {
            for(size_t cc=0; cc<d_constraints->size(); ++cc) {
              (*d_constraints)[cc](d,i_r,j,k) += (*d_constraints)[cc](d,i,j,k);
            }
          }
        }
      } else if(dir=="y" || dir=="Y") {
        if(mirrored[1]) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"mirrored twice in Y");
        mirrored[1] = true;
        for(int i=0;              i<DOM.I_el; ++i)
        for(int j=(DOM.J_el+1)/2; j<DOM.J_el; ++j)
        for(int k=0;              k<DOM.K_el; ++k)
        for(int d=0;            d<design_dof; ++d) {
          int j_r = DOM.J_el-1-j;
          if(design) (*design)(d,i,j,k) = (*design)(d,i,j_r,k);
          if( fixed) ( *fixed)(d,i,j,k) = ( *fixed)(d,i,j_r,k);
          if(d_design) (*d_design)(d,i,j_r,k) += (*d_design)(d,i,j,k);
          if(d_constraints) {
            for(size_t cc=0; cc<d_constraints->size(); ++cc) {
              (*d_constraints)[cc](d,i,j_r,k) += (*d_constraints)[cc](d,i,j,k);
            }
          }
        }
      } else if(dir=="z" || dir=="Z") {
        if(mirrored[2]) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"mirrored twice in Z");
        mirrored[2] = true;
        for(int i=0;              i<DOM.I_el; ++i)
        for(int j=0;              j<DOM.J_el; ++j)
        for(int k=(DOM.K_el+1)/2; k<DOM.K_el; ++k)
        for(int d=0;            d<design_dof; ++d) {
          int k_r = DOM.K_el-1-k;
          if(design) (*design)(d,i,j,k) = (*design)(d,i,j,k_r);
          if( fixed) ( *fixed)(d,i,j,k) = ( *fixed)(d,i,j,k_r);
          if(d_design) (*d_design)(d,i,j,k_r) += (*d_design)(d,i,j,k);
          if(d_constraints) {
            for(size_t cc=0; cc<d_constraints->size(); ++cc) {
              (*d_constraints)[cc](d,i,j,k_r) += (*d_constraints)[cc](d,i,j,k);
            }
          }
        }
      } else {
        SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
                 "bad direction in symmetry constraint (%s)\n", dir.c_str());
      }
    } else {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Unknown domain constraint %s\n",type.c_str());
    }
  }
  PetscFunctionReturn(0);
}


// Checks the terminating condition for the optimisation. If any condition is unmet the
// optimisation will continue until "max iterations".
PetscErrorCode CheckIfFinished(const JSONObject &spec,
                               const Array4d<double> &design_old, const Array4d<double> &design_new,
                               const double compliance,           const double compliance_old, 
                               const int it,
                               bool &finished) {
  PetscFunctionBeginUser;

  const JSONValue* cond = spec.ObjectMember("terminate condition");

  if(cond->ObjectHasMember("max iterations") && it > cond->ObjectMember("max iterations")->AsInt()) {
    finished = true;
    PetscFunctionReturn(0);
  }

  if(cond->ObjectHasMember("min iterations") && it < cond->ObjectMember("min iterations")->AsInt()) {
    finished = false;
    PetscFunctionReturn(0);
  }
  
  if(cond->ObjectHasMember("objective change tolerance")) {
    double compliance_tolerance = cond->ObjectMember("objective change tolerance")->AsDouble();
    if(std::abs(compliance-compliance_old)/compliance > compliance_tolerance) {
      finished = false;
      PetscFunctionReturn(0);
    }
  }

  if(cond->ObjectHasMember("mean design change tolerance")) {
    int D = design_new.GetI();
    int I = design_new.GetJ();
    int J = design_new.GetK();
    int K = design_new.GetL();

    double tol = cond->ObjectMember("mean design change tolerance")->AsDouble();
    double sum = 0;
    for(int k=0; k<K; ++k)
    for(int j=0; j<J; ++j)
    for(int i=0; i<I; ++i)
    for(int d=0; d<D; ++d) {
      sum += std::abs(design_old(d,i,j,k)-design_new(d,i,j,k));
    }
    if(sum > D*I*J*K*tol) {
      finished = false;
      PetscFunctionReturn(0);
    }
  }

  // Passes all conditions, so terminate.
  finished = true;
  PetscFunctionReturn(0);
}


extern int num_proc;
PetscErrorCode RunOptimisation(const JSONObject &spec, std::string design_output_file)  {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  const int design_dof = spec.ObjectMember("design dof")->AsInt();
  int save_period = spec.ObjectMember("save period")->AsInt();
  std::unique_ptr<OptFuncBase> opt_function(GetOptFunction(spec));

  bool zip_output = true;
  if(spec.ObjectHasMember("zip output")) {
    zip_output = spec.ObjectMember("zip output")->AsBool();
  }

  // initialise design
  Array4d<double> design1(design_dof, DOM.I_el, DOM.J_el, DOM.K_el);
  Array4d<double> design2(design_dof, DOM.I_el, DOM.J_el, DOM.K_el);
  Array4d<double> *design_new = &design1;
  Array4d<double> *design_old = &design2;
  Array4d<char>   fixed_design;
  
  const JSONValue *init = spec.ObjectMember("initial design");
  if(init->Is(JSON_ARRAY)) {
    std::vector<double> initial_design = init->ArrayAsVectorOfDoubles(design_dof);
    for(int k=0; k<DOM.K_el; ++k)
    for(int j=0; j<DOM.J_el; ++j)
    for(int i=0; i<DOM.I_el; ++i)
    for(int d=0; d<design_dof; ++d) {
      (*design_old)(d,i,j,k) = initial_design[d];
    }
  } else if(init->Is(JSON_STRING)) {
    (*design_old) = ReadElementSizeArray(init->AsString());
  } else {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
            "initial design should be either an array or a file name");
  }

  PetscInt  start_from;
  PetscBool was_set = PETSC_FALSE;
  ierr =  PetscOptionsGetInt(NULL,NULL,"-restart",&start_from,&was_set);               CHKERRQ(ierr);
  if(was_set) {
    std::stringstream s;
    s << design_output_file << std::setw(6) << std::setfill('0') << start_from << ".f4d";
    if(zip_output) s << ".gz";
    *design_old = ReadElementSizeArray(s.str());

  } else {
    start_from = 0;
  }
  
  ierr =  EnforceDesignConstraints(spec, design_old, &fixed_design, nullptr, nullptr); CHKERRQ(ierr);
  *design_new = *design_old;
  
  int it = start_from;

  // Loop the optimisation
  std::cout << "Optimising elastic solver\n" << GIT_REV << "\nConnected to " << num_proc << "processors\n\n";
  std::cout << "It:  Rel.Diff:   Objective:  Cons:      ";
  for(int i=1; i<spec.ObjectMember("constraints")->ArraySize(); ++i) std::cout << "          ";
  std::cout << "Time:   Optimiser:\n";
  std::cout << "-----------------------------------------------------------";
  for(int i=1; i<spec.ObjectMember("constraints")->ArraySize(); ++i) std::cout << "----------";
  std::cout << "\n                                                ";
  for(int i=1; i<spec.ObjectMember("constraints")->ArraySize(); ++i) std::cout << "          ";
  std::cout << std::flush;
  
  double objective = 0;
  std::vector<double> constraints;
  bool finished;
  do {
    time_t it_start = time(NULL);

    // Output design 
    if((it%save_period)==0) {
      std::stringstream s;
      s << std::setw(6) << std::setfill('0') << it;
      std::string filename = design_output_file + s.str() + ".f4d";
      OutputArray(*design_old, filename, zip_output);
    }

    // Call the optimiser
    double objective_old = objective;
    ierr =  opt_function->Step(it,spec,fixed_design,*design_old,*design_new,objective,constraints);
                                                                                       CHKERRQ(ierr);
    // Report
    if(it==start_from) {
      std::cout <<'\n'
                << std::noshowpos << std::setw(3) << it
                << "              "
                << std::scientific << std::setprecision(3) << std::showpos
                << objective << "  ";
      for(size_t i=0; i<constraints.size(); ++i) {
        std::cout << std::scientific << std::setprecision(2) << std::showpos << constraints[i] << " ";
      }
      std::cout << " "
                << std::noshowpos << std::setw(5) << (time(NULL)-it_start) << "s  "
                << std::flush << std::resetiosflags(std::ios_base::basefield) << std::noshowpos;
    } else {
      std::cout << '\n'
                << std::noshowpos << std::setw(3) << it << "  "
                << std::scientific << std::setprecision(3) << std::showpos
                << ((objective-objective_old)/objective) << "  "
                << objective << "  ";
      for(size_t i=0; i<constraints.size(); ++i) {
        std::cout << std::scientific << std::setprecision(2) << std::showpos << constraints[i] << " ";
      }
      std::cout << " "
                << std::noshowpos << std::setw(5) << (time(NULL)-it_start) << "s  "
                << std::flush << std::resetiosflags(std::ios_base::basefield) << std::noshowpos;
    }
    
    // Check terminating conditions
    ierr = CheckIfFinished(spec,*design_old,*design_new,objective,objective_old,it,finished);
                                                                                       CHKERRQ(ierr);

    // Finalise
    it++;
    std::swap(design_old, design_new);

  } while(!finished);

  std::cout << '\n';
  OutputArray(*design_old, design_output_file + "final.f4d");

  PetscFunctionReturn(0);
}



// *** Simple Calculations *** //

void CheckDisplacement(const JSONObject &spec, std::vector<Array4d<double> > u) {
  std::string test_name = spec.ObjectMember("expected result")->ObjectMember("test name")->AsString();
  std::cout << test_name << ":\n";

  std::vector<int>
    coords = spec.ObjectMember("expected result")->ObjectMember("coords")->ArrayAsVectorOfInts();
  
  std::vector<double>
    exp    = spec.ObjectMember("expected result")->ObjectMember("value")->ArrayAsVectorOfDoubles();

  double v[3] = {u[0](0,coords[0],coords[1],coords[2]),
                 u[0](1,coords[0],coords[1],coords[2]),
                 0.0};

  if(u[0].GetI()==2) {
    v[2] = 0.0;
  } else {
    v[2] = u[0](2,coords[0],coords[1],coords[2]);
  }

  double diff[3] = {v[0]-exp[0], v[1]-exp[1], v[2]-exp[2]};
  double err_norm = std::sqrt(diff[0]*diff[0] + diff[1]*diff[1] + diff[2]*diff[2]);
  double exp_norm = std::sqrt( exp[0]* exp[0] +  exp[1]* exp[1] +  exp[2]* exp[2]);
  std::cout << "  Expected Value:  " << exp[0] << ", " << exp[1] << ", " << exp[2] << '\n'
            << "  Actual Value:    " <<   v[0] << ", " <<   v[1] << ", " <<   v[2] << '\n'
            << "  Rel. Error Norm: " << (err_norm/exp_norm) << std::endl;
}


PetscErrorCode RunTest(const JSONObject &spec) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  const int design_dof = spec.ObjectMember("design dof")->AsInt();
  const int I = spec.ObjectMember("domain")->ArrayMember(0)->AsInt();
  const int J = spec.ObjectMember("domain")->ArrayMember(1)->AsInt();
  const int K = spec.ObjectMember("domain")->ArrayMember(2)->AsInt();

  int I_el, J_el, K_el;
  if(spec.ObjectHasMember("periodic") && spec.ObjectMember("periodic")->AsBool()) {
    I_el = I;   J_el = J;   K_el = K;
  } else {
    I_el = I-1; J_el = J-1; K_el = (K==1? 1 : K-1);
  }

  int num_load_cases = spec.ObjectMember("load cases")->ArraySize();

  std::vector<Array4d<double> > u(num_load_cases);

  // Get Rho
  Array4d<double> design;
  char buff[STR_BUFF_SIZE];
  PetscBool was_set;
  ierr =  PetscOptionsGetString(NULL,NULL,"-r",buff,STR_BUFF_SIZE,&was_set);           CHKERRQ(ierr);
  if(was_set) {
    design = ReadElementSizeArray(buff);
  } else {
    std::vector<double> initial_design = spec.ObjectMember("initial design")->ArrayAsVectorOfDoubles(design_dof);
    design.Resize(design_dof,I_el,J_el,K_el);
    for(int k=0; k<K_el; ++k)
    for(int j=0; j<J_el; ++j)
    for(int i=0; i<I_el; ++i)
    for(int d=0; d<design_dof; ++d) {
      design(d,i,j,k) = initial_design[d];
    }
  }

  // Run FEM
  ierr =  CalculateDisplacement(design, u);                                            CHKERRQ(ierr);

  if(spec.ObjectHasMember("expected result")) {
    CheckDisplacement(spec, u);
  }

  PetscPrintf(PETSC_COMM_WORLD, "\n");

  PetscFunctionReturn(0);
} 


PetscErrorCode RunHomogenization(const JSONObject &spec) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  int design_dof = spec.ObjectMember("design dof")->AsInt();
  int I = spec.ObjectMember("domain")->ArrayMember(0)->AsInt();
  int J = spec.ObjectMember("domain")->ArrayMember(1)->AsInt();
  int K = spec.ObjectMember("domain")->ArrayMember(2)->AsInt();

  int I_el, J_el, K_el;
  if(spec.ObjectHasMember("periodic") && spec.ObjectMember("periodic")->AsBool()) {
    I_el = I;   J_el = J;   K_el = K;
  } else {
    I_el = I-1; J_el = J-1; K_el = (K==1? 1 : K-1);
  }

  // Get Rho
  Array4d<double> design;
  char buff[STR_BUFF_SIZE];
  PetscBool was_set;
  ierr =  PetscOptionsGetString(NULL,NULL,"-r",buff,STR_BUFF_SIZE,&was_set);           CHKERRQ(ierr);
  if(was_set) {
    design = ReadElementSizeArray(buff);
  } else {
    std::vector<double> initial_design = spec.ObjectMember("initial design")->ArrayAsVectorOfDoubles(design_dof);
    design.Resize(design_dof,I_el,J_el,K_el);
    for(int k=0; k<K_el; ++k)
    for(int j=0; j<J_el; ++j)
    for(int i=0; i<I_el; ++i)
    for(int d=0; d<design_dof; ++d) {
      design(d,i,j,k) = initial_design[d];
    }
  }

  // Run FEM
  Array2d<double> actual;
  ierr =  CalculateHomogenization(design, actual);                                     CHKERRQ(ierr);

  if(spec.ObjectHasMember("expected result")) {
    double max_error = 0;
    for(int i=0; i<6; ++i) {
      std::vector<double> expected = 
        spec.ObjectMember("expected result")->ObjectMember("tensor")->ArrayMember(i)->ArrayAsVectorOfDoubles(6);

      for(int j=0; j<6; ++j) {
        max_error = std::max(max_error, std::abs(actual(i,j) - expected[j]));
      }
    }
    std::cout << spec.ObjectMember("expected result")->ObjectMember("test name")->AsString() << '\n';
    std::cout << "  Max Error: " << max_error << '\n' << std::endl;
  } else {
    for(unsigned i=0; i<actual.GetI(); ++i) {
      for(unsigned j=0; j<actual.GetJ(); ++j) {
        std::cout << actual(i,j) << ", ";
      }
      std::cout << std::endl;
    }
  }

  PetscFunctionReturn(0);
}



// *** main *** //

PetscErrorCode RunProgram(const JSONObject &spec, std::string output_file) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  PetscBool calc_obj, test_prob, do_homog;

  char buff[STR_BUFF_SIZE];
  ierr =  PetscOptionsGetString(NULL,NULL,"-calculate_objective_of",buff,STR_BUFF_SIZE,&calc_obj);
                                                                                       CHKERRQ(ierr);
  ierr =  PetscOptionsHasName(NULL,NULL,"-test_problem",&test_prob);                   CHKERRQ(ierr);
  ierr =  PetscOptionsHasName(NULL,NULL,"-homogenization",&do_homog);                  CHKERRQ(ierr);

  if((calc_obj?1:0) + (test_prob?1:0) + (do_homog?1:0)> 1) {
    std::cout << "Please select one or none of -calculate_objective_of, -test_problem or -homogenization\n";
    PetscFunctionReturn(0);
  }

  if(calc_obj) {
    Array4d<double> design = ReadElementSizeArray(buff);
    Array4d<double> d_design;
    double objective;

    std::vector<double> constraints;
    std::vector<Array4d<double> > d_constraints;

    ierr =  CalculateObjective(-1, false, design, d_design, constraints, d_constraints, objective);
                                                                                       CHKERRQ(ierr);
    std::cout << std::setprecision(15) << objective << std::endl;
    PetscFunctionReturn(0);
  }
  
  if(test_prob) {
    RunTest(spec);
    PetscFunctionReturn(0);
  }

  if(do_homog) {
    ierr =  RunHomogenization(spec);                                                   CHKERRQ(ierr);

    PetscFunctionReturn(0);
  }

  // Default to doing an optimisation
  time_t start = time(NULL);
  ierr =  RunOptimisation(spec,output_file);                                           CHKERRQ(ierr);
  std::cout << "Finished optimisation, took " << time(NULL) - start << std::endl;
  PetscFunctionReturn(0);
}


int main(int argc, char **argv) {
  PetscErrorCode ierr;

  PetscInitialize(&argc, &argv, NULL, help_line);

  //feenableexcept(FE_ALL_EXCEPT & ~FE_INEXACT);
  
  // Load problem spec file
  char buff[STR_BUFF_SIZE];
  PetscBool was_set;
  ierr =  PetscOptionsGetString(NULL,NULL,"-i",buff,STR_BUFF_SIZE,&was_set);           CHKERRQ(ierr);
  if(!was_set) {
    ierr =  PetscPrintf(PETSC_COMM_WORLD, "No problem spec provided, doing nothing\n");CHKERRQ(ierr);
    PetscFinalize();
    return 0;
  }

  std::ifstream file(buff);
  if(!file) {
    ierr =  PetscPrintf(PETSC_COMM_WORLD,"Failed to open problem specification file %s\n",buff);
                                                                                       CHKERRQ(ierr);
    PetscFinalize();
    return 0;
  }
  JSONObject spec;
  try {
    spec = ParseJSON(file,true,std::string(buff));
    file.close();
  } catch(const parse_error &e) {
    ierr =  PetscPrintf(PETSC_COMM_WORLD,"Error parsing problem specification file\n%s\n",e.what());
                                                                                       CHKERRQ(ierr);
    PetscFinalize();
    return 0;
  }
  // Get output file name
  std::string output_file = "";
  ierr =  PetscOptionsGetString(NULL,NULL,"-o",buff,STR_BUFF_SIZE,&was_set);           CHKERRQ(ierr);
  if(was_set) {
    output_file = buff;
  }

  // Initialise FEM and run a program
  bool is_root;
  try {
    ierr =  InitFEMCalc(&spec, is_root);                                               CHKERRQ(ierr);
  } catch(const std::exception &e) {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Caught exception: %s",e.what());
  }

  try {
    if(is_root) {
      ierr =  RunProgram(spec, output_file);                                           CHKERRQ(ierr);
    } else {
      ierr =  WorkerLoop();                                                            CHKERRQ(ierr);
    }
    ierr =  FinalizeFEMCalc();                                                         CHKERRQ(ierr);
  } catch(const std::exception &e) {
    std::cout << "Root caught exception, what: " << e.what() << '\n'
              << "  Quitting" << std::endl;
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Caught exception: %s",e.what());
  }
  PetscFinalize();
  return 0;
}

