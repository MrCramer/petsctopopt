

// Taken from FEM/ 12/4/13

/*

ArrayNd.h

    Header file for a ND array class. Supports copying, assignment and element access.
  Data is on the heap, if DEBUG is defined then then it will check bounds, if not then
  not.


  Andrew Cramer - andrewdalecramer@gmail.com
  University of Queensland.

*/


#ifndef ARRAYND_H_INCLUDED
#define ARRAYND_H_INCLUDED

#include <cstdio>  // To interface with zlib
#include <cstdlib>
#include <cstring> // For error reporting c-libraries

#include <functional>
#include <iomanip>
#include <iostream>
#include <limits>
#include <memory>
#include <stdexcept>
#include <vector>

#include "zlib.h"

/// Generic
template<class FT>
double SumDoubleArray(const FT &array) {
  double sum = 0;
  const std::vector<double> &data = array.GetData();
  for(size_t i = 0; i<data.size(); ++i) {
    sum += data[i];
  }
  return sum;
}


template<class FT>
double AverageDoubleArray(const FT &array) {
  return SumDoubleArray(array) / double(array.GetData().size());
}


/* Opens file file_name and provides a read function. Handles gzip files and 
   plain binary files. Files are closed when returned function goes out of 
   scope. */
inline std::function<void(char*,size_t)> OpenArrayFileForRead(const std::string &file_name) {
  std::unique_ptr<FILE,decltype(&fclose)> file(fopen(file_name.c_str(),"rb"),&fclose);
  if(!file) {
    int err = errno;
    std::cerr << "Couldn't open file " << file_name << " for reading an ArrayNd\n"
              << "  " << std::strerror(err) << std::endl;
    throw(std::runtime_error("Failed to open file for reading ArrayNd"));
  }

  // Peek the first two characters to see if it's zipped or not
  unsigned char buff[2];
  if(fread(buff,1,2,file.get())<2) {
    throw std::runtime_error("Failed while reading ArrayNd file: " + file_name);
  }
  rewind(file.get());

  if(buff[0]==0x1f && buff[1]==0x8b) {
    // 1f8b is the magic number for a gzip file
    std::shared_ptr<void> zfile(gzdopen(fileno(file.get()),"rb"),&gzclose);

    static_assert(Z_NULL==0,"Z_NULL!=0, will cause errors with std::shared_ptr");
    if(!zfile) throw std::runtime_error("gzdopen failed reading an ArrayNd " + file_name);
    file.release(); // gzclose will close the original descriptor
    
    return [zfile,file_name](char *buff, size_t len) {
      if(len>std::numeric_limits<int>::max()) {
        throw std::logic_error("gzfile read function asked to read too large a chunk (" __FILE__ ")");
      }
      do {
        int ret = gzread(zfile.get(),buff,len);
        if(ret== 0) throw std::runtime_error("Unexpected eof while reading gzipped ArrayNd: " + file_name);
        if(ret==-1) throw std::runtime_error("Error while reading gzipped ArrayNd: " + file_name);
        len  -= ret;
        buff += ret;
      } while(len>0);
    };

  } else {
    std::shared_ptr<FILE> file_shared(file.release(),&fclose);

    return [file_shared,file_name](char *buff, size_t len) {
      size_t ret = fread(buff,1,len,file_shared.get());
      if(ret!=len) throw std::runtime_error("Failed while reading binary ArrayNd file: " + file_name);
    };
  }
}

/* Opens file file_name and provides a write function. Handles gzip files and 
   plain binary files. Files are closed when returned function goes out of 
   scope. If zip is true, ".gz" is appended to the file name  */
inline std::function<void(const char*,size_t)> OpenArrayFileForWrite(const std::string &file_name, bool zip) {
  if(zip) {
    std::shared_ptr<void> zfile(gzopen((file_name+".gz").c_str(),"wb"),&gzclose);
    
    static_assert(Z_NULL==0,"Z_NULL!=0, will cause errors with std::shared_ptr");
    if(!zfile) throw std::runtime_error("gzdopen failed opening an Array4d for write: " + file_name);

    return [zfile,file_name](const char *buff, size_t len) {
      if(len>std::numeric_limits<int>::max()) {
        throw std::logic_error("gzfile write function asked to write too large a chunk (" __FILE__ ")");
      }
      do {
        int ret = gzwrite(zfile.get(),buff,len);
        if(ret==0) throw std::runtime_error("Failed while writing to ArrayNd file: " + file_name);
        len  -= ret;
        buff += ret;
      } while(len>0);
    };
  } else {
    std::shared_ptr<FILE> file(fopen(file_name.c_str(),"wb"),&fclose);
    if(!file) {
      throw std::runtime_error("Failed opening " + (file_name + ", ") + std::strerror(errno));
    }

    return [file,file_name](const char *buff, size_t len) {
      size_t ret = fwrite(buff,1,len,file.get());
      if(ret!=len) throw std::runtime_error("Failed while writing binary ArrayNd file: " + file_name);
    };
  }
}


/// 1-D
inline
void OutputArray(const std::vector<double> &array, std::string file_name, bool zip=false) {
  const int INT_SIZE = 4;
  const int DBL_SIZE = 8;
  static_assert(sizeof(unsigned)==INT_SIZE && sizeof(double)==DBL_SIZE,
                "size of a type is unexpected on this platform (" __FILE__ ")");

  std::function<void(const char*,size_t)> write_chunk = OpenArrayFileForWrite(file_name,zip);
  
  unsigned I = array.size();

  write_chunk("BArray1D", 8);
  write_chunk((char*)(&I), INT_SIZE);
  write_chunk((char*)(&array[0]), I*DBL_SIZE);
}

inline
std::vector<double> ReadArray1d(const std::string &file_name) {
  const int INT_SIZE = 4;
  const int DBL_SIZE = 8;
  static_assert(sizeof(unsigned)==INT_SIZE && sizeof(double)==DBL_SIZE,
                "size of a type is unexpected on this platform (" __FILE__ ")");
  
  std::function<void(char*,int)> read_chunk = OpenArrayFileForRead(file_name);

  // Check the first bit to ensure it has the correct identifier
  char buff[9];
  read_chunk(buff,8);
  buff[8] = '\0';

  if(std::string(buff).compare("BArray1D")!=0) {
    throw(std::runtime_error("Cannot read Array1D, bad identifier"));
  }

  unsigned I;
  read_chunk((char*)(&I), INT_SIZE);
  
  std::vector<double> out(I);
  read_chunk((char*)(&out[0]), I*DBL_SIZE);
  return out;
}

inline
std::vector<double> ReadArray1d(const std::string &file_name,size_t I) {
  auto temp = ReadArray1d(file_name);
  if(temp.size()!=I) {
    throw std::runtime_error("1D array of incorrect size, read from file \"" + file_name + "\"");
  }
  return temp;
}

/// 2-D
template<class T>
class Array2d {
  public:
    Array2d()                       { Resize(0,0); }
    Array2d(unsigned I, unsigned J) { Resize(I,J); }

          inline T& operator()(unsigned i, unsigned j);
    const inline T& operator()(unsigned i, unsigned j) const;

    inline void Set(const T &v) {
      for(unsigned i=0; i<I_*J_; i++) data_[i] = v;
    }

    inline void Resize(unsigned I, unsigned J) {
      I_=I; J_=J;
      data_.resize(I*J);
    }

    inline unsigned GetI() const { return I_; }
    inline unsigned GetJ() const { return J_; }

          std::vector<T>& GetData()       { return data_; }
    const std::vector<T>& GetData() const { return data_; }

  private:
    std::vector<T> data_; // Raw Data
    unsigned I_, J_; // Sizes
};

template<class T> inline
T& Array2d<T>::operator()(unsigned i, unsigned j) {
  #ifdef DEBUG
    if(i>=I_ || j>=J_) throw(std::logic_error("subscript out of range in (non-const) Array2d"));
  #endif
  return data_[i + I_*(j)];
}

template<class T> inline
const T& Array2d<T>::operator()(unsigned i, unsigned j) const {
  #ifdef DEBUG
    if(i>=I_ || j>=J_) throw(std::logic_error("subscript out of range in (const) Array2d"));
  #endif
  return data_[i + I_*(j)];
}


inline
void OutputArray(const Array2d<double> &array, std::string file_name, bool zip=false) {
  const int INT_SIZE = 4;
  const int DBL_SIZE = 8;
  static_assert(sizeof(unsigned)==INT_SIZE && sizeof(double)==DBL_SIZE,
                "size of a type is unexpected on this platform (" __FILE__ ")");

  std::function<void(const char*,size_t)> write_chunk = OpenArrayFileForWrite(file_name,zip);
  unsigned I[2] = {array.GetI(),array.GetJ()};

  write_chunk("BArray2D", 8);
  write_chunk((char*)(I), 2*INT_SIZE);
  
  unsigned i, j;
  for(j=0; j<I[1]; j++)
  for(i=0; i<I[0]; i++) {
    double v = array(i,j);
    write_chunk((char*)(&v), DBL_SIZE);
  }
}


inline
Array2d<double> ReadArray2d(const std::string &file_name) {
  const int INT_SIZE = 4;
  const int DBL_SIZE = 8;
  static_assert(sizeof(unsigned)==INT_SIZE && sizeof(double)==DBL_SIZE,
                "size of a type is unexpected on this platform (" __FILE__ ")");

  std::function<void(char*,int)> read_chunk = OpenArrayFileForRead(file_name);
  
  // Check the first bit to ensure it has the correct identifier
  char buff[9];
  read_chunk(buff,8);
  buff[8] = '\0';

  if(std::string(buff).compare("BArray2D")!=0) {
    throw(std::runtime_error("Cannot read Array2D, bad identifier"));
  }

  unsigned I[2], i, j;
  read_chunk((char*)(&I), 2*INT_SIZE);

  Array2d<double> out(I[0],I[1]);
  for(j=0; j<I[1]; ++j)
  for(i=0; i<I[0]; ++i) {
    double v;
    read_chunk((char*)&v, DBL_SIZE);
    out(i,j) = v;
  }

  return out;
}


inline
Array2d<double> ReadArray2d(const std::string &file_name, unsigned I, unsigned J) {
  auto temp = ReadArray2d(file_name);
  if(temp.GetI()!=I || temp.GetJ()!=J) {
    throw std::runtime_error("2D array of incorrect size, read from file \"" + file_name + "\"");
  }
  return temp;
}


inline
double ArrayDot(const Array2d<double> &lhs,
                const Array2d<double> &rhs) {
  unsigned I = lhs.GetI();
  unsigned J = lhs.GetJ();

  if(I!=rhs.GetI()) throw(std::logic_error("ArrayDot array sizes mismatch"));
  if(J!=rhs.GetJ()) throw(std::logic_error("ArrayDot array sizes mismatch"));

  static double result;
  result = 0;
  for(unsigned i=0; i<I; i++)
  for(unsigned j=0; j<J; j++) {
    result += lhs(i,j) * rhs(i,j);
  }
  return result;
}



/// 3-D
template<class T>
class Array3d {
  public:
    Array3d()                    { Resize(0,0,0); }
    Array3d(int I, int J, int K) { Resize(I,J,K); }

          inline T& operator()(unsigned i, unsigned j, unsigned k);
    const inline T& operator()(unsigned i, unsigned j, unsigned k) const;

    inline void Set(const T &v) {
      for(unsigned i=0; i<I_*J_*K_; i++) data_[i] = v;
    }

    inline void Resize(unsigned I, unsigned J, unsigned K) {
      I_=I; J_=J; K_=K;
      data_.resize(I*J*K);
    }

    inline unsigned GetI() const { return I_; }
    inline unsigned GetJ() const { return J_; }
    inline unsigned GetK() const { return K_; }


          std::vector<T>& GetData()       { return data_; }
    const std::vector<T>& GetData() const { return data_; }

  private:
    std::vector<T> data_; // Raw Data
    unsigned I_, J_, K_; // Sizes
};

template<class T> inline
T& Array3d<T>::operator()(unsigned i, unsigned j, unsigned k) {
  #ifdef DEBUG
    if(i>=I_ || j>=J_ || k>=K_) throw(std::logic_error("subscript out of range in (non-const) Array3d"));
  #endif
  return data_[i + I_*(j + J_*k)];
}

template<class T> inline
const T& Array3d<T>::operator()(unsigned i, unsigned j, unsigned k) const {
  #ifdef DEBUG
    if(i>=I_ || j>=J_ || k>=K_) throw(std::logic_error("subscript out of range in (const) Array3d"));
  #endif
  return data_[i + I_*(j + J_*k)];
}


inline
void OutputArray(const Array3d<double> &array, std::string file_name, bool zip=false) {
  const int INT_SIZE = 4;
  const int DBL_SIZE = 8;
  static_assert(sizeof(unsigned)==INT_SIZE && sizeof(double)==DBL_SIZE,
                "size of a type is unexpected on this platform (" __FILE__ ")");
  
  std::function<void(const char*,size_t)> write_chunk = OpenArrayFileForWrite(file_name,zip);

  unsigned I[3] = {array.GetI(),array.GetJ(),array.GetK()};

  write_chunk("BArray3D", 8);
  write_chunk((char*)(I), 3*INT_SIZE);

  unsigned i, j, k;
  for(k=0; k<I[2]; k++)
  for(j=0; j<I[1]; j++)
  for(i=0; i<I[0]; i++) {
    double v = array(i,j,k);
    write_chunk((char*)(&v), DBL_SIZE);
  }
}


inline
Array3d<double> ReadArray3d(const std::string &file_name) {
  const int INT_SIZE = 4;
  const int DBL_SIZE = 8;
  static_assert(sizeof(unsigned)==INT_SIZE && sizeof(double)==DBL_SIZE,
                "size of a type is unexpected on this platform (" __FILE__ ")");
  
  std::function<void(char*,int)> read_chunk = OpenArrayFileForRead(file_name);
  
  char buff[9];
  read_chunk(buff,8);
  buff[8] = '\0';
  if(std::string(buff).compare("BArray3D")!=0) {
    throw std::runtime_error("Array3d (" + file_name + ") is missing correct identifier");
  }

  unsigned I[3],i,j,k;
  read_chunk((char*)(I),3*INT_SIZE);

  Array3d<double> out(I[0],I[1],I[2]);
  for(k=0; k<I[2]; k++)
  for(j=0; j<I[1]; j++)
  for(i=0; i<I[0]; i++) {
    double v;
    read_chunk((char*)(&v),DBL_SIZE);
    out(i,j,k) = v;
  }
  return out;
}


inline
Array3d<double> ReadArray3d(const std::string &file_name, unsigned I, unsigned J, unsigned K) {
  auto temp = ReadArray3d(file_name);
  if(temp.GetI()!=I || temp.GetJ()!=J || temp.GetK()!=K) {
    throw std::runtime_error("3D array of incorrect size, read from file \"" + file_name + "\"");
  }
  return temp;
}


inline
double ArrayDot(const Array3d<double> &lhs,
                const Array3d<double> &rhs) {
  unsigned I = lhs.GetI();
  unsigned J = lhs.GetJ();
  unsigned K = lhs.GetK();

  if(I!=rhs.GetI()) throw(std::logic_error("ArrayDot array sizes mismatch"));
  if(J!=rhs.GetJ()) throw(std::logic_error("ArrayDot array sizes mismatch"));
  if(K!=rhs.GetK()) throw(std::logic_error("ArrayDot array sizes mismatch"));

  static double result;
  result = 0;
  for(unsigned i=0; i<I; i++)
  for(unsigned j=0; j<J; j++)
  for(unsigned k=0; k<K; k++) {
    result += lhs(i,j,k) * rhs(i,j,k);
  }
  return result;
}




/// 4-D
template<class T>
class Array4d {
  public:
    Array4d()                           { Resize(0,0,0,0); }
    Array4d(int I, int J, int K, int L) { Resize(I,J,K,L); }

          inline T& operator()(unsigned i, unsigned j, unsigned k, unsigned l);
    const inline T& operator()(unsigned i, unsigned j, unsigned k, unsigned l) const;

    inline void Set(const T &v) { 
      for(unsigned i=0; i<I_*J_*K_*L_; i++) data_[i] = v;
    }

    inline void Resize(unsigned I, unsigned J, unsigned K, unsigned L) {
      I_=I; J_=J; K_=K; L_=L;
      data_.resize(I*J*K*L);
    }


    inline unsigned GetI() const { return I_; }
    inline unsigned GetJ() const { return J_; }
    inline unsigned GetK() const { return K_; }
    inline unsigned GetL() const { return L_; }

          std::vector<T>& GetData()       { return data_; }
    const std::vector<T>& GetData() const { return data_; }

  private:
    std::vector<T> data_; // Raw Data
    unsigned I_, J_, K_, L_; // Sizes
};

template<class T> inline
T& Array4d<T>::operator()(unsigned i, unsigned j, unsigned k, unsigned l) {
  #ifdef DEBUG
    if(i>=I_ || j>=J_ || k>=K_ || l>=L_) throw(std::logic_error("subscript out of range in (non-const) Array4d"));
  #endif
  return data_[i + I_*(j + J_*(k + K_*l))];
}

template<class T> inline
const T& Array4d<T>::operator()(unsigned i, unsigned j, unsigned k, unsigned l) const {
  #ifdef DEBUG
    if(i>=I_ || j>=J_ || k>=K_ || l>=L_) throw(std::logic_error("subscript out of range in (const) Array4d"));
  #endif
  return data_[i + I_*(j + J_*(k + K_*l))];
}


inline
void OutputArray(const Array4d<double> &array, std::string file_name, bool zip=false) {
  const int INT_SIZE = 4;
  const int DBL_SIZE = 8;
  static_assert(sizeof(unsigned)==INT_SIZE && sizeof(double)==DBL_SIZE,
                "size of a type is unexpected on this platform (" __FILE__ ")");
  
  std::function<void(const char*,size_t)> write_chunk = OpenArrayFileForWrite(file_name,zip);

  unsigned I[4] = {array.GetI(),array.GetJ(),array.GetK(),array.GetL()};

  write_chunk("BArray4D", 8);
  write_chunk((char*)(I), 4*INT_SIZE);

  unsigned i, j, k, l;
  for(l=0; l<I[3]; l++)
  for(k=0; k<I[2]; k++)
  for(j=0; j<I[1]; j++)
  for(i=0; i<I[0]; i++) {
    double v = array(i,j,k,l);
    write_chunk((char*)(&v), DBL_SIZE);
  }
}


inline
Array4d<double> ReadArray4d(const std::string &file_name) {
  const int INT_SIZE = 4;
  const int DBL_SIZE = 8;
  static_assert(sizeof(unsigned)==INT_SIZE && sizeof(double)==DBL_SIZE,
                "size of a type is unexpected on this platform (" __FILE__ ")");
  
  std::function<void(char*,int)> read_chunk = OpenArrayFileForRead(file_name);
  
  char buff[9];
  read_chunk(buff,8);
  buff[8] = '\0';
  if(std::string(buff).compare("BArray4D")!=0) {
    throw std::runtime_error("Array4d (" + file_name + ") is missing correct identifier");
  }

  unsigned I[4],i,j,k,l;
  read_chunk((char*)(I),4*INT_SIZE);

  Array4d<double> out(I[0],I[1],I[2],I[3]);
  for(l=0; l<I[3]; l++)
  for(k=0; k<I[2]; k++)
  for(j=0; j<I[1]; j++)
  for(i=0; i<I[0]; i++) {
    double v;
    read_chunk((char*)(&v),DBL_SIZE);
    out(i,j,k,l) = v;
  }
  return out;
}


inline
Array4d<double> ReadArray4d(const std::string &file_name, unsigned I, unsigned J, unsigned K, unsigned L) {
  auto temp = ReadArray4d(file_name);
  if(temp.GetI()!=I || temp.GetJ()!=J || temp.GetK()!=K || temp.GetL()!=L) {
    throw std::runtime_error("4D array of incorrect size, read from file \"" + file_name + "\"");
  }
  return temp;
}



/// 5-D
template<class T>
class Array5d {
  public:
    Array5d()                                  { Resize(0,0,0,0,0); }
    Array5d(int I, int J, int K, int L, int M) { Resize(I,J,K,L,M); }

          inline T& operator()(unsigned i, unsigned j, unsigned k, unsigned l, unsigned m);
    const inline T& operator()(unsigned i, unsigned j, unsigned k, unsigned l, unsigned m) const;

    inline void Set(const T &v) { 
      for(auto &element : data_) {
        element = v;
      }
    }

    inline void Resize(unsigned I, unsigned J, unsigned K, unsigned L, unsigned M) {
      I_=I; J_=J; K_=K; L_=L; M_=M;
      data_.resize(I*J*K*L*M);
    }


    inline unsigned GetI() const { return I_; }
    inline unsigned GetJ() const { return J_; }
    inline unsigned GetK() const { return K_; }
    inline unsigned GetL() const { return L_; }
    inline unsigned GetM() const { return M_; }

          std::vector<T>& GetData()       { return data_; }
    const std::vector<T>& GetData() const { return data_; }

  private:
    std::vector<T> data_; // Raw Data
    unsigned I_, J_, K_, L_, M_; // Sizes
};

template<class T> inline
T& Array5d<T>::operator()(unsigned i, unsigned j, unsigned k, unsigned l, unsigned m) {
  #ifdef DEBUG
    if(i>=I_ || j>=J_ || k>=K_ || l>=L_ || m>=M_)
      throw(std::logic_error("subscript out of range in (non-const) Array5d"));
  #endif
  return data_[i + I_*(j + J_*(k + K_*(l + L_*m)))];
}

template<class T> inline
const T& Array5d<T>::operator()(unsigned i, unsigned j, unsigned k, unsigned l, unsigned m) const {
  #ifdef DEBUG
    if(i>=I_ || j>=J_ || k>=K_ || l>=L_ || m>=M_)
      throw(std::logic_error("subscript out of range in (non-const) Array5d"));
  #endif
  return data_[i + I_*(j + J_*(k + K_*(l + L_*m)))];
}


inline
void OutputArray(const Array5d<double> &array, std::string file_name, bool zip=false) {
  const int INT_SIZE = 4;
  const int DBL_SIZE = 8;
  static_assert(sizeof(unsigned)==INT_SIZE && sizeof(double)==DBL_SIZE,
                "size of a type is unexpected on this platform (" __FILE__ ")");
  
  std::function<void(const char*,size_t)> write_chunk = OpenArrayFileForWrite(file_name,zip);

  unsigned I[5] = {array.GetI(),array.GetJ(),array.GetK(),array.GetL(),array.GetM()};

  write_chunk("BArray5D", 8);
  write_chunk((char*)(I), 5*INT_SIZE);

  unsigned i,j,k,l,m;
  for(m=0; m<I[4]; m++)
  for(l=0; l<I[3]; l++)
  for(k=0; k<I[2]; k++)
  for(j=0; j<I[1]; j++)
  for(i=0; i<I[0]; i++) {
    double v = array(i,j,k,l,m);
    write_chunk((char*)(&v), DBL_SIZE);
  }
}

inline
Array5d<double> ReadArray5d(const std::string &file_name) {
  const int INT_SIZE = 4;
  const int DBL_SIZE = 8;
  static_assert(sizeof(unsigned)==INT_SIZE && sizeof(double)==DBL_SIZE,
                "size of a type is unexpected on this platform (" __FILE__ ")");
  
  std::function<void(char*,int)> read_chunk = OpenArrayFileForRead(file_name);
  
  char buff[9];
  read_chunk(buff,8);
  buff[8] = '\0';
  if(std::string(buff).compare("BArray5D")!=0) {
    throw std::runtime_error("Array5d (" + file_name + ") is missing correct identifier");
  }

  unsigned I[5],i,j,k,l,m;
  read_chunk((char*)(I),5*INT_SIZE);

  Array5d<double> out(I[0],I[1],I[2],I[3],I[4]);
  for(m=0; m<I[4]; m++)
  for(l=0; l<I[3]; l++)
  for(k=0; k<I[2]; k++)
  for(j=0; j<I[1]; j++)
  for(i=0; i<I[0]; i++) {
    double v;
    read_chunk((char*)(&v),DBL_SIZE);
    out(i,j,k,l,m) = v;
  }
  return out;
}


/// 6-D
template<class T>
class Array6d {
  public:
    Array6d()                                         { Resize(0,0,0,0,0,0); }
    Array6d(int I, int J, int K, int L, int M, int N) { Resize(I,J,K,L,M,N); }

          inline T& operator()(unsigned i, unsigned j, unsigned k, unsigned l, unsigned m, unsigned n);
    const inline T& operator()(unsigned i, unsigned j, unsigned k, unsigned l, unsigned m, unsigned n) const;

    inline void Set(const T &v) { 
      for(auto &element : data_) {
        element = v;
      }
    }

    inline void Resize(unsigned I, unsigned J, unsigned K, unsigned L, unsigned M, unsigned N) {
      I_=I; J_=J; K_=K; L_=L; M_=M; N_=N;
      data_.resize(I*J*K*L*M*N);
    }


    inline unsigned GetI() const { return I_; }
    inline unsigned GetJ() const { return J_; }
    inline unsigned GetK() const { return K_; }
    inline unsigned GetL() const { return L_; }
    inline unsigned GetM() const { return M_; }
    inline unsigned GetN() const { return N_; }

          std::vector<T>& GetData()       { return data_; }
    const std::vector<T>& GetData() const { return data_; }

  private:
    std::vector<T> data_; // Raw Data
    unsigned I_, J_, K_, L_, M_, N_; // Sizes
};

template<class T> inline
T& Array6d<T>::operator()(unsigned i, unsigned j, unsigned k, unsigned l, unsigned m, unsigned n) {
  #ifdef DEBUG
    if(i>=I_ || j>=J_ || k>=K_ || l>=L_ || m>=M_ || n>=N_)
      throw(std::logic_error("subscript out of range in (non-const) Array6d"));
  #endif
  return data_[i + I_*(j + J_*(k + K_*(l + L_*(m + M_*n))))];
}

template<class T> inline
const T& Array6d<T>::operator()(unsigned i, unsigned j, unsigned k, unsigned l, unsigned m, unsigned n) const {
  #ifdef DEBUG
    if(i>=I_ || j>=J_ || k>=K_ || l>=L_ || m>=M_ || n>=N_)
      throw(std::logic_error("subscript out of range in (non-const) Array6d"));
  #endif
  return data_[i + I_*(j + J_*(k + K_*(l + L_*(m + M_*n))))];
}


inline
void OutputArray(const Array6d<double> &array, std::string file_name, bool zip=false) {
  const int INT_SIZE = 4;
  const int DBL_SIZE = 8;
  static_assert(sizeof(unsigned)==INT_SIZE && sizeof(double)==DBL_SIZE,
                "size of a type is unexpected on this platform (" __FILE__ ")");
  
  std::function<void(const char*,size_t)> write_chunk = OpenArrayFileForWrite(file_name,zip);

  unsigned I[6] = {array.GetI(),array.GetJ(),array.GetK(),array.GetL(),array.GetM(),array.GetN()};

  write_chunk("BArray6D", 8);
  write_chunk((char*)(I), 6*INT_SIZE);

  unsigned i,j,k,l,m,n;
  for(n=0; n<I[5]; n++)
  for(m=0; m<I[4]; m++)
  for(l=0; l<I[3]; l++)
  for(k=0; k<I[2]; k++)
  for(j=0; j<I[1]; j++)
  for(i=0; i<I[0]; i++) {
    double v = array(i,j,k,l,m,n);
    write_chunk((char*)(&v), DBL_SIZE);
  }
}

inline
Array6d<double> ReadArray6d(const std::string &file_name) {
  const int INT_SIZE = 4;
  const int DBL_SIZE = 8;
  static_assert(sizeof(unsigned)==INT_SIZE && sizeof(double)==DBL_SIZE,
                "size of a type is unexpected on this platform (" __FILE__ ")");
  
  std::function<void(char*,int)> read_chunk = OpenArrayFileForRead(file_name);
  
  char buff[9];
  read_chunk(buff,8);
  buff[8] = '\0';
  if(std::string(buff).compare("BArray6D")!=0) {
    throw std::runtime_error("Array6d (" + file_name + ") is missing correct identifier");
  }

  unsigned I[6],i,j,k,l,m,n;
  read_chunk((char*)(I),6*INT_SIZE);

  Array6d<double> out(I[0],I[1],I[2],I[3],I[4],I[5]);
  for(n=0; n<I[5]; n++)
  for(m=0; m<I[4]; m++)
  for(l=0; l<I[3]; l++)
  for(k=0; k<I[2]; k++)
  for(j=0; j<I[1]; j++)
  for(i=0; i<I[0]; i++) {
    double v;
    read_chunk((char*)(&v),DBL_SIZE);
    out(i,j,k,l,m,n) = v;
  }
  return out;
}

inline
double ArrayDot(const Array4d<double> &lhs,
                const Array4d<double> &rhs) {
  unsigned I = lhs.GetI();
  unsigned J = lhs.GetJ();
  unsigned K = lhs.GetK();
  unsigned L = lhs.GetL();

  if(I!=rhs.GetI()) throw(std::logic_error("ArrayDot array sizes mismatch"));
  if(J!=rhs.GetJ()) throw(std::logic_error("ArrayDot array sizes mismatch"));
  if(K!=rhs.GetK()) throw(std::logic_error("ArrayDot array sizes mismatch"));
  if(L!=rhs.GetL()) throw(std::logic_error("ArrayDot array sizes mismatch"));

  static double result;
  result = 0;
  for(unsigned i=0; i<I; i++)
  for(unsigned j=0; j<J; j++)
  for(unsigned k=0; k<K; k++)
  for(unsigned l=0; l<L; l++) {
    result += lhs(i,j,k,l) * rhs(i,j,k,l);
  }
  return result;
}

#endif // ARRAYND_H_INCLUDED
