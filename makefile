
PETSC_ARCH=arch-linux-opt


CFLAGS	         = 
FFLAGS	         =
CPPFLAGS         = -MP -MMD -DGIT_REV="\"`git rev-parse HEAD`\"" -Wall -ffast-math
FPPFLAGS         =
MANSEC           = KSP
CLEANFILES       = rhs.vtk solution.vtk
NP               = 1

SOPT= -ksp_type cg \
      -pc_type composite -pc_composite_pcs jacobi,mg -pc_composite_type multiplicative \
      -sub_0_pc_jacobi_abs

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

-include *.d

oes: optimising_elastic_solver
	
.PHONY: tags
tags:
	ctags -R

jsontest: JSON.o JSONMain.o
	-@${CLINKER} -o jsontest JSON.o JSONMain.o ${PETSC_KSP_LIB}

f4d: F4D.o
	g++ -lz -o f4d F4D.o
	cp f4d ~/bin/

testsimp: 
	-@${MPIEXEC} -n ${NUM_PROC}  ./optimising_elastic_solver -i Prob.json ${SOPT}
testsimp2: 
	-@${MPIEXEC} -n ${NUM_PROC}  ./optimising_elastic_solver -i Prob2.json ${SOPT}

test:
	-@${MPIEXEC} -n ${NUM_PROC} ./optimising_elastic_solver -test_problem -i Tests/RectCantileverDeflection.json   ${SOPT} 
	-@${MPIEXEC} -n ${NUM_PROC} ./optimising_elastic_solver -test_problem -i Tests/RectCantileverDeflection2d.json ${SOPT}
	-@${MPIEXEC} -n ${NUM_PROC} ./optimising_elastic_solver -test_problem -i Tests/PeriodicTest.json               ${SOPT}
	-@${MPIEXEC} -n ${NUM_PROC} ./optimising_elastic_solver -test_problem -i Tests/PeriodicTest2d.json             ${SOPT}
	-@${MPIEXEC} -n ${NUM_PROC} ./optimising_elastic_solver -homogenization -i Tests/Homogenization3d.json \
                                                          -r Tests/homo_test.f4d    ${SOPT}
	-@${MPIEXEC} -n ${NUM_PROC} ./optimising_elastic_solver -homogenization -i Tests/Homogenization2d.json \
                                                          -r Tests/homo_test_2d.f4d.gz ${SOPT}
	-@./optimising_elastic_solver -homogenization -i Tests/CubicSplineTest.json \
                                -r Tests/CubicSplineTest.f4d.gz -ksp_type preonly -pc_type lu
	-@python3 Tests/TrilinearInterpTestSetup.py
	-@./optimising_elastic_solver -homogenization -i Tests/TrilinearInterpTest.json \
                                -r Tests/TrilinearInterpTestDesign.f4d.gz -ksp_type preonly -pc_type lu
	-@cd Tests; ./gen_axial_ellipsoidal_test_block.py; cd ..
	-@./optimising_elastic_solver -homogenization -ksp_type preonly -pc_type lu \
    -i Tests/AxisAlignedEllipsoidal.json -r Tests/AxisAlignedEllipsoidal.f4d 
	-@echo "Shear stress test (should be near 1)"
	-@echo "3D"
	-@./optimising_elastic_solver -pc_type lu \
	  -calculate_objective_of Tests/ShearStressTest/Plug.f4d -i Tests/ShearStressTest/Spec.json ${SOPT}
	-@echo "2D"
	-@./optimising_elastic_solver \
	  -calculate_objective_of Tests/ShearStressTest/Plug2D.f4d -i Tests/ShearStressTest/Spec2D.json -pc_type lu
	-@echo ""
	python3 Tests/DerivativeTest/test.py


optimising_elastic_solver: main.o Elastic.o ElasTensors.o Optimisers.o JSON.o \
                           GCMMA/ksgcmain.o GCMMA/ksgcsu07.o GCMMA/ksmaxim07.o GCMMA/ksmaxsu07.o
	-${CLINKER} -lz -o optimising_elastic_solver \
    main.o Elastic.o ElasTensors.o Optimisers.o GCMMA/*.o JSON.o ${PETSC_KSP_LIB}

GCMMA/%.o:
	GCMMA/compile

