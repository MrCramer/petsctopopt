#ifndef FMATRIX
#define FMATRIX

#include<cmath>
#include<stdexcept>
#include<vector>

/*\
 * FMatrix is a full matrix class. Storage is in row major form
\*/

class FMatrix {
  public:
    FMatrix(unsigned M, unsigned N)
      : M_(M), N_(N), elements_(M_*N_, 0.0) { }

    double& operator() (unsigned i, unsigned j)       { return elements_[i*N_ + j]; }
    double  operator() (unsigned i, unsigned j) const { return elements_[i*N_ + j]; }
    
    unsigned GetM() { return M_; }
    unsigned GetN() { return N_; }

    // Multiply by a vector
    void Mult(const std::vector<double> &u, std::vector<double> &result) const {
      if(&result==&u)  throw(std::logic_error("FMatrix::Mult- u and result are the same vector"));
      if(u.size()!=N_) throw(std::logic_error("FMatrix::Mult- u incorrect size"));
      result.resize(M_);

      for(unsigned i=0; i<M_; ++i) {
        double sum = 0.0;
        for(unsigned j=0; j<N_; ++j) {
          sum += elements_[i*N_ + j] * u[j];
        }
        result[i] = sum;
      }
    }

    // Transpose and multiply by a vector
    void TMult(const std::vector<double> &u, std::vector<double> &result) const {
      if(&result==&u)  throw(std::logic_error("FMatrix::TMult- u and result are the same vector"));
      if(u.size()!=M_) throw(std::logic_error("FMatrix::TMult- u incorrect size"));
      result.clear();
      result.resize(N_, 0.0);

      for(unsigned i=0; i<M_; ++i) {
        for(unsigned j=0; j<N_; ++j) {
          result[j] += elements_[i*N_ + j] * u[i];
        }
      }
    }

    // Solve a linear system
    void Solve(const std::vector<double> &u, std::vector<double> &result) const {
      if(&result==&u)  throw(std::logic_error("FMatrix::Solve- u and result are the same vector"));
      if(N_!=M_)       throw(std::logic_error("FMatrix::Solve- Not a square matrix"));
      if(u.size()!=N_) throw(std::logic_error("FMatrix::Solve- u incorrect size"));
      
      // Store the pivots. Don't actually pivot, that would be crazy.
      std::vector<unsigned> pivot;
      pivot.resize(N_);
      for(unsigned i=0; i<N_; ++i)
        pivot[i] = i;

      #define INDEX(i,j) (pivot[i]*N_ + j)

      // Make a local copy of the matrix so we can mash it up.
      std::vector<double> elements = elements_;
      std::vector<double> rhs = u;

      for(unsigned r=0; r<N_; ++r) {
        // Find the best row to use
        unsigned best_row = r;
        double   best_val = std::abs(elements[INDEX(r,r)]);
        for(unsigned i=r+1; i<N_; ++i) {
          double new_val = std::abs(elements[INDEX(i,r)]);
          if(new_val > best_val) {
            best_val = new_val;
            best_row = i;
          }
        }

        // Pivot active to that row
        std::swap(pivot[r], pivot[best_row]);

        // Eliminate rows below
        for(unsigned i=r+1; i<N_; ++i) {
          double factor = elements[INDEX(i,r)]/elements[INDEX(r,r)];
          elements[INDEX(i,r)] = 0.0;
          for(unsigned j=r+1; j<N_; ++j) {
            elements[INDEX(i,j)] -= factor*elements[INDEX(r,j)];
          }
          rhs[pivot[i]] -= factor*rhs[pivot[r]];
        }
      }

      // Backsolve
      result.resize(N_);
      for(unsigned i=N_-1; i!=unsigned(-1); --i) {
        double factor = 1.0/elements[INDEX(i,i)];
        result[i] = factor*rhs[pivot[i]];
        for(unsigned j=i+1; j<N_; ++j) {
          result[i] -= factor*elements[INDEX(i,j)]*result[j];
        }
      }
    }


  private:
    unsigned M_, N_;
    std::vector<double> elements_;
};


#endif
