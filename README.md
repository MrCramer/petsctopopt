# Optimising Elastic Solver #

The code from Andrew Cramer's PhD. It solves elastic problems
and optimises using material distribution methods, such as SIMP.

## Set Up ##

### Prerequisites ###

This program requires python3 with numpy for testing, and python2 for 
configuring PETSc. You can get an all in one python with scipy with 
[Anaconda](https://www.continuum.io/why-anaconda).

It also requires MPI for parallel processing. MPI can either be installed when
installing PETSc (see the documentation for PETSc for details) or
alternatively, [OpenMPI](https://www.open-mpi.org) is free and can be installed
into a local directory. When you install it configure with as so: `./configure
--prefix="/path/to/install"`.

Lastly PETSc is required, see instructions below.


### Getting PETSc ###

Make sure you have a copy of MPI installed somewhere. This is important if you
want to run code parallel. Alternatively you can download it as part of the
configure step. Be aware that it will be downloaded each time PETSc is configured.

Get Petsc from [here](https://www.mcs.anl.gov/petsc/download/index.html).  I
usually get the lite version without documentation as the documentation can be
found online.  Currently the code runs on 3.7.2, but any 3.7.x should work
fine.

Extract the archive to it's final resting place, I just have it in
`~/petsc-3.7.2/` but it doesn't really matter.  You need to set the environment
variable `PETSC_DIR=~/petsc-3.7.2` as appropriate, remember to put this in your
`.bashrc` as well!

Enter the PETSc directory and configure with something like one of the
following:

  * `python2 configure --CXXFLAGS="-std=c++11" --PETSC_ARCH=arch-linux-debug`
  * `python2 configure --CXXFLAGS="-std=c++11" COPTFLAGS="-O3 -march=native" CXXOPTFLAGS="-O3" --PETSC_ARCH=arch-linux-opt --with-debugging=no`

It's very important that you have the `-std=c++11` flag set as the code makes
use of C++11 features. If using a compiler other than GCC or llvm, set flags
appropriately.

If it's configured correctly run make as it suggests with the variables and
all.


### Compiling ###

If you installed PETSc as above and have the environment variable `PETSC_DIR`
set then compilation should be straight forward.

 * Modify the makefile to reflect which `PETSC_ARCH` you want to use, by default
   it's `arch-linux-opt`. This is the same string as you supplied to the PETSc
   configuration script for `PETSC_ARCH`.
 * run `make optimising_elastic_solver` or `make oes`
 * you can run `make NUM_PROC=4 test` if you like and it *should* run happily.
 * running `make f4d` will add a utility `f4d` to `~/bin/`. It's a command line
   program that allows you to quickly peak inside the array files.


## Running ##

The executable is `optimising_elastic_solver`, as the tests in the makefile
show, you need to supply it with a few options. The main option is `-i` which
provides an input file for the elastic problem. There is a description of what
these files should contain in `ConfigMain.txt`. These configuration files are 
in JSON format and specify things such as domains, materials, optimisation 
options and
such.

The best way to see how to run things is to look at the makefile and look at
the test examples. As it is a PETSc program there are quite a few options that
can be used related to how PETSc solves the stiffness matrix. Using the GAMG or
multigrid preconditioners is highly recommended.

## Licensing ##

This repo contains a modified version of Krister Svanberg's GCMMA implementation.
Users should get permission from him for its use by emailing him at
`krille@math.kth.se`. His code is confined to `GCMMA/`.

## Quirks of the Code ##

Firstly, being a PETSc program, almost all
functions return a `PetscErrorCode` and return with the macro
`PetscFunctionReturnUser(0)`. When calling such functions another macro
`CHECKERRQ` should always be used to allow propogation of errors up the stack.
PETSc is a C library and is written for multiprocess use, the error handling
system allows intelligent reporting when the error could come from one of a
number of processes, and gives a stack trace which is very useful. Some
exceptions are used where needed (such as in constructors) or where the error
is obvious and using the PETSc's scheme would be onerous, such as when
accessing the JSON configuration file. These exceptions are cought in `main`,
and the program attempts to quit cleanly.

I wrote a JSON parser because at the time I couldn't find a good one that could
traverse the object tree easily. Each object stores the name of all it's parent
objects so that errors can be more helpful. This is expensive and heavy so
large JSON files shouldn't be used, although it is handy for when you forgot to
put something in a config file.

I wrote my own multi-dimensional array classes. This is a hold over from the
very first versions of the code which didn't use PETSc and were written for GPU
processing. These arrays can be dumped to disk in their own format.
`WriteFieldNd.m` and `ReadFieldNd.m` allow matlab to read and write these
files. I would now recommend the use of HDF5 or another well known format. The
f4d utility provides a way to get information about the 4D version of these
files from the command line.

Unit testing numerical codes is difficult and so only validation testing is
performed. Most tests are problems with analytical solutions or with good
analytical approximations such as Euler-Bernoulli beam theory. Derivatives of
functions are tested with finite differences, some of these can be quite off,
especially in the case of interface function (like the shear stress objective)
when the point chosen to "wiggle" is too far from the interface.

## Licensing ##

This repo contains a modified version of Krister Svanberg's GCMMA implementation.
Users should get permission from him for its use by emailing him at
`krille@math.kth.se`. His code is confined to `GCMMA/`.

The rest of the project is covered under the MIT license:

Copyright (c) 2017 Andrew Cramer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.