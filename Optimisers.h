#ifndef OPTIMISERS_H
#define OPTIMISERS_H

#include <vector>
#include <memory>

#include <petscsys.h>

#include "ArrayNd.h"
#include "JSON.h"


class OptFuncBase {
  public:
    virtual PetscErrorCode Step(const int iteration,
                                const JSONObject &spec,
                                const Array4d<char> &fixed_design,
                                const Array4d<double> &old_design,
                                      Array4d<double> &new_design,
                                double &objective,
                                std::vector<double> &constriants) = 0;

    virtual ~OptFuncBase() { };
};


std::unique_ptr<OptFuncBase> GetOptFunction(const JSONObject &spec);


#endif
