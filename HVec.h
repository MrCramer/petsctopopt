#ifndef DVEC_H
#define DVEC_H


#include<cmath>
#include<stdexcept>
#include<vector>


typedef std::vector<double> HVec;



#define SIZEASSERT(u,v,name) if(u.size()!=v.size()) throw(std::logic_error("Size error in " #name));


// Element wise Unary Operations //
inline
HVec Abs(const HVec &u) {
  HVec output(u.size());
  for(unsigned i=0; i<u.size(); ++i)
    output[i] = std::abs(u[i]);
  return output;
}


// Element wise Scalar Vector Operators
inline
HVec operator*(const double a, const HVec &u) {
  HVec output(u.size());
  for(unsigned i=0; i<u.size(); ++i)
    output[i] = a*u[i];
  return output;
}


inline
HVec operator/(const double a, const HVec &u) {
  HVec output(u.size());
  for(unsigned i=0; i<u.size(); ++i)
    output[i] = a/u[i];
  return output;
}


inline
HVec operator/(const HVec &u, const double a) {
  HVec output(u.size());
  for(unsigned i=0; i<u.size(); ++i)
    output[i] = u[i]/a;
  return output;
}


inline
HVec Max(const double a, const HVec &u) {
  HVec output(u.size());
  for(unsigned i=0; i<u.size(); ++i)
    output[i] = std::max(a, u[i]);
  return output;
}


inline
HVec Min(const double a, const HVec &u) {
  HVec output(u.size());
  for(unsigned i=0; i<u.size(); ++i)
    output[i] = std::min(a, u[i]);
  return output;
}


// Element wise Vector Vector Operators
inline
void HVectAdd(const HVec &u, const HVec &v, HVec &result) {
  SIZEASSERT(u,v, HVectAdd)
  result.resize(u.size());
  for(unsigned i=0; i<u.size(); ++i) {
    result[i] = u[i] + v[i];
  }
}


inline
HVec operator+(const HVec &u, const HVec &v) {
  SIZEASSERT(u,v,operator_minus)

  HVec output(u.size());
  for(unsigned i=0; i<u.size(); ++i)
    output[i] = u[i]+v[i];
  return output;
}


inline void HVectMinus(const HVec &u, const HVec &v, HVec &result) {
  SIZEASSERT(u,v, HVectAdd)
  result.resize(u.size());
  for(unsigned i=0; i<u.size(); ++i) {
    result[i] = u[i] + v[i];
  }
}


inline
HVec operator-(const HVec &u, const HVec &v) {
  SIZEASSERT(u,v,operator_minus)

  HVec output(u.size());
  for(unsigned i=0; i<u.size(); ++i)
    output[i] = u[i]-v[i];
  return output;
}


inline
HVec operator*(const HVec &u, const HVec &v) {
  SIZEASSERT(u,v,operator_mult)
  HVec output(u.size());
  for(unsigned i=0; i<u.size(); ++i)
    output[i] = u[i]*v[i];
  return output;
}


inline
HVec operator/(const HVec &u, const HVec &v) {
  SIZEASSERT(u,v,operator_divide)
  HVec output(u.size());
  for(unsigned i=0; i<u.size(); ++i)
    output[i] = u[i]/v[i];
  return output;
}


inline
HVec Max(const HVec &u, const HVec &v) {
  HVec output(u.size());
  for(unsigned i=0; i<u.size(); ++i)
    output[i] = std::max(u[i], v[i]);
  return output;
}


inline
HVec Min(const HVec &u, const HVec &v) {
  HVec output(u.size());
  for(unsigned i=0; i<u.size(); ++i)
    output[i] = std::min(u[i], v[i]);
  return output;
}


// Reductions
inline
double Dot(const HVec &u, const HVec &v) {
  double result = 0;
  for(unsigned i=0; i<u.size(); ++i)
    result += u[i]*v[i];
  return result;
}


inline
double Norm(const HVec &u) {
  return std::sqrt(Dot(u,u));
}


inline
double Max(const HVec &u) {
  if(!u.empty()) {
    double result = u[0];
    for(unsigned i=1; i<u.size(); ++i) {
      result = std::max(result, u[i]);
    }
    return result;
  } else {
    throw(std::logic_error("Max: u is an empty vector"));
  }
}


inline
double Min(const HVec &u) {
  if(!u.empty()) {
    double result = u[0];
    for(unsigned i=1; i<u.size(); ++i) {
      result = std::max(result, u[i]);
    }
    return result;
  } else {
    throw(std::logic_error("Min: u is an empty vector"));
  }
}


inline
double Sum(const HVec &u) {
  double result = 0.0;
  for(unsigned i=0; i<u.size(); ++i) {
    result += u[i];
  }
  return result;
}


#endif
