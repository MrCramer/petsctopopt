
#include<cmath>
#include<algorithm>
#include<fstream>
#include<iostream>
#include<limits>
#include<string>
#include<vector>

#include<sys/ioctl.h>

#include "ArrayNd.h"


bool SizeCheck(const std::vector<std::string> &args, unsigned num, const char* name) {
  if(args.size() < num) {
    std::cerr << "Not enough arguments for " << name << std::endl;
    return false;
  }
  return true;
}


namespace genus_calc {
  void GetNeighbours(const Array3d<char> &voxels, unsigned i, unsigned j, unsigned k, bool local_voxels[2][2][2]) {
    if(i==0) {
      local_voxels[0][0][0] = false;
      local_voxels[0][0][1] = false;
      local_voxels[0][1][0] = false;
      local_voxels[0][1][1] = false;
    } else {
      if(j==0) {
        local_voxels[0][0][0] = false;
        local_voxels[0][0][1] = false;
      } else {
        if(k==0) {
          local_voxels[0][0][0] = false;
        } else {
          local_voxels[0][0][0] = voxels(i-1,j-1,k-1);
        }
        if(k==voxels.GetK()) {
          local_voxels[0][0][1] = false;
        } else {
          local_voxels[0][0][1] = voxels(i-1,j-1,k);
        }
      }
      if(j==voxels.GetJ()) {
        local_voxels[0][1][0] = false;
        local_voxels[0][1][1] = false;
      } else {
        if(k==0) {
          local_voxels[0][1][0] = false;
        } else {
          local_voxels[0][1][0] = voxels(i-1,j,k-1);
        }
        if(k==voxels.GetK()) {
          local_voxels[0][1][1] = false;
        } else {
          local_voxels[0][1][1] = voxels(i-1,j,k);
        }
      }
    }
    if(i==voxels.GetI()) {
      local_voxels[1][0][0] = false;
      local_voxels[1][0][1] = false;
      local_voxels[1][1][0] = false;
      local_voxels[1][1][1] = false;
    } else {
      if(j==0) {
        local_voxels[1][0][0] = false;
        local_voxels[1][0][1] = false;
      } else {
        if(k==0) {
          local_voxels[1][0][0] = false;
        } else {
          local_voxels[1][0][0] = voxels(i,j-1,k-1);
        }
        if(k==voxels.GetK()) {
          local_voxels[1][0][1] = false;
        } else {
          local_voxels[1][0][1] = voxels(i,j-1,k);
        }
      }
      if(j==voxels.GetJ()) {
        local_voxels[1][1][0] = false;
        local_voxels[1][1][1] = false;
      } else {
        if(k==0) {
          local_voxels[1][1][0] = false;
        } else {
          local_voxels[1][1][0] = voxels(i,j,k-1);
        }
        if(k==voxels.GetK()) {
          local_voxels[1][1][1] = false;
        } else {
          local_voxels[1][1][1] = voxels(i,j,k);
        }
      }
    }
  }

  void AssignComponent(const Array3d<char> &voxels,
                       unsigned i0, unsigned j0, unsigned k0, short component,
                       Array3d<short> &components) {
    struct triple {
      unsigned v[3];
    };

    std::vector<triple> to_check(1,triple({i0,j0,k0}));
    while(!to_check.empty()) {
      unsigned i = to_check.back().v[0];
      unsigned j = to_check.back().v[1];
      unsigned k = to_check.back().v[2];
      to_check.pop_back();
      if(components(i,j,k)!=-1) continue;
      components(i,j,k) = component;

      bool local_voxels[2][2][2];
      GetNeighbours(voxels,i,j,k,local_voxels);
      
      int num_solids;
      // -i
      num_solids = local_voxels[0][0][0]
                 + local_voxels[0][0][1]
                 + local_voxels[0][1][0]
                 + local_voxels[0][1][1];
      if(num_solids!=0 && num_solids!=4) {
        to_check.push_back(triple({i-1,j,k}));
      }
      // +i
      num_solids = local_voxels[1][0][0]
                 + local_voxels[1][0][1]
                 + local_voxels[1][1][0]
                 + local_voxels[1][1][1];
      if(num_solids!=0 && num_solids!=4) {
        to_check.push_back(triple({i+1,j,k}));
      }
      // -j
      num_solids = local_voxels[0][0][0]
                 + local_voxels[0][0][1]
                 + local_voxels[1][0][0]
                 + local_voxels[1][0][1];
      if(num_solids!=0 && num_solids!=4) {
        to_check.push_back(triple({i,j-1,k}));
      }
      // +j
      num_solids = local_voxels[0][1][0]
                 + local_voxels[0][1][1]
                 + local_voxels[1][1][0]
                 + local_voxels[1][1][1];
      if(num_solids!=0 && num_solids!=4) {
        to_check.push_back(triple({i,j+1,k}));
      }
      // -k
      num_solids = local_voxels[0][0][0]
                 + local_voxels[0][1][0]
                 + local_voxels[1][0][0]
                 + local_voxels[1][1][0];
      if(num_solids!=0 && num_solids!=4) {
        to_check.push_back(triple({i,j,k-1}));
      }
      // -k
      num_solids = local_voxels[0][0][1]
                 + local_voxels[0][1][1]
                 + local_voxels[1][0][1]
                 + local_voxels[1][1][1];
      if(num_solids!=0 && num_solids!=4) {
        to_check.push_back(triple({i,j,k+1}));
      }
    }
  }

  int AssignComponents(const Array3d<char> &voxels, Array3d<short> &components) {
    int num_components = 0;
    components.Resize(voxels.GetI()+1,voxels.GetJ()+1,voxels.GetK()+1);
    components.Set(-1);

    for(unsigned i=0; i<=voxels.GetI(); ++i)
    for(unsigned j=0; j<=voxels.GetJ(); ++j)
    for(unsigned k=0; k<=voxels.GetK(); ++k) {
      if(components(i,j,k)!=-1) continue;

      bool local_voxels[2][2][2];
      GetNeighbours(voxels,i,j,k,local_voxels);
      int N_solids = local_voxels[0][0][0] + local_voxels [0][0][1]
                   + local_voxels[0][1][0] + local_voxels [0][1][1]
                   + local_voxels[1][0][0] + local_voxels [1][0][1]
                   + local_voxels[1][1][0] + local_voxels [1][1][1];
               
      if(N_solids!=0 && N_solids!=8) {
        AssignComponent(voxels,i,j,k,num_components++,components);
      }
    }
    return num_components;
  }

  int CountEdges(const bool local_voxels[2][2][2]) {
    int num_solids;
    int edges = 0;
    // -i
    num_solids = local_voxels[0][0][0]
               + local_voxels[0][0][1]
               + local_voxels[0][1][0]
               + local_voxels[0][1][1];
    edges += (num_solids!=0 && num_solids!=4) ? 1 : 0;
    // +i
    num_solids = local_voxels[1][0][0]
               + local_voxels[1][0][1]
               + local_voxels[1][1][0]
               + local_voxels[1][1][1];
    edges += (num_solids!=0 && num_solids!=4) ? 1 : 0;
    // -j
    num_solids = local_voxels[0][0][0]
               + local_voxels[0][0][1]
               + local_voxels[1][0][0]
               + local_voxels[1][0][1];
    edges += (num_solids!=0 && num_solids!=4) ? 1 : 0;
    // +j
    num_solids = local_voxels[0][1][0]
               + local_voxels[0][1][1]
               + local_voxels[1][1][0]
               + local_voxels[1][1][1];
    edges += (num_solids!=0 && num_solids!=4) ? 1 : 0;
    // -k
    num_solids = local_voxels[0][0][0]
               + local_voxels[0][1][0]
               + local_voxels[1][0][0]
               + local_voxels[1][1][0];
    edges += (num_solids!=0 && num_solids!=4) ? 1 : 0;
    // -k
    num_solids = local_voxels[0][0][1]
               + local_voxels[0][1][1]
               + local_voxels[1][0][1]
               + local_voxels[1][1][1];
    edges += (num_solids!=0 && num_solids!=4) ? 1 : 0;
    return edges;
  }

  int CountFaces(const bool local_voxels[2][2][2]) {
    int faces = 0;
    if(local_voxels[0][0][0]!=local_voxels[0][0][1]) faces += 1;
    if(local_voxels[0][1][0]!=local_voxels[0][1][1]) faces += 1;
    if(local_voxels[1][0][0]!=local_voxels[1][0][1]) faces += 1;
    if(local_voxels[1][1][0]!=local_voxels[1][1][1]) faces += 1;

    if(local_voxels[0][0][0]!=local_voxels[0][1][0]) faces += 1;
    if(local_voxels[0][0][1]!=local_voxels[0][1][1]) faces += 1;
    if(local_voxels[1][0][0]!=local_voxels[1][1][0]) faces += 1;
    if(local_voxels[1][0][1]!=local_voxels[1][1][1]) faces += 1;

    if(local_voxels[0][0][0]!=local_voxels[1][0][0]) faces += 1;
    if(local_voxels[0][0][1]!=local_voxels[1][0][1]) faces += 1;
    if(local_voxels[0][1][0]!=local_voxels[1][1][0]) faces += 1;
    if(local_voxels[0][1][1]!=local_voxels[1][1][1]) faces += 1;
    return faces;
  }

  void CalculateGenus(const std::vector<std::string> args) {
    if(!SizeCheck(args, 3, "genus")) return;
    
    double level = 0.5;
    if(args.size()>3) level = stod(args[3]);

    Array4d<double> R = ReadArray4d(args[2]);

    if(R.GetI()!=1) {
      std::cerr << "Array has larger than one first dimension, cannot calculate genus" << std::endl;
      return;
    }

    Array3d<char> voxels(R.GetJ(),R.GetK(),R.GetL());
    for(unsigned i=0; i<R.GetJ(); ++i)
    for(unsigned j=0; j<R.GetK(); ++j)
    for(unsigned k=0; k<R.GetL(); ++k) {
      voxels(i,j,k) = R(0,i,j,k) > level;
    }
    
    Array3d<short> components;
    int num_components = AssignComponents(voxels,components);
    
    std::vector<int> num_edges(num_components,0);
    std::vector<int> num_nodes(num_components,0);
    std::vector<int> num_faces(num_components,0);

    // i,j,k are nodal indices
    for(unsigned i=0; i<=voxels.GetI(); ++i)
    for(unsigned j=0; j<=voxels.GetJ(); ++j)
    for(unsigned k=0; k<=voxels.GetK(); ++k) {
      bool local_voxels[2][2][2];
      GetNeighbours(voxels,i,j,k,local_voxels);
      int local_num_edges = CountEdges(local_voxels);
      if(local_num_edges>0) {
        short component = components(i,j,k);
        num_nodes[component] += 1;
        num_edges[component] += local_num_edges;
        num_faces[component] += CountFaces(local_voxels);
      }
    }
    std::vector<int> genus(num_components);
    for(int i=0; i<num_components; ++i) {
      // Edges will be counted twice while faces will be counted 4 times
      int euler_characteristic =  num_nodes[i] - num_edges[i]/2 + num_faces[i]/4;
      genus[i] = 1-euler_characteristic/2;
    }
    std::sort(genus.begin(),genus.end());
    for(auto g : genus) std::cout << g << " ";
    std::cout << std::endl;
  }
}


void VerbAverage(std::vector<std::string> &args) {
  if(!SizeCheck(args, 3, "average")) return;
  Array4d<double> u = ReadArray4d(args[2]);
  std::cout << AverageDoubleArray(u) << std::endl;
}


void VerbMin(std::vector<std::string> &args) {
  if(!SizeCheck(args, 3, "min")) return;
  Array4d<double> u = ReadArray4d(args[2]);
  double m = std::numeric_limits<double>::infinity();
  for(const auto &a : u.GetData()) {
    m = std::min(m,a);
  }
  std::cout << m << std::endl;
}


void VerbMax(std::vector<std::string> &args) {
  if(!SizeCheck(args, 3, "max")) return;
  Array4d<double> u = ReadArray4d(args[2]);
  double m = -std::numeric_limits<double>::infinity();
  for(const auto &a : u.GetData()) {
    m = std::max(m,a);
  }
  std::cout << m << std::endl;
}


void VerbDot(std::vector<std::string> &args) {
  if(!SizeCheck(args, 4, "dot")) return;

  Array4d<double> u = ReadArray4d(args[2]);
  Array4d<double> v = ReadArray4d(args[3]);

  if(u.GetI()!=v.GetI() || u.GetJ()!=v.GetJ() || u.GetK()!=v.GetK() || u.GetL()!=v.GetL()) {
    std::cerr << "Arrays are of different sizes" << std::endl;
    return;
  }
  
  std::vector<double>& u_data = u.GetData();
  std::vector<double>& v_data = v.GetData();
  double value = 0;
  for(unsigned i=0; i<u_data.size(); ++i) {
    value += u_data[i]*v_data[i];
  }

  std::cout << value << std::endl;
}


void VerbDouble(std::vector<std::string> &args) {
  if(!SizeCheck(args, 4, "double")) return;

  Array4d<double> u = ReadArray4d(args[2]);
  unsigned I = u.GetI();
  unsigned J = u.GetJ();
  unsigned K = u.GetK();
  unsigned L = u.GetL();
  
  Array4d<double> v(I, J*2, K*2, L*2);
  for(unsigned i=0; i<I; ++i)
  for(unsigned j=0; j<J; ++j)
  for(unsigned k=0; k<K; ++k)
  for(unsigned l=0; l<L; ++l) {
    double val = u(i,j,k,l);
    for(unsigned m=0; m<2; ++m)
    for(unsigned n=0; n<2; ++n)
    for(unsigned o=0; o<2; ++o)
      v(i, j*2+m, k*2+n, l*2+o) = val;
  }
  OutputArray(v, args[3]);
}


void VerbTriple(std::vector<std::string> &args) {
  if(!SizeCheck(args, 4, "triple")) return;

  Array4d<double> u = ReadArray4d(args[2]);
  unsigned I = u.GetI();
  unsigned J = u.GetJ();
  unsigned K = u.GetK();
  unsigned L = u.GetL();
  
  
  Array4d<double> v(I, J*2, K*2, L*2);
  for(unsigned i=0; i<I; ++i)
  for(unsigned j=0; j<J; ++j)
  for(unsigned k=0; k<K; ++k)
  for(unsigned l=0; l<L; ++l) {
    double val = u(i,j,k,l);
    for(unsigned m=0; m<3; ++m)
    for(unsigned n=0; n<3; ++n)
    for(unsigned o=0; o<3; ++o)
      v(i, j*3+m, k*3+n, l*3+o) = val;
  }
  OutputArray(v, args[3]);
}


void VerbDraw(std::vector<std::string> &args) {
  if(!SizeCheck(args, 3, "draw")) return;

  const char *scale = " .-+:*=%#@";
  double scale_min = 0.0;
  double scale_max = 1.0;

  if(args.size() >= 6) {
    scale_min = atof(args[4].c_str());
    scale_max = atof(args[5].c_str());
  }

  Array4d<double> u = ReadArray4d(args[2]);
  unsigned I = u.GetI();
  unsigned J = u.GetJ();
  unsigned K = u.GetK();
  unsigned L = u.GetL();

  unsigned design_var_index;
  if(args.size()<=3) {
    if(I==1) {
      design_var_index = 0;
    } else {
      std::cerr << args[2] << " has " << I << " (>1) design variables, please provide an index" << std::endl;
      return;
    }
  } else {
    design_var_index = atoi(args[3].c_str());
    if(design_var_index>=I) {
      std::cerr << "Asked to draw design variable " << design_var_index
                << " but " << args[2] << " only has " << I << " available." << std::endl;
      return;
    }
  }

  // Figure out the scale so it all fits in the width of the terminal
  winsize w;
  ioctl(0, TIOCGWINSZ, &w);
  unsigned cols = w.ws_col;
  unsigned x_scale = (J+ cols-2 -1)/(cols-2); // (Ceil Div) Combine this many cells in the x direction
  unsigned y_scale = x_scale * 2;


  // Draw loop
  for(unsigned k=0; k<K; k+=y_scale) {
    // About half way up put the y axis label
    if(k/y_scale==K/2/y_scale) {
      std::cout << "Y ";
    } else {
      std::cout << "  ";
    }
    // Draw the data line
    for(unsigned j=0; j<J; j+=x_scale) {
      double sum = 0;
      // Average the value
      for(unsigned jj=0;jj<x_scale; ++jj)
      for(unsigned kk=0;kk<y_scale; ++kk) {
        unsigned this_j = std::min(J-1,        j+jj  );
        int      this_k = std::max(0, int(K-1-(k+kk)));
        for(unsigned l=0; l<L; ++l) {
          sum += u(design_var_index,this_j,this_k,l);
        }
      }
      double avg = sum/(L*x_scale*y_scale);
      double val = (avg-scale_min)/(scale_max-scale_min);
      int level = int(std::max(0.0, std::min(9.0, 10.0*val)));
      std::cout << scale[level];
    }
    std::cout << '\n';
  }

  std::cout << "\n  ";
  for(unsigned i=0; i<J/x_scale/2; ++i) std::cout << ' ';
  std::cout << "X\n\n";

  std::cout << "Scale: |" << scale << "|\n"
            << std::setprecision(1) << std::fixed
            << "      " << scale_min << "        " << scale_max << '\n';
}


std::vector<double> ReadPixLine(const std::string &line) {
  std::vector<double> out;
  for(size_t p=0; p<line.length(); p+=2) {
    if(line[p]=='0') {
      out.push_back(0.0);
    } else if(line[p]=='1') {
      out.push_back(1.0);
    } else {
      throw(std::runtime_error("Found a char in a pix file not a 1 or 0"));
    }
  }
  return out;
}


void VerbPix2F4d(std::vector<std::string> &args) {
  if(!SizeCheck(args, 4, "pix2f3d")) return;

  std::ifstream file(args[2].c_str());
  std::string line;
  std::vector<std::vector<double> > data;
  while(std::getline(file,line)) {
    if(line == "") continue;
    data.push_back(ReadPixLine(line));
  }
  unsigned I,J,K;
  if(args.size() > 4) {
    if(args.size() < 7) {
      std::cerr << "f3d pix2f3d requires either 3 or 6 arguments\n";
      return;
    }
    I = atoi(args[4].c_str());
    J = atoi(args[5].c_str());
    K = atoi(args[6].c_str());
  } else {
    I = data[0].size();
    J = I;
    K = I;
  }

  if(data.size() != J*K || data[0].size() != I) {
    std::cerr << "Unexpected shape of data in pix file\n";
    return;
  }
  Array4d<double> out(1,I,J,K);
  for(unsigned k=0; k<K; ++k)
  for(unsigned j=0; j<J; ++j)
  for(unsigned i=0; i<I; ++i) {
    out(0,i,j,k) = data.at(j+k*J).at(i);
  }
  OutputArray(out, args[3]);
}


void VerbSharpen(std::vector<std::string> &args) {
  if(!SizeCheck(args, 4, "Sharpen")) return;

  Array4d<double> u = ReadArray4d(args[2]);

  double r_min = 0.0;
  double r_max = 1.0;

  if(args.size() >= 5) {
    r_min = atof(args[4].c_str());
  }
  if(args.size() >=6) {
    r_max = atof(args[5].c_str());
  }

  unsigned D = u.GetI();
  unsigned I = u.GetJ();
  unsigned J = u.GetK();
  unsigned K = u.GetL();

  if(D!=1) {
    std::cerr << "Sharpen doesn't make sense for anything other than 1 design variable" << std::endl;
    return;
  }

  // Calculate the average
  double sum = 0.0;
  for(unsigned i=0; i<I; ++i)
  for(unsigned j=0; j<J; ++j)
  for(unsigned k=0; k<K; ++k) {
    sum += u(0,i,j,k);
  }
  double avg = sum/(I*J*K);

  // Calculate the cutoff
  double lo = 0.0;
  double hi = 1.0;
  double tol = 1e-5;
  while(hi-lo>tol) {
    double mid = (hi+lo)/2.0;
    sum = 0.0;
    for(unsigned i=0; i<I; ++i)
    for(unsigned j=0; j<J; ++j)
    for(unsigned k=0; k<K; ++k) {
      sum += (u(0,i,j,k)>mid) ? r_max : r_min;
    }
    if(sum/(I*J*K) < avg) {
      hi = mid;
    } else {
      lo = mid;
    }
  }
  double cutoff = (hi+lo)/2.0;

  // Set the data and save
  for(unsigned i=0; i<I; ++i)
  for(unsigned j=0; j<J; ++j)
  for(unsigned k=0; k<K; ++k) {
    u(0,i,j,k) = (u(0,i,j,k)>cutoff) ? r_max : r_min;
  }
  OutputArray(u, args[3]);
}


void VerbSize(std::vector<std::string> &args) {
  if(!SizeCheck(args, 3, "size")) return;

  auto read_chunk = OpenArrayFileForRead(args[2]);
  char buff[9];
  read_chunk(buff,8);
  buff[8] = '\0';
  if(std::string(buff).compare("BArray4D")!=0) {
    std::cerr << args[0] << ": File missing identifier" << std::endl;
    return;
  }
  
  for(int i=0; i<4;++i) {
    unsigned I;
    read_chunk((char*)(&I), 4);
    std::cout << I << " ";
  }
  std::cout << std::endl;
}


std::vector<std::string> GenerateArgList(int argc, const char *argv[]) {
  std::vector<std::string> output;
  for(int i=0; i<argc; ++i)
    output.push_back(std::string(argv[i]));
  
  return output;
}


int main(int argc, const char *argv[])  {
  std::vector<std::string> arg_list = GenerateArgList(argc, argv);
  if(argc==1) {
    std::cout << "Usage:\n"
              << "  f4d average [file1]\n"
              << "  f4d min     [file1]\n"
              << "  f4d max     [file1]\n"
              << "  f4d genus   [file] {[cutoff]}\n"
              << "  f4d dot     [file1]   [file2]\n"
              << "  f4d double  [in_file] [out_file]\n"
              << "  f4d triple  [in_file] [out_file]\n"
              << "  f4d draw    [file]   {[design_var], [min], [max]}\n"
              << "  f4d pix2f4d [in_file] [out_file] {[I] [J] [K]}\n"
              << "  f4d sharpen [in_file] [out_file] {[r_min] [r_max]}\n"
              << "  f4d size    [file1]\n"
              << "\n"
              << "Git Rev: " << GIT_REV << "\n"
              << std::flush;
    return 0;
  }


    std::string& verb = arg_list[1];
  if(verb=="average") { VerbAverage(arg_list); } else
  if(verb=="min"    ) { VerbMin    (arg_list); } else
  if(verb=="max"    ) { VerbMax    (arg_list); } else
  if(verb=="genus"  ) { genus_calc::CalculateGenus(arg_list); } else
  if(verb=="dot"    ) { VerbDot    (arg_list); } else
  if(verb=="double" ) { VerbDouble (arg_list); } else
  if(verb=="triple" ) { VerbTriple (arg_list); } else
  if(verb=="draw"   ) { VerbDraw   (arg_list); } else 
  if(verb=="pix2f4d") { VerbPix2F4d(arg_list); } else
  if(verb=="sharpen") { VerbSharpen(arg_list); } else
  if(verb=="size"   ) { VerbSize   (arg_list); } 
  else {
    std::cerr << "Don't understand \"" << verb << "\"" << std::endl;
  }
      
  return 0;
}
