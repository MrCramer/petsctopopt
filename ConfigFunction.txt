Functions are used by the objective and the constraints fields of the problem
config file. This file describes a "function" object, an object that can be
used as either an objective or as a constraint. There are only 2 required
fields, the rest are dependent on the function type.

"type"
      String. Either "mean" or "worst case" or ...
 
"limit"
    Number. Only required if this is used as a constraint. The constraint is of
    the form F( ) < limit.

mean/worst case:
  Mean of the compliance of all loadings (Note: requires loadings to have
  "weight" parameter!).

worse case:
  compliance of the worst case loading where the space of allowed loadings is a
  linear combination of load cases provided. The vector of loading factors is
  restricted to to have Euclidean length 1. So with 3 load cases f1,f2 and f3,
  a satisfactory load case would be f = root2*f1 - root2*f2 + 0*f3.

interface:
  Calculates an integral over an interface surface.

  "surface"
      Object. Describes the surface.
      "type"
          String. Must be cylinder.
      "centre"
          Array of 2 doubles. Centre of the cylinder in real values.
      "radius"
          Double. Radius of the cylinder in real values.
      "k limits"
          Array of 2 ints. Range of elements the cylinder covers.

  "integrand"
      Object. Describes the integrand.
      "type"
          String. Either "shear stress" or "Hoffman criterion"

      shear stress:
          "attenuation parameter"
              Double. Integrand is f^m, this is m

      Hoffman criterion:
          "tensile strength"
          "compressive strength"
          "shear strength"
              Double. Parameters are in Pa.

mass:
  Calculates the mean of a singular design variable over the whole domain.

bone resorption:
  Calculates bone resorption based off of Kuiper & Huiskes (1997). Resorption
  is based on average element stain energy and uses a "soft" Heaviside
  function.
  
  "resorption limit"
      Double. Maximum amount of bone allowed to be resorped.
  "heaviside epsilon"
      Double. Width of the heaviside smearing. Refer to Osher and Fedkiw (2000)
      S1.5.
  "discount factor"
      Double. Fraction of the reference strain energy required for bone to be
      not resorped.
  "bone mask file"
      String. Name of the file containing the bone mask. File should be a 3D
      array and have values consisting of only 0.0 and 1.0.
  "reference energy file"
      String. Name of the file containing the reference strain energies. File
      should be a 3D array.

intermediate value penalisation:

sum:
  Adds together multiple functions.

  "sub functions"
    Array of objects following a similar spec to the current one with an additional
    member "sum weighting" which the function is multiplied by before being added
    to the sum.

trilinear interpolated mean:
  Finds the mean of a function whose value is trilinearly interpolated between key
  values defined in a file. There must be 3 design variables.

  "key value file"
      String. Name of the file containing the interpolant key values. Should be
      a 3D array file.

  "interpolation points"
      2 dimensional array. Outside array should be of length 3. Inside arrays
      should match the dimensions of the key value file. Provides the design
      value of the key points.

