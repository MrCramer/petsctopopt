
#include <cmath>
#include <exception>
#include <fstream>
#include <memory>
#include <sstream>

#include <petscdmda.h>
#include <petscksp.h>

#include "Elastic.h"
#include "ElasTensors.h"
#include "FMatrix.h"
#include "HVec.h"
#include "main.h"


#define MPI_CALL(err) { int e; e=err; if(e!=MPI_SUCCESS) { HandleMPIError(__FILE__,__LINE__,my_id, e); } }


bool BoolFromDouble(double d) {
  if(abs(d) < 1e-4) {
    return false;
  } else if(abs(d-1.) < 1e-4) {
    return true;
  }
  std::cout << "Warning: A bool array has a value away from 0.0 or 1.0 (" << d << ")" << std::endl;
  return true;
}


void HandleMPIError(const char *file, int line, int id, int val) {
  char buff[MPI_MAX_ERROR_STRING] = {'\0'};
  int i;
  MPI_Error_string(val, buff, &i);
  std::cerr << file << "(" << line << "): "
            << "MPI_error " << val << " on proc " << id << '\n'
            << buff << std::endl;
}


const char* ConvergedReasonString(KSPConvergedReason reason) {
  switch(reason) {
    case KSP_CONVERGED_RTOL_NORMAL:      return "KSP_CONVERGED_RTOL_NORMAL";     break;
    case KSP_CONVERGED_ATOL_NORMAL:      return "KSP_CONVERGED_ATOL_NORMAL";     break;
    case KSP_CONVERGED_RTOL:             return "KSP_CONVERGED_RTOL";            break;
    case KSP_CONVERGED_ATOL:             return "KSP_CONVERGED_ATOL";            break;
    case KSP_CONVERGED_ITS:              return "KSP_CONVERGED_ITS";             break;
    case KSP_CONVERGED_CG_NEG_CURVE:     return "KSP_CONVERGED_CG_NEG_CURVE";    break;
    case KSP_CONVERGED_CG_CONSTRAINED:   return "KSP_CONVERGED_CG_CONSTRAINED";  break;
    case KSP_CONVERGED_STEP_LENGTH:      return "KSP_CONVERGED_STEP_LENGTH";     break;
    case KSP_CONVERGED_HAPPY_BREAKDOWN:  return "KSP_CONVERGED_HAPPY_BREAKDOWN"; break;
    /* diverged */
    case KSP_DIVERGED_NULL:              return "KSP_DIVERGED_NULL";             break;
    case KSP_DIVERGED_ITS:               return "KSP_DIVERGED_ITS";              break;
    case KSP_DIVERGED_DTOL:              return "KSP_DIVERGED_DTOL";             break;
    case KSP_DIVERGED_BREAKDOWN:         return "KSP_DIVERGED_BREAKDOWN";        break;
    case KSP_DIVERGED_BREAKDOWN_BICG:    return "KSP_DIVERGED_BREAKDOWN_BICG";   break;
    case KSP_DIVERGED_NONSYMMETRIC:      return "KSP_DIVERGED_NONSYMMETRIC";     break;
    case KSP_DIVERGED_INDEFINITE_PC:     return "KSP_DIVERGED_INDEFINITE_PC";    break;
    case KSP_DIVERGED_NANORINF:          return "KSP_DIVERGED_NANORINF";         break;
    case KSP_DIVERGED_INDEFINITE_MAT:    return "KSP_DIVERGED_INDEFINITE_MAT";   break;
    default:
      return "Unrecognised value";
  }
}


struct VecArrayVertex3D {
  PetscReal u[3];
};

struct VecArrayVertex2D {
  PetscReal u[2];
};


// *** GLOBALS *** // 

#define STR_BUFF_SIZE 256

enum MPI_Tags {
  TAG_DOMAIN,
  TAG_RHO,
  TAG_ELEMENT_FIELD,
  TAG_DRHO,
  TAG_U_TYPE,
};

enum WorkerMessages {
  MSG_WORKER_KILL,
  MSG_RECV_NEW_RHO,
  MSG_SOLVE,
  MSG_POST_DISPLACEMENT,
  MSG_POST_ENERGY,
  MSG_POST_UISJ_MATRIX,
  MSG_POST_DRHO_COMP,
  MSG_POST_DRHO_WC,
  MSG_POST_INTERFACE_OBJECTIVE,
  MSG_POST_CALCULATE_RESORPTION,
  MSG_POST_HOMOGENIZATION
};


const int root_proc = 0;
int       my_id;
int       num_proc;

const JSONObject *spec;

DOM_TYPE DOM;

KSP ksp_context;
DM  dm_context;


struct ProcDomain {
  int sx, nx, sy, ny, sz, nz;
} my_dom;


struct Info {
  int                   load_case;
  int                   it;
  Array4d<double>       local_design;
  const Array4d<double> *local_u;
  const Array4d<char>   *local_fixed;
  std::vector<Mat>      MG_R_mats;
} my_info;


std::vector<Vec> local_u_dm_vec, local_s_dm_vec;


//  *** SHAPE FUNCTIONS ***  //
// and other misc mesh stuff //

const int voigt[6][3] = {{0,0,1},
                         {1,1,1},
                         {2,2,1},
                         {1,2,2},
                         {0,2,2},
                         {0,1,2}};


const int voigt2d[3][3] = {{0,0,1},
                           {1,1,1},
                           {0,1,2}};


// It is important that the first 4 points are in the x,y plane for 2D
const int points[8][3] = {{0,0,0},
                          {1,0,0},
                          {0,1,0},
                          {1,1,0},
                          {0,0,1},
                          {1,0,1},
                          {0,1,1},
                          {1,1,1}};


// Caussian quadrature with 3 points, aka SimpsonsRule over [0,1]
template<class T> inline double SimpsonsRule(T f) {
  return (1./6.)*f(0.) + (4./6.)*f(.5) + (1./6.)*f(1.);
}

// Gaussian quadrature with 5 points, equally spaced over [0,1]
template<class T> inline double GaussQuad5(T f) {
  return (7./90.)*f(0.) + (32./90.)*f(0.25) + (12./90.)*f(.50) + (32./90.)*f(0.75) + (7./90.)*f(1.0);
}

const double eta_face_integral = 1.0/4.0;
const double eta_line_integral = 1.0/2.0;

PetscReal Eta(int p, double x, double y, double z) {
  return (1-points[p][0]-(1-2*points[p][0])*x)*
         (1-points[p][1]-(1-2*points[p][1])*y)*
         (1-points[p][2]-(1-2*points[p][2])*z);
}


PetscReal GradEta(int p, int i, double x, double y, double z) {
  //  Evaluates grad_i Eta^(p) where (x,y,z) is ranges over the unit cube.
  switch(i) {
    // x
    case 0:
      return (                -(1.0-2.0*points[p][0])  )*
             (1.0-points[p][1]-(1.0-2.0*points[p][1])*y)*
             (1.0-points[p][2]-(1.0-2.0*points[p][2])*z);
    // y
    case 1:
      return (1.0-points[p][0]-(1.0-2.0*points[p][0])*x)*
             (                -(1.0-2.0*points[p][1])  )*
             (1.0-points[p][2]-(1.0-2.0*points[p][2])*z);
    // z
    case 2:
      return (1.0-points[p][0]-(1.0-2.0*points[p][0])*x)*
             (1.0-points[p][1]-(1.0-2.0*points[p][1])*y)*
             (                -(1.0-2.0*points[p][2])  );
    default:
      // throw(std::logic_error("GradEta: dimension i out of range [0,2]. (Only 3 dimensions expected)"));
      return 1.0/0.0;
  }
}


// *** KSP Solver Stuff *** //

PetscErrorCode MGSetSolver(const std::string &type, KSP ksp) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  if(type=="cg") {
    ierr =  KSPSetType(ksp,KSPCG);                                               CHKERRQ(ierr);
  } else if(type=="gmres") {
    ierr =  KSPSetType(ksp,KSPGMRES);                                            CHKERRQ(ierr);
  } else if(type=="fgmres") {
    ierr =  KSPSetType(ksp,KSPFGMRES);                                           CHKERRQ(ierr);
  } else {
    SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG, "MG: Unknown solver %s", type.c_str());
  }
  PetscFunctionReturn(0);
}


PetscErrorCode MGSetPC(const std::string &type, PC pc) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  if(type=="gamg") {
    ierr =  PCSetType(pc,PCGAMG);                                                      CHKERRQ(ierr);
  } else if(type=="sor") {
    ierr =  PCSetType(pc,PCSOR);                                                       CHKERRQ(ierr);
  } else {
    SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG, "MG: Unknown PC %s", type.c_str());
  }
  PetscFunctionReturn(0);
}


PetscErrorCode ApplyMultiGridInterpToPC(PC pc) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  PetscBool flag;
  ierr =  PetscObjectTypeCompare((PetscObject)(pc), PCMG, &flag);                      CHKERRQ(ierr);
  if(flag) {
    // Is a multigrid preconditioner, do our thing
    if(!spec->ObjectHasMember("multigrid")) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
              "Attempted to use multigrid PC without specifying multigrid in the specification file");
    }
    const JSONValue *mg_spec = spec->ObjectMember("multigrid");

    int num_levels = my_info.MG_R_mats.size();
    ierr =  PCMGSetLevels(pc, num_levels, nullptr);                                    CHKERRQ(ierr);
    ierr =  PCMGSetGalerkin(pc, PETSC_TRUE);                                           CHKERRQ(ierr);
    // Set coarse solver
    KSP coarse_ksp;
    ierr =  PCMGGetCoarseSolve(pc,&coarse_ksp);                                        CHKERRQ(ierr);
    if(mg_spec->ObjectHasMember("coarse solver")) {
      std::string type = mg_spec->ObjectMember("coarse solver")->AsString();
      ierr =  MGSetSolver(type, coarse_ksp);                                           CHKERRQ(ierr);
    }
    // Set coarse PC
    if(mg_spec->ObjectHasMember("coarse pc")) {
      std::string type = mg_spec->ObjectMember("coarse pc")->AsString();
      PC coarse_pc;
      ierr =  KSPGetPC(coarse_ksp,&coarse_pc);                                         CHKERRQ(ierr);
      ierr =  MGSetPC(type,coarse_pc);                                                 CHKERRQ(ierr);
    }
    for(int i=1; i<num_levels; ++i) {
      // Set interpolation matrix
      ierr =  PCMGSetInterpolation(pc,num_levels-i,my_info.MG_R_mats[i]);              CHKERRQ(ierr);
      
      // Set smoother solver
      KSP smooth_ksp;
      ierr =  PCMGGetSmoother(pc,i,&smooth_ksp);                                       CHKERRQ(ierr);
      if(mg_spec->ObjectHasMember("smoother solver")) {
        std::string type = mg_spec->ObjectMember("smoother solver")->AsString();
        ierr =  MGSetSolver(type, smooth_ksp);                                         CHKERRQ(ierr);
      }
      // Set smooth PC
      if(mg_spec->ObjectHasMember("smooth pc")) {
        std::string type = mg_spec->ObjectMember("smooth pc")->AsString();
        PC smooth_pc;
        ierr =  KSPGetPC(smooth_ksp,&smooth_pc);                                       CHKERRQ(ierr);
        ierr =  MGSetPC(type,smooth_pc);                                               CHKERRQ(ierr);
      }
    }
  }
  ierr =  PetscObjectTypeCompare((PetscObject)(pc), PCCOMPOSITE, &flag);               CHKERRQ(ierr);
  if(flag) {
    // Is a composite so go through and look at each in turn
    PetscInt num_pcs;
    ierr =  PCCompositeGetNumberPC(pc, &num_pcs);                                      CHKERRQ(ierr);
    for(int i=0; i<num_pcs; ++i) {
      PC sub_pc;
      ierr =  PCCompositeGetPC(pc, i, &sub_pc);                                        CHKERRQ(ierr);
      ierr =  ApplyMultiGridInterpToPC(sub_pc);                                        CHKERRQ(ierr);
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode ApplyMultiGridInterp(KSP ksp_context) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  PC pc;
  ierr =  KSPGetPC(ksp_context, &pc);                                                  CHKERRQ(ierr);
  ierr =  ApplyMultiGridInterpToPC(pc);                                                CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode ApplyDirichletBC(const JSONValue *bc_spec,
                                Array4d<char> &local_fixed, Array4d<double> &local_u) {
  PetscFunctionBeginUser;
  
  std::vector<double> displacement;

  int i_s = bc_spec->ObjectMember("i indices")->ArrayMember(0)->AsInt();
  int i_e = bc_spec->ObjectMember("i indices")->ArrayMember(1)->AsInt();
  int j_s = bc_spec->ObjectMember("j indices")->ArrayMember(0)->AsInt();
  int j_e = bc_spec->ObjectMember("j indices")->ArrayMember(1)->AsInt();
  int d_s = bc_spec->ObjectMember("d indices")->ArrayMember(0)->AsInt();
  int d_e = bc_spec->ObjectMember("d indices")->ArrayMember(1)->AsInt();
  
  int k_s=0, k_e=0;
  if(DOM.dof==2) {
    if(i_s < 0 || i_s > i_e || i_e >= DOM.I ||
       j_s < 0 || j_s > j_e || j_e >= DOM.J ||
       d_s < 0 || d_s > d_e || d_e >  1) {
       SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"2D dirichlet BC limits invalid");
    }

    displacement = bc_spec->ObjectMember("displacement")->ArrayAsVectorOfDoubles(2);

  } else {
    k_s = bc_spec->ObjectMember("k indices")->ArrayMember(0)->AsInt();
    k_e = bc_spec->ObjectMember("k indices")->ArrayMember(1)->AsInt();

    if(i_s < 0 || i_s > i_e || i_e >= DOM.I ||
       j_s < 0 || j_s > j_e || j_e >= DOM.J ||
       k_s < 0 || k_s > k_e || k_e >= DOM.K ||
       d_s < 0 || d_s > d_e || d_e >  2) {
       SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"3D dirichlet BC limits invalid");
    }

    displacement = bc_spec->ObjectMember("displacement")->ArrayAsVectorOfDoubles(3);

  }

  // Set the displacement points in the local_ vectors, setting ghost points if need be
  for(int gi=std::max(i_s,my_dom.sx-1); gi<=std::min(i_e,my_dom.sx+my_dom.nx); gi++)
  for(int gj=std::max(j_s,my_dom.sy-1); gj<=std::min(j_e,my_dom.sy+my_dom.ny); gj++)
  for(int gk=std::max(k_s,my_dom.sz-1); gk<=std::min(k_e,my_dom.sz+my_dom.nz); gk++)
  for(int d=d_s; d<=d_e; d++) {
    local_u    (d,gi-my_dom.sx+1,gj-my_dom.sy+1,gk-my_dom.sz+1) = displacement[d];
    local_fixed(d,gi-my_dom.sx+1,gj-my_dom.sy+1,gk-my_dom.sz+1) = true;
  }

  PetscFunctionReturn(0);
}


PetscErrorCode ApplyDirichletBCs(int load_case,
                                 Array4d<char>   &local_fixed,
                                 Array4d<double> &local_u) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  if(spec->ObjectHasMember("common boundary conditions")) {
    const JSONValue *cbc = spec->ObjectMember("common boundary conditions");
    if(cbc->ObjectHasMember("dirichlet")) {
      int n = cbc->ObjectMember("dirichlet")->ArraySize();
      for(int i=0; i<n; ++i) {
        ierr = ApplyDirichletBC(cbc->ObjectMember("dirichlet")->ArrayMember(i), local_fixed, local_u);
                                                                                       CHKERRQ(ierr);
      }
    }
  }

  const JSONValue *lc = spec->ObjectMember("load cases")->ArrayMember(load_case);
  if(lc->ObjectHasMember("dirichlet")) {
    int n = lc->ObjectMember("dirichlet")->ArraySize();
    for(int i=0; i<n; ++i) {
      ierr = ApplyDirichletBC(lc->ObjectMember("dirichlet")->ArrayMember(i), local_fixed, local_u);
                                                                                       CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}


PetscErrorCode ApplyTractionBC2D(const JSONValue *bc,
                                 const Array4d<char> &local_fixed,
                                 VecArrayVertex2D **b_array) {
  PetscFunctionBeginUser;
  
  int i_s = bc->ObjectMember("i indices")->ArrayMember(0)->AsInt();
  int i_e = bc->ObjectMember("i indices")->ArrayMember(1)->AsInt();
  int j_s = bc->ObjectMember("j indices")->ArrayMember(0)->AsInt();
  int j_e = bc->ObjectMember("j indices")->ArrayMember(1)->AsInt();
  int d_s = bc->ObjectMember("d indices")->ArrayMember(0)->AsInt();
  int d_e = bc->ObjectMember("d indices")->ArrayMember(1)->AsInt();
  
  if(i_s < 0 || i_s > i_e || i_e >= DOM.I ||
     j_s < 0 || j_s > j_e || j_e >= DOM.J ||
     d_s < 0 || d_s > d_e || d_e >  1) {
     SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"2D traction BC limits invalid");
  }
  
  std::vector<double> traction = bc->ObjectMember("traction")->ArrayAsVectorOfDoubles(2);


  // Loop through each point in the boundary condition
  for(int gi=std::max(i_s,my_dom.sx); gi<=std::min(i_e,my_dom.sx+my_dom.nx-1); gi++)
  for(int gj=std::max(j_s,my_dom.sy); gj<=std::min(j_e,my_dom.sy+my_dom.ny-1); gj++)
  for(int d=d_s; d<=d_e; d++) {
    if(local_fixed(d,gi-my_dom.sx+1,gj-my_dom.sy+1,0)) continue;
    
    double length;
    if(i_s==i_e) {
      length = DOM.h[1];
    } else if(j_s==j_e) {
      length = DOM.h[0];
    } else {
      PetscPrintf(PETSC_COMM_WORLD, "2D Traction boundary condition specifies an area");
      SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG, "2D Traction boundary condition specifies a area");
    }
    // Points on the interior are part of more than one edge, the following line
    // detects that and produces a multiplier (this saves looping over edges and
    // doing the same calculation 2x, we just loop over points).
    double face_multiplier = (gi==i_s||gi==i_e?1.0:2.0)
                            *(gj==j_s||gj==j_e?1.0:2.0);

    // Add the contribution
    b_array[gj][gi].u[d] += face_multiplier * length * traction[d] * eta_line_integral;
  }

  PetscFunctionReturn(0);
}


PetscErrorCode ApplyTractionBC3D(const JSONValue *bc,
                                 const Array4d<char> &local_fixed,
                                 VecArrayVertex3D ***b_array) {
  PetscFunctionBeginUser;
  
  int i_s = bc->ObjectMember("i indices")->ArrayMember(0)->AsInt();
  int i_e = bc->ObjectMember("i indices")->ArrayMember(1)->AsInt();
  int j_s = bc->ObjectMember("j indices")->ArrayMember(0)->AsInt();
  int j_e = bc->ObjectMember("j indices")->ArrayMember(1)->AsInt();
  int k_s = bc->ObjectMember("k indices")->ArrayMember(0)->AsInt();
  int k_e = bc->ObjectMember("k indices")->ArrayMember(1)->AsInt();
  int d_s = bc->ObjectMember("d indices")->ArrayMember(0)->AsInt();
  int d_e = bc->ObjectMember("d indices")->ArrayMember(1)->AsInt();
  
  if(i_s < 0 || i_s > i_e || i_e >= DOM.I ||
     j_s < 0 || j_s > j_e || j_e >= DOM.J ||
     k_s < 0 || k_s > k_e || k_e >= DOM.K ||
     d_s < 0 || d_s > d_e || d_e >  2) {
     SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"3D traction BC limits invalid");
  }
 
  std::vector<double> traction = bc->ObjectMember("traction")->ArrayAsVectorOfDoubles(3);

  double area;
  if(i_s==i_e) {
    area = DOM.h[1]*DOM.h[2];
  } else if(j_s==j_e) {
    area = DOM.h[0]*DOM.h[2];
  } else if(k_s==k_e) {
    area = DOM.h[0]*DOM.h[1];
  } else {
    PetscPrintf(PETSC_COMM_WORLD, "Traction boundary condition specifies a volume");
    SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG, "Traction boundary condition specifies a volume");
  }
  
  // Loop through each point in the boundary condition
  for(int gi=std::max(i_s,my_dom.sx); gi<=std::min(i_e,my_dom.sx+my_dom.nx-1); gi++)
  for(int gj=std::max(j_s,my_dom.sy); gj<=std::min(j_e,my_dom.sy+my_dom.ny-1); gj++)
  for(int gk=std::max(k_s,my_dom.sz); gk<=std::min(k_e,my_dom.sz+my_dom.nz-1); gk++)
  for(int d=d_s; d<=d_e; d++) {
    if(local_fixed(d,gi-my_dom.sx+1,gj-my_dom.sy+1,gk-my_dom.sz+1)) continue;

    // Points on the interior are part of more than one face, the following line
    // detects that and produces a multiplier (this saves looping over faces and
    // doing the same calculation 4x, we just loop over points).
    double face_multiplier = (gi==i_s||gi==i_e?1.0:2.0)
                            *(gj==j_s||gj==j_e?1.0:2.0)
                            *(gk==k_s||gk==k_e?1.0:2.0);

    // Add the contribution
    b_array[gk][gj][gi].u[d] += face_multiplier * area * traction[d] * eta_face_integral;
  }

  PetscFunctionReturn(0);
}


PetscErrorCode ApplyPrestrainBC2D(const JSONValue *bc,
                                  const Array4d<char>   &local_fixed,
                                  const Array4d<double> &local_design,
                                  VecArrayVertex2D **b_array) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  bool is_periodic = spec->ObjectHasMember("periodic") && spec->ObjectMember("periodic")->AsBool();
  
  std::unique_ptr<TensorBase> tensor;
  ierr =  GetTensor(spec, tensor);                                                     CHKERRQ(ierr);
  std::vector<double> element_design;
  Array4d<double> element_tensor;

  std::vector<double> strain = bc->ObjectMember("strain")->ArrayAsVectorOfDoubles(3);

  // Loop through each of my points in the boundary condition
  for(int gi=my_dom.sx; gi<my_dom.sx+my_dom.nx; gi++)
  for(int gj=my_dom.sy; gj<my_dom.sy+my_dom.ny; gj++)
  for(int d=0; d<2; d++) {
    if(local_fixed(d,gi-my_dom.sx+1,gj-my_dom.sy+1,1)) continue;

    // Add the contribution
    for(int p=0; p<4; ++p) {
      int ri = gi-points[p][0];
      int rj = gj-points[p][1];
      // If not periodic then skip elements outside the domain
      if(!is_periodic && (ri<0 || rj<0 || ri>=DOM.I_el || rj>=DOM.J_el)) continue;

      ierr =  CopyDesignVariables(local_design, ri-my_dom.sx+1, rj-my_dom.sy+1, 0, element_design);
                                                                                       CHKERRQ(ierr);
      ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);      CHKERRQ(ierr);

      double val = 0.0;
      for(int  v=0;  v<3;  ++v)
      for(int jj=0; jj<2; ++jj) {
         val -=  voigt2d[v][2]*strain[v]
                *element_tensor(voigt2d[v][0], voigt2d[v][1], d, jj)
                *DOM.grad_eta_int[p][jj];
      }
      b_array[gj][gi].u[d] += val;
    }
  }

  PetscFunctionReturn(0);
}


PetscErrorCode ApplyPrestrainBC3D(const JSONValue *bc,
                                  const Array4d<char>   &local_fixed,
                                  const Array4d<double> &local_design,
                                  VecArrayVertex3D ***b_array) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  bool is_periodic = spec->ObjectHasMember("periodic") && spec->ObjectMember("periodic")->AsBool();
  
  std::unique_ptr<TensorBase> tensor;
  ierr =  GetTensor(spec, tensor);                                                     CHKERRQ(ierr);
  std::vector<double> element_design;
  Array4d<double> element_tensor;

  std::vector<double> strain = bc->ObjectMember("strain")->ArrayAsVectorOfDoubles(6);

  // Loop through each of my points
  for(int gi=my_dom.sx; gi<my_dom.sx+my_dom.nx; gi++)
  for(int gj=my_dom.sy; gj<my_dom.sy+my_dom.ny; gj++)
  for(int gk=my_dom.sz; gk<my_dom.sz+my_dom.nz; gk++)
  for(int d=0; d<3; d++) {
    if(local_fixed(d,gi-my_dom.sx+1,gj-my_dom.sy+1,gk-my_dom.sz+1)) continue;

    // Add the contribution
    for(int p=0; p<8; ++p) {
      int ri = gi-points[p][0];
      int rj = gj-points[p][1];
      int rk = gk-points[p][2];
      // If not periodic then skip elements outside the domain
      if(!is_periodic && (ri<0 || rj<0 || rk<0 || ri>=DOM.I_el || rj>=DOM.J_el || rk>=DOM.K_el)) continue;

      ierr =  CopyDesignVariables(local_design, ri-my_dom.sx+1, rj-my_dom.sy+1, rk-my_dom.sz+1, element_design);
                                                                                       CHKERRQ(ierr);
      ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);      CHKERRQ(ierr);

      double val = 0.0;
      for(int  v=0;  v<6;  ++v)
      for(int jj=0; jj<3; ++jj) {
         val -=  voigt[v][2]*strain[v]
                *element_tensor(voigt[v][0], voigt[v][1], d, jj)
                *DOM.grad_eta_int[p][jj];
      }
      b_array[gk][gj][gi].u[d] += val;
    }
  }

  PetscFunctionReturn(0);
}


PetscErrorCode ApplyLoadTypeBCs2D(unsigned load_case,
                                  const Array4d<double> &local_design,
                                  const Array4d<char>   &local_fixed,
                                  VecArrayVertex2D **b_array) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  if(spec->ObjectHasMember("common boundary conditions")) {
    const JSONValue *cbc = spec->ObjectMember("common boundary conditions");
    if(cbc->ObjectHasMember("traction")) {
      int n = cbc->ObjectMember("traction")->ArraySize();
      for(int i=0; i<n; ++i) {
        ierr = ApplyTractionBC2D(cbc->ObjectMember("traction")->ArrayMember(i), local_fixed, b_array);
                                                                                       CHKERRQ(ierr);
      }
    }
    if(cbc->ObjectHasMember("prestrain")) {
      int n = cbc->ObjectMember("prestrain")->ArraySize();
      for(int i=0; i<n; ++i) {
        ierr = ApplyPrestrainBC2D(cbc->ObjectMember("prestrain")->ArrayMember(i),
                                  local_fixed, local_design, b_array);
                                                                                       CHKERRQ(ierr);
      }
    }
  }

  const JSONValue *lc = spec->ObjectMember("load cases")->ArrayMember(load_case);
  if(lc->ObjectHasMember("traction")) {
    int n = lc->ObjectMember("traction")->ArraySize();
    for(int i=0; i<n; ++i) {
      ierr = ApplyTractionBC2D(lc->ObjectMember("traction")->ArrayMember(i), local_fixed, b_array);
                                                                                       CHKERRQ(ierr);
    }
  }
     
  if(lc->ObjectHasMember("prestrain")) {
    int n = lc->ObjectMember("prestrain")->ArraySize();
    for(int i=0; i<n; ++i) {
      ierr = ApplyPrestrainBC2D(lc->ObjectMember("prestrain")->ArrayMember(i),
                                local_fixed, local_design, b_array);
                                                                                       CHKERRQ(ierr);
    }
  }
  
  PetscFunctionReturn(0);
}


PetscErrorCode ApplyLoadTypeBCs3D(unsigned load_case,
                                  const Array4d<double> &local_design,
                                  const Array4d<char>   &local_fixed,
                                  VecArrayVertex3D ***b_array) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  if(spec->ObjectHasMember("common boundary conditions")) {
    const JSONValue *cbc = spec->ObjectMember("common boundary conditions");
    if(cbc->ObjectHasMember("traction")) {
      int n = cbc->ObjectMember("traction")->ArraySize();
      for(int i=0; i<n; ++i) {
        ierr = ApplyTractionBC3D(cbc->ObjectMember("traction")->ArrayMember(i), local_fixed, b_array);
                                                                                       CHKERRQ(ierr);
      }
    }
    if(cbc->ObjectHasMember("prestrain")) {
      int n = cbc->ObjectMember("prestrain")->ArraySize();
      for(int i=0; i<n; ++i) {
        ierr = ApplyPrestrainBC3D(cbc->ObjectMember("prestrain")->ArrayMember(i),
                                  local_fixed, local_design, b_array);
                                                                                       CHKERRQ(ierr);
      }
    }
  }

  const JSONValue *lc = spec->ObjectMember("load cases")->ArrayMember(load_case);
  if(lc->ObjectHasMember("traction")) {
    int n = lc->ObjectMember("traction")->ArraySize();
    for(int i=0; i<n; ++i) {
      ierr = ApplyTractionBC3D(lc->ObjectMember("traction")->ArrayMember(i), local_fixed, b_array);
                                                                                       CHKERRQ(ierr);
    }
  }
     
  if(lc->ObjectHasMember("prestrain")) {
    int n = lc->ObjectMember("prestrain")->ArraySize();
    for(int i=0; i<n; ++i) {
      ierr = ApplyPrestrainBC3D(lc->ObjectMember("prestrain")->ArrayMember(i),
                                local_fixed, local_design, b_array);
                                                                                       CHKERRQ(ierr);
    }
  }
  
  PetscFunctionReturn(0);
}


PetscErrorCode MoveDirichletPointsToRHS2D(const Array4d<double> &local_design,
                                          const Array4d<char>   &local_fixed,
                                          const Array4d<double> &local_u,
                                          VecArrayVertex2D **b_array) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  bool is_periodic = spec->ObjectHasMember("periodic") && spec->ObjectMember("periodic")->AsBool();

  std::unique_ptr<TensorBase> tensor;
  ierr =  GetTensor(spec, tensor);                                                     CHKERRQ(ierr);
  std::vector<double> element_design;
  Array4d<double> element_tensor;
  
  int i,j,p,q,ii,jj,kk,ll;

  // Loop through all points and dof, including ghosts, looking for dirichlet points
  for( i=0;  i<my_dom.nx+2;  ++i)
  for( j=0;  j<my_dom.ny+2;  ++j)
  for(ii=0; ii<2;        ++ii) {
    if(local_fixed(ii,i,j,1)) {
      int gi = i-1+my_dom.sx;
      int gj = j-1+my_dom.sy;

      double u_ii_ij_val = local_u(ii,i,j,1);
      
      // Skip dirichlet points with zero value, their contribution is zero
      if(u_ii_ij_val==0.0) continue;

      // Loop over all elements around the dirichlet point
      for(p=0; p<4; ++p) {
        if(!is_periodic) {
          // If the block is out of global bounds, skip it
          if(!is_periodic) {
            if(gi-points[p][0] <  0        || gj-points[p][1] <  0       ) continue;
            if(gi-points[p][0] >= DOM.I_el || gj-points[p][1] >= DOM.J_el) continue;
          }
        }
        // If the block is out of local bounds (including ghost blocks), skip it
        if(i-points[p][0] < 0 || i-points[p][0]+1 >= my_dom.nx+2 ||
           j-points[p][1] < 0 || j-points[p][1]+1 >= my_dom.ny+2) continue;

        ierr =  CopyDesignVariables(local_design, i-points[p][0], j-points[p][1], 0, element_design);
                                                                                       CHKERRQ(ierr);
        ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);    CHKERRQ(ierr);

        // Loop over all points within the element to add my contribution to their rhs
        for( q=0;  q<4;  ++q) {
          int qi = i - points[p][0] + points[q][0];
          int qj = j - points[p][1] + points[q][1];
          // Skip points which are not constrolled by me
          if(qi<1 || qi >= my_dom.nx+1 ||
             qj<1 || qj >= my_dom.ny+1) continue;
          
          for(kk=0; kk<2; ++kk) {
            // Skip points that are part of a dirichlet condition
            if(local_fixed(kk,qi,qj,1)) continue;
            
            double v = 0.0;
            for(jj=0; jj<2; ++jj)
            for(ll=0; ll<2; ++ll) {
              v += element_tensor(ii,jj,kk,ll)
                 * DOM.grad_eta_grad_eta_int[p][jj][q][ll];
            }
            // Add the contribution to the RHS
            b_array[qj-1+my_dom.sy][qi-1+my_dom.sx].u[kk] -= v * u_ii_ij_val;
          }
        }
      }
    }
  }
  
  PetscFunctionReturn(0);
}


PetscErrorCode MoveDirichletPointsToRHS3D(const Array4d<double> &local_design,
                                          const Array4d<char>   &local_fixed,
                                          const Array4d<double> &local_u,
                                          VecArrayVertex3D ***b_array) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  bool is_periodic = spec->ObjectHasMember("periodic") && spec->ObjectMember("periodic")->AsBool();

  std::unique_ptr<TensorBase> tensor;
  ierr =  GetTensor(spec, tensor);                                                     CHKERRQ(ierr);
  std::vector<double> element_design;
  Array4d<double> element_tensor;
  
  int i,j,k,p,q,ii,jj,kk,ll;

  // Loop through all points and dof, including ghosts, looking for dirichlet points
  for( i=0;  i<my_dom.nx+2;  ++i)
  for( j=0;  j<my_dom.ny+2;  ++j)
  for( k=0;  k<my_dom.nz+2;  ++k)
  for(ii=0; ii<3;        ++ii) {
    if(local_fixed(ii,i,j,k)) {
      int gi = i-1+my_dom.sx;
      int gj = j-1+my_dom.sy;
      int gk = k-1+my_dom.sz;

      double u_ii_ijk_val = local_u(ii,i,j,k);
      
      // Skip dirichlet points with zero value, their contribution is zero
      if(u_ii_ijk_val==0.0) continue;

      // Loop over all elements around the dirichlet point
      for(p=0; p<8; ++p) {
        if(!is_periodic) {
          // If the block is out of global bounds, skip it
          if(!is_periodic) {
            if(gi-points[p][0]  <  0     || gj-points[p][1]  <  0     || gk-points[p][2]  <  0    ) continue;
            if(gi-points[p][0]+1>= DOM.I || gj-points[p][1]+1>= DOM.J || gk-points[p][2]+1>= DOM.K) continue;
          }
        }
        // If the block is out of local bounds (including ghost blocks), skip it
        if(i-points[p][0] < 0 || i-points[p][0]+1 >= my_dom.nx+2 ||
           j-points[p][1] < 0 || j-points[p][1]+1 >= my_dom.ny+2 ||
           k-points[p][2] < 0 || k-points[p][2]+1 >= my_dom.nz+2) continue;

        ierr =  CopyDesignVariables(local_design, i-points[p][0], j-points[p][1], k-points[p][2], element_design);
                                                                                       CHKERRQ(ierr);
        ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);    CHKERRQ(ierr);

        // Loop over all points within the element to add my contribution to their rhs
        for( q=0;  q<8;  ++q) {
          int qi = i - points[p][0] + points[q][0];
          int qj = j - points[p][1] + points[q][1];
          int qk = k - points[p][2] + points[q][2];
          // Skip points which are not constrolled by me
          if(qi<1 || qi >= my_dom.nx+1 ||
             qj<1 || qj >= my_dom.ny+1 ||
             qk<1 || qk >= my_dom.nz+1) continue;
          
          for(kk=0; kk<3; ++kk) {
            // Skip points that are part of a dirichlet condition
            if(local_fixed(kk,qi,qj,qk)) continue;
            
            double v = 0.0;
            for(jj=0; jj<3; ++jj)
            for(ll=0; ll<3; ++ll) {
              v += element_tensor(ii,jj,kk,ll)
                 * DOM.grad_eta_grad_eta_int[p][jj][q][ll];
            }
            // Add the contribution to the RHS
            b_array[qk-1+my_dom.sz][qj-1+my_dom.sy][qi-1+my_dom.sx].u[kk] -= v * u_ii_ijk_val;
          }
        }
      }
    }
  }
  
  PetscFunctionReturn(0);
}


PetscErrorCode ComputeRHS(KSP ksp_context, Vec b, void *void_info) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  int load_case = my_info.load_case;
  
  const Array4d<double> &local_design = my_info.local_design;
  const Array4d<char>   &local_fixed  = *(my_info.local_fixed);
  const Array4d<double> &local_u      = *(my_info.local_u);

  if(DOM.dof==2) {
    // Pull apart the array
    VecArrayVertex2D **b_array;
    ierr =  DMDAVecGetArray(dm_context,b,&b_array);                                    CHKERRQ(ierr);

    // Set the majority to be zeros
    PetscInt i,j,d;
    for(j=0; j<my_dom.ny; ++j)
    for(i=0; i<my_dom.nx; ++i)
    for(d=0; d<DOM.dof;   ++d) {
      b_array[j+my_dom.sy][i+my_dom.sx].u[d] = 0.0;
    }

    // Apply the load-type BCs
    ierr =  ApplyLoadTypeBCs2D(load_case, local_design, local_fixed, b_array); CHKERRQ(ierr);

    // Move the dirichlet points' matrix contributions to the rhs
    ierr =  MoveDirichletPointsToRHS2D(local_design, local_fixed, local_u, b_array);
                                                                                       CHKERRQ(ierr);
    // Set the dirichlet points to their values
    for(j=0; j<my_dom.ny; ++j)
    for(i=0; i<my_dom.nx; ++i)
    for(d=0; d<DOM.dof;   ++d) {
      if(local_fixed(d,i+1,j+1,1)) {
        b_array[j+my_dom.sy][i+my_dom.sx].u[d] = local_u(d,i+1,j+1,1);
      }
    }

    // Put the array back together again
    ierr =  DMDAVecRestoreArray(dm_context,b,&b_array);                                CHKERRQ(ierr);
  
  } else {
    // Pull apart the array
    VecArrayVertex3D ***b_array;
    ierr =  DMDAVecGetArray(dm_context,b,&b_array);                                    CHKERRQ(ierr);

    // Set the majority to be zeros
    PetscInt i,j,k,d;
    for(k=0; k<my_dom.nz; ++k)
    for(j=0; j<my_dom.ny; ++j)
    for(i=0; i<my_dom.nx; ++i)
    for(d=0; d<DOM.dof;   ++d) {
      b_array[k+my_dom.sz][j+my_dom.sy][i+my_dom.sx].u[d] = 0.0;
    }

    // Apply the load-type BCs
    ierr =  ApplyLoadTypeBCs3D(load_case, local_design, local_fixed, b_array); CHKERRQ(ierr);

    // Move the dirichlet points' matrix contributions to the rhs
    ierr =  MoveDirichletPointsToRHS3D(local_design, local_fixed, local_u, b_array);
                                                                                       CHKERRQ(ierr);
    // Set the dirichlet points to their values
    for(k=0; k<my_dom.nz; ++k)
    for(j=0; j<my_dom.ny; ++j)
    for(i=0; i<my_dom.nx; ++i)
    for(d=0; d<DOM.dof;   ++d) {
      if(local_fixed(d,i+1,j+1,k+1)) {
        b_array[k+my_dom.sz][j+my_dom.sy][i+my_dom.sx].u[d] = local_u(d,i+1,j+1,k+1);
      }
    }

    // Put the array back together again
    ierr =  DMDAVecRestoreArray(dm_context,b,&b_array);                                CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}


PetscErrorCode ComputeMatrix2D(KSP ksp_context, Mat mat, 
                               const Array4d<double> &local_design,
                               const Array4d<char>   &local_fixed) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  bool is_periodic = (spec->ObjectHasMember("periodic") && spec->ObjectMember("periodic")->AsBool());

  std::unique_ptr<TensorBase> tensor;
  ierr =  GetTensor(spec, tensor);                                                     CHKERRQ(ierr);
  std::vector<double> element_design;
  Array4d<double> element_tensor;
  
  // Set all possible values to zero
  int i,j,ii,jj,kk,ll,p,q,n;
  MatStencil row,cols[18];
  PetscReal v[18];

  for( j=0;  j<my_dom.ny; ++j)
  for( i=0;  i<my_dom.nx; ++i)
  for(ii=0;  ii<2;    ++ii) {
    int gi = i+my_dom.sx;
    int gj = j+my_dom.sy;
    n = 0;
    for(int di=-1; di<=1; ++di)
    for(int dj=-1; dj<=1; ++dj)
    for(    jj= 0; jj<2;  ++jj) {
      if(!is_periodic && (gi+di<0 || gi+di >=DOM.I)) continue;
      if(!is_periodic && (gj+dj<0 || gj+dj >=DOM.J)) continue;
      cols[n].i = gi+di; 
      cols[n].j = gj+dj; 
      cols[n].k = 0;
      cols[n].c = jj;
      v[n] = 0.0;
      ++n;
    }
    
    row.i = gi;
    row.j = gj;
    row.k = 0;
    row.c = ii;

    ierr =  MatSetValuesStencil(mat,1,&row,n,cols,v,INSERT_VALUES);                  CHKERRQ(ierr);
  }

  ierr =  MatAssemblyBegin(mat,MAT_FLUSH_ASSEMBLY);                                  CHKERRQ(ierr);
  ierr =  MatAssemblyEnd  (mat,MAT_FLUSH_ASSEMBLY);                                  CHKERRQ(ierr);

  /*
   *   Loop over all points, then loop over all dof, then loop over all surrounding
   * elements. Add the contributions for each neighbouring point's dof into my row.
   * Ignore columns.
   */
  for( j=0;  j<my_dom.ny; ++j)
  for( i=0;  i<my_dom.nx; ++i)
  for(ii=0;  ii<2;    ++ii) {
    int gi = i+my_dom.sx;
    int gj = j+my_dom.sy;

    bool fixed = local_fixed(ii,i+1, j+1, 1);

    // If I'm a dirichlet condition just set my diag to 1 and leave all else to zero
    if(fixed) {
      cols[0].i = gi;
      cols[0].j = gj;
      cols[0].k =  0;
      cols[0].c = ii;
      v[0] = 1.0;
      MatSetValuesStencil(mat,1,cols,1,cols,v,ADD_VALUES);
    } else {
      // Loop around elements points surrounding me
      for(p=0; p<4; ++p) {
        if(!is_periodic) {
          // If the block is out of bounds, skip it
          if(gi-points[p][0] <  0        || gj-points[p][1] <  0       ) continue;
          if(gi-points[p][0] >= DOM.I_el || gj-points[p][1] >= DOM.J_el) continue;
        }

        ierr =  CopyDesignVariables(local_design, i+1-points[p][0], j+1-points[p][1], 0, element_design);
                                                                                       CHKERRQ(ierr);
        ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);    CHKERRQ(ierr);

        n = 0; // Number of elements to place in the matrix
        // Loop over all points in this element and their dof
        for( q=0;  q<4;  ++q)
        for(kk=0; kk<2; ++kk) {
          // Skip points that are part of a dirichlet condition (they're added to the RHS)
          int qi = i - points[p][0] + points[q][0];
          int qj = j - points[p][1] + points[q][1];
          if(local_fixed(kk,qi+1,qj+1,1)) continue;
          
          cols[n].i = qi + my_dom.sx;
          cols[n].j = qj + my_dom.sy;
          cols[n].k = 0;
          cols[n].c = kk;

          v[n] = 0.0;
          for(jj=0; jj<2; ++jj)
          for(ll=0; ll<2; ++ll) {
            v[n] += element_tensor(ii,jj,kk,ll)
                  * DOM.grad_eta_grad_eta_int[p][jj][q][ll];
          }
          ++n;
        }
        
        row.i = gi;
        row.j = gj;
        row.k =  0;
        row.c = ii;

        ierr =  MatSetValuesStencil(mat,1,&row,n,cols,v,ADD_VALUES);                 CHKERRQ(ierr);
      }
    }
  }
  ierr =  MatAssemblyBegin(mat,MAT_FINAL_ASSEMBLY);                                  CHKERRQ(ierr);
  ierr =  MatAssemblyEnd  (mat,MAT_FINAL_ASSEMBLY);                                  CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


PetscErrorCode ComputeMatrix3D(KSP ksp_context, Mat mat, 
                               const Array4d<double> &local_design,
                               const Array4d<char>   &local_fixed) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  bool is_periodic = (spec->ObjectHasMember("periodic") && spec->ObjectMember("periodic")->AsBool());

  std::unique_ptr<TensorBase> tensor;
  ierr =  GetTensor(spec, tensor);                                                     CHKERRQ(ierr);
  std::vector<double> element_design;
  Array4d<double> element_tensor;
    
  // Set all possible values to zero
  int i,j,k,ii,jj,kk,ll,p,q,n;
  MatStencil row,cols[81];
  PetscReal v[81];

  for( k=0;  k<my_dom.nz; ++k)
  for( j=0;  j<my_dom.ny; ++j)
  for( i=0;  i<my_dom.nx; ++i)
  for(ii=0;  ii<3;    ++ii) {
    int gi = i+my_dom.sx;
    int gj = j+my_dom.sy;
    int gk = k+my_dom.sz;
    n = 0;
    for(int di=-1; di<=1; ++di)
    for(int dj=-1; dj<=1; ++dj)
    for(int dk=-1; dk<=1; ++dk)
    for(    jj= 0; jj<3;  ++jj) {
      if(!is_periodic) {
        if(gi+di<0 || gi+di >=DOM.I) continue;
        if(gj+dj<0 || gj+dj >=DOM.J) continue;
        if(gk+dk<0 || gk+dk >=DOM.K) continue;
      }
      cols[n].i = gi+di; 
      cols[n].j = gj+dj; 
      cols[n].k = gk+dk; 
      cols[n].c = jj;
      v[n] = 0.0;
      ++n;
    }
    
    row.i = gi;
    row.j = gj;
    row.k = gk;
    row.c = ii;

    ierr =  MatSetValuesStencil(mat,1,&row,n,cols,v,INSERT_VALUES);                  CHKERRQ(ierr);
  }

  ierr =  MatAssemblyBegin(mat,MAT_FLUSH_ASSEMBLY);                                  CHKERRQ(ierr);
  ierr =  MatAssemblyEnd  (mat,MAT_FLUSH_ASSEMBLY);                                  CHKERRQ(ierr);

  /*
   *   Loop over all points, then loop over all dof, then loop over all surrounding
   * elements. Add the contributions for each neighbouring point's dof into my row.
   * Ignore columns.
   */
  for( k=0;  k<my_dom.nz; ++k)
  for( j=0;  j<my_dom.ny; ++j)
  for( i=0;  i<my_dom.nx; ++i)
  for(ii=0;  ii<3;    ++ii) {
    int gi = i+my_dom.sx;
    int gj = j+my_dom.sy;
    int gk = k+my_dom.sz;

    bool fixed = local_fixed(ii,i+1, j+1, k+1);

    // If I'm a dirichlet condition just set my diag to 1 and leave all else to zero
    if(fixed) {
      cols[0].i = gi;
      cols[0].j = gj;
      cols[0].k = gk;
      cols[0].c = ii;
      v[0] = 1.0;
      MatSetValuesStencil(mat,1,cols,1,cols,v,ADD_VALUES);
    } else {
      // Loop around elements points surrounding me
      for(p=0; p<8; ++p) {
        if(!is_periodic) {
          // If the block is out of bounds, skip it
          if(gi-points[p][0] <  0        || gj-points[p][1] <  0        || gk-points[p][2] <  0       ) continue;
          if(gi-points[p][0] >= DOM.I_el || gj-points[p][1] >= DOM.J_el || gk-points[p][2] >= DOM.K_el) continue;
        }

        ierr =  CopyDesignVariables(local_design, i+1-points[p][0], j+1-points[p][1], k+1-points[p][2], element_design);
                                                                                       CHKERRQ(ierr);
        ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);    CHKERRQ(ierr);

        n = 0; // Number of elements to place in the matrix
        // Loop over all points in this element and their dof
        for( q=0;  q<8;  ++q)
        for(kk=0; kk<3; ++kk) {
          // Skip points that are part of a dirichlet condition (they're added to the RHS)
          int qi = i - points[p][0] + points[q][0];
          int qj = j - points[p][1] + points[q][1];
          int qk = k - points[p][2] + points[q][2];
          if(local_fixed(kk,qi+1,qj+1,qk+1)) continue;
          
          cols[n].i = qi + my_dom.sx;
          cols[n].j = qj + my_dom.sy;
          cols[n].k = qk + my_dom.sz;
          cols[n].c = kk;

          v[n] = 0.0;
          for(jj=0; jj<3; ++jj)
          for(ll=0; ll<3; ++ll) {
            v[n] += element_tensor(ii,jj,kk,ll) 
                  * DOM.grad_eta_grad_eta_int[p][jj][q][ll];
          }
          ++n;
        }
        
        row.i = gi;
        row.j = gj;
        row.k = gk;
        row.c = ii;

        ierr =  MatSetValuesStencil(mat,1,&row,n,cols,v,ADD_VALUES);                 CHKERRQ(ierr);
      }
    }
  }
  ierr =  MatAssemblyBegin(mat,MAT_FINAL_ASSEMBLY);                                  CHKERRQ(ierr);
  ierr =  MatAssemblyEnd  (mat,MAT_FINAL_ASSEMBLY);                                  CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


//   JJ is the operator, and jac is similar to the operator but used to generate the 
// pre-conditioner. The examples that I found using this style (SetComputeOperator) all
// seemed to ignore the JJ Mat and just put their operator in the jac Mat. As such, 
// that's what I do here.
PetscErrorCode ComputeMatrix(KSP ksp_context, Mat JJ, Mat jac, void *void_info) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  const Array4d<double> &local_design = my_info.local_design;
  const Array4d<char>   &local_fixed  = *(my_info.local_fixed);

  if(DOM.dof==2) {
    ierr =  ComputeMatrix2D(ksp_context, jac, local_design, local_fixed);              CHKERRQ(ierr);
  } else {
    ierr =  ComputeMatrix3D(ksp_context, jac, local_design, local_fixed);              CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}



// *** Data Sharing Functions *** //

/*   Data is shared between processes using the ArrayNd template class. There are a
   number of shapes that the data can take. u-type arrays are nodal arrays and do not
   contain ghost points, rho-type arrays are elemental and contain ghost points and 
   drho-type array are elemental with no ghost elements, in this case shared elements
   are owned by the processor in the negative direction. 2D rho type arrays have 3 
   dimensions with the third set to K=2 having essentially 2 copies of the data, this
   cost is considered not significant as it's 2D.
*/

struct MasterInfoStruct {
  std::vector<ProcDomain> processor_domains;
} master_info;


PetscErrorCode MasterShareDesign(const Array4d<double> &design, Array4d<double> &local_design) {
  PetscFunctionBeginUser;
  
  int design_dof = spec->ObjectMember("design dof")->AsInt();
  static std::vector<Array4d<double> > design_buffers(num_proc);
  int p, d, i, j, k;
  
  // Wake the workers
  int msg = MSG_RECV_NEW_RHO;
  MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));
  
  std::vector<MPI_Request> send_requests(num_proc);
  for(p=0; p<num_proc; ++p) {
    // Transfer to a buffer
    ProcDomain dom = master_info.processor_domains[p];
    design_buffers[p].Resize(design_dof, dom.nx+1, dom.ny+1, dom.nz+1);

    for(k=0; k<dom.nz+1; ++k)
    for(j=0; j<dom.ny+1; ++j)
    for(i=0; i<dom.nx+1; ++i)
    for(d=0; d<design_dof; ++d) {
      // always send as if periodic
      //   | the +I_el  is to fix the way C handles modulus for negative numbers
      // send a ghost point either side
      //   | n vertices are "mine"
      //   | n-1 elements are interior
      //   | n+1 elements are interior and exterior
      int gi = (dom.sx-1+i +DOM.I_el)%DOM.I_el;
      int gj = (dom.sy-1+j +DOM.J_el)%DOM.J_el;
      int gk = (dom.sz-1+k +DOM.K_el)%DOM.K_el; 
      design_buffers[p](d,i,j,k) = design(d, gi, gj, gk);
    }
    // Send it on its way
    int n_points = design_dof*(dom.nx+1)*(dom.ny+1)*(dom.nz+1);
    if(p!=root_proc) {
      MPI_CALL(MPI_Isend(&design_buffers[p](0,0,0,0), n_points, MPI_DOUBLE, p,
                         TAG_RHO, PETSC_COMM_WORLD, &send_requests[p]))
    }
  }

  local_design = design_buffers[root_proc];

  // Make sure the sends have all finished
  for(p=0; p<num_proc; ++p) {
    if(p!=root_proc)
      MPI_CALL(MPI_Wait(&send_requests[p], MPI_STATUS_IGNORE))
  }
  
  PetscFunctionReturn(0);
}


PetscErrorCode MasterShareElementalArray(const Array3d<double> &array, Array3d<double> &local_array) {
  PetscFunctionBeginUser;
  
  static std::vector<Array3d<double> > buffers(num_proc);
  int p, i, j, k;
  
  // Wake the workers
  std::vector<MPI_Request> send_requests(num_proc);
  for(p=0; p<num_proc; ++p) {
    // Transfer to a buffer
    ProcDomain dom = master_info.processor_domains[p];
    buffers[p].Resize(dom.nx+1, dom.ny+1, dom.nz+1);

    for(k=0; k<dom.nz+1; ++k)
    for(j=0; j<dom.ny+1; ++j)
    for(i=0; i<dom.nx+1; ++i) {
      // always send as if periodic
      //   | the +I_el  is to fix the way C handles modulus for negative numbers
      // send a ghost point either side
      //   | n vertices are "mine"
      //   | n-1 elements are interior
      //   | n+1 elements are interior and exterior
      int gi = (dom.sx-1+i +DOM.I_el)%DOM.I_el;
      int gj = (dom.sy-1+j +DOM.J_el)%DOM.J_el;
      int gk = (dom.sz-1+k +DOM.K_el)%DOM.K_el; 
      buffers[p](i,j,k) = array(gi, gj, gk);
    }
    // Send it on its way
    int n_points = (dom.nx+1)*(dom.ny+1)*(dom.nz+1);
    if(p!=root_proc) {
      MPI_CALL(MPI_Isend(&buffers[p](0,0,0), n_points, MPI_DOUBLE, p,
                         TAG_ELEMENT_FIELD, PETSC_COMM_WORLD, &send_requests[p]))
    }
  }

  local_array = buffers[root_proc];

  // Make sure the sends have all finished
  for(p=0; p<num_proc; ++p) { 
    if(p!=root_proc)
      MPI_CALL(MPI_Wait(&send_requests[p], MPI_STATUS_IGNORE))
  }
  
  PetscFunctionReturn(0);
}


PetscErrorCode WorkerRecvElementalArray(Array3d<double> &local_array) {
  PetscFunctionBeginUser;
  local_array.Resize(my_dom.nx+1, my_dom.ny+1, my_dom.nz+1);
  int n_points = (my_dom.nx+1)*(my_dom.ny+1)*(my_dom.nz+1);
  MPI_Request req;
  MPI_CALL(MPI_Irecv(&local_array(0,0,0), n_points, MPI_DOUBLE, root_proc,
                     TAG_ELEMENT_FIELD, PETSC_COMM_WORLD, &req))
  MPI_CALL(MPI_Wait(&req, MPI_STATUS_IGNORE))
  PetscFunctionReturn(0);
}


PetscErrorCode MasterRecvDDesign(const Array4d<double> &my_d_design,
                                 Array4d<double> &d_design) {
  // We have assumed that the domain is not periodic
  PetscFunctionBeginUser;
  int p;

  int design_dof = spec->ObjectMember("design dof")->AsInt();

  static std::vector<Array4d<double> > buffers (num_proc);
  static std::vector<MPI_Request>      requests(num_proc);

  for(p=0; p<num_proc; ++p) {
    ProcDomain dom = master_info.processor_domains[p];
    buffers[p].Resize(design_dof,dom.nx,dom.ny,dom.nz);
    if(p==root_proc) {
      buffers[p] = my_d_design;
    } else {
      int n_points = design_dof*dom.nx*dom.ny*dom.nz;
      MPI_CALL(MPI_Irecv(&buffers[p](0,0,0,0), n_points, MPI_DOUBLE, p,
                         TAG_DRHO, PETSC_COMM_WORLD, &requests[p]));
    }
  }
  
  d_design.Resize(design_dof, DOM.I_el, DOM.J_el, DOM.K_el); 

  for(p=0; p<num_proc; ++p) {
    ProcDomain dom = master_info.processor_domains[p];

    if(DOM.dof==2) dom.nz = 1;
    
    if(p!=root_proc) {
      MPI_CALL(MPI_Wait(&requests[p], MPI_STATUS_IGNORE));
    }
    
    // Boundary elements are owned by those on the left
    for(int k=0; k<dom.nz; ++k)
    for(int j=0; j<dom.ny; ++j)
    for(int i=0; i<dom.nx; ++i)
    for(int d=0; d<design_dof; ++d) {
      if(i+dom.sx >= DOM.I_el || 
         j+dom.sy >= DOM.J_el || 
         k+dom.sz >= DOM.K_el) continue;
      d_design(d, i+dom.sx, j+dom.sy, k+dom.sz) = buffers[p](d,i,j,k);
    }
  }

  PetscFunctionReturn(0);
}


PetscErrorCode WorkerSendDDesign(Array4d<double> &my_design) {
  PetscFunctionBeginUser;

  int design_dof = spec->ObjectMember("design dof")->AsInt();
  int n_points = design_dof*my_dom.nx*my_dom.ny*my_dom.nz;
  MPI_Request request;
  MPI_CALL(MPI_Isend(&my_design(0,0,0,0), n_points, MPI_DOUBLE, root_proc,
                     TAG_DRHO, PETSC_COMM_WORLD, &request));
  MPI_CALL(MPI_Wait(&request, MPI_STATUS_IGNORE));
  PetscFunctionReturn(0);
}


PetscErrorCode MasterReceiveUTypeArrayNoGhost(const Array4d<double> &my_local_array,
                                              Array4d<double> &array) {
  PetscFunctionBeginUser;
  
  std::vector<Array4d<double> > buffers(num_proc);

  // Pulls in a array discluding ghost points
  std::vector<MPI_Request> recv_requests(num_proc);
  for(int p=0; p<num_proc; ++p) {
    ProcDomain dom = master_info.processor_domains[p];
    if(p!=root_proc) {
      buffers[p].Resize(DOM.dof, dom.nx, dom.ny, dom.nz);
      int n_points = DOM.dof*dom.nx*dom.ny*dom.nz;
      MPI_CALL(MPI_Irecv(&buffers[p](0,0,0,0), n_points, MPI_DOUBLE, p,
                         TAG_U_TYPE, PETSC_COMM_WORLD, &recv_requests[p]))
    }
  }

  array.Resize(DOM.dof,DOM.I,DOM.J,DOM.K);

  for(int p=0; p<num_proc; ++p) {
    const Array4d<double> *buff;
    if(p==root_proc) {
      buff = &my_local_array;
    } else {
      MPI_CALL(MPI_Wait(&recv_requests[p], MPI_STATUS_IGNORE))
      buff = &buffers[p];
    }
    
    ProcDomain dom = master_info.processor_domains[p];
    for(int i=0; i<dom.nx;  ++i)
    for(int j=0; j<dom.ny;  ++j)
    for(int k=0; k<dom.nz;  ++k)
    for(int d=0; d<DOM.dof; ++d) {
      array(d,i+dom.sx,j+dom.sy,k+dom.sz) = (*buff)(d,i,j,k);
    }
  }

  PetscFunctionReturn(0);
}


PetscErrorCode WorkerSendUTypeArrayNoGhost(Array4d<double> &array) {
  PetscFunctionBeginUser;
  MPI_Request send_request;

  int size = array.GetI()*array.GetJ()*array.GetK()*array.GetL();
  MPI_CALL(MPI_Isend(&array(0,0,0,0), size, MPI_DOUBLE, root_proc,
                     TAG_U_TYPE, PETSC_COMM_WORLD, &send_request))

  MPI_CALL(MPI_Wait(&send_request, MPI_STATUS_IGNORE))
  PetscFunctionReturn(0);
}


PetscErrorCode TransferVecToArrayNoGhost(Vec vec, ProcDomain dom, Array4d<double> &array) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
   
  if(DOM.dof==2) {
    VecArrayVertex2D **vec_array;
    ierr =  DMDAVecGetArray(dm_context,vec,&vec_array);                                CHKERRQ(ierr);
    array.Resize(2, dom.nx, dom.ny, 1);

    PetscInt i,j,d;
    for(i=0; i<dom.nx;  ++i)
    for(j=0; j<dom.ny;  ++j)
    for(d=0; d<DOM.dof; ++d) {
      array(d,i,j,0) = vec_array[j+dom.sy][i+dom.sx].u[d];
    }
    ierr =  DMDAVecRestoreArray(dm_context,vec,&vec_array);                            CHKERRQ(ierr);

  } else {
    VecArrayVertex3D ***vec_array;
    ierr =  DMDAVecGetArray(dm_context,vec,&vec_array);                                CHKERRQ(ierr);
    array.Resize(3, dom.nx, dom.ny, dom.nz);

    PetscInt i,j,k,d;
    for(i=0; i<dom.nx;  ++i)
    for(j=0; j<dom.ny;  ++j)
    for(k=0; k<dom.nz;  ++k)
    for(d=0; d<DOM.dof; ++d) {
      array(d,i,j,k) = vec_array[k+dom.sz][j+dom.sy][i+dom.sx].u[d];
    }
    ierr =  DMDAVecRestoreArray(dm_context,vec,&vec_array);                            CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}



// *** Post Solve Functions *** //
  
PetscErrorCode CalculateMinusQdKU_2D(const Array4d<double> &local_design,
                                     VecArrayVertex2D **q_array,
                                     VecArrayVertex2D **u_array,
                                     Array4d<double> &result) {
  // Calculates the result
  //
  //   - q d_K/d_design u
  //
  // where q and u are 2D displacement arrays. Does not collect on the result nor does
  // it get the arrays. result is resized to be the correct size.
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  
  const int D = spec->ObjectMember("design dof")->AsInt();
  
  result.Resize(D,my_dom.nx, my_dom.ny, 1);

  std::unique_ptr<TensorBase> tensor;
  ierr =  GetTensor(spec, tensor);                                                     CHKERRQ(ierr);
  std::vector<double> element_design;
  Array4d<double> element_dtensor;

  // Loop over each element I own
  for(int i=0; i<my_dom.nx; i++)
  for(int j=0; j<my_dom.ny; j++) {
    if(i+my_dom.sx>=DOM.I_el || j+my_dom.sy>=DOM.J_el) continue;

    ierr =  CopyDesignVariables(local_design, i+1, j+1, 0, element_design);        CHKERRQ(ierr);
    for(int d=0; d<D; ++d) {
      ierr =  tensor->EvalDTensor(&element_design[0], my_info.it, d, element_dtensor);CHKERRQ(ierr);
      
      double element_d = 0.0;
      // Loop over every pair of points and dimension combinations
      for(int p=0;  p<4; p++)
      for(int q=0;  q<4; q++) {
        int pi = i+my_dom.sx+points[p][0]; 
        int pj = j+my_dom.sy+points[p][1]; 
        int qi = i+my_dom.sx+points[q][0]; 
        int qj = j+my_dom.sy+points[q][1]; 

        for(int ii=0; ii<2; ii++)
        for(int kk=0; kk<2; kk++)
        for(int jj=0; jj<2; jj++)
        for(int ll=0; ll<2; ll++) {
          element_d -=   q_array[pj][pi].u[ii] * u_array[qj][qi].u[kk]
                       * element_dtensor(ii,jj,kk,ll) * DOM.grad_eta_grad_eta_int[p][jj][q][ll];
        }
      }
      result(d,i,j,0) = element_d;
    }
  }
  PetscFunctionReturn(0);
}


PetscErrorCode CalculateMinusQdKU_3D(const Array4d<double> &local_design,
                                     VecArrayVertex3D ***q_array,
                                     VecArrayVertex3D ***u_array,
                                     Array4d<double> &result) {
  // Calculates the result
  //
  //   - q d_K/d_design u
  //
  // where q and u are 3D displacement arrays. Does not collect on the result nor does
  // it get the arrays. result is resized to be the correct size.
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  
  const int D = spec->ObjectMember("design dof")->AsInt();

  result.Resize(D,my_dom.nx,my_dom.ny,my_dom.nz);

  std::unique_ptr<TensorBase> tensor;
  ierr =  GetTensor(spec, tensor);                                                     CHKERRQ(ierr);
  std::vector<double> element_design;
  Array4d<double> element_dtensor;

  // Loop over each element
  for(int i=0; i<my_dom.nx; i++)
  for(int j=0; j<my_dom.ny; j++)
  for(int k=0; k<my_dom.nz; k++) {
    if(i+my_dom.sx>=DOM.I_el || j+my_dom.sy>=DOM.J_el || k+my_dom.sz>=DOM.K_el) continue;

    ierr =  CopyDesignVariables(local_design, i+1, j+1, k+1, element_design);      CHKERRQ(ierr);
    for(int d=0; d<D; ++d) {
      ierr =  tensor->EvalDTensor(&element_design[0], my_info.it, d, element_dtensor);CHKERRQ(ierr);
      double element_d = 0.0;

      for(int p=0;  p<8;  p++)
      for(int q=0;  q<8;  q++) {
        int pi = i+my_dom.sx+points[p][0]; 
        int pj = j+my_dom.sy+points[p][1]; 
        int pk = k+my_dom.sz+points[p][2]; 
        int qi = i+my_dom.sx+points[q][0]; 
        int qj = j+my_dom.sy+points[q][1]; 
        int qk = k+my_dom.sz+points[q][2]; 

        for(int ii=0; ii<3; ii++)
        for(int kk=0; kk<3; kk++)
        for(int jj=0; jj<3; jj++)
        for(int ll=0; ll<3; ll++) {
          element_d -=   q_array[pk][pj][pi].u[ii] * u_array[qk][qj][qi].u[kk]
                     * element_dtensor(ii,jj,kk,ll) * DOM.grad_eta_grad_eta_int[p][jj][q][ll];
        }
      }
      result(d,i,j,k) = element_d;
    }
  }
  PetscFunctionReturn(0);
}


PetscErrorCode CalculateMinusQdKU(const Array4d<double> &local_design,
                                  Vec q_local, Vec u_local,
                                  Array4d<double> &local_minus_qdKu) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  if(DOM.dof==2) {
    VecArrayVertex2D **u_array, **q_array;
    ierr =  DMDAVecGetArray(dm_context, u_local, &u_array);                            CHKERRQ(ierr);
    ierr =  DMDAVecGetArray(dm_context, q_local, &q_array);                            CHKERRQ(ierr);
    ierr =  CalculateMinusQdKU_2D(local_design, q_array, u_array, local_minus_qdKu);   CHKERRQ(ierr);
    ierr =  DMDAVecRestoreArray(dm_context, u_local, &u_array);                        CHKERRQ(ierr);
    ierr =  DMDAVecRestoreArray(dm_context, q_local, &q_array);                        CHKERRQ(ierr);
  } else {
    VecArrayVertex3D ***u_array, ***q_array;
    ierr =  DMDAVecGetArray(dm_context, u_local, &u_array);                            CHKERRQ(ierr);
    ierr =  DMDAVecGetArray(dm_context, q_local, &q_array);                            CHKERRQ(ierr);
    ierr =  CalculateMinusQdKU_3D(local_design, q_array, u_array, local_minus_qdKu);   CHKERRQ(ierr);
    ierr =  DMDAVecRestoreArray(dm_context, u_local, &u_array);                        CHKERRQ(ierr);
    ierr =  DMDAVecRestoreArray(dm_context, q_local, &q_array);                        CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}


PetscErrorCode PostSolveDisplacement(std::vector<Array4d<double> > &u) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  static Array4d<double> buffer;

  int num_load_cases = spec->ObjectMember("load cases")->ArraySize();
  u.resize(num_load_cases);

  for(int i=0; i<num_load_cases; ++i) {
    ierr =  TransferVecToArrayNoGhost(local_u_dm_vec[i], my_dom, buffer);              CHKERRQ(ierr);
    if(my_id == root_proc) {
      ierr =  MasterReceiveUTypeArrayNoGhost(buffer, u[i]);                            CHKERRQ(ierr);
    } else {
      ierr =  WorkerSendUTypeArrayNoGhost(buffer);                                     CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}


PetscErrorCode PostSolveCalculateEnergy(Array4d<double> local_design, int load_case, Array3d<double> &energy) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  
  std::unique_ptr<TensorBase> tensor;
  ierr =  GetTensor(spec, tensor);                                                     CHKERRQ(ierr);
  std::vector<double> element_design;
  Array4d<double> element_tensor;
  
  static Array4d<double> local_energy;
  local_energy.Resize(1,my_dom.nx,my_dom.ny,my_dom.nz);
  if(DOM.dof==2) {
    VecArrayVertex2D **u_array;
    ierr =  DMDAVecGetArray(dm_context, local_u_dm_vec[load_case], &u_array);          CHKERRQ(ierr);

    for(int i=0; i<my_dom.nx; i++)
    for(int j=0; j<my_dom.ny; j++) {
      if(i+my_dom.sx>=DOM.I_el || j+my_dom.sy>=DOM.J_el) continue;
      double element_energy = 0.;
      ierr =  CopyDesignVariables(local_design, i+1, j+1, 0, element_design);          CHKERRQ(ierr);
      ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);      CHKERRQ(ierr);
      for(int p=0;  p<4;  p++)
      for(int q=0;  q<4;  q++) {
        int pi = i+my_dom.sx+points[p][0]; 
        int pj = j+my_dom.sy+points[p][1]; 
        int qi = i+my_dom.sx+points[q][0]; 
        int qj = j+my_dom.sy+points[q][1]; 
        for(int ii=0; ii<2; ii++)
        for(int jj=0; jj<2; jj++)
        for(int kk=0; kk<2; kk++)
        for(int ll=0; ll<2; ll++) {
          element_energy += 0.5
                          * u_array[pj][pi].u[ii]
                          * u_array[qj][qi].u[kk]
                          * DOM.grad_eta_grad_eta_int[p][jj][q][ll]
                          * element_tensor(ii,jj,kk,ll);
        }
      }
      local_energy(0,i,j,0) = element_energy;
    }
    ierr =  DMDAVecRestoreArray(dm_context, local_u_dm_vec[load_case], &u_array);      CHKERRQ(ierr);

  } else {
    VecArrayVertex3D ***u_array;
    ierr =  DMDAVecGetArray(dm_context, local_u_dm_vec[load_case], &u_array);          CHKERRQ(ierr);

    for(int i=0; i<my_dom.nx; i++)
    for(int j=0; j<my_dom.ny; j++)
    for(int k=0; k<my_dom.nz; k++) {
      if(i+my_dom.sx>=DOM.I_el || j+my_dom.sy>=DOM.J_el || k+my_dom.sz>=DOM.K_el) continue;
      double element_energy = 0.;
      ierr =  CopyDesignVariables(local_design, i+1, j+1, k+1, element_design);        CHKERRQ(ierr);
      ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);      CHKERRQ(ierr);
      for(int p=0;  p<8;  p++)
      for(int q=0;  q<8;  q++) {
        int pi = i+my_dom.sx+points[p][0]; 
        int pj = j+my_dom.sy+points[p][1]; 
        int pk = k+my_dom.sz+points[p][2]; 
        int qi = i+my_dom.sx+points[q][0]; 
        int qj = j+my_dom.sy+points[q][1]; 
        int qk = k+my_dom.sz+points[q][2]; 
        for(int ii=0; ii<3; ii++)
        for(int jj=0; jj<3; jj++)
        for(int kk=0; kk<3; kk++)
        for(int ll=0; ll<3; ll++) {
          element_energy += 0.5
                          * u_array[pk][pj][pi].u[ii]
                          * u_array[qk][qj][qi].u[kk]
                          * DOM.grad_eta_grad_eta_int[p][jj][q][ll]
                          * element_tensor(ii,jj,kk,ll);
        }
      }
      local_energy(0,i,j,k) = element_energy;
    }
    ierr =  DMDAVecRestoreArray(dm_context, local_u_dm_vec[load_case], &u_array);      CHKERRQ(ierr);
  }

  if(my_id==root_proc) {
    // Because I couldn't be bothered rewriting a gather routine for this I'm 
    // hijacking the derivitives gather, sue me. This only works if we have one
    // design variable
    static Array4d<double> energy_4d;
    ierr =  MasterRecvDDesign(local_energy, energy_4d);                                CHKERRQ(ierr);
    energy.Resize(DOM.I_el, DOM.J_el, DOM.K_el);
    if(energy.GetData().size() != energy_4d.GetData().size()) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_CORRUPT, "Ignore error type, assert failed PostSolveCalculateEnergy");
    }
    energy.GetData() = energy_4d.GetData();
  } else {
    ierr =  WorkerSendDDesign(local_energy);                                           CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}


PetscErrorCode PostSolveUiSj(Array2d<double> &uisj) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  unsigned M=local_u_dm_vec.size();
  unsigned N=local_s_dm_vec.size();

  uisj.Resize(M,N);
  Array2d<double> my_uisj(M,N);
  my_uisj.Set(0.0);

  if(DOM.dof==2) {
    VecArrayVertex2D **u_array;
    VecArrayVertex2D **s_array;

    for(unsigned i=0; i<M; ++i)
    for(unsigned j=0; j<N; ++j) {
      ierr =  DMDAVecGetArray(dm_context, local_u_dm_vec[i], &u_array);                CHKERRQ(ierr);
      ierr =  DMDAVecGetArray(dm_context, local_s_dm_vec[j], &s_array);                CHKERRQ(ierr);
      for(int jj=0; jj<my_dom.ny; ++jj)
      for(int ii=0; ii<my_dom.nx; ++ii)
      for(int dd=0; dd<2;      ++dd) {
        my_uisj(i,j) +=  u_array[jj+my_dom.sy][ii+my_dom.sx].u[dd]
                       * s_array[jj+my_dom.sy][ii+my_dom.sx].u[dd];
      }
      ierr =  DMDAVecRestoreArray(dm_context, local_u_dm_vec[i], &u_array);            CHKERRQ(ierr);
      ierr =  DMDAVecRestoreArray(dm_context, local_s_dm_vec[j], &s_array);            CHKERRQ(ierr);
    }
  } else {
    VecArrayVertex3D ***u_array;
    VecArrayVertex3D ***s_array;

    for(unsigned i=0; i<M; ++i)
    for(unsigned j=0; j<N; ++j) {
      ierr =  DMDAVecGetArray(dm_context, local_u_dm_vec[i], &u_array);                CHKERRQ(ierr);
      ierr =  DMDAVecGetArray(dm_context, local_s_dm_vec[j], &s_array);                CHKERRQ(ierr);
      for(int kk=0; kk<my_dom.nz; ++kk)
      for(int jj=0; jj<my_dom.ny; ++jj)
      for(int ii=0; ii<my_dom.nx; ++ii)
      for(int dd=0; dd<3;      ++dd) {
        my_uisj(i,j) +=  u_array[kk+my_dom.sz][jj+my_dom.sy][ii+my_dom.sx].u[dd]
                       * s_array[kk+my_dom.sz][jj+my_dom.sy][ii+my_dom.sx].u[dd];
      }
      ierr =  DMDAVecRestoreArray(dm_context, local_u_dm_vec[i], &u_array);            CHKERRQ(ierr);
      ierr =  DMDAVecRestoreArray(dm_context, local_s_dm_vec[j], &s_array);            CHKERRQ(ierr);
    }
  }
  
  MPI_CALL(MPI_Reduce(&my_uisj(0,0),    // send_data
                      &uisj(0,0),       // recv_data
                      M*N,              // count
                      MPI_DOUBLE,       // datatype
                      MPI_SUM,          // op
                      root_proc,        // root
                      MPI_COMM_WORLD)); // communicator
  
  PetscFunctionReturn(0);
}


PetscErrorCode PostSolveComplianceDDesign(const Array4d<double> &local_design,
                                          std::vector<Array4d<double> > &d_design) {
  // If u_i is the vector of displacements for load i and s_i is the load vector for 
  // load i then this function calculates 
  //
  // d(u_i s_i) = - u_i dK u_i.
  // 
  // Output is in d_design which is a vector. This function is very similar to 
  // PostSolveComplianceDDesignMatrix, this function just saves work by only
  // considering the diagonal elements.
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  int num_load_cases = spec->ObjectMember("load cases")->ArraySize();
  d_design.resize(num_load_cases);

  Array4d<double> my_d_design;
  
  for(int load_case=0; load_case<num_load_cases; ++load_case) {
    Vec u = local_u_dm_vec[load_case];
    ierr =  CalculateMinusQdKU(local_design, u, u, my_d_design);                       CHKERRQ(ierr);
    
    if(my_id==root_proc) {
      ierr =  MasterRecvDDesign(my_d_design, d_design[load_case]);                     CHKERRQ(ierr);
    } else {
      ierr =  WorkerSendDDesign(my_d_design);                                          CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}


PetscErrorCode PostSolveComplianceDDesignMatrix(const Array4d<double> &local_design,
                                                Array2d<Array4d<double> > &d_design_matrix) {
  // If u_i is the vector of displacements for load i and s_i is the load vector for 
  // load i then this function calculates 
  //
  // d(u_i s_j) = - u_i dK u_j.
  // 
  // Output is in d_design_matrix.
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  int num_load_cases = spec->ObjectMember("load cases")->ArraySize();
  d_design_matrix.Resize(num_load_cases, num_load_cases);

  Array4d<double> my_d_design;

  for(int load_case_A=0; load_case_A<num_load_cases; ++load_case_A)
  for(int load_case_B=0; load_case_B<num_load_cases; ++load_case_B) {
    Vec u_A = local_u_dm_vec[load_case_A];
    Vec u_B = local_u_dm_vec[load_case_B];
    ierr =  CalculateMinusQdKU(local_design, u_A, u_B, my_d_design);                 CHKERRQ(ierr);

    if(my_id==root_proc) {
      ierr =  MasterRecvDDesign(my_d_design, d_design_matrix(load_case_A, load_case_B));CHKERRQ(ierr);
    } else {
      ierr =  WorkerSendDDesign(my_d_design);                                        CHKERRQ(ierr);
    }
  }

  PetscFunctionReturn(0);
}


PetscErrorCode CopyRhs(KSP ksp_con, Vec rhs, void *ctx) {
  // Copies the vector given through ctx to the rhs for KSPSetComputeRHS( )
  PetscFunctionBeginUser;
  PetscErrorCode ierr =  VecCopy(Vec(ctx), rhs);                                       CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode CalculateFullDerivatives(const Array4d<double> &local_design,
                                        Vec dFdu_local,
                                        const Array4d<double> &local_del_F_del_design,
                                        Array4d<double> &d_design) {
  // Calculates the derivitives of a general gunctiong F given that we have the partial
  // derivitives  del F/del u  provided in dFdu_local and  del F/del design provided in
  // local_del_F_del_design. Vector dFdu_local is transfered to a global vector using
  // ADD_VALUES.
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  const int D = spec->ObjectMember("design dof")->AsInt();

  // Create our resources
  static Array4d<double> local_d_design, local_minus_qdKu;


  Vec dFdu_global, q_soln, q_local;
  ierr =  DMCreateGlobalVector(dm_context, &dFdu_global);                              CHKERRQ(ierr);
  ierr =  DMCreateLocalVector (dm_context, &q_local);                                  CHKERRQ(ierr);

  // Solve K q = dF/du
  ierr =  VecSet(dFdu_global, 0.0);                                                    CHKERRQ(ierr);
  ierr =  DMLocalToGlobalBegin(dm_context, dFdu_local, ADD_VALUES, dFdu_global);       CHKERRQ(ierr);
  ierr =  DMLocalToGlobalEnd  (dm_context, dFdu_local, ADD_VALUES, dFdu_global);       CHKERRQ(ierr);
  ierr =  KSPCreate             (PETSC_COMM_WORLD, &ksp_context);                      CHKERRQ(ierr);
  ierr =  KSPSetDM              (ksp_context,dm_context);                              CHKERRQ(ierr);
  ierr =  KSPSetComputeRHS      (ksp_context,&CopyRhs,(void*)dFdu_global);             CHKERRQ(ierr);
  ierr =  KSPSetComputeOperators(ksp_context,&ComputeMatrix,NULL);                     CHKERRQ(ierr);
  ierr =  KSPSetFromOptions     (ksp_context);                                         CHKERRQ(ierr);
  ierr =  ApplyMultiGridInterp  (ksp_context);                                         CHKERRQ(ierr);
  ierr =  KSPSolve              (ksp_context,NULL,NULL);                               CHKERRQ(ierr);
  ierr =  KSPGetSolution        (ksp_context,&q_soln);                                 CHKERRQ(ierr);
  ierr =  DMGlobalToLocalBegin  (dm_context, q_soln, INSERT_VALUES, q_local);          CHKERRQ(ierr);
  ierr =  DMGlobalToLocalEnd    (dm_context, q_soln, INSERT_VALUES, q_local);          CHKERRQ(ierr);
  ierr =  KSPDestroy            (&ksp_context);                                        CHKERRQ(ierr);

  // derivitives are dF/ddesign - u dK/ddesign q, add the second term to local_d_design
  ierr =  CalculateMinusQdKU(local_design,q_local,local_u_dm_vec[0],local_minus_qdKu); CHKERRQ(ierr);

  local_d_design.Resize(D, my_dom.nx, my_dom.ny, my_dom.nz);
        std::vector<double> &d_design_vect       = local_d_design.GetData();
  const std::vector<double> &del_f_d_design_vect = local_del_F_del_design.GetData();
  const std::vector<double> &minus_qdKu_vect     = local_minus_qdKu.GetData();
  for(size_t i=0; i<d_design_vect.size(); ++i) {
    d_design_vect[i] = del_f_d_design_vect[i] + minus_qdKu_vect[i];
  }

  // Clean up resources
  ierr =  VecDestroy(&dFdu_global);                                                    CHKERRQ(ierr);
  ierr =  VecDestroy(&q_local);                                                        CHKERRQ(ierr);

  // Gather d_design and objective to the main process
  if(my_id==root_proc) {
    ierr =  MasterRecvDDesign(local_d_design, d_design);                               CHKERRQ(ierr);
  } else {
    ierr =  WorkerSendDDesign(local_d_design);                                         CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}


PetscErrorCode DetermineCylinderCrossingPoints(const int g_i, const int g_j,         // Element's coords (global)
                                               const double r_centre[2],  // Real coords
                                               const double r_radius,                // Real coords
                                               bool &does_cross,
                                               double e_out_x[2], double e_out_y[2], // Element coords ([0,1])
                                               double r_n[3], double &r_length) {    // Real coords
  // Determines where the circle of radius r_radius and centre r_center passes through
  // the element i,j. does_cross is set true when the circle passes through the element.
  // Element boundary crossings are given back through e_out_x and e_out_y, the distance
  // between these two points in real coords is given in r_length.
  //
  // Assumes that the circle will cross into and out of the element just once. One way
  // to ensure this is the case is to make the cylinder's centre on element boundaries.
  PetscFunctionBeginUser;
  double r_cx = r_centre[0] - g_i*DOM.h[0]; // Move centre coords to be relative to the
  double r_cy = r_centre[1] - g_j*DOM.h[1]; // bottom-right of the element.

  // Get a list of candidate points, two for each boundary line
  //
  // We use Pythagoras to determine the two possible crossings of each of the four 
  // element bounding lines.
  double t;
  double r_x[8] = {-1,-1,-1,-1,-1,-1,-1,-1};
  double r_y[8] = {-1,-1,-1,-1,-1,-1,-1,-1};
  r_x[0] = r_x[1] = 0.;
  t = r_radius*r_radius - (r_cx-0.)*(r_cx-0.);
  if(t>=0.) {
    r_y[0] = r_cy + sqrt(t);
    r_y[1] = r_cy - sqrt(t);
  }

  r_x[2] = r_x[3] = DOM.h[0];
  t = r_radius*r_radius - (r_cx-DOM.h[0])*(r_cx-DOM.h[0]);
  if(t>=0.) {
    r_y[2] = r_cy + sqrt(t);
    r_y[3] = r_cy - sqrt(t);
  }

  r_y[4] = r_y[5] = 0.;
  t = r_radius*r_radius - (r_cy-0.)*(r_cy-0.);
  if(t>=0.) {
    r_x[4] = r_cx + sqrt(t);
    r_x[5] = r_cx - sqrt(t);
  }

  r_y[6] = r_y[7] = DOM.h[1];
  t = r_radius*r_radius - (r_cy-DOM.h[1])*(r_cy-DOM.h[1]);
  if(t>=0.) {
    r_x[6] = r_cx + sqrt(t);
    r_x[7] = r_cx - sqrt(t);
  }

  // Determine which points are on the element, convert to local coords and store
  int count = 0;
  for(int ii=0; ii<8; ++ii) {
    if(0. <= r_x[ii] && r_x[ii] <= DOM.h[0] && 0. <= r_y[ii] && r_y[ii] <= DOM.h[1]) {
      if(count >=2) {
        // If we allow more than 2 crossings (as can happen) then we must determine
        // point pairing and change the client to receive more than 2 points
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
                "Interface objective: cylinder intersects an element more than 2 times");
      }
      e_out_x[count] = r_x[ii]/DOM.h[0];
      e_out_y[count] = r_y[ii]/DOM.h[1];
      count ++;
    }
  }
  if(count == 1) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
            "Interface objective: cylinder intersects an element only once");
  }

  // If we're crossing, calculate the other bits and pieces
  does_cross = (count==2);
  if(does_cross) {
    double r_dx = (e_out_x[1]-e_out_x[0])*DOM.h[0];
    double r_dy = (e_out_y[1]-e_out_y[0])*DOM.h[1];
    double r_nx =  r_dy; // Rotate the difference between the vectors to get a normal
    double r_ny = -r_dx;
    // Dot the radius vector to one of the crossing points with the proposed normal to 
    // see if it needs to be flipped, we want an outwards normal
    if((e_out_x[0]*DOM.h[0]-r_cx)*r_nx + (e_out_y[0]*DOM.h[1]-r_cy)*r_ny < 0) {
      r_nx *= -1;
      r_ny *= -1;
    }
    double r_n_len_inv = 1./sqrt(r_nx*r_nx + r_ny*r_ny);
    r_n[0] = r_nx*r_n_len_inv;
    r_n[1] = r_ny*r_n_len_inv;
    r_length = sqrt(r_dx*r_dx + r_dy*r_dy);
  }
  PetscFunctionReturn(0);
}


PetscErrorCode ShearStressElementIntegrals3D(std::ofstream *dump_file, // If not nullptr, dump values
                                             double m,
                                             const std::unique_ptr<TensorBase> &tensor_func,
                                             double *element_design,
                                             VecArrayVertex3D ***u_array,
                                             int g_i, int g_j, int g_k,          // Element's coords (global)
                                             double e_x[2], double e_y[2],       // In element coords ([0,1])
                                             double r_n[3], double r_length,     // In real coords
                                             double &objective,
                                             VecArrayVertex3D ***dFdu_array,
                                             Array4d<double> &local_dF_ddesign) {
  // Calculates the shear stress integrals for a given element
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  if(dump_file) {
    (*dump_file) << e_x[0] << ',' << e_x[1] << ',' << e_y[0] << ',' << e_y[1] << ','
                    << g_i << ',' << g_j << ',' << g_k;
  }

  static Array4d<double> tensor;
  ierr =  tensor_func->EvalTensor(element_design, my_info.it, tensor);                 CHKERRQ(ierr);
 
  const double area = r_length*DOM.h[2];
  const int D = spec->ObjectMember("design dof")->AsInt();

  int l_i = g_i - my_dom.sx;
  int l_j = g_j - my_dom.sy;
  int l_k = g_k - my_dom.sz;

  auto calc_stress_vector = [&](double e_x, double e_y, double e_z, double T[3]) {
    T[0] = T[1] = T[2] = 0.;
    for(int pp=0; pp<8; ++pp) {
      int g_pi = g_i+points[pp][0]; 
      int g_pj = g_j+points[pp][1]; 
      int g_pk = g_k+points[pp][2]; 
      for(int ii=0; ii<3; ++ii)
      for(int jj=0; jj<2; ++jj) // Only goes to 1 as n[2] == 0 anyway
      for(int kk=0; kk<3; ++kk)
      for(int ll=0; ll<3; ++ll) {
        T[ii] += tensor(ii,jj,kk,ll)                             // C_ijkl
               * u_array[g_pk][g_pj][g_pi].u[kk]                 // u^(p)_k
               * GradEta(pp,ll,e_x,e_y,e_z)/DOM.h[ll] * r_n[jj]; // d eta^(p)/d x_l * n_j
      }
    }
  };

  // Calculate the objective //
  auto tau_sq_integrand = [&](double e_x, double e_y, double e_z) {
    double T[3]; // Stress vector
    calc_stress_vector(e_x,e_y,e_z,T);
    // Pythagoras: TiTi is the square of the length of the stress vector
    //             TiNi is the length of Ti's normal component, thus
    // TiTi - TiNi*TiNi is the square of the length of Ti's tangential component
    double TiTi = 0.0;
    double TiNi = 0.0;
    for(int ii=0; ii<3; ++ii) {
      TiTi += T[ii]*T[ii];
      TiNi += T[ii]*r_n[ii];
    }
    double tau_sq = TiTi - TiNi*TiNi;
    if(dump_file) {
      (*dump_file) << ',' << std::pow(tau_sq,m);
    }
    return std::pow(tau_sq,m);
  };

  objective += area * SimpsonsRule([&](double e_z) {
    return GaussQuad5([&](double alpha) {
      return tau_sq_integrand(alpha*e_x[0]+(1.-alpha)*e_x[1],
                              alpha*e_y[0]+(1.-alpha)*e_y[1],
                              e_z);
    });
  });
  
  if(dump_file) (*dump_file) << '\n';

  // Calculate dFdu //
  auto dFdu_integrand = [&](int pp, int kk, double e_x, double e_y, double e_z) {
    double T[3];
    calc_stress_vector(e_x,e_y,e_z,T);
    // Calculate dT
    double dT[3];
    for(int ii=0; ii<3; ++ii) {
      double v = 0.;
      for(int jj=0; jj<2; ++jj) // Only goes to 1 as n[2] == 0 anyway
      for(int ll=0; ll<3; ++ll) {
        v += tensor(ii,jj,kk,ll)
           * GradEta(pp,ll,e_x,e_y,e_z)/DOM.h[ll] * r_n[jj];
      }
      dT[ii] = v;
    }
    // Calculate the derivitive of tau_sq above
    double dTiTi = 0.0;
    double dTiNi = 0.0;
    double  TiNi = 0.0;
    double  TiTi = 0.0;
    for(int ii=0; ii<3; ++ii) {
      dTiTi += dT[ii]*T[ii];
      dTiNi += dT[ii]*r_n[ii];
       TiNi +=  T[ii]*r_n[ii];
       TiTi +=  T[ii]*T[ii];
    }
    double d_tau_sq = 2*dTiTi - 2*dTiNi*TiNi;
    double tau_sq = TiTi - TiNi*TiNi;
    return m*d_tau_sq*std::pow(tau_sq,m-1);
  };

  for(int pp=0; pp<8; ++pp) {
    int g_pi = g_i+points[pp][0]; 
    int g_pj = g_j+points[pp][1]; 
    int g_pk = g_k+points[pp][2]; 
    for(int kk=0; kk<3; ++kk) {
      dFdu_array[g_pk][g_pj][g_pi].u[kk] += 
        area*SimpsonsRule([&](double e_z) {
          return GaussQuad5([&](double alpha) {
            return dFdu_integrand(pp, kk,
                                  alpha*e_x[0]+(1.-alpha)*e_x[1],
                                  alpha*e_y[0]+(1.-alpha)*e_y[1],
                                  e_z);
          });
        });
    }
  }

  // Calculate dFddesign
  auto dF_ddesign_integrand = [&](const Array4d<double> &dtensor, double e_x, double e_y, double e_z) {
    double T[3];
    calc_stress_vector(e_x,e_y,e_z,T);
    // Calculate dT
    double dT[3] = {0.0, 0.0, 0.0};
    for(int pp=0; pp<8; ++pp) {
      int g_pi = g_i+points[pp][0]; 
      int g_pj = g_j+points[pp][1]; 
      int g_pk = g_k+points[pp][2]; 
      for(int ii=0; ii<3; ++ii)
      for(int jj=0; jj<2; ++jj) // Only goes to 1 as n[2] == 0 anyway
      for(int kk=0; kk<3; ++kk)
      for(int ll=0; ll<3; ++ll) {
        dT[ii] += dtensor(ii,jj,kk,ll)                            // dC_ijkl
                * u_array[g_pk][g_pj][g_pi].u[kk]                 // u^(p)_k
                * GradEta(pp,ll,e_x,e_y,e_z)/DOM.h[ll] * r_n[jj]; // d eta^(p)/d x_l * n_j
      }
    }
    // Calculate the derivitive of tau_sq above
    double dTiTi = 0.0;
    double dTiNi = 0.0;
    double  TiNi = 0.0;
    double  TiTi = 0.0;
    for(int ii=0; ii<3; ++ii) {
      dTiTi += dT[ii]*T[ii];
      dTiNi += dT[ii]*r_n[ii];
       TiNi +=  T[ii]*r_n[ii];
       TiTi +=  T[ii]*T[ii];
    }
    double tau_sq = TiTi - TiNi*TiNi;
    double d_tau_sq = 2*dTiTi - 2*dTiNi*TiNi;
    return m*d_tau_sq*std::pow(tau_sq,m-1);
  };

  static Array4d<double> dtensor;
  for(int d=0; d<D; ++d) {
    ierr =  tensor_func->EvalDTensor(element_design, my_info.it, d, dtensor);          CHKERRQ(ierr);
    local_dF_ddesign(d, l_i, l_j, l_k) =
      area*SimpsonsRule([&](double e_z) {
        return GaussQuad5([&](double alpha) {
          return dF_ddesign_integrand(dtensor,
                                      alpha*e_x[0]+(1.-alpha)*e_x[1],
                                      alpha*e_y[0]+(1.-alpha)*e_y[1],
                                      e_z);
        });
      });
  }

  PetscFunctionReturn(0);
}


PetscErrorCode HoffmanElementIntegrals3D(double S_t, double S_c, double S_s,
                                         const std::unique_ptr<TensorBase> &tensor_func,
                                         double *element_design,
                                         VecArrayVertex3D ***u_array,
                                         int g_i, int g_j, int g_k,          // Element's coords (global)
                                         double e_x[2], double e_y[2],       // In element coords ([0,1])
                                         double r_n[3], double r_length,     // In real coords
                                         double &objective,
                                         VecArrayVertex3D ***dFdu_array,
                                         Array4d<double> &local_dF_ddesign) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  static Array4d<double> tensor;
  ierr =  tensor_func->EvalTensor(element_design, my_info.it, tensor);                 CHKERRQ(ierr);
 
  const double area = r_length*DOM.h[2];
  const int D = spec->ObjectMember("design dof")->AsInt();

  int l_i = g_i - my_dom.sx;
  int l_j = g_j - my_dom.sy;
  int l_k = g_k - my_dom.sz;

  auto calc_stress_vector = [&](double e_x, double e_y, double e_z, double T[3]) {
    T[0] = T[1] = T[2] = 0.;
    for(int pp=0; pp<8; ++pp) {
      int g_pi = g_i+points[pp][0]; 
      int g_pj = g_j+points[pp][1]; 
      int g_pk = g_k+points[pp][2]; 
      for(int ii=0; ii<3; ++ii)
      for(int jj=0; jj<2; ++jj) // Only goes to 1 as n[2] == 0 anyway
      for(int kk=0; kk<3; ++kk)
      for(int ll=0; ll<3; ++ll) {
        T[ii] += tensor(ii,jj,kk,ll)                             // C_ijkl
               * u_array[g_pk][g_pj][g_pi].u[kk]                 // u^(p)_k
               * GradEta(pp,ll,e_x,e_y,e_z)/DOM.h[ll] * r_n[jj]; // d eta^(p)/d x_l * n_j
      }
    }
  };

  // Calculate the objective //
  auto F_integrand = [&](double e_x, double e_y, double e_z) {
    double T[3]; // Stress vector
    calc_stress_vector(e_x,e_y,e_z,T);
    // Pythagoras: TiTi is the square of the length of the stress vector
    //             TiNi is the length of Ti's normal component, thus
    // TiTi - TiNi*TiNi is the square of the length of Ti's tangential component
    double TiTi = 0.0;
    double TiNi = 0.0;
    for(int ii=0; ii<3; ++ii) {
      TiTi += T[ii]*T[ii];
      TiNi += T[ii]*r_n[ii];
    }
    double tau_sq    = TiTi - TiNi*TiNi;
    return TiNi*TiNi/(S_t*S_c) + (1./S_t+1./S_c)*TiNi + tau_sq/(S_s*S_s);
  };

  objective += area * SimpsonsRule([&](double e_z) {
    return GaussQuad5([&](double alpha) {
      return F_integrand(alpha*e_x[0]+(1.-alpha)*e_x[1],
                         alpha*e_y[0]+(1.-alpha)*e_y[1],
                         e_z);
    });
  });

  // Calculate dFdu //
  auto dFdu_integrand = [&](int pp, int kk, double e_x, double e_y, double e_z) {
    double T[3];
    calc_stress_vector(e_x,e_y,e_z,T);
    // Calculate dT
    double dT[3];
    for(int ii=0; ii<3; ++ii) {
      double v = 0.;
      for(int jj=0; jj<2; ++jj) // Only goes to 1 as n[2] == 0 anyway
      for(int ll=0; ll<3; ++ll) {
        v += tensor(ii,jj,kk,ll)
           * GradEta(pp,ll,e_x,e_y,e_z)/DOM.h[ll] * r_n[jj];
      }
      dT[ii] = v;
    }
    // Calculate the derivitive of tau_sq above
    double dTiTi = 0.0;
    double dTiNi = 0.0;
    double  TiNi = 0.0;
    double  TiTi = 0.0;
    for(int ii=0; ii<3; ++ii) {
      dTiTi += dT[ii]*T[ii];
      dTiNi += dT[ii]*r_n[ii];
       TiNi +=  T[ii]*r_n[ii];
       TiTi +=  T[ii]*T[ii];
    }
    double d_tau_sq = 2*dTiTi - 2*dTiNi*TiNi;
    return 2.*dTiNi*TiNi/(S_t*S_c) + (1./S_t+1./S_c)*dTiNi + d_tau_sq/(S_s*S_s);
  };

  for(int pp=0; pp<8; ++pp) {
    int g_pi = g_i+points[pp][0]; 
    int g_pj = g_j+points[pp][1]; 
    int g_pk = g_k+points[pp][2]; 
    for(int kk=0; kk<3; ++kk) {
      dFdu_array[g_pk][g_pj][g_pi].u[kk] += 
        area*SimpsonsRule([&](double e_z) {
          return GaussQuad5([&](double alpha) {
            return dFdu_integrand(pp, kk,
                                  alpha*e_x[0]+(1.-alpha)*e_x[1],
                                  alpha*e_y[0]+(1.-alpha)*e_y[1],
                                  e_z);
          });
        });
    }
  }

  // Calculate dFddesign
  auto dF_ddesign_integrand = [&](const Array4d<double> &dtensor, double e_x, double e_y, double e_z) {
    double T[3];
    calc_stress_vector(e_x,e_y,e_z,T);
    // Calculate dT
    double dT[3] = {0.0, 0.0, 0.0};
    for(int pp=0; pp<8; ++pp) {
      int g_pi = g_i+points[pp][0]; 
      int g_pj = g_j+points[pp][1]; 
      int g_pk = g_k+points[pp][2]; 
      for(int ii=0; ii<3; ++ii)
      for(int jj=0; jj<2; ++jj) // Only goes to 1 as n[2] == 0 anyway
      for(int kk=0; kk<3; ++kk)
      for(int ll=0; ll<3; ++ll) {
        dT[ii] += dtensor(ii,jj,kk,ll)                            // dC_ijkl
                * u_array[g_pk][g_pj][g_pi].u[kk]                 // u^(p)_k
                * GradEta(pp,ll,e_x,e_y,e_z)/DOM.h[ll] * r_n[jj]; // d eta^(p)/d x_l * n_j
      }
    }
    // Calculate the derivitive of tau_sq above
    double dTiTi = 0.0;
    double dTiNi = 0.0;
    double  TiNi = 0.0;
    double  TiTi = 0.0;
    for(int ii=0; ii<3; ++ii) {
      dTiTi += dT[ii]*T[ii];
      dTiNi += dT[ii]*r_n[ii];
       TiNi +=  T[ii]*r_n[ii];
       TiTi +=  T[ii]*T[ii];
    }
    double d_tau_sq = 2*dTiTi - 2*dTiNi*TiNi;
    return 2.*dTiNi*TiNi/(S_t*S_c) + (1./S_t+1./S_c)*dTiNi + d_tau_sq/(S_s*S_s);
  };

  static Array4d<double> dtensor;
  for(int d=0; d<D; ++d) {
    ierr =  tensor_func->EvalDTensor(element_design, my_info.it, d, dtensor);          CHKERRQ(ierr);
    local_dF_ddesign(d, l_i, l_j, l_k) =
      area*SimpsonsRule([&](double e_z) {
        return GaussQuad5([&](double alpha) {
          return dF_ddesign_integrand(dtensor,
                                      alpha*e_x[0]+(1.-alpha)*e_x[1],
                                      alpha*e_y[0]+(1.-alpha)*e_y[1],
                                      e_z);
        });
      });
  }

  PetscFunctionReturn(0);
}


PetscErrorCode Calculate3DInterfaceIntegrals(const JSONValue* obj_desc,
                                             const Array4d<double> &local_design,
                                             VecArrayVertex3D ***u_array,
                                             double &objective,
                                             VecArrayVertex3D ***dFdu_array,
                                             Array4d<double> &local_dF_ddesign) {
  // Calculates the shear stress integrals for the shear stress objective. Expects that
  // dFdu_array is zeroed before calling.
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  
  const int D = spec->ObjectMember("design dof")->AsInt();

  if(spec->ObjectHasMember("periodic") && spec->ObjectMember("periodic")->AsBool()) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE, "Interface objective assumes non-periodic boundaries");
  }

  // Get cylinder info
  struct {
    double r_centre[2];
    double r_radius;
    int g_k_lim[2];
  } desc;

  if(my_id==root_proc) {
    const JSONValue *surf_spec = obj_desc->ObjectMember("surface");
    if(surf_spec->ObjectMember("type")->AsString() != "cylinder") {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE, "Shear stress objective surface type must be cylinder");
    }
    std::vector<int>    g_k_lim  = surf_spec->ObjectMember("k limits")->ArrayAsVectorOfInts(2);
    desc.r_centre[0] = surf_spec->ObjectMember("centre")  ->ArrayMember(0)->AsDouble();
    desc.r_centre[1] = surf_spec->ObjectMember("centre")  ->ArrayMember(1)->AsDouble();
    desc.g_k_lim[0]  = surf_spec->ObjectMember("k limits")->ArrayMember(0)->AsDouble();
    desc.g_k_lim[1]  = surf_spec->ObjectMember("k limits")->ArrayMember(1)->AsDouble();
    desc.r_radius    = surf_spec->ObjectMember("radius")  ->AsDouble();
    if(desc.g_k_lim[0] >= desc.g_k_lim[1] || desc.g_k_lim[0] < 0 || desc.g_k_lim[1] >= DOM.K_el) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE, "Shear stress objective cylinder k limits invalid");
    }
  }

  MPI_CALL(MPI_Bcast(desc.r_centre, 2,MPI_DOUBLE,root_proc,PETSC_COMM_WORLD));
  MPI_CALL(MPI_Bcast(&desc.r_radius,1,MPI_DOUBLE,root_proc,PETSC_COMM_WORLD));
  MPI_CALL(MPI_Bcast(desc.g_k_lim,  2,MPI_INT,   root_proc,PETSC_COMM_WORLD));
  
  desc.g_k_lim[0] = std::max(desc.g_k_lim[0], my_dom.sz);
  desc.g_k_lim[1] = std::min(desc.g_k_lim[1], my_dom.sz+my_dom.nz-1); // Inclusive bounds
  
  // Initialise tensor
  std::unique_ptr<TensorBase> tensor;
  ierr =  GetTensor(spec, tensor);                                                     CHKERRQ(ierr);
  std::vector<double> element_design;

  // Initialise output
  local_dF_ddesign.Resize(D, my_dom.nx, my_dom.ny, my_dom.nz);
  std::ofstream dump_file;
  std::ofstream *dump_file_ptr=nullptr;

  enum {
    SHEAR_STRESS,
    HOFFMAN,
  } integrand_type;

  struct {
    double S_t,S_c,S_s;
  } hoffman_info;
   
  struct {
    double m;
  } shear_info;

  if(my_id==root_proc) {
    const std::string name = obj_desc->ObjectMember("integrand")->ObjectMember("type")->AsString();
    if(name=="shear stress") {
      integrand_type = SHEAR_STRESS;
      shear_info.m = obj_desc->ObjectMember("integrand")->ObjectMember("attenuation parameter")->AsDouble();
    } else if(name=="Hoffman criterion") {
      integrand_type = HOFFMAN;
      hoffman_info.S_t = obj_desc->ObjectMember("integrand")->ObjectMember(    "tensile strength")->AsDouble();
      hoffman_info.S_c = obj_desc->ObjectMember("integrand")->ObjectMember("compressive strength")->AsDouble();
      hoffman_info.S_s = obj_desc->ObjectMember("integrand")->ObjectMember(      "shear strength")->AsDouble();
    } else {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
        "unknown integrand type for interface objective: %s",name.c_str());
    }
  }
  MPI_CALL(MPI_Bcast(&integrand_type,1,MPI_INT,root_proc,PETSC_COMM_WORLD));
  std::stringstream name;
  char buff[STR_BUFF_SIZE];
  PetscBool was_set;
  switch(integrand_type) {
    case SHEAR_STRESS:
      MPI_CALL(MPI_Bcast(&shear_info.m,1,MPI_DOUBLE,root_proc,PETSC_COMM_WORLD));
      ierr =  PetscOptionsGetString(NULL,NULL,"-save_shear_stress_values",buff,STR_BUFF_SIZE,&was_set);
                                                                                       CHKERRQ(ierr);
      if(was_set) {
        name << buff << '_' << my_id << ".dat";
        dump_file.open(name.str(),std::ios_base::trunc);
        dump_file_ptr = &dump_file;
      }
      break;
    case HOFFMAN:
      MPI_CALL(MPI_Bcast(&hoffman_info.S_t,1,MPI_DOUBLE,root_proc,PETSC_COMM_WORLD));
      MPI_CALL(MPI_Bcast(&hoffman_info.S_c,1,MPI_DOUBLE,root_proc,PETSC_COMM_WORLD));
      MPI_CALL(MPI_Bcast(&hoffman_info.S_s,1,MPI_DOUBLE,root_proc,PETSC_COMM_WORLD));
      break;
  }

  // Loop over each element - brute force search for the elements that contain the circle 
  objective = 0.0;
  for(int g_i=my_dom.sx; g_i<my_dom.sx+my_dom.nx; g_i++)
  for(int g_j=my_dom.sy; g_j<my_dom.sy+my_dom.ny; g_j++) {
    if(g_i>=DOM.I_el || g_j>=DOM.J_el) continue;

    // Determine crossing points
    bool does_cross;
    double r_length;
    double e_x[2], e_y[2], r_n[3] = {0., 0., 0.};
    ierr =  DetermineCylinderCrossingPoints(g_i,g_j,desc.r_centre,desc.r_radius,
                                            does_cross,e_x,e_y,r_n,r_length);          CHKERRQ(ierr);
    if(!does_cross) continue;
    
    for(int g_k=desc.g_k_lim[0]; g_k<=desc.g_k_lim[1]; g_k++) {
      ierr =  CopyDesignVariables(local_design,
                                  g_i-my_dom.sx+1, g_j-my_dom.sy+1, g_k-my_dom.sz+1,
                                  element_design);                                     CHKERRQ(ierr);
      switch(integrand_type) {
        case SHEAR_STRESS:
          ierr =  ShearStressElementIntegrals3D(dump_file_ptr,
                                                shear_info.m,
                                                tensor,&element_design[0],
                                                u_array,
                                                g_i,g_j,g_k,
                                                e_x,e_y,r_n,r_length,
                                                objective,
                                                dFdu_array,
                                                local_dF_ddesign);                     CHKERRQ(ierr);
          break;
        case HOFFMAN:
          ierr =  HoffmanElementIntegrals3D(hoffman_info.S_t,hoffman_info.S_c,hoffman_info.S_s,
                                            tensor,&element_design[0],
                                            u_array,
                                            g_i,g_j,g_k,
                                            e_x,e_y,r_n,r_length,
                                            objective,
                                            dFdu_array,
                                            local_dF_ddesign);                         CHKERRQ(ierr);
          break;
      }
    }
  }
  PetscFunctionReturn(0);
}
 

PetscErrorCode ShearStressEdgeIntegrals2D(const std::unique_ptr<TensorBase> &tensor_func,
                                          const Array4d<double> &local_design,
                                          VecArrayVertex2D **u_array,
                                          int g_i, int g_j, // Of lower node
                                          double m,
                                          double &objective,
                                          VecArrayVertex2D **dFdu_array,
                                          Array4d<double> &local_dF_ddesign) {
  // Calculates the shear stress integrals for a given vertical line segment that  has
  // a lower node at g_i, g_j.
  //
  // As the stress is dis-continuous over element boundaries we average the stress on
  // either side prior to evaluating the integrand.
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  const int l_i = g_i - my_dom.sx; // of lower node / right element
  const int l_j = g_j - my_dom.sy;

  static std::vector<double> element_design_L, element_design_R;
  ierr =  CopyDesignVariables(local_design,l_i  ,l_j+1,0,element_design_L);            CHKERRQ(ierr);
  ierr =  CopyDesignVariables(local_design,l_i+1,l_j+1,0,element_design_R);            CHKERRQ(ierr);

  static Array4d<double> tensor_L, tensor_R;
  ierr =  tensor_func->EvalTensor(&element_design_L[0], my_info.it, tensor_L);           CHKERRQ(ierr);
  ierr =  tensor_func->EvalTensor(&element_design_R[0], my_info.it, tensor_R);           CHKERRQ(ierr);

  auto tau_L = [&](double alpha, const Array4d<double> &tensor_L) {
    double tau = 0;
    for(int pp=0; pp<4; ++pp) {
      int g_pi = g_i+points[pp][0] - 1;
      int g_pj = g_j+points[pp][1]; 
      for(int ii=0; ii<2; ++ii)
      for(int jj=0; jj<2; ++jj) {
        tau += tensor_L(0,1,ii,jj)*GradEta(pp,jj,1.0,alpha,0)/DOM.h[jj]*u_array[g_pj][g_pi].u[ii];
      }
    }
    return tau;
  };

  auto tau_R = [&](double alpha, const Array4d<double> &tensor_R) {
    double tau = 0;
    for(int pp=0; pp<4; ++pp) {
      int g_pi = g_i+points[pp][0];
      int g_pj = g_j+points[pp][1];
      for(int ii=0; ii<2; ++ii)
      for(int jj=0; jj<2; ++jj) {
        tau += tensor_R(0,1,ii,jj)*GradEta(pp,jj,0.0,alpha,0)/DOM.h[jj]*u_array[g_pj][g_pi].u[ii];
      }
    }
    return tau;
  };

  // Objective
  auto F_integrand = [&](double alpha) {
    double tL = std::abs(tau_L(alpha,tensor_L));
    double tR = std::abs(tau_R(alpha,tensor_R));
    return std::pow((tL+tR)/2.0,2*m);
  };
  
  objective += SimpsonsRule(F_integrand) * DOM.h[1];

  // dFdu
  auto dFdu_integrand_L = [&](int pp, int ii, double alpha) {
    double t = (tau_L(alpha,tensor_L) + tau_R(alpha,tensor_R))/2.0;
    double dt = 0;
    for(int jj=0; jj<2; ++jj) {
      dt += tensor_L(0,1,ii,jj)*GradEta(pp,jj,1.0,alpha,0)/DOM.h[jj];
    }
    dt /= 2.0;
    return 2.0*m*dt*std::pow(t,2*m-1);
  };
  
  auto dFdu_integrand_R = [&](int pp, int ii, double alpha) {
    double t = (tau_L(alpha,tensor_L) + tau_R(alpha,tensor_R))/2.0;
    double dt = 0;
    for(int jj=0; jj<2; ++jj) {
      dt += tensor_R(0,1,ii,jj)*GradEta(pp,jj,0.0,alpha,0)/DOM.h[jj];
    }
    dt /= 2.0;
    return 2*m*dt*std::pow(t,2*m-1);
  };

  for(int pp=0; pp<4; ++pp) {
    int g_pi = g_i+points[pp][0]; // Global index for considered node in right element
    int g_pj = g_j+points[pp][1];
    for(int ii=0; ii<2; ++ii) {
      dFdu_array[g_pj][g_pi-1].u[ii] += 
        DOM.h[1]*SimpsonsRule([&](double a) { return dFdu_integrand_L(pp,ii,a); });

      dFdu_array[g_pj][g_pi  ].u[ii] += 
        DOM.h[1]*SimpsonsRule([&](double a) { return dFdu_integrand_R(pp,ii,a); });
    }
  }

  // dFddesign
  auto dFddL_integrand = [&](const Array4d<double> &dtensor_L, double alpha) {
    // Calculated dF/d_design
    double t = (tau_L(alpha,tensor_L) + tau_R(alpha,tensor_R))/2.0;
    double dt = tau_L(alpha,dtensor_L)/2.0;
    return 2*m*dt*std::pow(t,2*m-1);
  };

  auto dFddR_integrand = [&](const Array4d<double> &dtensor_R, double alpha) {
    // Calculated dF/d_design
    double t = (tau_L(alpha,tensor_L) + tau_R(alpha,tensor_R))/2.0;
    double dt = tau_R(alpha,dtensor_R)/2.0;
    return 2*m*dt*std::pow(t,2*m-1);
  };

  static Array4d<double> dtensor;
  const int D = spec->ObjectMember("design dof")->AsInt();
  for(int d=0; d<D; ++d) {
    ierr =  tensor_func->EvalDTensor(&element_design_L[0], my_info.it, d, dtensor);      CHKERRQ(ierr);
    local_dF_ddesign(d, l_i-1, l_j, 0) =
      SimpsonsRule([&](double a) { return dFddL_integrand(dtensor,a); });
    
    ierr =  tensor_func->EvalDTensor(&element_design_R[0], my_info.it, d, dtensor);      CHKERRQ(ierr);
    local_dF_ddesign(d, l_i,   l_j, 0) =
      SimpsonsRule([&](double a) { return dFddR_integrand(dtensor,a); });
  }

  PetscFunctionReturn(0);
}


PetscErrorCode Calculate2DShearStressIntegrals(const Array4d<double> &local_design,
                                               VecArrayVertex2D **u_array,
                                               double &objective,
                                               VecArrayVertex2D **dFdu_array,
                                               Array4d<double> &local_dF_ddesign) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  
  const double m = spec->ObjectMember("objective function")
                       ->ObjectMember("integrand")
                       ->ObjectMember("attenuation parameter")
                       ->AsDouble();

  const int design_dof = spec->ObjectMember("design dof")->AsInt();

  std::unique_ptr<TensorBase> tensor;
  ierr =  GetTensor(spec, tensor);                                                     CHKERRQ(ierr);

  const JSONValue *surf_spec = spec->ObjectMember("objective function")->ObjectMember("surface");
  
  if(surf_spec->ObjectMember("type")->AsString() != "vertical") {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE, "Shear stress objective surface type must be \"vertical\"");
  }

  std::vector<int> i_indices = surf_spec->ObjectMember("i indices")->ArrayAsVectorOfInts();
  std::vector<int> j_limits  = surf_spec->ObjectMember("j limits")->ArrayAsVectorOfInts(2);
  if(j_limits[0] < 0 || j_limits[1]>DOM.J-1) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"2d shear stress function: j indices out of range");
  }
  j_limits[0] = std::max(j_limits[0], my_dom.sy);
  j_limits[1] = std::min(j_limits[1], my_dom.sy+my_dom.ny-1); // Inclusive bounds

  // Initialise output
  local_dF_ddesign.Resize(design_dof, my_dom.nx, my_dom.ny, my_dom.nz);
  local_dF_ddesign.Set(0);
  objective = 0;

  for(size_t i=0; i<i_indices.size(); ++i) {
    if(i_indices[i] <= 0 || i_indices[i] >= DOM.I - 1) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"2d shear stress function: i index out of range");
    }
    if(i_indices[i] < my_dom.sx || i_indices[i] >= my_dom.sx+my_dom.nx) continue;

    for(int g_j=j_limits[0]; g_j<j_limits[1]; ++g_j) {
      ierr =  ShearStressEdgeIntegrals2D(tensor, local_design, u_array, i_indices[i], g_j, m,
                                         objective, dFdu_array, local_dF_ddesign);     CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}


PetscErrorCode PostSolveInterfaceObjective(const JSONValue* obj_desc,
                                           bool calculate_derivatives,
                                           const Array4d<double> &local_design,
                                           double &objective,
                                           Array4d<double> &d_design) {
  // Calculates the shear stress objective. The real bulk of the work is done in
  // CalculateShearStressIntegrals.
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  if(spec->ObjectHasMember("periodic") && spec->ObjectMember("periodic")->AsBool()) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
            "Shear stress objective cannot be used on a periodic domain");
  }

  // Create our resources
  static Array4d<double> local_del_F_del_design, local_minus_qdKu;

  Vec dFdu_local;
  ierr =  DMCreateLocalVector (dm_context, &dFdu_local);                               CHKERRQ(ierr);
  ierr =  VecSet(dFdu_local, 0.0);                                                     CHKERRQ(ierr);

  // Calculate F, dF/du and dF/ddesign
  double local_objective = 0.0;
  if(DOM.dof==2) {
    VecArrayVertex2D **u_array, **dFdu_array;
    ierr =  DMDAVecGetArray(dm_context, dFdu_local, &dFdu_array);                      CHKERRQ(ierr);
    ierr =  DMDAVecGetArray(dm_context, local_u_dm_vec[0], &u_array);                  CHKERRQ(ierr);
    ierr =  Calculate2DShearStressIntegrals(local_design, u_array,
                                            local_objective,
                                            dFdu_array, local_del_F_del_design);       CHKERRQ(ierr);
    ierr =  DMDAVecRestoreArray(dm_context, dFdu_local, &dFdu_array);                  CHKERRQ(ierr);
    ierr =  DMDAVecRestoreArray(dm_context, local_u_dm_vec[0], &u_array);              CHKERRQ(ierr);
  } else {
    VecArrayVertex3D ***u_array, ***dFdu_array;
    ierr =  DMDAVecGetArray(dm_context, dFdu_local, &dFdu_array);                      CHKERRQ(ierr);
    ierr =  DMDAVecGetArray(dm_context, local_u_dm_vec[0], &u_array);                  CHKERRQ(ierr);
    ierr =  Calculate3DInterfaceIntegrals(obj_desc,
                                          local_design, u_array,
                                          local_objective,
                                          dFdu_array, local_del_F_del_design);         CHKERRQ(ierr);
    ierr =  DMDAVecRestoreArray(dm_context, dFdu_local, &dFdu_array);                  CHKERRQ(ierr);
    ierr =  DMDAVecRestoreArray(dm_context, local_u_dm_vec[0], &u_array);              CHKERRQ(ierr);
  }

  // Calculate the full derivitives with this info
  if(calculate_derivatives) {
    ierr =  CalculateFullDerivatives(local_design, dFdu_local, local_del_F_del_design, d_design);
                                                                                       CHKERRQ(ierr);
  }
  ierr =  VecDestroy(&dFdu_local);                                                     CHKERRQ(ierr);
  
  MPI_CALL(MPI_Reduce(&local_objective, &objective,
                      1, MPI_DOUBLE, MPI_SUM,
                      root_proc, PETSC_COMM_WORLD))
  PetscFunctionReturn(0);
}


PetscErrorCode PostSolveCalculateResorption(bool calculate_derivatives,
                                            double eps,
                                            double discount_factor,
                                            const Array4d<double> &local_design,
                                            const Array3d<double> &local_bone_mask,
                                            const Array3d<double> &local_reference_energy,
                                            double &resorption_frac,
                                            Array4d<double> &d_design) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  static Array4d<double> local_del_F_del_design;

  auto heaviside = [&](double x) {
    return 0.5+0.5*std::tanh(x/eps);
  };

  auto dirac = [&](double x) {
    return 1./(2*eps*std::cosh(x/eps)*std::cosh(x/eps));
  };

  Vec dFdu_local;
  ierr =  DMCreateLocalVector (dm_context, &dFdu_local);                               CHKERRQ(ierr);
  ierr =  VecSet(dFdu_local, 0.0);                                                     CHKERRQ(ierr);
  
  const int D = spec->ObjectMember("design dof")->AsInt();
  
  std::unique_ptr<TensorBase> tensor;
  ierr =  GetTensor(spec, tensor);                                                     CHKERRQ(ierr);
  std::vector<double> element_design;
  Array4d<double> element_tensor;

  // Calculate the volume of bone (in elements)
  int num_bone_el=0, local_num_el=0;
  for(int i=0; i<my_dom.nx; i++)
  for(int j=0; j<my_dom.ny; j++)
  for(int k=0; k<my_dom.nz; k++) {
    if(i+my_dom.sx>=DOM.I_el || j+my_dom.sy>=DOM.J_el || k+my_dom.sz>=DOM.K_el) continue;
    if(BoolFromDouble(local_bone_mask(i+1, j+1, k+1))) {
      local_num_el += 1;
    }
  }
  MPI_CALL(MPI_Allreduce(&local_num_el, &num_bone_el, 1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD))

  // Calculate the resorption and partial derivatives
  double local_resorption = 0.0;
  local_del_F_del_design.Resize(D,my_dom.nx,my_dom.ny,my_dom.nz);

  if(DOM.dof==2) {
    VecArrayVertex2D **u_array, **dFdu_array;
    ierr =  DMDAVecGetArray(dm_context, dFdu_local, &dFdu_array);                      CHKERRQ(ierr);
    ierr =  DMDAVecGetArray(dm_context, local_u_dm_vec[0], &u_array);                  CHKERRQ(ierr);
    
    for(int i=0; i<my_dom.nx; i++)
    for(int j=0; j<my_dom.ny; j++) {
      if(i+my_dom.sx>=DOM.I_el || j+my_dom.sy>=DOM.J_el) continue;

      if(!BoolFromDouble(local_bone_mask(i+1, j+1, 1))) {
        for(int d=0; d<D; ++d) { local_del_F_del_design(d,i,j,0) = 0.0; }
        continue;
      }
      
      ierr =  CopyDesignVariables(local_design, i+1, j+1, 1, element_design);          CHKERRQ(ierr);
      ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);      CHKERRQ(ierr);
      // Calculate energy and dUdu
      double element_energy = 0.;
      double dUdu[4][2] = {{0,0},{0,0},{0,0},{0,0}};
      for(int p=0;  p<4;  p++)
      for(int q=0;  q<4;  q++) {
        int pi = i+my_dom.sx+points[p][0]; 
        int pj = j+my_dom.sy+points[p][1]; 
        int qi = i+my_dom.sx+points[q][0]; 
        int qj = j+my_dom.sy+points[q][1]; 
        for(int ii=0; ii<2; ii++)
        for(int jj=0; jj<2; jj++)
        for(int kk=0; kk<2; kk++)
        for(int ll=0; ll<2; ll++) {
          element_energy += 0.5
                          * u_array[pj][pi].u[ii]
                          * u_array[qj][qi].u[kk]
                          * DOM.grad_eta_grad_eta_int[p][jj][q][ll]
                          * element_tensor(ii,jj,kk,ll);
          
          dUdu[p][ii] += u_array[qj][qi].u[kk]
                       * DOM.grad_eta_grad_eta_int[p][jj][q][ll]
                       * element_tensor(ii,jj,kk,ll)
                       / num_bone_el;
        }
      }
      // The following rearrangement is the same in the limit as eps->0 (true Heaviside)
      // however for finite eps the smearing amount is proportional to the local 
      // reference energy. That is h(s Ur - U) -> h(s - U/Ur)
      double element_ref = local_reference_energy(i+1,j+1,1);
      double arg = discount_factor - element_energy/element_ref;
      local_resorption += heaviside(arg)/num_bone_el;

      // Calculate del F/del u
      for(int p=0;  p<4;  p++) {
        int pi = i+my_dom.sx+points[p][0];
        int pj = j+my_dom.sy+points[p][1];
        for(int ii=0; ii<2; ii++) {
          dFdu_array[pj][pi].u[ii] -= dUdu[p][ii] / element_ref * dirac(arg);
        }
      }

      // Calculate del F/del design
      for(int d=0; d<D; ++d) {
        double element_energy_d_design = 0.;
        ierr =  tensor->EvalDTensor(&element_design[0], my_info.it, d, element_tensor);CHKERRQ(ierr);
        for(int p=0;  p<4;  p++)
        for(int q=0;  q<4;  q++) {
          int pi = i+my_dom.sx+points[p][0]; 
          int pj = j+my_dom.sy+points[p][1]; 
          int qi = i+my_dom.sx+points[q][0]; 
          int qj = j+my_dom.sy+points[q][1]; 
          for(int ii=0; ii<2; ii++)
          for(int jj=0; jj<2; jj++)
          for(int kk=0; kk<2; kk++)
          for(int ll=0; ll<2; ll++) {
            element_energy_d_design += 0.5
                                     * u_array[pj][pi].u[ii]
                                     * u_array[qj][qi].u[kk]
                                     * DOM.grad_eta_grad_eta_int[p][jj][q][ll]
                                     * element_tensor(ii,jj,kk,ll)
                                     / num_bone_el;
          }
        }
        local_del_F_del_design(d,i,j,0) = - element_energy_d_design / element_ref * dirac(arg);
      }
    }
    ierr =  DMDAVecRestoreArray(dm_context, dFdu_local, &dFdu_array);                  CHKERRQ(ierr);
    ierr =  DMDAVecRestoreArray(dm_context, local_u_dm_vec[0], &u_array);              CHKERRQ(ierr);

  } else {
    VecArrayVertex3D ***u_array, ***dFdu_array;
    ierr =  DMDAVecGetArray(dm_context, dFdu_local, &dFdu_array);                      CHKERRQ(ierr);
    ierr =  DMDAVecGetArray(dm_context, local_u_dm_vec[0], &u_array);                  CHKERRQ(ierr);
    
    for(int i=0; i<my_dom.nx; i++)
    for(int j=0; j<my_dom.ny; j++)
    for(int k=0; k<my_dom.nz; k++) {
      if(i+my_dom.sx>=DOM.I_el || j+my_dom.sy>=DOM.J_el || k+my_dom.sz>=DOM.K_el) continue;

      if(!BoolFromDouble(local_bone_mask(i+1, j+1, k+1))) {
        for(int d=0; d<D; ++d) { local_del_F_del_design(d,i,j,k) = 0.0; }
        continue;
      }
      
      ierr =  CopyDesignVariables(local_design, i+1, j+1, k+1, element_design);        CHKERRQ(ierr);
      ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);      CHKERRQ(ierr);
      // Calculate energy and dUdu
      double element_energy = 0.;
      double dUdu[8][3] = {{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
      for(int p=0;  p<8;  p++)
      for(int q=0;  q<8;  q++) {
        int pi = i+my_dom.sx+points[p][0]; 
        int pj = j+my_dom.sy+points[p][1]; 
        int pk = k+my_dom.sz+points[p][2]; 
        int qi = i+my_dom.sx+points[q][0]; 
        int qj = j+my_dom.sy+points[q][1]; 
        int qk = k+my_dom.sz+points[q][2]; 
        for(int ii=0; ii<3; ii++)
        for(int jj=0; jj<3; jj++)
        for(int kk=0; kk<3; kk++)
        for(int ll=0; ll<3; ll++) {
          element_energy += 0.5
                          * u_array[pk][pj][pi].u[ii]
                          * u_array[qk][qj][qi].u[kk]
                          * DOM.grad_eta_grad_eta_int[p][jj][q][ll]
                          * element_tensor(ii,jj,kk,ll);
          
          dUdu[p][ii] += u_array[qk][qj][qi].u[kk]
                       * DOM.grad_eta_grad_eta_int[p][jj][q][ll]
                       * element_tensor(ii,jj,kk,ll)
                       / num_bone_el;
        }
      }
      // The following rearrangement is the same in the limit as eps->0 (true Heaviside)
      // however for finite eps the smearing amount is proportional to the local 
      // reference energy. That is h(s Ur - U) -> h(s - U/Ur)
      double element_ref = local_reference_energy(i+1,j+1,k+1);
      double arg = discount_factor - element_energy/element_ref;
      local_resorption += heaviside(arg)/num_bone_el;

      // Calculate del F/del u
      for(int p=0;  p<8;  p++) {
        int pi = i+my_dom.sx+points[p][0];
        int pj = j+my_dom.sy+points[p][1];
        int pk = k+my_dom.sz+points[p][2];
        for(int ii=0; ii<3; ii++) {
          dFdu_array[pk][pj][pi].u[ii] -= dUdu[p][ii] / element_ref * dirac(arg);
        }
      }

      // Calculate del F/del design
      for(int d=0; d<D; ++d) {
        double element_energy_d_design = 0.;
        ierr =  tensor->EvalDTensor(&element_design[0], my_info.it, d, element_tensor);CHKERRQ(ierr);
        for(int p=0;  p<8;  p++)
        for(int q=0;  q<8;  q++) {
          int pi = i+my_dom.sx+points[p][0]; 
          int pj = j+my_dom.sy+points[p][1]; 
          int pk = k+my_dom.sz+points[p][2]; 
          int qi = i+my_dom.sx+points[q][0]; 
          int qj = j+my_dom.sy+points[q][1]; 
          int qk = k+my_dom.sz+points[q][2]; 
          for(int ii=0; ii<3; ii++)
          for(int jj=0; jj<3; jj++)
          for(int kk=0; kk<3; kk++)
          for(int ll=0; ll<3; ll++) {
            element_energy_d_design += 0.5
                                     * u_array[pk][pj][pi].u[ii]
                                     * u_array[qk][qj][qi].u[kk]
                                     * DOM.grad_eta_grad_eta_int[p][jj][q][ll]
                                     * element_tensor(ii,jj,kk,ll)
                                     / num_bone_el;
          }
        }
        local_del_F_del_design(d,i,j,k) = - element_energy_d_design / element_ref * dirac(arg);
      }
    }
    ierr =  DMDAVecRestoreArray(dm_context, dFdu_local, &dFdu_array);                  CHKERRQ(ierr);
    ierr =  DMDAVecRestoreArray(dm_context, local_u_dm_vec[0], &u_array);              CHKERRQ(ierr);
  }
  MPI_CALL(MPI_Reduce(&local_resorption, &resorption_frac,
                      1, MPI_DOUBLE, MPI_SUM,
                      root_proc, PETSC_COMM_WORLD))

  // Calculate the full dereivitives and reduce value
  if(calculate_derivatives) {
    ierr =  CalculateFullDerivatives(local_design, dFdu_local, local_del_F_del_design, d_design);
                                                                                       CHKERRQ(ierr);
  }
  ierr =  VecDestroy(&dFdu_local);                                                     CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode PostSolveHomogenization(const Array4d<double> &local_design,
                                       Array2d<double> &E_star) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  E_star.Resize(6,6);
  E_star.Set(0.0);
  Array2d<double> my_E(6,6);
  my_E.Set(0.0);
  
  std::unique_ptr<TensorBase> tensor;
  ierr =  GetTensor(spec, tensor);                                                     CHKERRQ(ierr);
  std::vector<double> element_design;
  Array4d<double> element_tensor;

  if(DOM.dof==2) {
    for(int i_test=0; i_test<3; ++i_test) {
      double avg_stiffness[3] = {0.0};

      for(int i=0; i<my_dom.nx; ++i)
      for(int j=0; j<my_dom.ny; ++j) {
        for(unsigned v=0; v<3; ++v) {
          ierr =  CopyDesignVariables(local_design, i+1, j+1, 0, element_design);      CHKERRQ(ierr);
          ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);  CHKERRQ(ierr);

          avg_stiffness[v] += element_tensor(voigt2d[v     ][0],voigt2d[v     ][1],
                                             voigt2d[i_test][0],voigt2d[i_test][1]) / (DOM.I*DOM.J);
        }
      }
      
      // Calculate avg strain
      VecArrayVertex2D **u_array;
      ierr =  DMDAVecGetArray(dm_context, local_u_dm_vec[i_test], &u_array);           CHKERRQ(ierr);

      for(unsigned v=0; v<3; ++v) {
        double stress_total = 0.0;
        for(int i=0; i<my_dom.nx; ++i)
        for(int j=0; j<my_dom.ny; ++j) {
          for(int p=0; p<4; ++p)
          for(int d=0; d<2; ++d)
          for(int e=0; e<2; ++e) {
            int gip = i+points[p][0]+my_dom.sx;
            int gjp = j+points[p][1]+my_dom.sy;
            ierr =  CopyDesignVariables(local_design, i+1, j+1, 0, element_design);    CHKERRQ(ierr);
            ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);CHKERRQ(ierr);
            
            stress_total += element_tensor(voigt2d[v][0], voigt2d[v][1], d,e)
                           * u_array[gjp][gip].u[d]
                           * DOM.grad_eta_int[p][e];
          }
        }
        my_E(i_test, v) = avg_stiffness[v] + stress_total/(DOM.I*DOM.J*DOM.h[0]*DOM.h[1]);
      }

      ierr =  DMDAVecRestoreArray(dm_context, local_u_dm_vec[i_test], &u_array);       CHKERRQ(ierr);
    }
  } else {
    for(int i_test=0; i_test < 6; ++i_test) {
      double avg_stiffness[6] = {0.0};
      for(int i=0; i<my_dom.nx; ++i)
      for(int j=0; j<my_dom.ny; ++j)
      for(int k=0; k<my_dom.nz; ++k) {
        for(unsigned v=0; v<6; ++v) {
          ierr =  CopyDesignVariables(local_design, i+1, j+1, k+1, element_design);    CHKERRQ(ierr);
          ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);  CHKERRQ(ierr);

          avg_stiffness[v] += element_tensor(voigt[v][0],     voigt[v][1],
                                             voigt[i_test][0],voigt[i_test][1]) / (DOM.I*DOM.J*DOM.K);
        }
      }

      // Calculate avg strain
      VecArrayVertex3D ***u_array;
      ierr =  DMDAVecGetArray(dm_context, local_u_dm_vec[i_test], &u_array);           CHKERRQ(ierr);

      for(unsigned v=0; v<6; ++v) {
        double stress_total = 0.0;
        for(int i=0; i<my_dom.nx; ++i)
        for(int j=0; j<my_dom.ny; ++j)
        for(int k=0; k<my_dom.nz; ++k) {
          for(int p=0; p<8; ++p)
          for(int d=0; d<3; ++d)
          for(int e=0; e<3; ++e) {
            int gip = i+points[p][0]+my_dom.sx;
            int gjp = j+points[p][1]+my_dom.sy;
            int gkp = k+points[p][2]+my_dom.sz;

            ierr =  CopyDesignVariables(local_design, i+1, j+1, k+1, element_design);  CHKERRQ(ierr);
            ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);CHKERRQ(ierr);
            stress_total += element_tensor(voigt[v][0], voigt[v][1], d,e)
                           * u_array[gkp][gjp][gip].u[d]
                           * DOM.grad_eta_int[p][e];
          }
        }
        my_E(i_test, v) = avg_stiffness[v] + stress_total/(DOM.I*DOM.J*DOM.K*DOM.h[0]*DOM.h[1]*DOM.h[2]);
      }

      ierr =  DMDAVecRestoreArray(dm_context, local_u_dm_vec[i_test], &u_array);       CHKERRQ(ierr);
    }
  }
  
  MPI_CALL(MPI_Reduce(&my_E(0,0),       // send_data
                      &E_star(0,0),     // recv_data
                      6*6,              // count
                      MPI_DOUBLE,       // datatype
                      MPI_SUM,          // op
                      root_proc,        // root
                      MPI_COMM_WORLD)); // communicator

  PetscFunctionReturn(0);
}
 


// *** Process Management *** //

PetscErrorCode DoFemWork(int load_case, const Array4d<double> &local_design) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  static Array4d<double> local_u;                      // Static so we only allocate once
  static Array4d<char>   local_fixed;
  local_u.Resize    (3, my_dom.nx+2, my_dom.ny+2, my_dom.nz+2); // Keep room for ghost points
  local_u.Set(0.0);
  local_fixed.Resize(3, my_dom.nx+2, my_dom.ny+2, my_dom.nz+2);
  local_fixed.Set(false);

  my_info.load_case   = load_case;
  my_info.local_u     = &local_u;
  my_info.local_fixed = &local_fixed;

  ierr =  ApplyDirichletBCs(load_case, local_fixed, local_u);                          CHKERRQ(ierr);

  // Solve
  ierr =  KSPCreate(PETSC_COMM_WORLD, &ksp_context);                                   CHKERRQ(ierr);
  ierr =  KSPSetDM              (ksp_context,dm_context);                              CHKERRQ(ierr);

  ierr =  KSPSetComputeRHS      (ksp_context,&ComputeRHS,NULL);                        CHKERRQ(ierr);
  ierr =  KSPSetComputeOperators(ksp_context,&ComputeMatrix,NULL);                     CHKERRQ(ierr);
  ierr =  KSPSetFromOptions     (ksp_context);                                         CHKERRQ(ierr);
  ierr =  ApplyMultiGridInterp  (ksp_context);                                         CHKERRQ(ierr);
  ierr =  KSPSetUp              (ksp_context);                                         CHKERRQ(ierr);
  ierr =  KSPSolve              (ksp_context, NULL, NULL);                             CHKERRQ(ierr);

  // Check if the solution converged
  KSPConvergedReason reason;
  ierr =  KSPGetConvergedReason(ksp_context, &reason);                                 CHKERRQ(ierr);
  if(reason<0) {
    PetscReal rtol, abstol, dtol;
    PetscInt maxits;
    ierr =  KSPGetTolerances(ksp_context, &rtol, &abstol, &dtol, &maxits);             CHKERRQ(ierr);
    const char *str = ConvergedReasonString(reason);
    ierr =  PetscPrintf(PETSC_COMM_WORLD, "PetscFEM did not converge, %s\n", str);     CHKERRQ(ierr);
    ierr =  PetscPrintf(PETSC_COMM_WORLD, "  rtol: %e abstol: %e dtol: %e, maxits: %d\n",
                                             rtol,    abstol,    dtol,     maxits);    CHKERRQ(ierr);
    PetscReal res;
    PetscInt its;
    ierr =  KSPGetResidualNorm   (ksp_context, &res);                                  CHKERRQ(ierr);
    ierr =  KSPGetIterationNumber(ksp_context, &its);                                  CHKERRQ(ierr);
    ierr =  PetscPrintf(PETSC_COMM_WORLD, "  its: %d\n res: %e\n", its,res);           CHKERRQ(ierr);
  }

  // Put s and u into the buffer
  Vec u, s;
  ierr =  KSPGetSolution(ksp_context,&u);                                              CHKERRQ(ierr);
  ierr =  DMGlobalToLocalBegin(dm_context, u, INSERT_VALUES, local_u_dm_vec[load_case]);CHKERRQ(ierr);
  ierr =  DMGlobalToLocalEnd(dm_context, u, INSERT_VALUES, local_u_dm_vec[load_case]); CHKERRQ(ierr);

  ierr =  KSPGetRhs(ksp_context,&s);                                                   CHKERRQ(ierr);
  ierr =  DMGlobalToLocalBegin(dm_context, s, INSERT_VALUES, local_s_dm_vec[load_case]);CHKERRQ(ierr);
  ierr =  DMGlobalToLocalEnd(dm_context, s, INSERT_VALUES, local_s_dm_vec[load_case]); CHKERRQ(ierr);
  
  ierr =  KSPDestroy(&ksp_context);                                                    CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


PetscErrorCode WorkerLoop() {
  PetscFunctionBeginUser;
  PetscErrorCode  ierr;

  unsigned  num_load_cases = spec->ObjectMember("load cases")->ArraySize();


  // Sleep until woken to do work
  while(true) {
    int msg;
    bool calculate_derivatives;
    MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));
    
    switch(msg) {
      case MSG_WORKER_KILL:
        PetscFunctionReturn(0);
        break;

      case MSG_RECV_NEW_RHO:
        {
          MPI_Request request;
          int n_points = DOM.design_dof*(my_dom.nx+1)*(my_dom.ny+1)*(my_dom.nz+1);
          my_info.local_design.Resize(DOM.design_dof,my_dom.nx+1,my_dom.ny+1,my_dom.nz+1); // Contains ghosts
          MPI_CALL(MPI_Irecv(&my_info.local_design(0,0,0,0), n_points, MPI_DOUBLE, root_proc,
                             TAG_RHO, PETSC_COMM_WORLD, &request))
          MPI_CALL(MPI_Wait(&request, MPI_STATUS_IGNORE))
        }
        break;

      case MSG_SOLVE:
        MPI_CALL(MPI_Bcast(&my_info.it, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));

        for(unsigned i=0; i<num_load_cases; ++i) {
          ierr =  DoFemWork(i, my_info.local_design);                                  CHKERRQ(ierr);
        }
        break;

      case MSG_POST_DISPLACEMENT:
        static std::vector<Array4d<double> > u;
        ierr =  PostSolveDisplacement(u);                                              CHKERRQ(ierr);
        break;

      case MSG_POST_ENERGY:
        static Array3d<double> energy;
        int load_case;
        MPI_CALL(MPI_Bcast(&load_case, 1, MPI_INT, root_proc, PETSC_COMM_WORLD))
        ierr =  PostSolveCalculateEnergy(my_info.local_design, load_case, energy);     CHKERRQ(ierr);
        break;

      case MSG_POST_UISJ_MATRIX:
        static Array2d<double> uisj;
        ierr =  PostSolveUiSj(uisj);                                                   CHKERRQ(ierr);
        break;

      case MSG_POST_DRHO_COMP:
        static std::vector<Array4d<double> > d_design_cmpl;
        ierr =  PostSolveComplianceDDesign(my_info.local_design, d_design_cmpl);       CHKERRQ(ierr);
        break;

      case MSG_POST_DRHO_WC:
        static Array2d<Array4d<double> > d_design_matrix;
        ierr =  PostSolveComplianceDDesignMatrix(my_info.local_design, d_design_matrix);CHKERRQ(ierr);
        break;
      
      case MSG_POST_INTERFACE_OBJECTIVE:
        double ss;
        static Array4d<double> d_design_ss;
        MPI_CALL(MPI_Bcast(&msg,1,MPI_INT,root_proc,PETSC_COMM_WORLD));
        calculate_derivatives = msg;
        ierr =  PostSolveInterfaceObjective(nullptr, calculate_derivatives,
                                            my_info.local_design, ss, d_design_ss);    CHKERRQ(ierr);
        break;

      case MSG_POST_CALCULATE_RESORPTION:
        double discount_factor, eps, res;
        static Array3d<double> local_bone_mask, local_reference_energy;
        static Array4d<double> d_design_res;
        MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));
        calculate_derivatives = msg;
        ierr =  WorkerRecvElementalArray(local_bone_mask);                             CHKERRQ(ierr);
        ierr =  WorkerRecvElementalArray(local_reference_energy);                      CHKERRQ(ierr);
        MPI_CALL(MPI_Bcast(&discount_factor, 1, MPI_DOUBLE, root_proc, PETSC_COMM_WORLD))
        MPI_CALL(MPI_Bcast(&eps, 1, MPI_DOUBLE, root_proc, PETSC_COMM_WORLD))
        ierr =  PostSolveCalculateResorption(calculate_derivatives,
                                             eps,
                                             discount_factor,
                                             my_info.local_design,
                                             local_bone_mask,
                                             local_reference_energy,
                                             res, d_design_res);                       CHKERRQ(ierr);
        break;

      case MSG_POST_HOMOGENIZATION:
        static Array2d<double> E_star;
        ierr =  PostSolveHomogenization(my_info.local_design, E_star);                 CHKERRQ(ierr);
        break;

      default:
        // PETSC_ERR_ARG_WRONG is probably wrong, but none of them really fit.
        SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG, "Unknown message recieved by worker");
        break;
    }
  }

  for(unsigned i=0; i<num_load_cases; ++i) {
    ierr =  VecDestroy(&local_u_dm_vec[i]);                                          CHKERRQ(ierr);
    ierr =  VecDestroy(&local_s_dm_vec[i]);                                          CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}


PetscErrorCode InitDOMStruct(const JSONObject *spec_obj, DOM_TYPE &dom_struct) {
  PetscFunctionBeginUser;

  dom_struct.I = spec->ObjectMember("domain")->ArrayMember(0)->AsInt();
  dom_struct.J = spec->ObjectMember("domain")->ArrayMember(1)->AsInt();
  dom_struct.K = spec->ObjectMember("domain")->ArrayMember(2)->AsInt();
  
  dom_struct.design_dof = spec->ObjectMember("design dof")->AsInt();
  dom_struct.dof = (dom_struct.K==1) ? 2 : 3;

  if(spec->ObjectHasMember("periodic") && spec->ObjectMember("periodic")->AsBool()) {
    dom_struct.I_el = dom_struct.I;
    dom_struct.J_el = dom_struct.J;
    dom_struct.K_el = dom_struct.K;
  } else {
    dom_struct.I_el = dom_struct.I-1;
    dom_struct.J_el = dom_struct.J-1;
    dom_struct.K_el = dom_struct.dof==2? 1 : dom_struct.K-1;
  }

  dom_struct.h[0] = spec->ObjectMember("dimensions")->ArrayMember(0)->AsDouble()/dom_struct.I_el;
  dom_struct.h[1] = spec->ObjectMember("dimensions")->ArrayMember(1)->AsDouble()/dom_struct.J_el;
  if(dom_struct.dof==3) {
    dom_struct.h[2] = spec->ObjectMember("dimensions")->ArrayMember(2)->AsDouble()/dom_struct.K_el;
  }

  if(dom_struct.dof==2) {
    double area = dom_struct.h[0]*dom_struct.h[1];
    for(int p=0; p<4; ++p)
    for(int i=0; i<2; ++i) {
      dom_struct.grad_eta_int[p][i] 
        = area / dom_struct.h[i]
        * SimpsonsRule([&](double y) { 
          return SimpsonsRule([&](double x) { 
            return GradEta(p,i,x,y,0.);
          });
        });

      for(int q=0; q<4; ++q)
      for(int j=0; j<2; ++j) {
        dom_struct.grad_eta_grad_eta_int[p][i][q][j]
          = area / (dom_struct.h[i]*dom_struct.h[j])
          * SimpsonsRule([&](double y) {
            return SimpsonsRule([&](double x) {
              return GradEta(p,i,x,y,0.)*GradEta(q,j,x,y,0.);
            });
          });
      }
    }
  } else {
    double volume = dom_struct.h[0]*dom_struct.h[1]*dom_struct.h[2];
    for(int p=0; p<8; ++p)
    for(int i=0; i<3; ++i) {
      dom_struct.grad_eta_int[p][i] 
        = volume / dom_struct.h[i]
        * SimpsonsRule([&](double z) { 
          return SimpsonsRule([&](double y) {
            return SimpsonsRule([&](double x) {
              return GradEta(p,i,x,y,z);
            });
          });
        });

      for(int q=0; q<8; ++q)
      for(int j=0; j<3; ++j) {
        dom_struct.grad_eta_grad_eta_int[p][i][q][j]
          = volume / (dom_struct.h[i]*dom_struct.h[j])
          * SimpsonsRule([&](double z) {
            return SimpsonsRule([&](double y) {
              return SimpsonsRule([&](double x) {
                return GradEta(p,i,x,y,z)*GradEta(q,j,x,y,z);
              });
            });
          });
      }
    }
  }

  PetscFunctionReturn(0);
}


PetscErrorCode InitFEMCalc(const JSONObject *spec_obj, bool &is_root) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  spec = spec_obj;

  // MPI Constants
  MPI_Comm_size(PETSC_COMM_WORLD, &num_proc);
  MPI_Comm_rank(PETSC_COMM_WORLD, &my_id);

  ierr =  InitDOMStruct(spec_obj,DOM);                                                 CHKERRQ(ierr);

  std::vector<int> dom = spec->ObjectMember("domain")->ArrayAsVectorOfInts(3);
  const PetscInt stencil_width = 1;

  bool is_periodic = (spec->ObjectHasMember("periodic") && spec->ObjectMember("periodic")->AsBool());
  DMBoundaryType boundary_type = (is_periodic ? DM_BOUNDARY_PERIODIC : DM_BOUNDARY_GHOSTED);
  
  std::vector<double> dim;
  if(DOM.dof==2) {
    ierr =  DMDACreate2d(PETSC_COMM_WORLD,
                         boundary_type,      // X boundary (hard boundaries)
                         boundary_type,      // Y boundary
                         DMDA_STENCIL_BOX,   // Ghost corner points too
                         dom[0], dom[1],
                         PETSC_DECIDE,       // Num of processors in each direction
                         PETSC_DECIDE,
                         DOM.dof,
                         stencil_width,
                         NULL, NULL,         // Distribution of points among processors
                         &dm_context);                                                 CHKERRQ(ierr);
    dim = spec->ObjectMember("dimensions")->ArrayAsVectorOfDoubles(2);
    dim.push_back(0.0);
  } else {
    ierr =  DMDACreate3d(PETSC_COMM_WORLD,
                         boundary_type,      // X boundary (hard boundaries)
                         boundary_type,      // Y boundary
                         boundary_type,      // Z boundary
                         DMDA_STENCIL_BOX,   // Ghost corner points too
                         dom[0], dom[1], dom[2],
                         PETSC_DECIDE,       // Num of processors in each direction
                         PETSC_DECIDE,
                         PETSC_DECIDE,
                         DOM.dof,
                         stencil_width,
                         NULL, NULL, NULL,   // Distribution of points among processors
                         &dm_context);                                                 CHKERRQ(ierr);
    dim = spec->ObjectMember("dimensions")->ArrayAsVectorOfDoubles(3);
  }
  ierr =  DMDASetUniformCoordinates(dm_context, 0.0, dim[0], 0.0, dim[1], 0.0, dim[2]);CHKERRQ(ierr);

  unsigned num_load_cases = spec->ObjectMember("load cases")->ArraySize();
  local_u_dm_vec.resize(num_load_cases);
  local_s_dm_vec.resize(num_load_cases);
  for(unsigned i=0; i<num_load_cases; ++i) {
    ierr =  DMCreateLocalVector(dm_context, &(local_u_dm_vec[i]));                     CHKERRQ(ierr);
    ierr =  DMCreateLocalVector(dm_context, &(local_s_dm_vec[i]));                     CHKERRQ(ierr);
  }

  ierr =  DMDAGetCorners(dm_context,
                         &my_dom.sx, &my_dom.sy, &my_dom.sz,
                         &my_dom.nx, &my_dom.ny, &my_dom.nz);                          CHKERRQ(ierr);

  if(DOM.dof==2) {
    my_dom.nz = 1;
    my_dom.sz = 0;
  }

  if(spec->ObjectHasMember("multigrid")) {
    int num_levels = spec->ObjectMember("multigrid")->ObjectMember("num levels")->AsInt();
    // Check that the domain can be split in two num_levels times.
    int coarsest_grid_min_size = 1;
    int MG_size_factor = coarsest_grid_min_size<<(num_levels-1); // coarsest 2^num_levels
    bool is_correct = true;
    if(is_periodic) {
      is_correct &= (dom[0] % MG_size_factor == 0) && (dom[1] % MG_size_factor == 0);
      is_correct &= (DOM.dof==2) || (dom[2] % MG_size_factor == 0);
    } else {
      is_correct &= ((dom[0]-1) % MG_size_factor == 0) && ((dom[1]-1) % MG_size_factor == 0);
      is_correct &= (DOM.dof==2) || (dom[2]-1) % MG_size_factor == 0;
    }
    if(!is_correct) {
      ierr =  PetscPrintf(PETSC_COMM_WORLD,
                          "WARNING: domain provided might not be able to be subdivided as requested"
                          " for the multi-grid preconditioner\n");
      //SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
      //        "Problem Domain must be compatible with the number of multi-grid levels\n"
      //        "   (Need to be able to subdivide num_levels times)");
    }
    // Create the da hierarchy
    std::vector<DM> da_levels(num_levels, nullptr);
    da_levels[0] = dm_context;
    ierr =  DMCoarsenHierarchy(dm_context, num_levels-1, &da_levels[1]);               CHKERRQ(ierr);
    for(int i=1; i<num_levels; ++i) {
      ierr =  DMDASetUniformCoordinates(da_levels[i], 0.0, dim[0], 0.0, dim[1], 0.0, dim[2]);
                                                                                       CHKERRQ(ierr);
    }
    // Create the interpolation matrix
    my_info.MG_R_mats.resize(num_levels, nullptr);
    for(int i=1; i<num_levels; ++i) {
      ierr =  DMCreateInterpolation(da_levels[i], da_levels[i-1], &my_info.MG_R_mats[i],nullptr);
                                                                                       CHKERRQ(ierr);
    }
    // Clean up
    for(int i=1; i<num_levels; ++i) {
      ierr =  DMDestroy(&da_levels[i]);                                                CHKERRQ(ierr);
    }
  }

  if(my_id==root_proc) {
    // Receive domains
    master_info.processor_domains.resize(num_proc);
    for(int p=0; p<num_proc; ++p) {
      if(p==root_proc) {
        master_info.processor_domains[root_proc] = my_dom;
      } else {
        MPI_CALL(MPI_Recv(&master_info.processor_domains[p], 6, MPI_INT, p,
                          TAG_DOMAIN, PETSC_COMM_WORLD, MPI_STATUS_IGNORE));
      }
    }
    is_root = true;
  } else {
    // Send my domain
    MPI_CALL(MPI_Send(&my_dom,6,MPI_INT,root_proc,TAG_DOMAIN,PETSC_COMM_WORLD))
    is_root = false;
  }

  PetscFunctionReturn(0);
}


PetscErrorCode FinalizeFEMCalc() {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  if(my_id==root_proc) {
    int msg = MSG_WORKER_KILL;
    MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));
  }

  ierr =  DMDestroy (&dm_context);                                                     CHKERRQ(ierr);
  ierr =  KSPDestroy(&ksp_context);                                                    CHKERRQ(ierr);
    
  unsigned num_load_cases = spec->ObjectMember("load cases")->ArraySize();
  for(unsigned i=0; i<num_load_cases; ++i) {
    ierr =  VecDestroy(&local_u_dm_vec[i]);                                            CHKERRQ(ierr);
    ierr =  VecDestroy(&local_s_dm_vec[i]);                                            CHKERRQ(ierr);
  }

  for(unsigned i=0; i<my_info.MG_R_mats.size(); ++i) {
    ierr =  MatDestroy(&my_info.MG_R_mats[i]);                                         CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}


PetscErrorCode FEMRun(const int iteration, const Array4d<double> &design) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  ierr =  MasterShareDesign(design, my_info.local_design);                             CHKERRQ(ierr);
  
  // If we're only doing one iteration make sure that it is the long range tensor
  my_info.it = iteration==-1 ? 1000000 : iteration;

  int num_load_cases = spec->ObjectMember("load cases")->ArraySize();

  int msg = MSG_SOLVE;
  MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));
  MPI_CALL(MPI_Bcast(&my_info.it, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));

  // Solve for the displacement
  for(int i=0; i<num_load_cases; ++i) {
    ierr =  DoFemWork(i, my_info.local_design);                                        CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}


PetscErrorCode FEMGetDisplacements(std::vector<Array4d<double> > &u) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  int msg = MSG_POST_DISPLACEMENT;
  MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));

  ierr =  PostSolveDisplacement(u);                                                    CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


PetscErrorCode FEMCalculateEnergy(int load_case, Array3d<double> &energy) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  int msg = MSG_POST_ENERGY;
  MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));
  MPI_CALL(MPI_Bcast(&load_case, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));

  ierr =  PostSolveCalculateEnergy(my_info.local_design, load_case, energy);           CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


PetscErrorCode FEMGetUiSj(Array2d<double> &uisj) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  int msg = MSG_POST_UISJ_MATRIX;
  MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));

  ierr =  PostSolveUiSj(uisj);                                                         CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


PetscErrorCode FEMGetComplianceDDesign(std::vector<Array4d<double> > &d_design) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  int msg = MSG_POST_DRHO_COMP;
  MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));

  ierr =  PostSolveComplianceDDesign(my_info.local_design, d_design);                  CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


PetscErrorCode FEMGetWCDDesign(Array2d<Array4d<double> > &d_design_matrix) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  int msg = MSG_POST_DRHO_WC;
  MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));

  ierr =  PostSolveComplianceDDesignMatrix(my_info.local_design, d_design_matrix);     CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


PetscErrorCode FEMGetInterfaceObjective(const JSONValue* obj_desc,
                                        bool calculate_derivatives,
                                        double &obj, Array4d<double> &d_design) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  if(spec->ObjectMember("load cases")->ArraySize()!=1) {
    std::cout << " (shear stress objective only considering the first load case) ";
  }

  int msg = MSG_POST_INTERFACE_OBJECTIVE;
  MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));
  msg = calculate_derivatives;
  MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));

  ierr =  PostSolveInterfaceObjective(obj_desc,calculate_derivatives,
                                      my_info.local_design,obj,d_design);              CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


PetscErrorCode FEMCalculateResportion(bool calculate_derivatives,
                                      double eps,
                                      double discount_factor,
                                      const std::string &mask_file,
                                      const std::string &reference_energy_file,
                                      double &resorption,
                                      Array4d<double> &d_design) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  int msg = MSG_POST_CALCULATE_RESORPTION;
  MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));
  msg = calculate_derivatives;
  MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));

  static Array3d<double> local_bone_mask, bone_mask, local_reference_energy, reference_energy;
  bone_mask        = ReadArray3d(mask_file,            DOM.I_el,DOM.J_el,DOM.K_el);
  reference_energy = ReadArray3d(reference_energy_file,DOM.I_el,DOM.J_el,DOM.K_el);
  ierr =  MasterShareElementalArray(bone_mask, local_bone_mask);                       CHKERRQ(ierr);
  ierr =  MasterShareElementalArray(reference_energy, local_reference_energy);         CHKERRQ(ierr);
  MPI_CALL(MPI_Bcast(&discount_factor, 1, MPI_DOUBLE, root_proc, PETSC_COMM_WORLD))
  MPI_CALL(MPI_Bcast(&eps, 1, MPI_DOUBLE, root_proc, PETSC_COMM_WORLD))
  ierr =  PostSolveCalculateResorption(calculate_derivatives,
                                       eps,
                                       discount_factor,
                                       my_info.local_design,
                                       local_bone_mask,
                                       local_reference_energy,
                                       resorption, d_design);                          CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode FEMGetHomogenizationTensor(Array2d<double> &homog_tensor) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  int msg = MSG_POST_HOMOGENIZATION;
  MPI_CALL(MPI_Bcast(&msg, 1, MPI_INT, root_proc, PETSC_COMM_WORLD));

  ierr =  PostSolveHomogenization(my_info.local_design, homog_tensor);                 CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


PetscErrorCode CalculateDisplacement(const Array4d<double> &design,
                                     std::vector<Array4d<double> > &displacements) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  
  ierr =  FEMRun(-1, design);                                                          CHKERRQ(ierr);
  ierr =  FEMGetDisplacements(displacements);                                          CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


// *** Objective Calculations *** //

PetscErrorCode CalculateObjectiveMean(const Array4d<double> &design,   
                                      double &objective,
                                      Array4d<double> &d_design) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  std::vector<Array4d<double> > d_design_vec;
  Array2d<double> uisj;
  ierr =  FEMGetUiSj(uisj);                                                          CHKERRQ(ierr);
  ierr =  FEMGetComplianceDDesign(d_design_vec);                                     CHKERRQ(ierr);

  const int design_dof = spec->ObjectMember("design dof")->AsInt();
  
  // Ensure that the provided d_design is correct size
  d_design.Resize(design_dof, DOM.I_el, DOM.J_el, DOM.K_el);
  
  double compliance = 0;

  int num_load_cases = spec->ObjectMember("load cases")->ArraySize();

  double total_load_case_weight = 0.0;
  for(int load_case=0; load_case<num_load_cases; ++load_case) {
    double weight = spec->ObjectMember("load cases")->ArrayMember(load_case)
                        ->ObjectMember("weighting")->AsDouble();
    total_load_case_weight += weight;
  }

  d_design.Set(0.0);
  for(int load_case=0; load_case<num_load_cases; ++load_case) {
    // Calculate compliance
    double weight = spec->ObjectMember("load cases")->ArrayMember(load_case)
                        ->ObjectMember("weighting")->AsDouble();
    compliance += weight * uisj(load_case,load_case);
    // Calculate sensitivities
    for(int i=0; i<DOM.I_el; ++i)
    for(int j=0; j<DOM.J_el; ++j)
    for(int k=0; k<DOM.K_el; ++k)
    for(int d=0; d<design_dof; ++d) {
      d_design(d,i,j,k) += weight / total_load_case_weight * d_design_vec[load_case](d,i,j,k);
    }
  }
  objective = compliance/total_load_case_weight;
  PetscFunctionReturn(0);
}


double CalculateObjectiveWorstCase(const Array4d<double> &design, 
                                   double &objective,
                                   Array4d<double> &d_design) {
  // Calculates the worst case loading from a linear combination of the loading cases
  // if the loading cases are a_1u_1s_1 + a_2u_2s_2 + ... then |a| = 1.
  // Objective function is a quadratic a A a where A_ij is u_i.s_j
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  Array2d<Array4d<double> > d_design_matrix;
  Array2d<double> uisj;
  ierr =  FEMGetUiSj(uisj);                                                            CHKERRQ(ierr);
  ierr =  FEMGetWCDDesign(d_design_matrix);                                            CHKERRQ(ierr);
  int design_dof = spec->ObjectMember("design dof")->AsInt();
  
  // Ensure that the provided d_design is correct size
  d_design.Resize(design_dof,DOM.I_el, DOM.J_el, DOM.K_el);
  
  int num_load_cases = spec->ObjectMember("load cases")->ArraySize();

  FMatrix A(num_load_cases, num_load_cases);
  for(int lc1=0; lc1<num_load_cases; ++lc1) {
    for(int lc2=0; lc2<num_load_cases; ++lc2) {
      A(lc2,lc1) = uisj(lc1, lc2);
    }
  }

  // Use repeated application to determine largest eigenvalue eigenvector
  std::vector<double> a(num_load_cases, 1.0);
  std::vector<double> a_old = a;
  std::vector<double> temp;
  a[0] = 1.2345; // This helps if it turns out (1.0, 1.0, ...) is another eigenvalue.
  while(Norm(a_old-a) > 1e-6) {
    a_old = a;
    A.TMult(a_old, a);
    a = a/Norm(a);
  }

  // Calculate sensitivities
  d_design.Set(0.0);
  for(int load_case_A=0; load_case_A<num_load_cases; ++load_case_A)
  for(int load_case_B=0; load_case_B<num_load_cases; ++load_case_B) {
    double aa = a[load_case_A] * a[load_case_B];
    const Array4d<double> &d_design_AB = d_design_matrix(load_case_A, load_case_B);

    for(int k=0; k<DOM.K_el;   ++k)
    for(int j=0; j<DOM.J_el;   ++j)
    for(int i=0; i<DOM.I_el;   ++i)
    for(int d=0; d<design_dof; ++d) {
      d_design(d,i,j,k) += aa * d_design_AB(d,i,j,k);
    }
  }

  A.Mult(a, temp);
  objective = Dot(a, temp);
  PetscFunctionReturn(0);
}


double CalculateTrilinearInterpolatedMean(const JSONValue *desc,
                                          bool calculate_derivatives,
                                          const Array4d<double> &design,
                                          double &value,
                                          Array4d<double> &d_design) {
  PetscFunctionBeginUser;
  if(DOM.design_dof!=3) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
      "trilinear interpolated mean function requires exactly 3 design variables");
  }

  // Get the key values for the interpolation
  static std::map<const JSONValue*,Array3d<double>> values_map;
  Array3d<double> &key_values = values_map[desc];
  if(key_values.GetI()==0) { 
    key_values = ReadArray3d(desc->ObjectMember("key value file")->AsString());
  }

  const std::vector<double> interp_pts[3] = {
    desc->ObjectMember("interpolation points")->ArrayMember(0)->ArrayAsVectorOfDoubles(),
    desc->ObjectMember("interpolation points")->ArrayMember(1)->ArrayAsVectorOfDoubles(),
    desc->ObjectMember("interpolation points")->ArrayMember(2)->ArrayAsVectorOfDoubles()
  };
  if(key_values.GetI()!=interp_pts[0].size() ||
     key_values.GetJ()!=interp_pts[1].size() ||
     key_values.GetK()!=interp_pts[2].size()) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
            "number of interpolation values does not match number of interpolation points");
  }

  // Calculate the result
  int num_el = 0;
  value = 0;
  if(calculate_derivatives) d_design.Resize(3,DOM.I_el,DOM.J_el,DOM.K_el);

  for(int i=0;i<DOM.I_el;++i)
  for(int j=0;j<DOM.J_el;++j)
  for(int k=0;k<DOM.K_el;++k) {
    // bottom is the index to the left of the interval
    int bottom[3];
    double alpha[3];
    double interval_size[3];
    for(int ii=0; ii<3; ++ii) {
      size_t p_i = 1;
      while(design(ii,i,j,k)>interp_pts[ii][p_i] && p_i<interp_pts[ii].size()-1) ++p_i;
      bottom[ii] = p_i - 1;
      interval_size[ii] = interp_pts[ii][p_i]-interp_pts[ii][p_i-1];
      alpha[ii] = (design(ii,i,j,k)-interp_pts[ii][p_i-1])/interval_size[ii];
    }
    // If the value is outside of the range then just ignore it
    if(alpha[0]<0 || 1<alpha[0] ||
       alpha[1]<0 || 1<alpha[1] ||
       alpha[2]<0 || 1<alpha[2]) {
      continue;
    }
    num_el++;

    for(int di=0;di<2;++di)
    for(int dj=0;dj<2;++dj)
    for(int dk=0;dk<2;++dk) {
      value += (1-di+2*(di-0.5)*alpha[0])
             * (1-dj+2*(dj-0.5)*alpha[1])
             * (1-dk+2*(dk-0.5)*alpha[2])
             * key_values(bottom[0]+di,bottom[1]+dj,bottom[2]+dk);
    }
    if(calculate_derivatives) {
      double d_value[3] = {0,0,0};
      for(int di=0;di<2;++di)
      for(int dj=0;dj<2;++dj)
      for(int dk=0;dk<2;++dk) {
        d_value[0] += (    +2*(di-0.5)         )/interval_size[0]
                    * (1-dj+2*(dj-0.5)*alpha[1])
                    * (1-dk+2*(dk-0.5)*alpha[2])
                    * key_values(bottom[0]+di,bottom[1]+dj,bottom[2]+dk);

        d_value[1] += (1-di+2*(di-0.5)*alpha[0])
                    * (    +2*(dj-0.5)         )/interval_size[1]
                    * (1-dk+2*(dk-0.5)*alpha[2])
                    * key_values(bottom[0]+di,bottom[1]+dj,bottom[2]+dk);

        d_value[2] += (1-di+2*(di-0.5)*alpha[0])
                    * (1-dj+2*(dj-0.5)*alpha[1])
                    * (    +2*(dk-0.5)         )/interval_size[2]
                    * key_values(bottom[0]+di,bottom[1]+dj,bottom[2]+dk);
      }
      for(int ii=0;ii<3;++ii) d_design(ii,i,j,k) = d_value[ii];
    }
  }
  // Make the sum an average
  value /= num_el;
  if(calculate_derivatives) for(auto &val : d_design.GetData()) val /= num_el;

  PetscFunctionReturn(0);
}


double Dist(double u0, double u1, double u2, double v0, double v1, double v2) {
  return sqrt(pow(u0-v0,2)+pow(u1-v1,2)+pow(u2-v2,2));
}

int imax(int x, int y) {
  return x>y?x:y;
}

int imin(int x, int y) {
  return x<y?x:y;
}

PetscErrorCode FilterSensitivities(const Array4d<double> &design, Array4d<double> &d_design) {
  PetscFunctionBeginUser;

  const int design_dof = spec->ObjectMember("design dof")->AsInt();

  double r_min = spec->ObjectMember("filter r_min")->AsDouble();
  if(r_min < 1e-4) PetscFunctionReturn(0);
  int r_min_int = int(ceil(r_min));

  Array4d<double> d_design_new = d_design;
  int d,i,j,k,ii,jj,kk;
  double denominator;
  double numerator;
  for(i=0; i<DOM.I_el;   ++i)
  for(j=0; j<DOM.J_el;   ++j)
  for(k=0; k<DOM.K_el;   ++k)
  for(d=0; d<design_dof; ++d) {
    numerator = 0;
    denominator = 0;
    for(ii=imax(i-r_min_int,0); ii<imin(i+r_min_int+1, DOM.I_el); ++ii)
    for(jj=imax(j-r_min_int,0); jj<imin(j+r_min_int+1, DOM.J_el); ++jj)
    for(kk=imax(k-r_min_int,0); kk<imin(k+r_min_int+1, DOM.K_el); ++kk) {
      double dist = Dist(i,j,k,ii,jj,kk);
      if(dist>r_min) continue;
      numerator += (r_min-dist)*design(d,ii,jj,kk)*d_design(d,ii,jj,kk);
      denominator += r_min-dist;
    }
    d_design_new(d,i,j,k) = numerator/(design(d,i,j,k)*denominator);
  }
  d_design = d_design_new;

  PetscFunctionReturn(0);
}


PetscErrorCode CalculateFunction(const JSONValue *desc,
                                 bool calculate_derivatives,
                                 const Array4d<double> &design,
                                 double &value,
                                 Array4d<double> &d_design) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  
  std::string type = desc->ObjectMember("type")->AsString();
  
  if(type=="mean") {
    ierr =  CalculateObjectiveMean(design,value,d_design);                             CHKERRQ(ierr);

  } else if(type=="worst case") {
    ierr =  CalculateObjectiveWorstCase(design,value,d_design);                        CHKERRQ(ierr);

  } else if(type=="interface") {
    ierr =  FEMGetInterfaceObjective(desc,calculate_derivatives,value,d_design);       CHKERRQ(ierr);

  } else if(type == "mass") {
    if(design.GetI()!=1) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE, "Mass constraint assumes only one design var");
    }
    value = AverageDoubleArray(design);
    d_design.Resize(design.GetI(), design.GetJ(), design.GetK(), design.GetL());
    d_design.Set(1.0/double(DOM.I_el*DOM.J_el*DOM.K_el));
  
  } else if(type == "bone resorption") {
    double      eps                   = desc->ObjectMember("heaviside epsilon")->AsDouble();
    double      discount_factor       = desc->ObjectMember("discount factor")->AsDouble();
    std::string mask_file             = desc->ObjectMember("bone mask file")->AsString();
    std::string reference_energy_file = desc->ObjectMember("reference energy file")->AsString();
    ierr =  FEMCalculateResportion(calculate_derivatives,
                                   eps, discount_factor,
                                   mask_file, reference_energy_file,
                                   value, d_design);                                   CHKERRQ(ierr);
  
  } else if(type == "intermediate value penalisation") {
    if(design.GetI() != 1) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
              "intermediate value penalisation assumes only one design var");
    }
    d_design.Resize(DOM.dof,DOM.I_el,DOM.J_el,DOM.K_el);
    value = 0;
    double num_elements = DOM.I_el*DOM.J_el*DOM.K_el;
    for(int i=0; i<DOM.I_el; ++i)
    for(int j=0; j<DOM.J_el; ++j)
    for(int k=0; k<DOM.K_el; ++k) {
      double t = 2*design(0,i,j,k) - 1;
      value += (1-t*t)/num_elements;
      d_design(0,i,j,k) = -2*2*t/num_elements;
    }

  } else if(type == "sum") {
    double value_temp;
    Array4d<double> d_design_temp;
    d_design.Resize(DOM.dof, DOM.I_el, DOM.J_el, DOM.K_el); 
    d_design.Set(0.0);
    value = 0.0;
    const JSONValue* sub_funcs = desc->ObjectMember("sub functions");
    for(int i=0; i<sub_funcs->ArraySize(); ++i) {
      double weighting = sub_funcs->ArrayMember(i)->ObjectMember("sum weighting")->AsDouble();
      ierr =  CalculateFunction(sub_funcs->ArrayMember(i),calculate_derivatives,
                                design,value_temp,d_design_temp);                      CHKERRQ(ierr);
      value += weighting * value_temp;
      for(int i=0; i<DOM.I_el; ++i)
      for(int j=0; j<DOM.J_el; ++j)
      for(int k=0; k<DOM.K_el; ++k)
      for(int d=0; d<DOM.dof;  ++d) {
        d_design(d,i,j,k) += weighting * d_design_temp(d,i,j,k);
      }
    }

  } else if(type == "trilinear interpolated mean") {
    ierr =  CalculateTrilinearInterpolatedMean(desc,calculate_derivatives,design,value,d_design);
                                                                                       CHKERRQ(ierr);

  } else {
    throw(std::runtime_error("CalculateFunction() doesn't recognise function \""+type+'"'));
  }

  PetscFunctionReturn(0);
}


PetscErrorCode GetStress(const Array4d<double> &design, Array6d<double> &s) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;
  
  int num_load_cases = spec->ObjectMember("load cases")->ArraySize();
  s.Resize(num_load_cases,DOM.dof,DOM.dof,DOM.I_el,DOM.J_el,DOM.K_el);
  s.Set(0.0);

  std::unique_ptr<TensorBase> tensor;
  ierr =  GetTensor(spec, tensor);                                                     CHKERRQ(ierr);
  std::vector<double> element_design;
  Array4d<double> element_tensor;

  std::vector<Array4d<double> > u;
  ierr =  FEMGetDisplacements(u);                                                      CHKERRQ(ierr);
  
  for(int i=0; i<DOM.I_el; ++i)
  for(int j=0; j<DOM.J_el; ++j)
  for(int k=0; k<DOM.K_el; ++k) {
    ierr =  CopyDesignVariables(design, i, j, k, element_design);                    CHKERRQ(ierr);
    ierr =  tensor->EvalTensor(&element_design[0], my_info.it, element_tensor);      CHKERRQ(ierr);
    for(int lc=0; lc<num_load_cases; ++lc) {
      if(DOM.dof==2) {
        for(int p=0;  p<4; ++p) {
          int pi = (i+points[p][0]) % DOM.I; 
          int pj = (j+points[p][1]) % DOM.J; 
          for(int ii=0; ii<2; ++ii)
          for(int jj=0; jj<2; ++jj)
          for(int kk=0; kk<2; ++kk)
          for(int ll=0; ll<2; ++ll) {
            s(lc,ii,jj,i,j,k) += element_tensor(ii,jj,kk,ll) 
                               * DOM.grad_eta_int[p][kk] / (DOM.h[0]*DOM.h[1])
                               * u[lc](ll,pi,pj,0);
          }
        }
      } else {
        for(int p=0;  p<8; ++p) {
          int pi = (i+points[p][0]) % DOM.I; 
          int pj = (j+points[p][1]) % DOM.J; 
          int pk = (k+points[p][2]) % DOM.K; 
          for(int ii=0; ii<3; ++ii)
          for(int jj=0; jj<3; ++jj)
          for(int kk=0; kk<3; ++kk)
          for(int ll=0; ll<3; ++ll) {
            s(lc,ii,jj,i,j,k) += element_tensor(ii,jj,kk,ll)
                               * DOM.grad_eta_int[p][kk] / (DOM.h[0]*DOM.h[1]*DOM.h[2])
                               * u[lc](ll,pi,pj,pk);
          }
        }
      }
    }
  }

  PetscFunctionReturn(0);
}


PetscErrorCode CalculateObjective(const int iteration,
                                  bool calculate_derivatives,
                                  const Array4d<double>  &design, 
                                  Array4d<double> &d_design,
                                  std::vector<double> &constraints,
                                  std::vector<Array4d<double> > &d_constraints,
                                  double &objective) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  PetscBool was_set;
  char buff[STR_BUFF_SIZE];
  ierr =  PetscOptionsGetString(NULL,NULL,"-save_d_design",buff,STR_BUFF_SIZE,&was_set);CHKERRQ(ierr);
  calculate_derivatives |= was_set;

  // Run FEM
  ierr =  FEMRun(iteration, design);                                                   CHKERRQ(ierr);

  // Calculate objective
  const JSONValue *obj = spec->ObjectMember("objective function");
  ierr =  CalculateFunction(obj, calculate_derivatives, design, objective, d_design);  CHKERRQ(ierr);
  
  // Filter Sensitivies
  if(calculate_derivatives) {
    ierr =  FilterSensitivities(design, d_design);                                     CHKERRQ(ierr);
  }

  // Calculate Constraints
  int num_constraints = spec->ObjectMember("constraints")->ArraySize();
  constraints.resize(num_constraints);
  d_constraints.resize(num_constraints);
  
  for(int i=0; i<num_constraints; ++i) {
    const JSONValue *desc = spec->ObjectMember("constraints")->ArrayMember(i);
    ierr =  CalculateFunction(desc, calculate_derivatives, design, constraints[i], d_constraints[i]);
                                                                                       CHKERRQ(ierr);
    constraints[i] -= desc->ObjectMember("limit")->AsDouble();
  }

  // Enforce Symmetries
  auto d_design_ptr = calculate_derivatives ? &d_design      : nullptr;
  auto d_constr_ptr = calculate_derivatives ? &d_constraints : nullptr;
  ierr =  EnforceDesignConstraints(*spec,nullptr,nullptr,d_design_ptr,d_constr_ptr);   CHKERRQ(ierr);

  // Output data if requested
  ierr =  PetscOptionsGetString(NULL,NULL,"-save_d_design",buff,STR_BUFF_SIZE,&was_set);CHKERRQ(ierr);
  buff[STR_BUFF_SIZE-1] = '\0';
  if(was_set) {
    std::stringstream filename;
    if(iteration < 0) {
      filename << buff << ".f4d";
    } else {
      filename << buff << "_" << iteration << ".f4d";
    }
    OutputArray(d_design, filename.str());
  }

  ierr =  PetscOptionsGetString(NULL,NULL,"-save_u",buff,STR_BUFF_SIZE,&was_set);      CHKERRQ(ierr);
  buff[STR_BUFF_SIZE-1] = '\0';
  if(was_set) {
    std::vector<Array4d<double> > u;
    ierr =  FEMGetDisplacements(u);                                                    CHKERRQ(ierr);
    for(unsigned i=0; i<u.size(); ++i) {
      std::stringstream filename;
      if(iteration < 0) {
        filename << buff << "_" << i << ".f4d";
      } else {
        filename << buff << "_" << iteration << "_" << i << ".f4d";
      }
      OutputArray(u[i], filename.str());
    }
  }

  ierr =  PetscOptionsGetString(NULL,NULL,"-save_stress",buff,STR_BUFF_SIZE,&was_set); CHKERRQ(ierr);
  buff[STR_BUFF_SIZE-1] = '\0';
  if(was_set) {
    Array6d<double> s;
    ierr =  GetStress(design, s);                                                      CHKERRQ(ierr);
    
    std::stringstream filename;
    if(iteration < 0) {
      filename << buff << ".f6d";
    } else {
      filename << buff << "_" << iteration << ".f6d";
    }
    OutputArray(s, filename.str());
  }

  ierr =  PetscOptionsGetString(NULL,NULL,"-save_energy",buff,STR_BUFF_SIZE,&was_set); CHKERRQ(ierr);
  buff[STR_BUFF_SIZE-1] = '\0';
  if(was_set) { 
    static Array3d<double> energy;
    int num_load_cases = spec->ObjectMember("load cases")->ArraySize();
    for(int i=0; i<num_load_cases; ++i) {
      std::stringstream filename;
      if(iteration < 0) {
        filename << buff << "_" << i << ".f3d";
      } else {
        filename << buff << "_" << iteration << "_" << i << ".f3d";
      }
      ierr =  FEMCalculateEnergy(i, energy);                                           CHKERRQ(ierr);
      OutputArray(energy, filename.str());
    }
  }
  
  PetscFunctionReturn(0);
}



// *** Homogenisation *** //

PetscErrorCode CalculateHomogenization(const Array4d<double> &design, Array2d<double> &tensor) {
  PetscFunctionBeginUser;
  PetscErrorCode ierr;

  ierr =  FEMRun(-1, design);                                                          CHKERRQ(ierr);
  ierr =  FEMGetHomogenizationTensor(tensor);                                          CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

