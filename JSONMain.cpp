

#include <iostream>
#include <fstream>

#include "JSON.h"


int main(int argc, char **argv) {
  std::istream *stream;
  std::fstream file;
  if(argc>1) {
    file.open(argv[1]);
    if(!file.good()) {
      std::cout << "Couldn't open " << argv[1] << " to read\n";
      return 1;
    }
    stream = &file;
  } else {
    std::cout << "Reading from stdin\n";
    stream = &(std::cin);
  }
  JSONObject obj = ParseJSON(*stream);
  JSONObject obj2("");
  obj2 = obj;
  JSONObject obj3 = obj2;
  obj3.CloneMembersFrom(obj);

  std::cout << obj3;
}
