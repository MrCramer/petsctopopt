
import struct
import numpy
import gzip

### Multidimensional array stuff ###
class FieldNdParseError(Exception):
  pass

def _ReadFieldNdHelper(input_file):
  # Read the header, BArray[N]D where [N] is the number of dimensions
  if input_file.read(6) != b'BArray':
    raise FieldNdParseError("File has incorrect header")
  N_dimensions = int(input_file.read(1))
  if N_dimensions >= 7:
    raise FieldNdParseError("File is reporting 7 or more dimensions")
  if  input_file.read(1) != b'D':
    raise FieldNdParseError("File has incorrect header")

  # '<i' is little endian integer
  shape = [struct.unpack('<i', input_file.read(4))[0] for _ in range(N_dimensions)]

  N_elements = 1
  for dimension in shape:
    N_elements *= dimension

  flat = numpy.empty(N_elements)
  N_loaded = 0
  while True:
    data = input_file.read(8)
    # check for the end of the file
    if len(data) < 8:
      break
    
    if N_loaded > N_elements:
      raise FieldNdParseError("File size not congruent to declared size (too large)")
    flat[N_loaded] = struct.unpack('d', data)[0]
    N_loaded += 1


  if N_loaded < N_elements:
    raise FieldNdParseError("File size not congruent to declared size (too small)")
    
  # 'F' means Fortran ordering, that is, the first index changes fastest
  return flat.reshape(shape,order='F') 


def ReadFieldNd(filename):
  with open(filename, 'rb') as input_file:
    if input_file.peek(2)[:2] == bytearray([0x1f,0x8b]):
      return _ReadFieldNdHelper(gzip.open(input_file))
    else:
      return _ReadFieldNdHelper(input_file)

def _WriteFieldNdHelper(array,output_file):
  # Write the header, BArray[N]D where [N] is the number of dimensions
  output_file.write(b'BArray')
  output_file.write(bytes(str(len(array.shape)),'ascii'))
  output_file.write(b'D')

  # '<i' is little endian integer
  for dimension in array.shape:
    output_file.write(struct.pack('<i', dimension))

  N_elements = 1
  for dimension in array.shape:
    N_elements *= dimension

  # 'F' means Fortran ordering, that is, the first index changes fastest
  flat = array.reshape(N_elements, order='F')
  for value in flat:
    output_file.write(struct.pack('d', value))
   
def WriteFieldNd(array,filename,zip_output=False):
  if zip_output:
    with gzip.open(filename,'wb') as output_file:
      _WriteFieldNdHelper(array,output_file)
  else:
    with open(filename, 'wb') as output_file:
      _WriteFieldNdHelper(array,output_file)

