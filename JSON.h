#ifndef JSON_H_INCLUDED
#define JSON_H_INCLUDED


#include <iostream>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>


class parse_error : public std::runtime_error {
  public:
    parse_error(const char        *arg) : std::runtime_error(arg) { }
    parse_error(const std::string &arg) : std::runtime_error(arg) { }
};

class JSONVector;
class JSONObject;

enum JSONTypeEnum {
  JSON_STRING,
  JSON_INT,
  JSON_DOUBLE,
  JSON_BOOL,
  JSON_ARRAY,
  JSON_OBJECT,
  JSON_NULL,
};



class JSONValue {
  public:
    JSONValue(std::string path) : path_(path) { }
    virtual ~JSONValue() {}

    virtual std::string& AsString()
      { WrongType("string"); }

    virtual std::string  AsString() const
      { WrongType("string"); }

    virtual int& AsInt()
      { WrongType("int"); }

    virtual int  AsInt() const
      { WrongType("int"); }

    virtual double& AsDouble()
      { WrongType("double"); }

    virtual double  AsDouble() const
      { WrongType("double"); }

    virtual bool& AsBool()
      { WrongType("bool"); }

    virtual bool  AsBool() const
      { WrongType("bool"); }
    
    virtual       JSONValue* ArrayMember(size_t i)
      { WrongType("vector"); }

    virtual const JSONValue* ArrayMember(size_t i) const
      { WrongType("vector"); }

    virtual int ArraySize() const
      { WrongType("vector"); }

    virtual std::vector<double> ArrayAsVectorOfDoubles(size_t expected_length=0) const
      { WrongType("vector"); }
    
    virtual std::vector<int> ArrayAsVectorOfInts(size_t expected_length=0) const
      { WrongType("vector"); }

    virtual       JSONValue* ObjectMember(const std::string &key)
      { WrongType("object"); }

    virtual const JSONValue* ObjectMember(const std::string &key) const
      { WrongType("object"); }

    virtual bool ObjectHasMember(const std::string &key) const
      { WrongType("object"); }

    virtual std::unique_ptr<JSONValue> Clone() const = 0;
    virtual JSONTypeEnum GetType() const = 0;
    virtual void Streamify(std::ostream& stream, std::string indentation="") const = 0;

    bool Is(JSONTypeEnum type) const { return GetType()==type; }

  protected:
    std::string path_;

  private:
    void WrongType(const char *const expected_type) const __attribute__((noreturn)) {
      std::cerr << "JSONValue " << path_ << " not a " << expected_type << " as expected" << std::endl;
      throw(std::out_of_range("JSONValue is unexpected type"));
    }
};


class JSONValueString : public JSONValue {
  public:
    JSONValueString(std::string value, std::string path) : JSONValue(path), value_(value) { }
    std::string& AsString()       override { return value_;      }
    std::string  AsString() const override { return value_;      }
    std::unique_ptr<JSONValue> Clone() const override
      { return std::unique_ptr<JSONValue>(new JSONValueString(value_, path_)); }
    JSONTypeEnum GetType() const override{ return JSON_STRING; }
    void Streamify(std::ostream& stream, std::string indentation) const override { stream << '"' << value_ << '"'; }
  private:
    std::string value_;
};


class JSONValueInt : public JSONValue {
  public:
    JSONValueInt(int value, std::string path) : JSONValue(path), value_(value), d_value_(value) { }
    int&         AsInt()         override { return value_; }
    int          AsInt()   const override { return value_; }
    double&      AsDouble()       override { return d_value_; }
    double       AsDouble() const override { return d_value_; }
    std::unique_ptr<JSONValue> Clone() const override
      { return std::unique_ptr<JSONValue>(new JSONValueInt(value_, path_)); }
    JSONTypeEnum GetType() const override { return JSON_INT; }
    void Streamify(std::ostream& stream, std::string indentation) const override { stream << value_; }
  private:
    int value_;
    double d_value_;
};


class JSONValueDouble : public JSONValue {
  public:
    JSONValueDouble(double value, std::string path) : JSONValue(path), value_(value) { }
    double&      AsDouble()       override { return value_;      }
    double       AsDouble() const override { return value_;      }
    std::unique_ptr<JSONValue> Clone() const override
      { return std::unique_ptr<JSONValue>(new JSONValueDouble(value_, path_)); }
    JSONTypeEnum GetType()  const override { return JSON_DOUBLE; }
    void Streamify(std::ostream& stream, std::string indentation) const override { stream << value_; }
  private:
    double value_;
};


class JSONValueBool : public JSONValue {
  public:
    JSONValueBool(bool value, std::string path) : JSONValue(path), value_(value)  { }
    bool&        AsBool()        override { return value_;    }
    bool         AsBool()  const override { return value_;    }
    std::unique_ptr<JSONValue> Clone() const override
      { return std::unique_ptr<JSONValue>(new JSONValueBool(value_, path_)); }
    JSONTypeEnum GetType() const override { return JSON_BOOL; }
    void Streamify(std::ostream& stream, std::string indentation) const override { stream << (value_?"true":"false"); }
  private:
    bool value_;
};


class JSONArray : public JSONValue {
  public:
    JSONArray(std::string path) : JSONValue(path) { }
    // Unimplemented, not meant to copy.
    JSONArray(const JSONArray  &rhs) = delete;
    JSONArray& operator=(const JSONArray  &rhs) = delete;

    void             Push(std::unique_ptr<JSONValue> value) { values_.push_back(std::move(value)); }
          JSONValue* ArrayMember(size_t i)       override;     
    const JSONValue* ArrayMember(size_t i) const override;
    int              ArraySize() const override { return values_.size(); }
    JSONTypeEnum     GetType()   const override { return JSON_ARRAY;     }

    std::unique_ptr<JSONValue> Clone() const override;

    std::vector<double> ArrayAsVectorOfDoubles(size_t expected_length=0) const override;
    std::vector<int>    ArrayAsVectorOfInts   (size_t expected_length=0) const override;
    
    void Streamify(std::ostream& stream, std::string indentation) const override;

  private:
    std::vector<std::unique_ptr<JSONValue>> values_;
};


class JSONObject : public JSONValue {
  public:
    JSONObject()                 : JSONObject("Uninitialised") { }
    JSONObject(std::string path) : JSONValue(path) { }
    
    JSONObject(const JSONObject &rhs)
      : JSONValue(rhs.path_) {
      CloneMembersFrom(rhs);
    }

    JSONObject(JSONObject &&rhs) 
      : JSONValue(std::move(rhs.path_)), values_(std::move(rhs.values_)) { }
    
    JSONObject& operator=(const JSONObject &rhs) {
      Clear();
      path_ = rhs.path_;
      CloneMembersFrom(rhs);
      return *this;
    }

    JSONObject& operator=(JSONObject &&rhs) {
      Clear();
      path_ = std::move(rhs.path_);
      values_ = std::move(rhs.values_);
      rhs.values_.clear();
      return *this;
    }

    void Clear() { values_.clear(); }
    void Set(const std::string &key, std::unique_ptr<JSONValue> value)
      { values_[key] = std::move(value); }

          JSONValue* ObjectMember   (const std::string &key)       override;
    const JSONValue* ObjectMember   (const std::string &key) const override;
    bool             ObjectHasMember(const std::string &key) const override
      { return values_.find(key)!=values_.end(); }
    JSONTypeEnum GetType() const override { return JSON_OBJECT; }
    void             CloneMembersFrom(const JSONObject &rhs);
    std::unique_ptr<JSONValue> Clone() const override;

    void Streamify(std::ostream& stream, std::string indentation) const override;

  private:
    std::map<std::string, std::unique_ptr<JSONValue>> values_;
};

inline std::ostream& operator<<(std::ostream& stream, const JSONObject& obj)
  { obj.Streamify(stream, ""); return stream << '\n'; }


class JSONValueNull : public JSONValue {
  public:
    JSONValueNull(std::string path) : JSONValue(path) { }
    std::unique_ptr<JSONValue> Clone() const override
      { return std::unique_ptr<JSONValue>(new JSONValueNull(path_)); }
    JSONTypeEnum GetType() const override { return JSON_NULL; }
    void Streamify(std::ostream& stream, std::string indentation) const override { stream << "null"; }
};


JSONObject ParseJSON(std::istream &stream, bool track_path=true, const std::string &root_path_name="unknown_json");
void ParseJSONObject(std::istream &, JSONObject&, int&);

#endif // JSON_H_INCLUDED
