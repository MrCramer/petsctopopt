
C********+*********+*********+*********+*********+*********+*********+
C
C    MAIN PROGRAM for using the globally convergent version of MMA
C    to solve a problem defined by the user's subroutines
C    INITI, FUNC1 and FUNC2.
C
C       Version "August 2007".
C    !-----------------------------------------!
C    !  The author of this program is          !
C    !  Krister Svanberg <krille@math.kth.se>  !
C    !-----------------------------------------!
C
C    The problem is assumed to be on the following form:
C
C      minimize  f_0(x) + z + 0.05*z^2 + sum{c_i*y_i + 0.5*(y_i)^2}
C
C    subject to  f_i(x) - a_i*z - y_i <= fmax_i ,   i=1,..,M
C                       xmin_j <= x_j <= xmax_j ,   j=1,..,N
C                                 y_i >= 0 ,        i=1,..,M
C                                   z >= 0 .
C
C    where both N and M are strictly positive integers (not zero).
C    If the user wants to solve an unconstrained problem,
C    i.e. a problem with M=0, then  a "dummy constraint" should
C    be introduced so that M=1. An example of such a constraint is
C    sum{x_j} <= S, where the constant S = 1 + sum{xmax_j}.
C   
C    The functions f_0(x) and f_i(x) should be defined by the
C    user's subroutines
C    FUNC1, which defines only function values, and
C    FUNC2, which defines both function values and derivatives.
C    The problem sizes M and N, the tolerance parameter GEPS,
C    the constants c_i, a_i, fmax_i, xmin_j, xmax_j,
C    and the starting values on the variables x_j,
C    should all be given in the user's subroutine INITI.
C    Output is specified by the users subroutine OUTXOF.
C



C********+*********+*********+*********+*********+*********+*********+
C
C  The meaning of some of the scalars and vectors in the program:
C
C     N  = Number of variables x_j in the problem.
C     M  = Number of constraints in the problem (not including
C          the simple upper and lower bounds on the variables).
C INNMAX = Maximal number of inner iterations within each outer iter.
C          A reasonable choice is INNMAX=10.
C  ITER  = Current outer iteration number ( =1 the first iteration).
C  GEPS  = Tolerance parameter for the constraints.
C          (Used in the termination criteria for the subproblem.)
C
C   XVAL(j) = Current value of the variable x_j.
C  XOLD1(j) = Value of the variable x_j one iteration ago.
C  XOLD2(j) = Value of the variable x_j two iterations ago.
C   XMMA(j) = Optimal value of x_j in the MMA subproblem.
C   XMIN(j) = Original lower bound for the variable x_j.
C   XMAX(j) = Original upper bound for the variable x_j.
C   XLOW(j) = Value of the lower asymptot l_j.
C   XUPP(j) = Value of the upper asymptot u_j.
C   ALFA(j) = Lower bound for x_j in the MMA subproblem.
C   BETA(j) = Upper bound for x_j in the MMA subproblem.
C    F0VAL  = Value of the objective function f_0(x)
C   FVAL(i) = Value of the i:th constraint function f_i(x).
C  DF0DX(j) = Derivative of f_0(x) with respect to x_j.
C   FMAX(i) = Right hand side of the i:th constraint.
C   DFDX(k) = Derivative of f_i(x) with respect to x_j,
C             where k = (j-1)*M + i.
C      P(k) = Coefficient p_ij in the MMA subproblem, where
C             k = (j-1)*M + i.
C      Q(k) = Coefficient q_ij in the MMA subproblem, where
C             k = (j-1)*M + i.
C     P0(j) = Coefficient p_0j in the MMA subproblem.
C     Q0(j) = Coefficient q_0j in the MMA subproblem.
C      B(i) = Right hand side b_i in the MMA subproblem.
C    F0APP  = Value of the approximating objective function
C             at the optimal soultion of the MMA subproblem.
C   FAPP(i) = Value of the approximating i:th constraint function
C             at the optimal soultion of the MMA subproblem.
C    RAA0   = Parameter raa_0 in the MMA subproblem. 
C    RAA(i) = Parameter raa_i in the MMA subproblem. 
C      Y(i) = Value of the "artificial" variable y_i.
C      Z    = Value of the "minimax" variable z.
C      A(i) = Coefficient a_i for the variable z.
C      C(i) = Coefficient c_i for the variable y_i.
C   ULAM(i) = Value of the dual variable lambda_i.
C  GRADF(i) = Gradient component of the dual objective function.
C  DSRCH(i) = Search direction component in the dual subproblem.
C  HESSF(k) = Hessian matrix component of the dual function.
C IYFREE(i) = 0 for dual variables which are fixed to zero in
C               the current subspace of the dual subproblem,
C           = 1 for dual variables which are "free" in
C               the current subspace of the dual subproblem.
C
C********+*********+*********+*********+*********+*********+*********+
C
C  The USER should now give values to the parameters
C  M, N, GEPS, XVAL (starting point),
C  XMIN, XMAX, FMAX, A and C.
C
C
C********+*********+*********+*********+*********+*********+*********+
C  The sizes of the above arrays must be at least as follows:
C
C    XVAL(N),XOLD1(N),XOLD2(N),XMMA(N),
C    XMIN(N), XMAX(N), XLOW(N),XUPP(N),
C    ALFA(N), BETA(N),DF0DX(N),
C    A(M),B(M),C(M),Y(M),RAA(M),ULAM(M),
C    FVAL(M),FAPP(M),FNEW(M),FMAX(M),
C    DFDX(M*N),P(M*N),Q(M*N),P0(N),Q0(N),
C    UU(M),GRADF(M),DSRCH(M),HESSF(M*(M+1)/2),
C    IYFREE(M)
C
C


c     C-CODE:      CALL FUNC2(M,N,XVAL,F0VAL,DF0DX,FVAL,DFDX)

      subroutine GCMMA_F1(INNER, INNMAX, ITE, ITER, MAXITE, M, N,
     1                    GEPS, F0VAL, F0NEW, F0APP, RAA0, Z,
     2                    ALFA, BETA, DF0DX, P0,    Q0,
     3                    XMAX, XMIN, XMMA,  XOLD1, XOLD2,
     4                    XLOW, XUPP, XVAL,
     5                    A, B, C,     DRSCH,  FAPP, FMAX, FNEW,
     6                    FVAL, GRADF, IYFREE, RAA,  ULAM, UU,
     7                    Y,
     8                    DFDX, P, Q, HESSF)

      integer INNER, INNMAX, ITE, ITER, MAXITE, M, N
      real*8  GEPS, F0VAL, F0NEW, F0APP, RAA0, Z
      real*8  ALFA(N), BETA(N), DF0DX(N), P0(N),    Q0(N),
     1        XMAX(N), XMIN(N), XMMA(N),  XOLD1(N), XOLD2(N),
     2        XLOW(N), XUPP(N), XVAL
      real*8  A(M), B(M), C(M),  DRSCH(M),  FAPP(M), FMAX(M), FNEW(M),
     1        FVAL(M), GRADF(M), IYFREE(M), RAA(M),  ULAM(M), UU(M),
     2        Y(M)
      real*8  DFDX(M*N), P(M*N), Q(M*N), HESSF(M*(M+1)/2)


C  The outer iterative process starts.
C
      ITER=ITER+1
      ITE=ITE+1
C
C  RAA0,RAA,XLOW,XUPP,ALFA and BETA are calculated.
C
      CALL RAASTA(M,N,RAA0,RAA,XMIN,XMAX,DF0DX,DFDX)
      CALL ASYMPG(ITER,M,N,XVAL,XMIN,XMAX,XOLD1,XOLD2,
     1            XLOW,XUPP,ALFA,BETA)
C      CALL ASYMPG(ITER,M,N,XVAL,XMIN,XMAX,XOLD1,XOLD2,
C     1            XLOW,XUPP,ALFA,BETA,RAA0,RAA)
      INNER=0

      return
      end



c     C-CODE:      Go back here if inner needs a redo

      subroutine GCMMA_F2(INNER, INNMAX, ITE, ITER, MAXITE, M, N,
     1                    GEPS, F0VAL, F0NEW, F0APP, RAA0, Z,
     2                    ALFA, BETA, DF0DX, P0,    Q0,
     3                    XMAX, XMIN, XMMA,  XOLD1, XOLD2,
     4                    XLOW, XUPP, XVAL,
     5                    A, B, C,     DRSCH,  FAPP, FMAX, FNEW,
     6                    FVAL, GRADF, IYFREE, RAA,  ULAM, UU,
     7                    Y,
     8                    DFDX, P, Q, HESSF)

      integer INNER, INNMAX, ITE, ITER, MAXITE, M, N
      real*8  GEPS, F0VAL, F0NEW, F0APP, RAA0, Z
      real*8  ALFA(N), BETA(N), DF0DX(N), P0(N),    Q0(N),
     1        XMAX(N), XMIN(N), XMMA(N),  XOLD1(N), XOLD2(N),
     2        XLOW(N), XUPP(N), XVAL
      real*8  A(M), B(M), C(M),  DRSCH(M),  FAPP(M), FMAX(M), FNEW(M),
     1        FVAL(M), GRADF(M), IYFREE(M), RAA(M),  ULAM(M), UU(M),
     2        Y(M)
      real*8  DFDX(M*N), P(M*N), Q(M*N), HESSF(M*(M+1)/2)

C  The subproblem is generated and solved.
C
      CALL MMASUG(ITER,M,N,GEPS,IYFREE,XVAL,XMMA,
     1            XMIN,XMAX,XLOW,XUPP,ALFA,BETA,
     2            A,B,C,Y,Z,RAA0,RAA,ULAM,
     3            F0VAL,FVAL,F0APP,FAPP,FMAX,DF0DX,DFDX,
     4            P,Q,P0,Q0,UU,GRADF,DSRCH,HESSF)
C
C  The USER should now calculate function values at XMMA.
C  The result should be put in F0NEW and FNEW.
C
      return
      end



c     C-CODE:       CALL FUNC1(M,N,XMMA,F0NEW,FNEW)

      subroutine GCMMA_F3(INNER, INNMAX, ITE, ITER, MAXITE, M, N,
     1                    GEPS, F0VAL, F0NEW, F0APP, RAA0, Z,
     2                    ALFA, BETA, DF0DX, P0,    Q0,
     3                    XMAX, XMIN, XMMA,  XOLD1, XOLD2,
     4                    XLOW, XUPP, XVAL,
     5                    A, B, C,     DRSCH,  FAPP, FMAX, FNEW,
     6                    FVAL, GRADF, IYFREE, RAA,  ULAM, UU,
     7                    Y,
     8                    DFDX, P, Q, HESSF,
     9                    REDO)

      integer INNER, INNMAX, ITE, ITER, MAXITE, M, N
      real*8  GEPS, F0VAL, F0NEW, F0APP, RAA0, Z
      real*8  ALFA(N), BETA(N), DF0DX(N), P0(N),    Q0(N),
     1        XMAX(N), XMIN(N), XMMA(N),  XOLD1(N), XOLD2(N),
     2        XLOW(N), XUPP(N), XVAL
      real*8  A(M), B(M), C(M),  DRSCH(M),  FAPP(M), FMAX(M), FNEW(M),
     1        FVAL(M), GRADF(M), IYFREE(M), RAA(M),  ULAM(M), UU(M),
     2        Y(M)
      real*8  DFDX(M*N), P(M*N), Q(M*N), HESSF(M*(M+1)/2)
      integer REDO


      IF(INNER.GE.INNMAX) GOTO 60
C
C  It is checked if the approximations were conservative.
C
      CALL CONSER(M,ICONSE,GEPS,F0NEW,F0APP,FNEW,FAPP)
      IF(ICONSE.EQ.1) GOTO 60
C
C  The approximations were not conservative, so RAA0 and RAA
C  are updated and one more inner iteration is started.
C
      INNER=INNER+1
      CALL RAAUPD(M,N,GEPS,XMMA,XVAL,XMIN,XMAX,XLOW,XUPP,
     1            F0NEW,FNEW,F0APP,FAPP,RAA0,RAA)
c     We need to redo
      REDO=1
      return
C
 60   continue
c     We can terminate the inner loop
      REDO=0

      CALL XUPDAT(N,ITER,XMMA,XVAL,XOLD1,XOLD2)
      CALL FUPDAT(M,F0NEW,FNEW,F0VAL,FVAL)

      return
      end

c     C-CODE:   if redo=1 then go back up for a repeat of the inner loop
c               else that's the iteration finished

