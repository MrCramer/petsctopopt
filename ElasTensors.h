#ifndef ELASTENSORS_H
#define ELASTENSORS_H

#include <memory>

#include <petscsys.h>
#include <petscerror.h>

#include "ArrayNd.h"
#include "JSON.h"


extern const int voigt[6][3];


class TensorBase {
  public:
    virtual PetscErrorCode  EvalTensor(const double *element_design, int it, Array4d<double> &tensor) const = 0;
    virtual PetscErrorCode EvalDTensor(const double *element_design, int it, const int design_var, Array4d<double> &dtensor) const= 0;

    virtual ~TensorBase() { }
};


PetscErrorCode GetTensor(const JSONObject *spec, std::unique_ptr<TensorBase> &tensor);

inline
PetscErrorCode CopyDesignVariables(const Array4d<double> &design, int i, int j, int k,
                                   std::vector<double> &element_vars) {
  PetscFunctionBeginUser;

  int N = design.GetI();
  element_vars.resize(N);
  for(int ii=0; ii<N; ++ii) {
    element_vars[ii] = design(ii,i,j,k);
  }
  PetscFunctionReturn(0);
}


#endif

