function WriteFieldNd(filename, field, ndim)
  if ~exist('ndim','var')
    ndim = size(size(field),2);
  end
  
  if ndim==2
    ident = 'BArray2D';
    sz = [size(field,1), size(field,2)];
  elseif ndim==3
    ident = 'BArray3D';
    sz = [size(field,1), size(field,2), size(field,3)];
  elseif ndim==4
    ident = 'BArray4D';
    sz = [size(field,1), size(field,2), size(field,3), size(field,4)];
  elseif ndim==5
    ident = 'BArray5D';
    sz = [size(field,1), size(field,2), size(field,3), size(field,4), size(field,5)];
  elseif ndim==6
    ident = 'BArray6D';
    sz = [size(field,1), size(field,2), size(field,3), size(field,4), size(field,5), size(field,6)];
  else
    error('Unexpected size of field');
  end
  
  [file, msg] = fopen(filename, 'w');
  if file==-1
    error(['Failed to open file: ', msg])
  end
  fwrite(file, ident, 'uchar');
  fwrite(file, sz,    'uint32');
  fwrite(file, field, 'double');
  fclose(file);
end

% function WriteFieldNd(filename, field)
%   [file, msg] = fopen(filename, 'w');
%   
%   if(file==-1)
%     error(msg);
%   end
%   
%   dim = size(field);
%   fprintf(file, '%d\n', dim);
%   fprintf(file, '%.15e\n', reshape(field, [prod(dim), 1]));