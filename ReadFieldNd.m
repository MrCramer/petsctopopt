
function output = ReadFieldNd(filename)
  zipped = false;
  if length(filename)>3 && strcmp(filename(end-2:end),'.gz')
    filenames = gunzip(filename,tempdir);
    filename = filenames{1};
    zipped = true;
  end
  
  [file, msg] = fopen(filename);
  if file==-1
    error(['Failed to open file: ', msg]);
  end
  
  identifier = fread(file, [1,8], 'char');

  if isequal(identifier, 'BArray2D')
    dims = fread(file, [1,2], 'uint32');
    data = fread(file, prod(dims), 'double');
    output = reshape(data, dims);
  elseif isequal(identifier, 'BArray3D')
    dims = fread(file, [1,3], 'uint32');
    data = fread(file, prod(dims), 'double');
    output = reshape(data, dims);
  elseif isequal(identifier, 'BArray4D')
    dims = fread(file, [1,4], 'uint32');
    data = fread(file, prod(dims), 'double');
    output = reshape(data, dims);
  elseif isequal(identifier, 'BArray5D')
    dims = fread(file, [1,5], 'uint32');
    data = fread(file, prod(dims), 'double');
    output = reshape(data, dims);
  elseif isequal(identifier, 'BArray6D')
    dims = fread(file, [1,6], 'uint32');
    data = fread(file, prod(dims), 'double');
    output = reshape(data, dims);
  else
    error('    Unknown header .');
  end

  fclose(file);
  if zipped
    delete(filename)
  end
end

%   raw = importdata(filename);
%   assert(size(raw,2)==1,                  'File does not produce a column vector');
%   assert(isequal(size(size(raw)), [1 2]), 'File does not produce a column vector');
%   dim = raw(1:4)';
%   
%   if(size(raw,1)==prod(dim(1:3)) + 3)
%     % 3D
%     output = reshape(raw(4:end),dim(1:3));
%     
%   elseif(size(raw,1)==prod(dim)+ 4)
%     % 2D
%     output = reshape(raw(5:end),dim);
%     
%   else
%     error('    Declared size doesn''t match data.');
%   end