% 2d homogenisation test

E = 1;
n = 1/3;

C_h = zeros(3,3);
C_b = E/(1-n*n) * [1 n       0;
                   n 1       0;
                   0 0 (1-n)/2];

% Strain xx: (e1yy + e2yy)/2 == 0
sig1 = @(e1yy) 1.0*C_b*[1; e1yy; 0]; % sigma_ij of phase 1
sig2 = @(e1yy) 0.5*C_b*[1;-e1yy; 0];

e1yy = fzero(@(x) [0 1 0]*sig1(x) - [0 1 0]*sig2(x), 0); % sigma_yy must be equal for both phases

C(:,2) = 0.5*(sig1(e1yy) + sig2(e1yy));

% Strain yy: (e1yy + e2yy)/2 == 1
sig1 = @(e1yy) 1.0*C_b*[0;  e1yy; 0]; % sigma_ij of phase 1
sig2 = @(e1yy) 0.5*C_b*[0;2-e1yy; 0];

e1yy = fzero(@(x) [0 1 0]*sig1(x) - [0 1 0]*sig2(x), 0); % sigma_yy must be equal for both phases

C(:,2) = 0.5*(sig1(e1yy) + sig2(e1yy));

%Strain xy: (e1xy + e2xy)/2 == 2
sig1 = @(e1xy) 1.0*C_b*[0; 0;   e1xy]; % sigma_ij of phase 1
sig2 = @(e1xy) 0.5*C_b*[0; 0; 2-e1xy];

e1xy = fzero(@(x) [0 0 1]*sig1(x) - [0 0 1]*sig2(x), 0); % sigma_xy must be equal for both phases

C(:,3) = 0.5*(sig1(e1xy) + sig2(e1xy));