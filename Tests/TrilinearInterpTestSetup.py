
import numpy

from ArrayNd import *

design = numpy.zeros([3,4,4,4])
design[0,:,:,:] = 0.3
design[1,:,:,:] = 1.2
design[2,:,:,:] = 0.9

WriteFieldNd(design,'Tests/TrilinearInterpTestDesign.f4d.gz',zip_output=True)

def GenIsoTensor(E,nu):
  lam = E*nu/(1+nu)/(1-2*nu)
  mu = E/2/(1+nu)
  l2m = lam+2*mu
  return numpy.array([
    [l2m,lam,lam, 0, 0, 0],
    [lam,l2m,lam, 0, 0, 0],
    [lam,lam,l2m, 0, 0, 0],
    [  0,  0,  0,mu, 0, 0],
    [  0,  0,  0, 0,mu, 0],
    [  0,  0,  0, 0, 0,mu]])


tensors = numpy.zeros([3,3,3,6,6])
# Set the seed to be a constant so we get the same tensors every time
numpy.random.seed(13528993)

for i in range(3):
  for j in range(3):
    for k in range(3):
      base = GenIsoTensor(i+j+k+1,(i+j-k+3)/20)
      modded = base + numpy.random.rand(6,6)/5
      modded[base==0] = 0
      tensors[i,j,k,:,:] = (modded + modded.transpose())/2

WriteFieldNd(tensors,'Tests/TrilinearInterpTestTensors.f5d.gz',zip_output=True)

a1 = 0.3;
a2 = 0.2;
a3 = 0.9;
actual = (1-a1)*(1-a2)*(1-a3)*tensors[0,1,0] \
       +    a1 *(1-a2)*(1-a3)*tensors[1,1,0] \
       + (1-a1)*   a2 *(1-a3)*tensors[0,2,0] \
       +    a1 *   a2 *(1-a3)*tensors[1,2,0] \
       + (1-a1)*(1-a2)*   a3 *tensors[0,1,1] \
       +    a1 *(1-a2)*   a3 *tensors[1,1,1] \
       + (1-a1)*   a2 *   a3 *tensors[0,2,1] \
       +    a1 *   a2 *   a3 *tensors[1,2,1]

#print(actual)
