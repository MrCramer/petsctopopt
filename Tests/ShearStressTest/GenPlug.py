
import cramer_fem as fem
import numpy as np

domain = (30,30,20)

rho = 1e-6*np.ones(shape=domain)

# x and z are element centres for the whole domain
x = np.linspace(0,1,domain[0]+1)[:-1]
x = x+x[1]/2.
z = np.linspace(0,1,domain[2]+1)[:-1]
z = z+z[1]/2.


# 3D
X,Y,Z = np.meshgrid(x,x,z,indexing='ij')
R = ((X-0.5)**2 + (Y-0.5)**2)**0.5

# Traction surface
rho[:,:,-1] = 1.
# "interface" block
rho[np.logical_and(Z>0.25, Z<0.75)] = 1.
# Top of the cylinder
rho[np.logical_and(R<0.30, Z>=0.75)] = 1.
# Bottom of the block
rho[np.logical_and(R>0.40, Z<=0.25)] = 1.

fem.WriteFieldNd(rho.reshape((1,domain[0],domain[1],domain[2])),'Plug.f4d')

# 2D
X,Z = np.meshgrid(x,z,indexing='ij')
R = abs(X-0.5)

# Traction surface
rho = 1e-6*np.ones(shape=(domain[0],domain[2]))
rho[:,-1] = 1.
# "interface" block
rho[np.logical_and(Z>0.25, Z<0.75)] = 1.
# Top of the cylinder
rho[np.logical_and(R<0.30, Z>=0.75)] = 1.
# Bottom of the block
rho[np.logical_and(R>0.40, Z<=0.25)] = 1.

fem.WriteFieldNd(rho.reshape((1,domain[0],domain[2],1)),'Plug2D.f4d')
