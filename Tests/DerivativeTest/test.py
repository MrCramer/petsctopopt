#!/usr/bin/env python 

import json
import numpy
import random
import subprocess
import sys

from ArrayNd import *

solver_opts = ["-ksp_type", "cg", "-ksp_max_it", "1000", "-ksp_rtol", "1e-8",
               "-pc_type", "composite", "-pc_composite_pcs", "jacobi,gamg", "-pc_composite_type", "multiplicative",
               "-sub_0_pc_jacobi_abs", "-sub_1_pc_gamg_type", "agg", "-sub_1_pc_gamg_agg_nsmooths", "1"]

solver_opts = ["-ksp_type", "preonly", "-pc_type", "lu"]

print("Testing calculation of derivitives. All output should be near 1.0.")


def RunProblem(obj_name,filename):
  p = subprocess.Popen(["./optimising_elastic_solver", "-i", "Tests/DerivativeTest/Spec"+obj_name+".json",
                        "-calculate_objective_of", filename]
                         +solver_opts,
                       stdout=subprocess.PIPE)
  out, err = p.communicate()
  if err:
    print(err)
  return float(out)


def TestObjective(name):
  print("\nTesting " + name)

  with open("Tests/DerivativeTest/Spec"+name+".json") as f:
    problem_spec = json.load(f)
  
  dom = problem_spec["domain"]
  if dom[2]==1:
    dom[2] = 2
  
  if "periodic" in problem_spec and problem_spec["periodic"]:
    base = numpy.ones((1,dom[0],dom[1],dom[2]))
  else:
    base = numpy.ones((1,dom[0]-1,dom[1]-1,dom[2]-1))
 
  n_design = 1
  if name=="AxisAlignedEllipsoidal":
    base,_,_,_ = numpy.meshgrid([1,2,3,.2,.3,.4],numpy.ones(8),numpy.ones(8),numpy.ones(8),indexing='ij')
    n_design = 6
  if name=="TrilinearInterp":
    base,_,_,_ = numpy.meshgrid([.2,.3,.4],numpy.ones(8),numpy.ones(8),numpy.ones(8),indexing='ij')
    n_design = 3
  if name=="TrilinearInterpolatedMeanFunction":
    base = numpy.random.rand(3,dom[0]-1,dom[1]-1,dom[2]-1)*5.0
    n_design = 3
    interpolant_keys = numpy.random.rand(5,5,5)
    WriteFieldNd(interpolant_keys,'Tests/DerivativeTest/interp_keys.f3d')
  if name=="TrilinearInterpolatedTensor":
    base = numpy.random.rand(3,dom[0]-1,dom[1]-1,dom[2]-1)*5.0
    n_design = 3

  WriteFieldNd(base, 'Tests/DerivativeTest/base.f4d');

  p = subprocess.Popen(["./optimising_elastic_solver", "-i", "Tests/DerivativeTest/Spec"+name+".json",
                        "-calculate_objective_of", "Tests/DerivativeTest/base.f4d",
                        "-save_d_design", "Tests/DerivativeTest/base_d"]
                         +solver_opts,
                       stdout=subprocess.PIPE)
  out, err = p.communicate()

  if err:
    print(err)
  base_obj = float(out)
  base_d = ReadFieldNd("Tests/DerivativeTest/base_d.f4d")

  # Here we add to all the elements a small amount and see the result
  #
  # rho_i = rho_i + delta  ->  f = f + delta df/drho_i
  # 
  # set delta to drho_i/df and we should see f change by 1.
  mag = 1e-5 * base_obj;
  WriteFieldNd(base + mag/base.size * base_d**-1, 'Tests/DerivativeTest/add_all.f4d')
  print("  Adding a bit to all: %.4f" % (1/mag*(RunProblem(name,"Tests/DerivativeTest/add_all.f4d")-base_obj)))


  # Here we add to one element a small amount and see the result, point chosen by random
  for _ in range(5):
    d = random.randrange(n_design)
    i = random.randrange(dom[0]-1)
    j = random.randrange(dom[1]-1)
    k = random.randrange(dom[2]-1)
    new = base.copy()
    eps = 1e-5
    new[d,i,j,k] += eps
    WriteFieldNd(new, 'Tests/DerivativeTest/add_one.f4d')
    obj = RunProblem(name,"Tests/DerivativeTest/add_one.f4d")
    dfdr = (obj-base_obj)/eps
    expected = base_d[d,i,j,k]
    print("  Adding a bit to ([%d],%d,%d,%d): %.4f" % (d, i, j, k, dfdr/expected))


if len(sys.argv)==1:
  TestObjective("Mean2D")
  TestObjective("WCL2D")
  TestObjective("Mass2D")
  TestObjective("AxisAlignedEllipsoidal")
  TestObjective("TrilinearInterpolatedMeanFunction")
  TestObjective("Mean3D")
  TestObjective("WCL3D")
  TestObjective("ShearStress3D")
  TestObjective("Hoffman3D")
  TestObjective("ShearStress2D")
  TestObjective("BoneResorption3D")
else:
  for i in range(1,len(sys.argv)):
    TestObjective(sys.argv[i])
