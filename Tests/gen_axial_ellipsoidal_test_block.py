#!/usr/bin/env python

from ArrayNd import *
import numpy as np

design,_,_,_ = np.meshgrid([1,2,3,.21,.23,.25],[1,1,1,1],[1,1,1,1],[1,1,1,1],indexing='ij')
WriteFieldNd(design,"AxisAlignedEllipsoidal.f4d")
