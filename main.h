#ifndef MAIN_H
#define MAIN_H

PetscErrorCode EnforceDesignConstraints(const JSONObject &spec,
                                        Array4d<double>  *design,
                                        Array4d<char>    *fixed,
                                        Array4d<double>  *d_design,
                                        std::vector<Array4d<double> > *d_constraints);

#endif
