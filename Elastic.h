#ifndef ELASTIC_H
#define ELASTIC_H

#include <vector>

#include <petscksp.h>

#include "ArrayNd.h"
#include "JSON.h"


PetscErrorCode InitFEMCalc(const JSONObject *spec, bool &is_root);
PetscErrorCode FinalizeFEMCalc();
PetscErrorCode WorkerLoop();

PetscErrorCode CalculateObjective(const int it,
                                  bool calculate_derivatives,
                                  const Array4d<double> &design,
                                  Array4d<double>       &d_design,
                                  std::vector<double>           &constraints,
                                  std::vector<Array4d<double> > &d_constraints,
                                  double &objective);

PetscErrorCode CalculateHomogenization(const Array4d<double> &design,
                                       Array2d<double> &tensor);

PetscErrorCode CalculateDisplacement(const Array4d<double> &design,
                      std::vector<Array4d<double> > &displacement);

struct DOM_TYPE {
  // Global domain details
  int dof;            // degrees of freedom of the displacement (2 or 3).
  int design_dof;     // degrees of freedom of the design.
  int I,J,K;          // number of nodes in domain
  int I_el,J_el,K_el; // number of elements in domain
  double h[3];        // size of elements in real units

  // Integrals of the shape functions
  double grad_eta_int[8][3];
  double grad_eta_grad_eta_int[8][3][8][3];
};
extern DOM_TYPE DOM;


inline Array4d<double> ReadElementSizeArray(const std::string &filename) {
  return ReadArray4d(filename,DOM.design_dof,DOM.I_el,DOM.J_el,DOM.K_el);
}

#endif
