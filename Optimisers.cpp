#include <cmath>

#include "Elastic.h"
#include "FMatrix.h"
#include "HVec.h"
#include "main.h"
#include "Optimisers.h"

#include "zlib.h"

#undef __FUNCT__
#define __FUNCT__ __func__

const bool verbose = false;


double ComputeVolumeFraction(const Array4d<double> &design) {
  const int I = design.GetJ();
  const int J = design.GetK();
  const int K = design.GetL();

  double vol = 0.0;
  for(int i=0; i<I; ++i)
  for(int j=0; j<J; ++j)
  for(int k=0; k<K; ++k) {
    vol += design(0,i,j,k);
  }
  return vol/(I*J*K);
};


/// /////////////////// ///
/// Optimality Criteria ///
/// /////////////////// ///

class OCOptimiser : public OptFuncBase {
  public:
    OCOptimiser(const JSONObject &spec) : it_(0) {
      std::string objective_type = spec.ObjectMember("objective function")
                                      ->ObjectMember("type")
                                      ->AsString();

      if(objective_type != "mean" && objective_type != "worst case") {
        throw(std::runtime_error("The OC optimiser must be paired with a mean or WC objective"));
      }

      if(spec.ObjectMember("constraints")->ArraySize()!=1) {
        throw(std::runtime_error("OC optimiser must have a mass constraint"));
      }
      const JSONValue *constraint = spec.ObjectMember("constraints")->ArrayMember(0);
      
      if(constraint->ObjectMember("type")->AsString()!="mass") {
        throw(std::runtime_error("OC optimiser must have a mass constraint"));
      }

      vol_frac_ = constraint->ObjectMember("limit")->AsDouble();
      
      const JSONValue *opt_spec = spec.ObjectMember("optimiser");
      rho_limits_= opt_spec->ObjectMember("rho limits"   )->ArrayAsVectorOfDoubles(2);
      move_      = opt_spec->ObjectMember("rho move"     )->AsDouble();
      move_decr_ = opt_spec->ObjectMember("move decrease")->AsDouble();
      move_incr_ = opt_spec->ObjectMember("move increase")->AsDouble();
      move_limit_ = move_;
    }
      
      
    PetscErrorCode Step(const int it,
                        const JSONObject &spec,
                        const Array4d<char> &fixed_design,
                        const Array4d<double> &old_design,
                              Array4d<double> &new_design,
                        double &objective,
                        std::vector<double> &constraints) {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;

      const int design_dof = spec.ObjectMember("design dof")->AsInt();
      if(design_dof!=1) {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,
                "Optimality criteria optimised must have only one design variable: rho.");
      }

      ierr =  CalculateObjective(it,true,
                                 old_design, d_design,
                                 constraints, d_constraints,
                                 objective);                                           CHKERRQ(ierr);

      double tol  =  1e-6;
      double high =  0.0;
      double low  = -1e10;
      double mid  = (high+low)/2.0;
  
      new_design.Resize(old_design.GetI(),old_design.GetJ(), old_design.GetK(), old_design.GetL());

      while(std::abs((high-low)/low) > tol) {
        ierr =  UpdateRho(spec,d_design, old_design, fixed_design, mid, new_design);   CHKERRQ(ierr);
        if(ComputeVolumeFraction(new_design) > vol_frac_) {
          high = mid;
        } else {
          low = mid;
        }
        mid = (high+low)/2;
      }
      ierr =  UpdateRho(spec,d_design, old_design, fixed_design, mid, new_design);     CHKERRQ(ierr);
      
      if(it_ >= 3) {
        // Change the move limit 
        double p1 = objective - c_m1;
        double p2 = c_m1 - c_m2;
        double p3 = c_m2 - c_m3;
        if(p1*p2>0 && p2*p3>0) move_ *= move_incr_;
        if(p1*p2<0 && p2*p3<0) move_ *= move_decr_;
        if(move_>move_limit_) move_ = move_limit_;
      }
      c_m3 = c_m2;
      c_m2 = c_m1;
      c_m1 = objective;
      it_++;

      PetscFunctionReturn(0);
    }

  private:
    PetscErrorCode UpdateRho(const JSONObject &spec,
                             const Array4d<double> &d_design,
                             const Array4d<double> &design_old,
                             const Array4d<char> &fixed_design,
                             double cutoff,
                             Array4d<double> &design_new) {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      for(unsigned i=0; i<design_old.GetJ(); i++)
      for(unsigned j=0; j<design_old.GetK(); j++)
      for(unsigned k=0; k<design_old.GetL(); k++) {
        if(fixed_design(0,i,j,k)) {
          design_new(0,i,j,k) = design_old(0,i,j,k);
        } else if(d_design(0,i,j,k)<cutoff) {
          design_new(0,i,j,k) = std::min(rho_limits_[1], design_old(0,i,j,k)+move_);
        } else {
          design_new(0,i,j,k) = std::max(rho_limits_[0], design_old(0,i,j,k)-move_);
        }
      }
      ierr =  EnforceDesignConstraints(spec,&design_new,nullptr,nullptr,nullptr);      CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

    std::vector<double> rho_limits_;
    double move_;
    double move_decr_;
    double move_incr_;
    double move_limit_;
    double vol_frac_;
    int it_;
    double c_m1, c_m2, c_m3;

    Array4d<double> d_design;

    std::vector<Array4d<double> > d_constraints;
};



/// /////////////////////////// ///
/// Method of Moving Asymptotes ///
/// /////////////////////////// ///


// This is a mess, it's pretty much just transposed from the MATLAB functions directly.

/* Taken from the original header in the matlab file: 
%    Written in May 1999 by
%    Krister Svanberg <krille@math.kth.se>
%    Department of Mathematics
%    SE-10044 Stockholm, Sweden.


%      Minimize  f_0(x) + a_0*z + sum( c_i*y_i + 0.5*d_i*(y_i)^2 )
%    subject to  f_i(x) - a_i*z - y_i <= 0,  i = 1,...,m
%                xmin_j <= x_j <= xmax_j,    j = 1,...,n
%                z >= 0,   y_i >= 0,         i = 1,...,m

*/


const double PI = 3.14159265358979306;

typedef std::vector<HVec> HVecVec;

HVec Subsolve(double  epsimin, const HVec &low, const HVec  &upp, const HVec &alph, const HVec &beta,
              const HVec  &p0, const HVec  &q0, const HVecVec &P, const HVecVec &Q,
              const double a0, const HVec  &ai, const HVec    &b, const HVec   &ci, const HVec &di) {
  unsigned N = low.size();
  unsigned M = P.size();

  double epsi = 1;

  HVec   x = 0.5*(alph+beta);
  HVec   y(M,1.0);
  double z = 1;
  HVec   lam(M,1.0);
  HVec   xsi = Max(1.0, 1/(x-alph));
  HVec   eta = Max(1.0, 1/(beta-x));
  HVec   mu = Max(1.0, 0.5*ci);
  HVec   s(M,1.0);
  double zet = 1;

  while(epsi > epsimin) {
    HVec epsvecn(N, epsi);
    HVec epsvecm(M, epsi);
    HVec ux1 = upp - x;
    HVec xl1 = x - low;
    HVec ux2 = ux1*ux1;
    HVec xl2 = xl1*xl1;
    HVec uxinv1 = 1/ux1;
    HVec xlinv1 = 1/xl1;

    HVec plam = p0;
    HVec qlam = q0;
    HVec gvec(M);
    for(unsigned i=0; i<M; ++i)
    for(unsigned j=0; j<N; ++j) {
      plam[j] += P[i][j]*lam[i];
      qlam[j] += Q[i][j]*lam[i];
      gvec[i] = P[i][j]*uxinv1[j] + Q[i][j]*xlinv1[j];
    }
    HVec dpsidx = plam/ux2 - qlam /xl2;

    HVec rex = dpsidx - xsi + eta;
    HVec rey = ci + di*y - mu - lam;
    double rez = a0 - zet - Dot(ai, lam);
    HVec relam = gvec - z*ai - y + s - b;
    HVec rexsi = xsi*(x-alph) - epsvecn;
    HVec reeta = eta*(beta-x) - epsvecn;
    HVec remu  = mu*y - epsvecm;
    double rezet = zet*z - epsi;
    HVec res = lam*s - epsvecm;
    
    HVec residue;
    residue.reserve(rex.size() + rey.size() + 1 +
                    relam.size() + rexsi.size() + reeta.size() + remu.size() + 1 + res.size());
  
    #define APPEND(v) residue.insert(residue.end(), v.begin(), v.end())
    residue.push_back(rez);
    residue.push_back(rezet);
    APPEND(rex);   APPEND(rey);
    APPEND(relam); APPEND(rexsi);
    APPEND(reeta); APPEND(remu);
    APPEND(res);
    #undef APPEND
    
    int it = 0;
    while(Max(Abs(residue)) > 0.9*epsi && it < 100) {
      ++it;

      ux1 = upp-x;
      xl1 = x-low;
      ux2 = ux1*ux1;
      xl2 = xl1*xl1;
      HVec ux3 = ux1*ux2;
      HVec xl3 = xl1*xl2;
      uxinv1 = 1/ux1;
      xlinv1 = 1/xl1;
      HVec uxinv2 = 1/ux2;
      HVec xlinv2 = 1/xl2;

      plam = p0;
      qlam = q0;
      HVecVec GG(M, HVec(N));
      for(unsigned i=0; i<M; ++i)
      for(unsigned j=0; j<N; ++j) {
        plam[j] += lam[i]*P[i][j];
        qlam[j] += lam[i]*Q[i][j];
        gvec[i] = P[i][j]*uxinv1[j] + Q[i][j]*xlinv1[j];
        GG[i][j] = P[i][j]*uxinv2[j] - Q[i][j]*xlinv2[j];
      }
      dpsidx = plam*uxinv2 - qlam*xlinv2;
      HVec delx = dpsidx - epsvecn/(x-alph) + epsvecn/(beta-x);
      HVec dely = ci + di*y - lam - epsvecm/y;
      double delz  = a0 - Dot(ai, lam) - epsi/z;
      HVec dellam = gvec - z*ai - y - b + epsvecm/lam;
      HVec diagx = 2.0*(plam/ux3 + qlam/xl3) + xsi/(x-alph) + eta/(beta-x);
      HVec diagxinv = 1.0/diagx;
      HVec diagy = di + mu/y;
      HVec diagyinv = 1.0/diagy;
      HVec diaglam = s/lam;
      HVec diaglamyi = diaglam + diagyinv;

      if(M>=N) {
        throw(std::logic_error("mmasub: Didn't implement the M>=N case"));
      }

      HVec blam = dellam + dely/diagy;
      HVec delx_on_diagx = delx/diagx;
      for(unsigned i=0; i<M; ++i) {
        blam[i] -= Dot(GG[i],delx_on_diagx);
      }
      FMatrix AA(M+1,M+1);
      for(unsigned i=0; i<M; ++i) {
        AA(i,i) += diaglamyi[i];
      }
      
      for(unsigned i=0; i<M; ++i) 
      for(unsigned j=0; j<M; ++j) {
        AA(i,j) += Dot(GG[i]*diagxinv, GG[j]);
      }
      for(unsigned i=0; i<M; ++i) {
        AA(i,M) = ai[i];
        AA(M,i) = ai[i];
      }
      AA(M,M) = -zet/z;

      HVec bb = blam;
      bb.push_back(delz);

      HVec solut;
      AA.Solve(bb, solut);

      HVec dlam = solut;
      dlam.pop_back();

      double dz = solut[M];
      HVec dx = -1.0*delx/diagx;
      for(unsigned i=0; i<M; ++i)
      for(unsigned j=0; j<N; ++j) {
        dx[j] -= dlam[i]*GG[i][j] / diagx[j];
      }

      HVec dy = dlam/diagy - dely/diagy;
      HVec dxsi = epsvecn/(x-alph) - xsi - (xsi*dx)/(x-alph);
      HVec deta = epsvecn/(beta-x) - eta + (eta*dx)/(beta-x);
      HVec dmu = epsvecm/y - mu - (mu*dy)/y;
      double dzet = -zet + epsi/z - zet*dz/z;
      HVec ds = epsvecm/lam - s - (s*dlam)/lam;
      
      HVec  xx;
      xx.reserve(y.size() + 1 + lam.size() + xsi.size() + eta.size() + mu.size() + 1 + s.size());
      xx.push_back(z);
      xx.push_back(zet);

      HVec dxx;
      dxx.reserve(dy.size() + 1 + dlam.size() + dxsi.size() + deta.size() + dmu.size() + 1 + ds.size());
      dxx.push_back(dz);
      dxx.push_back(dzet);
      
      // Concatonates a bunch of vectors into xx and dxx
      #define APPEND(v)  xx.insert(xx.end(), v.begin(), v.end()); \
                        dxx.insert(dxx.end(), d##v.begin(), d##v.end())

      APPEND(y);   APPEND(lam);
      APPEND(xsi); APPEND(eta);
      APPEND(mu);  APPEND(s);
      #undef APPEND

      double stmxx   = Max(-1.01*dxx/xx);
      double stmalph = Max(-1.01*dx/(x-alph));
      double stmbeta = Max( 1.01*dx/(beta-x));
      double steg    = 1.0/std::max(std::max(1.0, stmxx), std::max(stmbeta, stmalph));

      HVec     xold =   x;
      HVec     yold =   y;
      double   zold =   z;
      HVec   lamold = lam;
      HVec   xsiold = xsi;
      HVec   etaold = eta;
      HVec    muold =  mu;
      double zetold = zet;
      HVec     sold =   s;

      int itto = 0;
      double residuenorm2 = Dot(residue,residue);
      double resinew2 = 2*residuenorm2;
      while(resinew2 > residuenorm2 && itto < 50) {
        ++itto;

        x   =   xold + steg*dx;
        y   =   yold + steg*dy;
        z   =   zold + steg*dz;
        lam = lamold + steg*dlam;
        xsi = xsiold + steg*dxsi;
        eta = etaold + steg*deta;
        mu  = muold  + steg*dmu;
        zet = zetold + steg*dzet;
        s   =   sold + steg*ds;
        ux1 = upp-x;
        xl1 = x-low;
        ux2 = ux1*ux1;
        xl2 = xl1*xl1;
        uxinv1 = 1.0/ux1;
        xlinv1 = 1.0/xl1;
        
        plam = p0;
        qlam = q0;
        for(unsigned i=0; i<M; ++i)
        for(unsigned j=0; j<N; ++j) {
          plam[j] += lam[i]*P[i][j];
          qlam[j] += lam[i]*Q[i][j];
          gvec[i] = P[i][j]*uxinv1[j] + Q[i][j]*xlinv1[j];
        }
        dpsidx = plam/ux2 - qlam/xl2;

        rex   = dpsidx - xsi + eta;
        rey   = ci + di*y - mu - lam;
        rez   = a0 - zet - Dot(ai,lam);
        relam = gvec - z*ai - y + s - b;
        rexsi = xsi*(x-alph) - epsvecn;
        reeta = eta*(beta-x) - epsvecn;
        remu  = mu*y - epsvecm;
        rezet = z*zet - epsi;
        res   = lam*s - epsvecm;

        residue.clear();
        residue.reserve(rex.size() + rey.size() + 1 +
                        relam.size() + rexsi.size() + reeta.size() + remu.size() + 1 + res.size());
      
        #define APPEND(v) residue.insert(residue.end(), v.begin(), v.end())
        residue.push_back(rez);
        residue.push_back(rezet);
        APPEND(rex);   APPEND(rey);
        APPEND(relam); APPEND(rexsi);
        APPEND(reeta); APPEND(remu);
        APPEND(res);
        #undef APPEND
        resinew2 = Dot(residue,residue);
        steg /= 2;
      }

      residuenorm2 = resinew2;
      steg = 2*steg;
    }
    epsi = 0.1*epsi;
  }
  return x;
}



HVec MMASub(unsigned it, double asyinit, double asyincr, double asydecr,
            const HVec  &x, const HVec   &x_min, const HVec   &x_max, const HVec &x_m1, const HVec &x_m2,
            double      f0, const HVec     &df0, const HVec    &d2f0,
            const HVec &fi, const HVecVec &dfdx, const HVecVec &dfdx2,
            double      a0, const HVec      &ai, const HVec      &ci, const HVec &di,
            HVec &low, HVec& upp) {
  
  // Check the size of the vectors
  unsigned N = x.size();
  if(x_min.size()!=N || x_max.size()!=N || x_m1.size()!=N || x_m2.size()!=N ||
       df0.size()!=N ||  d2f0.size()!=N) {
    throw(std::logic_error("MMASub: an 'x-type' vector is of incorrect size"));
  }

  unsigned M = fi.size();
  if(fi.size()!=M || dfdx.size()!=M || dfdx2.size()!=M ||
     ai.size()!=M ||   ci.size()!=M ||   di.size()!=M) {
    throw(std::logic_error("MMASub: an 'f-type' vector is of incorrect size"));
  }

  double epsimin = std::sqrt(M+N)*1e-9;
  double feps    = 1e-6;
  double albefa  = 0.10;
  HVec een  (N, 1.0);
  HVec zeron(N, 0.0);

  // Calculate the asymptotes low and upp
  if(it<3) {
    low = x - asyinit*(x_max-x_min);
    upp = x + asyinit*(x_max-x_min);
  } else {
    for(unsigned i=0; i<N; ++i) {
      double z = (x[i]-x_m1[i])*(x_m1[i] - x_m2[i]);
      double f = z>0 ? asyincr : asydecr;
      low[i] = x[i] - f * (x_m1[i] - low[i]);
      upp[i] = x[i] + f * (upp[i] - x_m1[i]);
    }
  }

  // Calculation of the bounds alpha and beta
  HVec alph = Max(low+albefa*(x-low), x_min);
  HVec beta = Min(upp-albefa*(upp-x), x_max);

  // Calculate p0, q0, P, Q and b
  HVec ux1 = upp-x;
  HVec ux2 = ux1*ux1;
  HVec ux3 = ux2*ux1;
  HVec xl1 = x - low;
  HVec xl2 = xl1*xl1;
  HVec xl3 = xl2*xl1;
  HVec ul1 = upp-low;
  
  HVec ulinv1 = 1.0/xl1;
  HVec uxinv1 = 1.0/ux1;
  HVec xlinv1 = 1.0/xl1;
  HVec uxinv3 = 1.0/ux3;
  HVec xlinv3 = 1.0/xl3;
  
  HVec diap = (ux3*xl1)/(2*ul1);
  HVec diaq = (ux1*xl3)/(2*ul1);
  HVec p0 = (      Max(0.0, df0) + 1e-3*Abs(df0) + feps*ulinv1 )*ux2;
  HVec q0 = ( -1.0*Min(0.0, df0) + 1e-3*Abs(df0) + feps*ulinv1 )*xl2;
  HVec dg0dx2 = 2*(p0/ux3 + q0/xl3);
  HVec delpos0 = Max(0.0, d2f0-dg0dx2);
  p0 = p0 + delpos0*diap;
  q0 = q0 + delpos0*diaq;
  
  HVecVec P(M);
  HVecVec Q(M);
  HVec b(M);
  for(unsigned i=0; i<M; ++i) {
    P[i] =      Max(0.0, dfdx[i])*ux2;
    Q[i] = -1.0*Min(0.0, dfdx[i])*xl2;
    HVec del = dfdx2[i] - 2*(P[i]*uxinv3 + Q[i]*xlinv3);
    P[i] = P[i] + Max(0.0, del)*diap;
    Q[i] = Q[i] + Max(0.0, del)*diaq;
    b[i] = Dot(P[i],uxinv1) + Dot(Q[i],xlinv1) - fi[i];
  }

  return Subsolve(epsimin, low, upp, alph, beta, p0, q0, P, Q, a0, ai, b, ci, di);
}


class MMAOptimiser : public OptFuncBase {
  public:
    MMAOptimiser(const JSONObject &spec) : it_(1), a_(1,0.0), c_(1,0.0), d_(1,0.0), N(0) {
      const JSONValue *opt_spec = spec.ObjectMember("optimiser");
      rho_limits_= opt_spec->ObjectMember("rho limits")->ArrayAsVectorOfDoubles(2);
      asy_init_ = opt_spec->ObjectMember("asymptote initial" )->AsDouble();
      asy_decr_ = opt_spec->ObjectMember("asymptote decrease")->AsDouble();
      asy_incr_ = opt_spec->ObjectMember("asymptote increase")->AsDouble();
      a0_ = 10;
      a_[0] = 0.0;
      c_[0] = opt_spec->ObjectMember("linear slack coefficient")->AsDouble();
      d_[0] = opt_spec->ObjectMember("quadratic slack coefficient")->AsDouble();
    }
     
    PetscErrorCode Step(const int iteration,
                        const JSONObject &spec,
                        const Array4d<char> &fixed_design,
                        const Array4d<double> &old_design,
                              Array4d<double> &new_design,
                        double &objective,
                        std::vector<double> &constraints) {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;

      ierr =  CalculateObjective(iteration, true, old_design, d_design_, constraints, d_constraints_, objective);
                                                                                       CHKERRQ(ierr);

      int D = old_design.GetI();
      int I = old_design.GetJ();
      int J = old_design.GetK();
      int K = old_design.GetL();
      new_design.Resize(D,I,J,K);
      
      // Count the number of free elements
      if(N==0) {
        if(D!=1) {
          throw(std::runtime_error("MMA assumes one design variable: rho."));
        }
          
        int C = 0;
        for(int d=0; d<D; ++d)
        for(int i=0; i<I; ++i)
        for(int j=0; j<J; ++j)
        for(int k=0; k<K; ++k) {
          C += (fixed_design(d,i,j,k) ? 0 : 1);
        }
        N = C;
        linearised_rho.resize(C);
        linearised_d_rho.resize(C);
        dcdr2.resize(C, 0.0);          // Second objective derivitive, don't have so set to zero (as recommended)
        dVdr.resize (1, HVec(C, 1.0)); // Assume we only have one design variable
        dVdr2.resize(1, HVec(C, 0.0)); // Set second derivitive to zero as calculate objective doens't give them
        rho_min_v.resize(C, rho_limits_[0]);
        rho_max_v.resize(C, rho_limits_[1]);
        rho_m1.resize(C);
        rho_m2.resize(C);
      }

      // Transfer to linearised vectors
      int C = 0;
      for(int i=0; i<I; ++i)
      for(int j=0; j<J; ++j)
      for(int k=0; k<K; ++k) {
        if(!fixed_design(0,i,j,k)) {
          linearised_rho  [C] = old_design(0,i,j,k);
          linearised_d_rho[C] = d_design_ (0,i,j,k);
          dVdr[0][C]          = d_constraints_[0](0,i,j,k);
          ++C;
        }
      }
      if(C!=N) { throw(std::logic_error("didn't count N free points in linearising vectors for MMA")); }

      // Run the step
      HVec vol_f = constraints;
      
      OutputArray(linearised_rho,   "tst_x.f1d");
      OutputArray(linearised_d_rho, "tst_dx.f1d");
      OutputArray(dVdr[0],          "tst_dVdr.f1d");
      OutputArray(low,              "tst_low.f1d");
      OutputArray(upp,              "tst_upp.f1d");
      OutputArray(rho_m1,           "tst_x_m1.f1d");
      OutputArray(rho_m2,           "tst_x_m2.f1d");
      std::vector<double> new_rho_l = MMASub(it_, asy_init_, asy_incr_, asy_decr_,
                                             linearised_rho, rho_min_v, rho_max_v, rho_m1, rho_m2,
                                             objective, linearised_d_rho, dcdr2,
                                             vol_f, dVdr, dVdr2,
                                             a0_, a_, c_, d_, low, upp);
      rho_m2 = rho_m1;
      rho_m1 = linearised_rho;
      OutputArray(new_rho_l, "tst_x_new.f1d");
      // Transfer back from the linearised vector
      C = 0;
      for(int i=0; i<I; ++i)
      for(int j=0; j<J; ++j)
      for(int k=0; k<K; ++k) {
        if(!fixed_design(0,i,j,k)) {
          new_design(0,i,j,k) = new_rho_l[C];
          ++C;
        }
      }
      if(C!=N) { throw(std::logic_error("didn't count N free points in linearising vectors for MMA")); }
      ++it_;

      ierr =  EnforceDesignConstraints(spec,&new_design,nullptr,nullptr,nullptr);      CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

  private:
    int it_; // MMA iteration, different from the iteration passed to step.
    double asy_init_; 
    double asy_incr_;
    double asy_decr_;

    double a0_;
    HVec a_, c_, d_;

    std::vector<double> rho_limits_;
    std::vector<double> linearised_rho, linearised_d_rho, dcdr2;
    std::vector<double> rho_m1, rho_m2;
    std::vector<double> low, upp;
    std::vector<double> rho_min_v, rho_max_v;
    HVecVec dVdr, dVdr2;
     
    int N; // Number of free points
    Array4d<double> d_design_;
    std::vector<Array4d<double> > d_constraints_;
};




/// ///// ///
/// GCMMA ///
/// ///// ///


/*\
 *  
 *  The GCMMA code is Krister Svanbergs and it's written in Fotran (77 I think). There
 *  are a couple of issues interfacing with it as it's written. Firstly, it wants to 
 *  drive the whole thing. This is awkward as we'd have C calling Fortran calling C. 
 *  Instead, all the global variables of Krister's code has a counterpart in the 
 *  GCMMAOptimiser class and moving between the two languages is accomplished by calling
 *  the gcmma_fx_ functions. By maintaing the globals in the class and passing them
 *  through every time we maintain persistence of the variables that need it without 
 *  having to read the code, the nature of Fortran's pass by reference makes it nigh
 *  impossible to reason about what is used where.
 *
 *  It might be a bit ugly, forgive me -Andrew
 *
\*/

#define FORTRAN_FUNC_ARGS_DECL \
                          int &INNER, int &INNMAX, int &ITE, int &ITER, int &MAXITE, int &M, int &N, \
                          double &GEPS, double &F0VAL, double &F0NEW, double &F0APP, double &RAA0, double &Z, \
                          double *ALFA, double *BETA, double *DF0DX, double *P0,    double *Q0, \
                          double *XMAX, double *XMIN, double *XMMA,  double *XOLD1, double *XOLD2, \
                          double *XLOW, double *XUPP, double *XVAL, \
                          double *A, double *B, double *C, double *DRSCH,  double *FAPP, double *FMAX, double *FNEW, \
                          double *FVAL, double *GRADF, double *IYFREE, double *RAA, double *ULAM,  double *UU, \
                          double *Y, \
                          double *DFDX, double *P, double *Q, double *HESSF

#define FORTRAN_FUNC_ARGS_CALL \
                          INNER, INNMAX, ITE, ITER, MAXITE, M, N, \
                          GEPS, F0VAL, F0NEW, F0APP, RAA0, Z, \
                          ALFA.data(), BETA.data(), DF0DX.data(), P0.data(), Q0.data(), \
                          XMAX.data(), XMIN.data(), XMMA.data(),  XOLD1.data(), XOLD2.data(), \
                          XLOW.data(), XUPP.data(), XVAL.data(), \
                          A.data(), B.data(), C.data(), DRSCH.data(),  FAPP.data(), FMAX.data(), FNEW.data(), \
                          FVAL.data(), GRADF.data(), IYFREE.data(), RAA.data(), ULAM.data(),  UU.data(), \
                          Y.data(), \
                          DFDX.data(), P.data(), Q.data(), HESSF.data()


extern "C" void gcmma_f1_(FORTRAN_FUNC_ARGS_DECL);
extern "C" void gcmma_f2_(FORTRAN_FUNC_ARGS_DECL);
extern "C" void gcmma_f3_(FORTRAN_FUNC_ARGS_DECL, int &redo);


static const int32_t GCMMA_serial_format_version = 1;


class GCMMAOptimiser : public OptFuncBase {
  public:
    GCMMAOptimiser() : M(0) { }

    PetscErrorCode Step(const int it,
                        const JSONObject &spec,
                        const Array4d<char> &fixed_design,
                        const Array4d<double> &old_design,
                              Array4d<double> &new_design,
                        double &objective,
                        std::vector<double> &constraints) {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;

      if(M==0) {
        auto objective_spec = spec.ObjectMember("optimiser");
        if(it==0) {
          recalc_obj_ = objective_spec->ObjectHasMember("recalculate objective")
                      ? objective_spec->ObjectMember("recalculate objective")->AsBool()
                      : true;

          // Get an initial result for ProbFunc2
          ierr =  CalculateObjective(it, true,
                                     old_design, d_design,
                                     constraints_, d_constraints,
                                     last_fval);                                       CHKERRQ(ierr);

          M = constraints_.size();
          ierr =  Initialise(spec, fixed_design, old_design);                          CHKERRQ(ierr);
        } else {
          std::cout << "Attempting to restart GCMMA from iteration " << it << ", " << std::flush;
          ierr =  Deserialise(it);                                                     CHKERRQ(ierr);
        }

        first_iteration = it;
        save_state = true;
        if(objective_spec->ObjectHasMember("save state")) {
          save_state = objective_spec->ObjectMember("save state")->AsBool();
        }
        save_whole_history = false;
        if(objective_spec->ObjectHasMember("save whole history")) {
          save_whole_history = objective_spec->ObjectMember("save whole history")->AsBool();
        }
      }
      if(save_state) {
        ierr =  Serialise(it);                                                         CHKERRQ(ierr);
      }

      ierr =  ProbFunc2(it, spec, fixed_design);                                       CHKERRQ(ierr);
      // Skim off the values of the objective and constraints
      objective = last_fval;
      constraints = constraints_;

      gcmma_f1_(FORTRAN_FUNC_ARGS_CALL);
      
      int redo;
      int inner_its = 0;
      do {
        inner_its++;
        gcmma_f2_(FORTRAN_FUNC_ARGS_CALL);
        ierr =  ProbFunc1(it, spec, fixed_design);                                     CHKERRQ(ierr);
        gcmma_f3_(FORTRAN_FUNC_ARGS_CALL, redo);
      } while(redo==1);

      std::cout << inner_its << " GCMMA loops" << std::flush;

      if(INNER==INNMAX) {
        std::cout << "\nInner loop of GCMMA did not converge\n"
                  << "                 F0NEW: " << F0NEW << '\n'
                  << "                 F0VAL: " << F0VAL << '\n'
                  << "                 F0APP: " << F0APP <<  '\n'
                  << "                   |Y|: " << Dot(Y,Y) <<  '\n'
                  << "                     z: " << Z << std::endl;
      } else if(verbose) {
        std::cout << "\nTerminating with F0NEW: " << F0NEW << '\n'
                  << "                 F0VAL: " << F0VAL << '\n'
                  << "                 F0APP: " << F0APP <<  '\n'
                  << "                   |Y|: " << Dot(Y,Y) <<  '\n'
                  << "                     z: " << Z << std::endl;
      }

      new_design.Resize(old_design.GetI(), old_design.GetJ(), old_design.GetK(), old_design.GetL());
      ierr =  DeLinearise(spec, fixed_design, XVAL, new_design);                       CHKERRQ(ierr);

      PetscFunctionReturn(0);
    }

    
  private:
    // Essentially allocation and INITI(M,N,....)
    PetscErrorCode Initialise(const JSONObject &spec,
                              const Array4d<char> &fixed_design,
                              const Array4d<double> &design) {
      PetscFunctionBeginUser;

      D = design.GetI();
      I = design.GetJ();
      J = design.GetK();
      K = design.GetL();
      temp_design.Resize(D,I,J,K);

      const JSONValue *opt_spec = spec.ObjectMember("optimiser");

      // Count free indices and transfer design into temp_design_
      int c = 0;
      for(int k=0; k<K; ++k)
      for(int j=0; j<J; ++j)
      for(int i=0; i<I; ++i)
      for(int d=0; d<D; ++d) {
        if(!fixed_design(d,i,j,k)) {
          c++;
        }
        temp_design(d,i,j,k) = design(d,i,j,k);
      }
      // Maximum number of inner iterations searching for a conservative step
      INNMAX = 15;
      if(opt_spec->ObjectHasMember("inner iteration max")) {
        INNMAX = opt_spec->ObjectMember("inner iteration max")->AsInt();
      }
      // Tolerance parameter for the constraints
      GEPS = 1e-6;
      if(opt_spec->ObjectHasMember("inner g tolerance")) {
        GEPS = opt_spec->ObjectMember("inner g tolerance")->AsDouble();
      }
      
      MAXITE =  1; // For interactive use
      ITER   =  0; // Iteration number
      ITE    =  0; // For interactive use
      INNER  =  0; // Number of inner iterations

      // Initialise - here we should do all the work in the fortran function INITI( )
      N = c;
      ALFA.resize(N);
      BETA.resize(N);
      DF0DX.resize(N);
      P0.resize(N);
      Q0.resize(N);

      std::vector<std::vector<double>> rho_limits(D);
      for(int d=0; d<D; ++d) {
        rho_limits[d] = opt_spec->ObjectMember("rho limits")->ArrayMember(d)->ArrayAsVectorOfDoubles(2);
      }
      Array4d<double> min_array(D,I,J,K);
      Array4d<double> max_array(D,I,J,K);
      for(int k=0; k<K; ++k)
      for(int j=0; j<J; ++j)
      for(int i=0; i<I; ++i)
      for(int d=0; d<D; ++d) {
        min_array(d,i,j,k) = rho_limits[d][0];
        max_array(d,i,j,k) = rho_limits[d][1];
      }
      XMIN.resize(N);
      Linearise(fixed_design,min_array,XMIN);
      XMAX.resize(N);
      Linearise(fixed_design,max_array,XMAX);

      XMMA.resize(N);
      XOLD1.resize(N);
      XOLD2.resize(N);
      XLOW.resize(N);
      XUPP.resize(N);
      XVAL.resize(N);

      A.resize(M, 0.0); // Set a_1=0 as it's just a duplicate of y when M=1
      B.resize(M);
      C = opt_spec->ObjectMember("c")->ArrayAsVectorOfDoubles(M);
      DRSCH.resize(M);
      FAPP.resize(M);
      FMAX.resize(M, 0.0);
      FNEW.resize(M);
      FVAL.resize(M);
      GRADF.resize(M);
      IYFREE.resize(M);
      RAA.resize(M);
      ULAM.resize(M);
      UU.resize(M);
      Y.resize(M);
      DFDX.resize(M*N);
      P.resize(M*N);
      Q.resize(M*N);
      HESSF.resize((M*(M+1))/2);
      
      Linearise(fixed_design, design, XVAL);

      PetscFunctionReturn(0);
    }


    // Only gets called with the same params each time
    //   CALL FUNC1(M,N,XMMA,F0NEW,FNEW)
    PetscErrorCode ProbFunc1(int it, const JSONObject &spec, const Array4d<char> &fixed_design) {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      
      double scale = 1.0;
      if(spec.ObjectMember("optimiser")->ObjectHasMember("objective scale")) {
        scale = spec.ObjectMember("optimiser")->ObjectMember("objective scale")->AsDouble();
      }

      ierr =  DeLinearise(spec, fixed_design, XMMA, temp_design);                      CHKERRQ(ierr);
      
      bool calculate_derivatives = !recalc_obj_; // Only calculate derivatives if we're not going to
                                                 // redo the function calculation later
      ierr =  CalculateObjective(it, calculate_derivatives,
                                 temp_design, d_design,
                                 constraints_, d_constraints,
                                 last_fval);                                           CHKERRQ(ierr);

      F0NEW = scale * last_fval;
      FNEW = constraints_;

      PetscFunctionReturn(0);
    }


    // Only gets called with the same params each time
    //   CALL FUNC2(M,N,XVAL,F0VAL,DF0DX,FVAL,DFDX)
    PetscErrorCode ProbFunc2(int it, const JSONObject &spec, const Array4d<char> &fixed_design) {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      
      if(recalc_obj_) {
        ierr =  DeLinearise(spec, fixed_design, XVAL, temp_design);                    CHKERRQ(ierr);
        ierr =  CalculateObjective(it, true,
                                   temp_design, d_design,
                                   constraints_, d_constraints,
                                   last_fval);                                         CHKERRQ(ierr);
      }

      double scale = 1.0;
      if(spec.ObjectMember("optimiser")->ObjectHasMember("objective scale")) {
        scale = spec.ObjectMember("optimiser")->ObjectMember("objective scale")->AsDouble();
      }

      // Assume that design hasn't changed since the last evaluation. Just give those results back.
      F0VAL = scale * last_fval;
      Linearise(fixed_design, d_design, DF0DX, scale);
      
      FVAL = constraints_;
      for(size_t i=0; i<constraints_.size(); i++) {
        Linearise(fixed_design, d_constraints[i], &DFDX[i*N]);
      }

      PetscFunctionReturn(0);
    }
    

    void Linearise(const Array4d<char>   &fixed_design,
                   const Array4d<double> &from,
                   std::vector<double> &to,
                   double scale=1.0) {
       Linearise(fixed_design, from, &to[0], scale);
    }

    void Linearise(const Array4d<char>   &fixed_design,
                   const Array4d<double> &from,
                   double *to,
                   double scale=1.0) {
      int c = 0;
      for(int k=0; k<K; ++k)
      for(int j=0; j<J; ++j)
      for(int i=0; i<I; ++i)
      for(int d=0; d<D; ++d) {
        if(!fixed_design(d,i,j,k)) {
          to[c] = scale * from (d,i,j,k);
          ++c;
        }
      }
      if(c!=N) throw(std::logic_error("GCMMAOptimiser::Linearise didn't count N free points"));
    }

    PetscErrorCode DeLinearise(const JSONObject          &spec,
                               const Array4d<char>       &fixed_design,
                               const std::vector<double> &from,
                               Array4d<double>           &to) {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      int c = 0;
      for(int k=0; k<K; ++k)
      for(int j=0; j<J; ++j)
      for(int i=0; i<I; ++i)
      for(int d=0; d<D; ++d) {
        if(!fixed_design(d,i,j,k)) {
          to(d,i,j,k) = from[c];
          ++c;
        }
      }
      if(c!=N) throw(std::logic_error("GCMMAOptimiser::DeLinearise didn't count N free points"));
      ierr =  EnforceDesignConstraints(spec,&to,nullptr,nullptr,nullptr);              CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

    std::string GetSerialFileName(int it) {
      std::stringstream ss;
      ss << "GCMMA_STATE_" << std::setw(6) << std::setfill('0') << it << ".gz";
      return ss.str();
    }

    PetscErrorCode FWrite(gzFile file, void *buff, int len) {
      PetscFunctionBeginUser;
      int ret = gzwrite(file, buff, len);
      if(ret!=len) {
        int err;
        SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_FILE_WRITE,"%s",gzerror(file,&err));
      }
      PetscFunctionReturn(0);
    }

    PetscErrorCode SerialiseVector(gzFile file, std::vector<double> &v, int expected_length) {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      if(v.size() != size_t(expected_length)) {
        SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_ARG_SIZ,
                 "Unexpected size of vector serialising GCMMA, is %d, expected %d",
                 int(v.size()),expected_length);
      }
      ierr =  FWrite(file,v.data(),v.size()*sizeof(double));                           CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

    PetscErrorCode FRead(gzFile file, void *buff, int len) {
      PetscFunctionBeginUser;
      int ret = gzread(file, buff, len);
      if(ret!=len) {
        int err;
        SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_FILE_READ,"%s",gzerror(file,&err));
      }
      PetscFunctionReturn(0);
    }
      
    PetscErrorCode DeSerialiseVector(gzFile file, std::vector<double> &v, int length) {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      if(v.size() != 0 && v.size() != size_t(length)) {
        std::cerr << " Deserialising vector which has incorrect non-zero length. ";
      }
      v.resize(length);
      ierr =  FRead(file,v.data(),length*sizeof(double));                              CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

    PetscErrorCode Serialise(int it) {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      // Open file
      std::string filename = GetSerialFileName(it);
      static_assert(Z_NULL==0,"Z_NULL!=0, will cause errors with std::unique_ptr");
      std::unique_ptr<void,decltype(&gzclose)> zfile(gzopen(filename.c_str(), "wb"),&gzclose);
      if(!zfile) {
        SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_FILE_OPEN,"GCMMA failed to open %s", filename.c_str());
      }
      // Write version
      int32_t version = GCMMA_serial_format_version;
      ierr =  FWrite(zfile.get(),&version,sizeof(int32_t));                            CHKERRQ(ierr);
      
      // Write ints
      int32_t integers[12];
      integers[0] = INNER;
      integers[1] = INNMAX;
      integers[2] = ITE;
      integers[3] = ITER;
      integers[4] = MAXITE;
      integers[5] = M;
      integers[6] = N;
      integers[7] = D;
      integers[8] = I;
      integers[9] = J;
      integers[10] = K;
      integers[11] = recalc_obj_;
      ierr =  FWrite(zfile.get(),integers,sizeof(integers));                           CHKERRQ(ierr);

      // Write doubles 
      double floats[8];
      floats[0] = GEPS;
      floats[1] = F0VAL;
      floats[2] = F0NEW;
      floats[3] = F0APP;
      floats[4] = RAA0;
      floats[5] = Z;
      floats[6] = vol_frac;
      floats[7] = last_fval;
      ierr =  FWrite(zfile.get(),floats,sizeof(floats));                               CHKERRQ(ierr);
 
      ierr =  SerialiseVector(zfile.get(),ALFA,N);                                     CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),BETA,N);                                     CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),DF0DX,N);                                    CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),P0,N);                                       CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),Q0,N);                                       CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),XMAX,N);                                     CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),XMIN,N);                                     CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),XMMA,N);                                     CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),XOLD1,N);                                    CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),XOLD2,N);                                    CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),XLOW,N);                                     CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),XUPP,N);                                     CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),XVAL,N);                                     CHKERRQ(ierr);

      ierr =  SerialiseVector(zfile.get(),A,M);                                        CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),B,M);                                        CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),C,M);                                        CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),DRSCH,M);                                    CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),FAPP,M);                                     CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),FMAX,M);                                     CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),FNEW,M);                                     CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),FVAL,M);                                     CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),GRADF,M);                                    CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),IYFREE,M);                                   CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),RAA,M);                                      CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),ULAM,M);                                     CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),UU,M);                                       CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),Y,M);                                        CHKERRQ(ierr);
    
      ierr =  SerialiseVector(zfile.get(),DFDX,M*N);                                   CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),P,M*N);                                      CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),Q,M*N);                                      CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),HESSF,(M*(M+1))/2);                          CHKERRQ(ierr);
    
      ierr =  SerialiseVector(zfile.get(),d_design.GetData(),D*I*J*K);                 CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),temp_design.GetData(),D*I*J*K);              CHKERRQ(ierr);
      ierr =  SerialiseVector(zfile.get(),constraints_,M);                             CHKERRQ(ierr);
      for(int i=0; i<M; ++i) {
        ierr =  SerialiseVector(zfile.get(),d_constraints[i].GetData(),D*I*J*K);       CHKERRQ(ierr);
      }
      
      if(!save_whole_history && (it-1 > first_iteration || it-1 == 0)) {
        filename = GetSerialFileName(it-1);
        if(std::remove(filename.c_str())) {
          std::cerr << " Failed to remove old GCMMA data file " << filename << " " << std::flush;
        }
      }
      PetscFunctionReturn(0);
    }

    PetscErrorCode Deserialise(int it) {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      // Open file
      std::string filename = GetSerialFileName(it);
      static_assert(Z_NULL==0,"Z_NULL!=0, will cause errors with std::unique_ptr");
      std::unique_ptr<void,decltype(&gzclose)> zfile(gzopen(GetSerialFileName(it).c_str(), "rb"),&gzclose);
      if(!zfile) {
        SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_FILE_OPEN,"GCMMA attempted to restart with %s, it failed to open", filename.c_str());
      }

      // Check version
      int32_t version;
      ierr =  FRead(zfile.get(),&version,sizeof(version));                             CHKERRQ(ierr);
      if(version != GCMMA_serial_format_version) {
        SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_FILE_OPEN,"GCMMA attemped to restart with %s, it has the wrong version", filename.c_str());
      }
      // Load ints
      int32_t integers[12];
      ierr =  FRead(zfile.get(),integers,sizeof(integers));                            CHKERRQ(ierr);
      INNER = integers[0];
      INNMAX = integers[1];
      ITE = integers[2];
      ITER = integers[3];
      MAXITE = integers[4];
      M = integers[5];
      N = integers[6];
      D = integers[7];
      I = integers[8];
      J = integers[9];
      K = integers[10];
      recalc_obj_ = integers[11];

      // Load doubles
      double floats[8];
      ierr =  FRead(zfile.get(),floats,sizeof(floats));                                CHKERRQ(ierr);
      GEPS = floats[0];
      F0VAL = floats[1];
      F0NEW = floats[2];
      F0APP = floats[3];
      RAA0 = floats[4];
      Z = floats[5];
      vol_frac = floats[6];
      last_fval = floats[7];
  
      ierr =  DeSerialiseVector(zfile.get(),ALFA,N);                                   CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),BETA,N);                                   CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),DF0DX,N);                                  CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),P0,N);                                     CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),Q0,N);                                     CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),XMAX,N);                                   CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),XMIN,N);                                   CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),XMMA,N);                                   CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),XOLD1,N);                                  CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),XOLD2,N);                                  CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),XLOW,N);                                   CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),XUPP,N);                                   CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),XVAL,N);                                   CHKERRQ(ierr);

      ierr =  DeSerialiseVector(zfile.get(),A,M);                                      CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),B,M);                                      CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),C,M);                                      CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),DRSCH,M);                                  CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),FAPP,M);                                   CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),FMAX,M);                                   CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),FNEW,M);                                   CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),FVAL,M);                                   CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),GRADF,M);                                  CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),IYFREE,M);                                 CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),RAA,M);                                    CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),ULAM,M);                                   CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),UU,M);                                     CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),Y,M);                                      CHKERRQ(ierr);
    
      ierr =  DeSerialiseVector(zfile.get(),DFDX,M*N);                                 CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),P,M*N);                                    CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),Q,M*N);                                    CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),HESSF,(M*(M+1))/2);                        CHKERRQ(ierr);

      d_design.Resize(D,I,J,K);
      ierr =  DeSerialiseVector(zfile.get(),d_design.GetData(),D*I*J*K);               CHKERRQ(ierr);
      temp_design.Resize(D,I,J,K);
      ierr =  DeSerialiseVector(zfile.get(),temp_design.GetData(),D*I*J*K);            CHKERRQ(ierr);
      ierr =  DeSerialiseVector(zfile.get(),constraints_,M);                           CHKERRQ(ierr);
      d_constraints.resize(M);
      for(int i=0; i<M; ++i) {
        d_constraints[i].Resize(D,I,J,K);
        ierr =  DeSerialiseVector(zfile.get(),d_constraints[i].GetData(),D*I*J*K);     CHKERRQ(ierr);
      }

      PetscFunctionReturn(0);
    }

     // Book keeping for the fortran functions, doing it here means we know the right ones
     // persist throughout iterations. The ones that it doens't matter it dosn't hurt to
     // have here.
     int INNER, INNMAX, ITE, ITER, MAXITE, M, N;
     double  GEPS, F0VAL, F0NEW, F0APP, RAA0, Z;
     std::vector<double> ALFA, BETA, DF0DX, P0, Q0,
                         XMAX, XMIN, XMMA,  XOLD1, XOLD2,
                         XLOW, XUPP, XVAL,
                         A, B, C,  DRSCH,  FAPP, FMAX, FNEW,
                         FVAL, GRADF, IYFREE, RAA,  ULAM, UU,
                         Y,
                         DFDX, P, Q, HESSF;

    double vol_frac, last_fval;

    Array4d<double> d_design;
    Array4d<double> temp_design;
    std::vector<double> constraints_;
    std::vector<Array4d<double> > d_constraints;

    int D, I, J, K; // ELEMENT WISE
    bool recalc_obj_;

    bool save_state, save_whole_history;
    int first_iteration;
};



/// //////// ///
/// Downhill ///
/// //////// ///


class DownhillOptimiser : public OptFuncBase {
  static constexpr int history_length_ = 7; // Possible switches is this-2
  static constexpr int history_delta_sign_switches_limit_ = 4;
  public:
    DownhillOptimiser(const JSONObject &spec) : N_(0) {
      auto opt_spec = spec.ObjectMember("optimiser");
      max_move_ = opt_spec->ObjectMember("max move")->AsDouble();
      cur_move_ = max_move_;
      size_t n = opt_spec->ObjectMember("design limits")->ArraySize();
      if(n!=size_t(spec.ObjectMember("design dof")->AsInt())) {
        throw std::runtime_error("optimiser->limits should have length of design dof in config");
      }
      
      limits_.resize(n);
      for(size_t i=0; i<n; ++i) {
        limits_[i] = opt_spec->ObjectMember("design limits")
                             ->ArrayMember(i)
                             ->ArrayAsVectorOfDoubles(2);
      }

      for(auto &val : history_) val = 0;
    }

    PetscErrorCode Step(const int it,
                        const JSONObject &spec,
                        const Array4d<char> &fixed_design,
                        const Array4d<double> &old_design,
                              Array4d<double> &new_design,
                        double &objective,
                        std::vector<double> &constraints) {
      PetscFunctionBeginUser;
      PetscErrorCode ierr;
      // On the first pass, find out the number of free points
      if(N_==0) {
        for(int k=0; k<DOM.K_el; ++k)
        for(int j=0; j<DOM.J_el; ++j)
        for(int i=0; i<DOM.I_el; ++i)
        for(int d=0; d<DOM.design_dof; ++d) {
          if(!fixed_design(d,i,j,k)) {
            N_++;
          }
        }
      }
      
      ierr =  CalculateObjective(it, true,
                                 old_design, d_design_,
                                 constraints, d_constraints_,
                                 objective);                                           CHKERRQ(ierr);
      
      for(int i=history_length_-1; i>0; --i) history_[i] = history_[i-1];
      history_[0] = objective;
      
      int switches = 0;
      for(int i = 0; i<history_length_-2; ++i)
        switches += (history_[i+1]-history_[i])*(history_[i+2]-history_[i+1]) < 0;

      if(switches >= history_delta_sign_switches_limit_) {
        cur_move_ /= std::sqrt(2.0);
        std::cout << "Move shortened to " << cur_move_;
      }

      // If derivatives will take us outside of what's allowed, set them to zero
      for(int k=0; k<DOM.K_el; ++k)
      for(int j=0; j<DOM.J_el; ++j)
      for(int i=0; i<DOM.I_el; ++i)
      for(int d=0; d<DOM.design_dof; ++d) {
        if(old_design(d,i,j,k) <= limits_[d][0] + 1e-5) {
          // Check if positive as we are going downhill
          if(d_design_(d,i,j,k) > 0) d_design_(d,i,j,k) = 0;
          for(auto &d_constraint : d_constraints_) {
            if(d_constraint(d,i,j,k) > 0) d_constraint(d,i,j,k) = 0;
          }
        }
        if(old_design(d,i,j,k) >= limits_[d][1] - 1e-5) {
          if(d_design_(d,i,j,k) < 0) d_design_(d,i,j,k) = 0;
          for(auto &d_constraint : d_constraints_) {
            if(d_constraint(d,i,j,k) < 0) d_constraint(d,i,j,k) = 0;
          }
        }     
      }

      Linearise(fixed_design,old_design,design_lin_);
      Linearise(fixed_design,d_design_,design_move_);
      d_constraints_lin_.resize(d_constraints_.size());
      for(size_t i=0; i<d_constraints_lin_.size(); ++i) {
        if(constraints[i] < 0) continue;
        Linearise(fixed_design,d_constraints_[i],d_constraints_lin_[i]);
        // Exclude the constraint directions from the objective direction
        double a = 0;
        double b = 0;
        for(int j=0; j<N_; ++j) {
          a += design_move_[j]*d_constraints_lin_[i][j];
          b += d_constraints_lin_[i][j]*d_constraints_lin_[i][j];
        }
        if(b==0) {
          if(constraints[i]==0) continue;
          std::cerr << "Constraint " << i << " is positive but has no gradient" << std::endl;
          throw std::runtime_error("constraint is positive but has no gradient");
        }

        for(int j=0; j<N_; ++j) {
          design_move_[j] -= a/b*d_constraints_lin_[i][j];
        }
      }
      
      // Set the length to have the highest element be cur_move_;
      double largest = 0;
      for(int j=0; j<N_; ++j) {
        if(std::abs(design_move_[j]) > largest) largest = std::abs(design_move_[j]);
      }
      
      if(largest != 0) {
        for(int j=0; j<N_; ++j) {
          design_lin_[j] -= design_move_[j]*cur_move_/largest;
        }
      }
      
      for(size_t i=0; i<constraints.size(); ++i) {
        if(constraints[i]>0) {
          // If we have a positive constraint move at most cur_move_ in any one
          // dimension towards zero, but also no further than newtwon would
          // require us.
          largest = 0;
          double norm_sq = 0;
          for(int j=0; j<N_; ++j) {
            norm_sq += d_constraints_lin_[i][j]*d_constraints_lin_[i][j];
            if(std::abs(d_constraints_lin_[i][j]) > largest) {
              largest = std::abs(d_constraints_lin_[i][j]);
            }
          }
          if(largest == 0) continue;
          double zeroer = constraints[i]/norm_sq; // Prefactor to zero constraint
          if(zeroer*largest < cur_move_) {
            for(int j=0; j<N_; ++j) 
              design_lin_[j] -= zeroer*d_constraints_lin_[i][j];
          } else {
            for(int j=0; j<N_; ++j) 
              design_lin_[j] -= cur_move_/largest*d_constraints_lin_[i][j];
          }
        }
      }

      ierr =  DeLinearise(spec,fixed_design,design_lin_,new_design);                   CHKERRQ(ierr);

      for(int k=0; k<DOM.K_el; ++k)
      for(int j=0; j<DOM.J_el; ++j)
      for(int i=0; i<DOM.I_el; ++i)
      for(int d=0; d<DOM.design_dof; ++d) {
        if(new_design(d,i,j,k) < limits_[d][0]) new_design(d,i,j,k) = limits_[d][0];
        if(new_design(d,i,j,k) > limits_[d][1]) new_design(d,i,j,k) = limits_[d][1];
      }
      
      ierr =  EnforceDesignConstraints(spec,&new_design,nullptr,nullptr,nullptr);      CHKERRQ(ierr);
      
      PetscFunctionReturn(0);
    }

  private:
    void Linearise(const Array4d<char>   &fixed_design,
                   const Array4d<double> &from,
                   std::vector<double> &to,
                   double scale=1.0) {
      to.resize(N_);
      int c = 0;
      for(int k=0; k<DOM.K_el; ++k)
      for(int j=0; j<DOM.J_el; ++j)
      for(int i=0; i<DOM.I_el; ++i)
      for(int d=0; d<DOM.design_dof; ++d) {
        if(!fixed_design(d,i,j,k)) {
          to[c] = scale * from (d,i,j,k);
          ++c;
        }
      }
      if(c!=N_) throw(std::logic_error("DownhillOptimiser::Linearise didn't count N free points"));
    }

    PetscErrorCode DeLinearise(const JSONObject          &spec,
                               const Array4d<char>       &fixed_design,
                               const std::vector<double> &from,
                               Array4d<double>           &to) {
      PetscFunctionBeginUser;
      int c = 0;
      to.Resize(DOM.design_dof,DOM.I_el,DOM.J_el,DOM.K_el);
      for(int k=0; k<DOM.K_el; ++k)
      for(int j=0; j<DOM.J_el; ++j)
      for(int i=0; i<DOM.I_el; ++i)
      for(int d=0; d<DOM.design_dof; ++d) {
        if(!fixed_design(d,i,j,k)) {
          to(d,i,j,k) = from[c];
          ++c;
        }
      }
      if(c!=N_) throw(std::logic_error("DownhillOptimiser::DeLinearise didn't count N free points"));
      PetscFunctionReturn(0);
    }
    
    int N_;
    double max_move_;
    double cur_move_;

    Array4d<double> d_design_;
    
    std::vector<Array4d<double> > d_constraints_;

    std::vector<std::vector<double>> limits_;

    std::vector<double> design_lin_, d_design_lin_, design_move_;
    std::vector<std::vector<double>> d_constraints_lin_;
    
    std::array<double,history_length_> history_;
};



/// ///////// ///
/// Generator ///
/// ///////// ///

std::unique_ptr<OptFuncBase> GetOptFunction(const JSONObject &spec) {
  std::string type_name = spec.ObjectMember("optimiser")->ObjectMember("type")->AsString();

  if(type_name == "OC") {
    return std::unique_ptr<OptFuncBase>(new OCOptimiser(spec));
  } else if(type_name == "MMA") {
    return std::unique_ptr<OptFuncBase>(new MMAOptimiser(spec));
  } else if(type_name == "GCMMA") {
    return std::unique_ptr<OptFuncBase>(new GCMMAOptimiser);
  } else if(type_name == "downhill") {
    return std::unique_ptr<OptFuncBase>(new DownhillOptimiser(spec));
  } else {
    throw(std::runtime_error("GetOptFunction: Unknown optimiser " + type_name));
  }
}
